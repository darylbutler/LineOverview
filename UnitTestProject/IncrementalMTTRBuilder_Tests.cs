﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTTROverview;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
	[TestClass]
	public class IncrementalMTTRBuilder_Tests
	{

		[TestMethod]
		public void TestMTTR_SingleCase() {
			var events = new List<Event> {
				CreateEv("3/30/2019 21:33", "3/30/2019 21:48",  "RPIMTTR_FAULTD",   "Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning									"),
				CreateEv("3/30/2019 21:11", "3/30/2019 21:33",  "RPIMTTR_INAUTO",   "Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning									"),
				CreateEv("3/30/2019 21:11", "3/30/2019 21:33",  "RPIMTTR_INPROD",   "Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning									"),
				CreateEv("3/30/2019 21:33", "3/30/2019 21:34",  "RPIMTTR_OPRESP",   "Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning									"),
				CreateEv("3/30/2019 21:33", "3/30/2019 21:34",  "ST2CY01_8623Y5",   "Electric ST2CY01_8623Y5 CT Stacker Track 2 - Prestopper                                                 WT next sensor not reached						"),
				CreateEv("3/30/2019 21:34", "3/30/2019 21:34",  "RPIMTTR_INAUTO",   "Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning									"),
				CreateEv("3/30/2019 21:34", "3/30/2019 21:34",  "RPIMTTR_INPROD",   "Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning									"),
				CreateEv("3/30/2019 21:34", "3/30/2019 21:35",  "ST2CY01_8623Y5",   "Electric ST2CY01_8623Y5 CT Stacker Track 2 - Prestopper                                                 WT next sensor not reached						"),
				CreateEv("3/30/2019 21:35", "3/30/2019 21:35",  "TM1SACR_SATMX1",   "Electric Warning TM1SACR_SATMX1 PM1_One of Safety Doors TM1 Open                                                warning								"),
				CreateEv("3/30/2019 21:35", "3/30/2019 21:35",  "SU1SA01_TR0x75",   "Mechanic Warning SU1SA01_TR0x75 PM1_Safety Door (611S4) TM1 SD-175                                              open									"),
				CreateEv("3/30/2019 21:35", "3/30/2019 21:35",  "CB1FA01_150QA1",   "Electric CB1FA01_150QA1 Doors not closed (PLC: 0005)                                                    fault											"),
				CreateEv("3/30/2019 21:35", "3/30/2019 21:35",  "CB1FA01_S10422",   "Mediamissing CB1FA01_S10422 Compressed air missing. Please acknowledge with CLR (PLC: 0008)                 warning									"),
				CreateEv("3/30/2019 21:35", "3/30/2019 21:35",  "SU1SA01_TR0x76",   "Mechanic Warning SU1SA01_TR0x76 PM1_Safety Door (592S4) TM1 SD-176                                              open									"),
				CreateEv("3/30/2019 21:35", "3/30/2019 21:35",  "CB1FA01_VakBeh",   "Electric Warning CB1FA01_VakBeh Knock-out vessel is emptied (PLC: 0014)                                         warning								"),
				CreateEv("3/30/2019 21:35", "3/30/2019 21:36",  "RPIMTTR_INAUTO",   "Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning									"),
				CreateEv("3/30/2019 21:35", "3/30/2019 21:36",  "RPIMTTR_INPROD",   "Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning									"),
				CreateEv("3/30/2019 21:36", "3/30/2019 21:38",  "ST2CY01_8623Y5",   "Electric ST2CY01_8623Y5 CT Stacker Track 2 - Prestopper                                                 WT next sensor not reached						"),
				CreateEv("3/30/2019 21:37", "3/30/2019 21:38",  "SU1SA01_TR0x75",   "Mechanic Warning SU1SA01_TR0x75 PM1_Safety Door (611S4) TM1 SD-175                                              open									"),
				CreateEv("3/30/2019 21:37", "3/30/2019 21:38",  "TM1SACR_SATMX1",   "Electric Warning TM1SACR_SATMX1 PM1_One of Safety Doors TM1 Open                                                warning								"),
				CreateEv("3/30/2019 21:37", "3/30/2019 21:38",  "SU1SA01_TR0x76",   "Mechanic Warning SU1SA01_TR0x76 PM1_Safety Door (592S4) TM1 SD-176                                              open									"),
				CreateEv("3/30/2019 21:37", "3/30/2019 21:38",  "CB1FA01_150QA1",   "Electric CB1FA01_150QA1 Doors not closed (PLC: 0005)                                                    fault											"),
				CreateEv("3/30/2019 21:37", "3/30/2019 21:38",  "CB1FA01_S10422",   "Mediamissing CB1FA01_S10422 Compressed air missing. Please acknowledge with CLR (PLC: 0008)                 warning									"),
				CreateEv("3/30/2019 21:37", "3/30/2019 21:38",  "ST2FA02_6337S7",   "Electric ST2FA02_6337S7 CT Stacker Track 2 - Sensor lid place defective (PLC: 1401)                     fault											"),
				CreateEv("3/30/2019 21:38", "3/30/2019 21:38",  "CB1FA01_VakBeh",   "Electric Warning CB1FA01_VakBeh Knock-out vessel is emptied (PLC: 0014)                                         warning								"),
				CreateEv("3/30/2019 21:34", "3/30/2019 21:48",  "RPIMTTR_RECOVE",   "Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning									"),
				CreateEv("3/30/2019 22:01", "3/30/2019 22:01",  "PL4FA43_6045S7",   "Electric PL4FA43_6045S7 P Loader Tr 4 - Gripper 2 sticking lens warning (PLC: 1449)                     fault											"),
			};

			var expected = new List<Event>() {
				CreateEv("3/30/2019 21:33", "3/30/2019 21:48",  "ST2CY01_8623Y5",   "Electric ST2CY01_8623Y5 CT Stacker Track 2 - Prestopper                                                 WT next sensor not reached						"),
			};

			var mttr = new IncrementalMTTRFaultBuilder();
			mttr.AddEvents(events, false);
			var result = mttr.FinishBuildingFaults();

			Assert.IsTrue(EventListsEqual(result, expected), "General MTTR build failed");
		}

		[TestMethod]
		public void TestIncrementalMTTR() {
			var ev1 = new List<Event> {
				CreateEv("3/31/2019 1:56",   "3/31/2019 1:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:56",   "3/31/2019 1:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:56",   "3/31/2019 1:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:56",   "3/31/2019 1:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:56",   "3/31/2019 2:07",  "RPIMTTR_FAULTD",   "Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning                                   "),
				CreateEv("3/31/2019 1:56",   "3/31/2019 1:57",  "MA1CY01_0834Y5",   "Mechanic MA1CY01_0834Y5 Inner clamp bar of swivel unit MA                                               not in homeposition                           "),
				CreateEv("3/31/2019 1:56",   "3/31/2019 1:57",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("3/31/2019 1:56",   "3/31/2019 1:57",  "RPIMTTR_OPRESP",   "Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning                                   "),
				CreateEv("3/31/2019 1:57",   "3/31/2019 2:07",  "RPIMTTR_RECOVE",   "Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning                                   "),
				CreateEv("3/31/2019 1:57",   "3/31/2019 1:57",  "MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("3/31/2019 1:57",   "3/31/2019 1:57",  "PM1TIME_TOHIGH",   "Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("3/31/2019 1:57",   "3/31/2019 1:57",  "CM1AV34_CMHOME",   "Misc Warning CM1AV34_CMHOME Cleaning module CM - not in home position                                       warning                                   "),
				CreateEv("3/31/2019 1:57",   "3/31/2019 1:58",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("3/31/2019 1:57",   "3/31/2019 1:58",  "EM1TRAN_NOTREL",   "Misc Warning EM1TRAN_NOTREL EM not ready to cycle                                                           warning                                   "),
				CreateEv("3/31/2019 1:57",   "3/31/2019 1:58",  "ST2TRAN_NOTREL",   "Misc Warning ST2TRAN_NOTREL No transport release station 2                                                  warning                                   "),
				CreateEv("3/31/2019 1:57",   "3/31/2019 1:58",  "ST3TRAN_NOTREL",   "Misc Warning ST3TRAN_NOTREL No transport release station 3                                                  warning                                   "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:58",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:58",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:58",  "EM1TRAN_NOTREL",   "Misc Warning EM1TRAN_NOTREL EM not ready to cycle                                                           warning                                   "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:58",  "ST2TRAN_NOTREL",   "Misc Warning ST2TRAN_NOTREL No transport release station 2                                                  warning                                   "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:58",  "ST3TRAN_NOTREL",   "Misc Warning ST3TRAN_NOTREL No transport release station 3                                                  warning                                   "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:58",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:58",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:58",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:58",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:58",   "3/31/2019 1:59",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 1:59",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 1:59",  "EM1TRAN_NOTREL",   "Misc Warning EM1TRAN_NOTREL EM not ready to cycle                                                           warning                                   "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 1:59",  "ST2TRAN_NOTREL",   "Misc Warning ST2TRAN_NOTREL No transport release station 2                                                  warning                                   "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 1:59",  "ST3TRAN_NOTREL",   "Misc Warning ST3TRAN_NOTREL No transport release station 3                                                  warning                                   "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 2:07",  "MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 2:12",  "PM1TIME_TOHIGH",   "Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 1:59",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 1:59",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 1:59",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 1:59",   "3/31/2019 1:59",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("3/31/2019 2:00",   "3/31/2019 2:00",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("3/31/2019 2:00",   "3/31/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:00",   "3/31/2019 2:00",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("3/31/2019 2:00",   "3/31/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:00",   "3/31/2019 2:00",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("3/31/2019 2:00",   "3/31/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:00",   "3/31/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:00",   "3/31/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:00",   "3/31/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:01",   "3/31/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:01",   "3/31/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:01",   "3/31/2019 2:01",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("3/31/2019 2:01",   "3/31/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:01",   "3/31/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:01",   "3/31/2019 2:02",  "RM2Bay3_WaLim3",   "Electric Warning RM2Bay3_WaLim3 Warning limit exceeded in Bay 3                                                 warning                               "),
				CreateEv("3/31/2019 2:01",   "3/31/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:02",   "3/31/2019 2:02",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:02",   "3/31/2019 2:02",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:02",   "3/31/2019 2:02",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:02",   "3/31/2019 2:02",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:03",   "3/31/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:03",   "3/31/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:03",   "3/31/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:03",   "3/31/2019 2:03",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("3/31/2019 2:03",   "3/31/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:03",   "3/31/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:03",   "3/31/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:03",   "3/31/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:04",   "3/31/2019 2:05",  "RM2Bay3_WaLim3",   "Electric Warning RM2Bay3_WaLim3 Warning limit exceeded in Bay 3                                                 warning                               "),
				CreateEv("3/31/2019 2:04",   "3/31/2019 2:04",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:04",   "3/31/2019 2:04",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:04",   "3/31/2019 2:04",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:04",   "3/31/2019 2:04",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:05",   "3/31/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:05",   "3/31/2019 2:05",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("3/31/2019 2:05",   "3/31/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:05",   "3/31/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:05",   "3/31/2019 2:06",  "RM2Bay3_WaLim3",   "Electric Warning RM2Bay3_WaLim3 Warning limit exceeded in Bay 3                                                 warning                               "),
				CreateEv("3/31/2019 2:05",   "3/31/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:05",   "3/31/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:05",   "3/31/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:06",   "3/31/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:06",   "3/31/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:06",   "3/31/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:06",   "3/31/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:06",   "3/31/2019 2:10",  "RM2Bay3_WaLim3",   "Electric Warning RM2Bay3_WaLim3 Warning limit exceeded in Bay 3                                                 warning                               "),
				CreateEv("3/31/2019 2:06",   "3/31/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:07",   "3/31/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:07",   "3/31/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:07",   "3/31/2019 2:07",  "MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("3/31/2019 2:07",   "3/31/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:07",   "3/31/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:08",   "3/31/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:08",   "3/31/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:08",   "3/31/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:08",   "3/31/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:08",   "3/31/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:08",   "3/31/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("3/31/2019 2:08",   "3/31/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",   "4/30/2019 2:08",	"ST1RB11_ADVICE",	"Far off test fault so we incrementatlly build the above"),
			};
			var ev2 = new List<Event> {
				CreateEv("4/30/2019 1:56",   "4/30/2019 1:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:56",   "4/30/2019 1:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:56",   "4/30/2019 1:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:56",   "4/30/2019 1:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:56",   "4/30/2019 2:07",  "RPIMTTR_FAULTD",   "Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning                                   "),
				CreateEv("4/30/2019 1:56",   "4/30/2019 1:57",  "MA1CY01_0834Y5",   "Mechanic MA1CY01_0834Y5 Inner clamp bar of swivel unit MA                                               not in homeposition                           "),
				CreateEv("4/30/2019 1:56",   "4/30/2019 1:57",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("4/30/2019 1:56",   "4/30/2019 1:57",  "RPIMTTR_OPRESP",   "Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning                                   "),
				CreateEv("4/30/2019 1:57",   "4/30/2019 2:07",  "RPIMTTR_RECOVE",   "Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning                                   "),
				CreateEv("4/30/2019 1:57",   "4/30/2019 1:57",  "MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:57",   "4/30/2019 1:57",  "PM1TIME_TOHIGH",   "Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:57",   "4/30/2019 1:57",  "CM1AV34_CMHOME",   "Misc Warning CM1AV34_CMHOME Cleaning module CM - not in home position                                       warning                                   "),
				CreateEv("4/30/2019 1:57",   "4/30/2019 1:58",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("4/30/2019 1:57",   "4/30/2019 1:58",  "EM1TRAN_NOTREL",   "Misc Warning EM1TRAN_NOTREL EM not ready to cycle                                                           warning                                   "),
				CreateEv("4/30/2019 1:57",   "4/30/2019 1:58",  "ST2TRAN_NOTREL",   "Misc Warning ST2TRAN_NOTREL No transport release station 2                                                  warning                                   "),
				CreateEv("4/30/2019 1:57",   "4/30/2019 1:58",  "ST3TRAN_NOTREL",   "Misc Warning ST3TRAN_NOTREL No transport release station 3                                                  warning                                   "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:58",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:58",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:58",  "EM1TRAN_NOTREL",   "Misc Warning EM1TRAN_NOTREL EM not ready to cycle                                                           warning                                   "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:58",  "ST2TRAN_NOTREL",   "Misc Warning ST2TRAN_NOTREL No transport release station 2                                                  warning                                   "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:58",  "ST3TRAN_NOTREL",   "Misc Warning ST3TRAN_NOTREL No transport release station 3                                                  warning                                   "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:58",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:58",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:58",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:58",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:58",   "4/30/2019 1:59",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 1:59",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 1:59",  "EM1TRAN_NOTREL",   "Misc Warning EM1TRAN_NOTREL EM not ready to cycle                                                           warning                                   "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 1:59",  "ST2TRAN_NOTREL",   "Misc Warning ST2TRAN_NOTREL No transport release station 2                                                  warning                                   "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 1:59",  "ST3TRAN_NOTREL",   "Misc Warning ST3TRAN_NOTREL No transport release station 3                                                  warning                                   "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 2:07",  "MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 2:12",  "PM1TIME_TOHIGH",   "Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 1:59",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 1:59",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 1:59",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:59",   "4/30/2019 1:59",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 2:00",   "4/30/2019 2:00",  "RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("4/30/2019 2:00",   "4/30/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:00",   "4/30/2019 2:00",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 2:00",   "4/30/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:00",   "4/30/2019 2:00",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 2:00",   "4/30/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:00",   "4/30/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:00",   "4/30/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:00",   "4/30/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:01",   "4/30/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:01",   "4/30/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:01",   "4/30/2019 2:01",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 2:01",   "4/30/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:01",   "4/30/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:01",   "4/30/2019 2:02",  "RM2Bay3_WaLim3",   "Electric Warning RM2Bay3_WaLim3 Warning limit exceeded in Bay 3                                                 warning                               "),
				CreateEv("4/30/2019 2:01",   "4/30/2019 2:01",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:02",   "4/30/2019 2:02",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:02",   "4/30/2019 2:02",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:02",   "4/30/2019 2:02",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:02",   "4/30/2019 2:02",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:03",   "4/30/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:03",   "4/30/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:03",   "4/30/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:03",   "4/30/2019 2:03",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 2:03",   "4/30/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:03",   "4/30/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:03",   "4/30/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:03",   "4/30/2019 2:03",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:04",   "4/30/2019 2:05",  "RM2Bay3_WaLim3",   "Electric Warning RM2Bay3_WaLim3 Warning limit exceeded in Bay 3                                                 warning                               "),
				CreateEv("4/30/2019 2:04",   "4/30/2019 2:04",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:04",   "4/30/2019 2:04",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:04",   "4/30/2019 2:04",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:04",   "4/30/2019 2:04",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:05",   "4/30/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:05",   "4/30/2019 2:05",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 2:05",   "4/30/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:05",   "4/30/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:05",   "4/30/2019 2:06",  "RM2Bay3_WaLim3",   "Electric Warning RM2Bay3_WaLim3 Warning limit exceeded in Bay 3                                                 warning                               "),
				CreateEv("4/30/2019 2:05",   "4/30/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:05",   "4/30/2019 2:05",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:05",   "4/30/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:06",   "4/30/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:06",   "4/30/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:06",   "4/30/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:06",   "4/30/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:06",   "4/30/2019 2:10",  "RM2Bay3_WaLim3",   "Electric Warning RM2Bay3_WaLim3 Warning limit exceeded in Bay 3                                                 warning                               "),
				CreateEv("4/30/2019 2:06",   "4/30/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:07",   "4/30/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:07",   "4/30/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:07",   "4/30/2019 2:07",  "MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 2:07",   "4/30/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:07",   "4/30/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",   "4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",   "4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",   "4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",   "4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",   "4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",   "4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",   "4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("5/30/2019 2:08",   "5/30/2019 2:08",  "ST1RB11_ADVICE",   "Far off test fault so we incrementatlly build the above"),
			};

			var expected = new List<Event> {
				CreateEv("3/31/2019 1:56",   "3/31/2019 2:07",   "MA1CY01_0834Y5",  "Mechanic MA1CY01_0834Y5 Inner clamp bar of swivel unit MA                                               not in homeposition                           "),
				CreateEv("4/30/2019 1:56",   "4/30/2019 2:07",   "MA1CY01_0834Y5",  "Mechanic MA1CY01_0834Y5 Inner clamp bar of swivel unit MA                                               not in homeposition                           "),
			};

			var mttr = new IncrementalMTTRFaultBuilder();
			mttr.AddEvents(ev1, true);
			Assert.AreEqual(mttr.Events.Count, 1, "Events should be removed after interation");
			Assert.AreEqual(mttr.Faults.Count, 1, "Should have interated on events and found 1 mttr fault event.");
			mttr.AddEvents(ev2, false);
			var result = mttr.FinishBuildingFaults();

			Assert.IsTrue(EventListsEqual(result, expected), "Mttr event lists not equal");
		}

		[TestMethod]
		public void TestIncrementalMTTR_Overlapping() {
			var ev1 = new List<Event> {
				CreateEv("4/29/2019 12:56",	"4/29/2019 13:56",	"ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/29/2019 12:57", "4/29/2019 13:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/29/2019 14:54", "4/29/2019 13:56",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),

				CreateEv("4/29/2019 14:56",	"4/29/2019 14:56",	"ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/29/2019 14:56",	"4/29/2019 14:56",	"ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/29/2019 14:56",	"4/29/2019 14:56",	"ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/29/2019 14:56",	"4/29/2019 14:57",	"RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("4/29/2019 14:56",	"4/29/2019 14:57",	"RPIMTTR_OPRESP",   "Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning                                   "),
				CreateEv("4/29/2019 14:56", "4/29/2019 14:57",  "MA1CY01_0834Y5",   "Mechanic MA1CY01_0834Y5 Inner clamp bar of swivel unit MA                                               not in homeposition                           "),
				CreateEv("4/29/2019 14:56",	"4/30/2019 2:07",	"RPIMTTR_FAULTD",   "Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning                                   "),				
				CreateEv("4/30/2019 1:56",	"4/30/2019 1:57",	"RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
				CreateEv("4/30/2019 1:57",	"4/30/2019 2:07",	"RPIMTTR_RECOVE",   "Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning                                   "),
				CreateEv("4/30/2019 1:57",	"4/30/2019 1:57",	"MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:57",	"4/30/2019 1:57",	"PM1TIME_TOHIGH",   "Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:57",	"4/30/2019 1:57",	"MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:57",	"4/30/2019 1:57",	"PM1TIME_TOHIGH",   "Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:57",	"4/30/2019 1:57",	"CM1AV34_CMHOME",   "Misc Warning CM1AV34_CMHOME Cleaning module CM - not in home position                                       warning                                   "),
				CreateEv("4/30/2019 1:59",	"4/30/2019 1:59",	"ST3TRAN_NOTREL",   "Misc Warning ST3TRAN_NOTREL No transport release station 3                                                  warning                                   "),
				CreateEv("4/30/2019 1:59",	"4/30/2019 2:07",	"MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:59",	"4/30/2019 2:12",	"PM1TIME_TOHIGH",   "Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 1:59",	"4/30/2019 1:59",	"ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:59",	"4/30/2019 1:59",	"ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:59",	"4/30/2019 1:59",	"ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 1:59",	"4/30/2019 1:59",	"RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 2:00",	"4/30/2019 2:00",	"RM2FA01_ToABS1",   "Electric Warning RM2FA01_ToABS1 Waiting for robot to load drawer 1                                              warning                               "),
			};
			var ev2 = new List<Event> {				
				CreateEv("4/30/2019 2:00",	"4/30/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:00",	"4/30/2019 2:00",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 2:00",	"4/30/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:00",	"4/30/2019 2:00",  "RM2FA01_ToABS2",   "Electric Warning RM2FA01_ToABS2 Waiting for robot to load drawer 2                                              warning                               "),
				CreateEv("4/30/2019 2:00",	"4/30/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:00",	"4/30/2019 2:00",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:06",	"4/30/2019 2:06",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:07",	"4/30/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:07",	"4/30/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:07",	"4/30/2019 2:07",  "MM1TIME_TOHIGH",   "Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning                                   "),
				CreateEv("4/30/2019 2:07",	"4/30/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:07",	"4/30/2019 2:07",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",	"4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",	"4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",	"4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",	"4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",	"4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",	"4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 2:08",	"4/30/2019 2:08",  "ST1RB11_ADVICE",   "Electric Warning ST1RB11_ADVICE Transponder St. 1 BC Transponder 11 not readable                                warning                               "),
				CreateEv("4/30/2019 4:00",	"4/30/2019 5:00",  "RPIMTTR_FAULTD",   "Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning                                   "),
				CreateEv("4/30/2019 4:00",	"4/30/2019 4:01",  "MA1CY01_0834Y5",   "Mechanic MA1CY01_0834Y5 Inner clamp bar of swivel unit MA                                               not in homeposition                           "),
				CreateEv("5/30/2019 2:08",	"5/30/2019 2:08",  "ST1RB11_ADVICE",   "Far off test fault so we incrementatlly build the above"),
			};

			var expected = new List<Event> {
				CreateEv("4/29/2019 14:56",	"4/30/2019 2:07",   "MA1CY01_0834Y5",  "Mechanic MA1CY01_0834Y5 Inner clamp bar of swivel unit MA                                               not in homeposition                           "),
				CreateEv("4/30/2019 4:00",	"4/30/2019 5:00",   "MA1CY01_0834Y5",  "Mechanic MA1CY01_0834Y5 Inner clamp bar of swivel unit MA                                               not in homeposition                           "),
			};

			var mttr = new IncrementalMTTRFaultBuilder();
			mttr.AddEvents(ev1, true);
			Assert.AreEqual(22, mttr.Events.Count, "When the MTTR fault is encountered, it cannot be processed. It should quit and only cull faults that happened >1 minute prior to the fault start");	
			mttr.AddEvents(ev2, false);
			var result = mttr.FinishBuildingFaults();

			Assert.IsTrue(EventListsEqual(result, expected), "Mttr event lists not equal");
		}

		private static Event CreateEv(string start, string end, string tag, string msg) {
			return new Event { Area = "PM1MM", Start = DateTime.Parse(start), End = DateTime.Parse(end), Tag = tag, Message = msg };
		}
		private bool EventListsEqual(List<Event> a, List<Event> b) {
			if (a.Count != b.Count) return false;

			for (int i = 0; i < a.Count; i++) {
				if (a[i].Area != b[i].Area
					|| a[i].Start != b[i].Start
					|| a[i].End != b[i].End
					|| a[i].Message != b[i].Message) {
					return false;
				}
			}

			return true;
		}
	}
}
