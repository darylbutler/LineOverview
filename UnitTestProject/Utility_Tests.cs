﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTTROverview;
using System;
using System.Collections.Generic;

namespace UnitTestProject
{
	[TestClass]
	public class Utility_Tests
	{
		// Helper Function for tests
		private List<Event> StringToEvents(string str) {
			var events = new List<Event>();
			foreach (var line in str.Split('\n', '\r')) {
				if (string.IsNullOrWhiteSpace(line)) continue;
				var split = line.Split('\t');

				events.Add(new Event() {
					Area = split[0].Trim(),
					Tag = split[1].Trim(),
					Start = DateTime.Parse(split[2].Trim()),
					End = DateTime.Parse(split[3].Trim()),
					Message = split[5].Trim()
				});
			}
			return events;
		}

		private bool FaultListsEqual(List<Fault> a, List<Fault> b) {
			if (a.Count != b.Count) return false;

			for (int i = 0; i < a.Count; i++) {
				if (a[i].ToString() != b[i].ToString())
					return false;
			}

			return true;
		}

		private bool EventListsEqual(List<Event> a, List<Event> b) {
			if (a.Count != b.Count) return false;

			for (int i = 0; i < a.Count; i++) {
				if (a[i].Area != b[i].Area
					|| a[i].Start != b[i].Start
					|| a[i].End != b[i].End
					|| a[i].Message != b[i].Message) {
					return false;
				}
			}

			return true;
		}

		[TestMethod]
		public void Test_FindFault_MM() {
			var events = StringToEvents(@"
PM1MM	EM1TRAN_NOTREL	12/11/2018 07:28:35	12/11/2018 07:28:40	5s	Misc Warning EM1TRAN_NOTREL EM not ready to cycle                                                           warning
PM1MM	ST2TRAN_NOTREL	12/11/2018 07:28:35	12/11/2018 07:28:40	5s	Misc Warning ST2TRAN_NOTREL No transport release station 2                                                  warning
PM1MM	ST3TRAN_NOTREL	12/11/2018 07:28:35	12/11/2018 07:28:40	5s	Misc Warning ST3TRAN_NOTREL No transport release station 3                                                  warning
PM1MM	TW2TRAN_NOTREL	12/11/2018 07:28:35	12/11/2018 07:28:40	5s	Misc Warning TW2TRAN_NOTREL No transport release station TW2                                                warning
PM1MM	DO1DI02_0521M1	12/11/2018 07:28:40	12/11/2018 07:29:36	56s	Electric DO1DI02_0521M1 Axiserror Transport FC 1                                                        fault
PM1MM	RPIMTTR_OPRESP	12/11/2018 07:28:40	12/11/2018 07:29:34	54s	Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning
PM1MM	RPIMTTR_FAULTD	12/11/2018 07:28:40	12/11/2018 07:44:32	15m52s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM1MM	ME1DI41_I776S5	12/11/2018 07:29:34	12/11/2018 07:31:06	1m32s	Misc Warning ME1DI41_I776S5 SCE drawer FC is not empty                                                      warning
PM1MM	PM1TIME_TOHIGH	12/11/2018 07:29:34	12/11/2018 07:31:06	1m32s	Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning
PM1MM	TW2SCE1_ONLYHW	12/11/2018 07:29:34	12/11/2018 07:31:06	1m32s	Misc Warning TW2SCE1_ONLYHW SCE started outside of cleaning round                                           warning
PM1MM	RPIMTTR_RECOVE	12/11/2018 07:29:34	12/11/2018 07:44:32	14m58s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM1MM	DF1DI04_0524M1	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric DF1DI04_0524M1 Axiserror Transport FC 3                                                        fault
PM1MM	DF1DI08_0523M4	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric DF1DI08_0523M4 Axiserror Transport BC 4                                                        fault
PM1MM	DO1DI02_0521M1	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric DO1DI02_0521M1 Axiserror Transport FC 1                                                        fault
PM1MM	DO1DI07_0522M4	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric DO1DI07_0522M4 Axiserror Transport BC 2                                                        fault
PM1MM	FO1DI03_0522M1	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric FO1DI03_0522M1 Axiserror Transport FC 2                                                        fault
PM1MM	PR1DI06_0521M4	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric PR1DI06_0521M4 Axiserror Transport BC 1                                                        fault
PM1MM	TW1DI67_0516M4	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric TW1DI67_0516M4 Insert axis BC - axis error                                                     fault
PM1MM	TW2DI15_0517A0	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric TW2DI15_0517A0 Discharge axis FC- TW 2 top - axis error                                        fault
PM1MM	WF1DI05_0523M1	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric WF1DI05_0523M1 Axiserror Transport FC 4                                                        fault
PM1MM	TW1DI19_0516M1	12/11/2018 07:29:40	12/11/2018 07:30:54	1m14s	Electric TW1DI19_0516M1 Insert axis FC - axis error                                                     fault
PM1MM	FO1SA01_TR0x05	12/11/2018 07:29:42	12/11/2018 07:30:53	1m11s	Mechanic Warning FO1SA01_TR0x05 PM1_Safety Door (513S1) MM1 SD-105                                              open
PM1MM	MM1SACR_SAMMX1	12/11/2018 07:29:42	12/11/2018 07:30:53	1m11s	Electric Warning MM1SACR_SAMMX1 PM1_One of Safety Doors MM1 Section1 Open                                       warning
PM1MM	SC1SE01_301A46	12/11/2018 07:29:42	12/11/2018 07:30:55	1m13s	Electric SC1SE01_301A46 All safety contactors closed                                                    not on
PM1MM	CB1DI08_2423LP	12/11/2018 07:29:43	12/11/2018 07:30:54	1m11s	Electric CB1DI08_2423LP Voltage 2423LPLUS Safety door 1 circuit - no release                            fault
PM1MM	CB1DI13_CLEANM	12/11/2018 07:29:43	12/11/2018 07:30:54	1m11s	Misc CB1DI13_CLEANM Cleaning Module CM - Error                                                      fault
PM1MM	CB1DI14_CLEANM	12/11/2018 07:29:43	12/11/2018 07:31:02	1m19s	Misc CB1DI14_CLEANM Cleaning module CM - not ready                                                  fault
PM1MM	CB1ST01_2427LP	12/11/2018 07:29:43	12/11/2018 07:30:54	1m11s	Electric CB1ST01_2427LP Voltage 2427LPLUS SDC 1+2 closed or released - enable button                    fault
PM1MM	MA1DO01_Master	12/11/2018 07:29:43	12/11/2018 07:31:05	1m22s	Electric MA1DO01_Master Mating master axis  fault                                                       fault
PM1MM	DB1SE01_160R03	12/11/2018 07:29:58	12/11/2018 07:30:45	47s	Electric Warning DB1SE01_160R03 Temperature air T4                                                              below limit
PM1MM	DO1SA01_TR0x04	12/11/2018 07:30:48	12/11/2018 07:30:53	5s	Mechanic Warning DO1SA01_TR0x04 PM1_Safety Door (502S2) MM1 SD-104                                              open
PM1MM	CB1D279_DRIVES	12/11/2018 07:30:54	12/11/2018 07:31:05	11s	Electric CB1D279_DRIVES Axis Fault (Wait for release)                                                   fault
PM1MM	CB1DI13_CLEANM	12/11/2018 07:30:55	12/11/2018 07:31:02	7s	Misc CB1DI13_CLEANM Cleaning Module CM - Error                                                      fault
PM1MM	TW1D157_0540M1	12/11/2018 07:31:05	12/11/2018 07:31:11	6s	Electric TW1D157_0540M1 Insertion Z-axis: axis error                                                    fault
PM1MM	ME1DI41_I776S5	12/11/2018 07:31:08	12/11/2018 07:31:15	7s	Misc Warning ME1DI41_I776S5 SCE drawer FC is not empty                                                      warning
PM1MM	PM1TIME_TOHIGH	12/11/2018 07:31:08	12/11/2018 07:31:15	7s	Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning
PM1MM	TW2SCE1_ONLYHW	12/11/2018 07:31:08	12/11/2018 07:31:15	7s	Misc Warning TW2SCE1_ONLYHW SCE started outside of cleaning round                                           warning
PM1MM	PR1PAKW_I732S1	12/11/2018 07:31:15	12/11/2018 07:31:23	8s	Electric PR1PAKW_I732S1 Carrier station 1 FC-side not in position (732S1)                               fault
PM1MM	ME1DI41_I776S5	12/11/2018 07:31:22	12/11/2018 07:34:52	3m30s	Misc Warning ME1DI41_I776S5 SCE drawer FC is not empty                                                      warning
PM1MM	PM1TIME_TOHIGH	12/11/2018 07:31:22	12/11/2018 07:34:31	3m9s	Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning
PM1MM	TW2SCE1_ONLYHW	12/11/2018 07:31:22	12/11/2018 09:34:06	2h2m44s	Misc Warning TW2SCE1_ONLYHW SCE started outside of cleaning round                                           warning
PM1MM	TW2SCE1_HOMING	12/11/2018 07:31:33	12/11/2018 07:32:38	1m5s	Mechanic Warning TW2SCE1_HOMING Single Carrier Exchange unable to home successfully. Perform SCE reset          fault
PM1MM	SCEFUNC_RESET1	12/11/2018 07:32:16	12/11/2018 07:32:16	0s	Reset all flags from SCE Selection HICKSJA5
PM1MM	SCEFUNC_RESET1	12/11/2018 07:32:19	12/11/2018 07:32:19	0s	Reset all flags from SCE Deselection HICKSJA5
PM1MM	SCEFUNC_RESET1	12/11/2018 07:32:44	12/11/2018 07:32:44	0s	Reset all flags from SCE Selection HICKSJA5
PM1MM	TW2SCE1_HOMING	12/11/2018 07:32:46	12/11/2018 07:33:35	49s	Mechanic Warning TW2SCE1_HOMING Single Carrier Exchange unable to home successfully. Perform SCE reset          fault
PM1MM	SCEFUNC_RESET1	12/11/2018 07:32:48	12/11/2018 07:32:48	0s	Reset all flags from SCE Deselection HICKSJA5
PM1MM	ME1DI41_I776S7	12/11/2018 07:33:43	12/11/2018 07:34:46	1m3s	Misc Warning ME1DI41_I776S7 SCE drawer BC is not empty                                                      warning
PM1MM	CM1AV34_CMHOME	12/11/2018 07:34:07	12/11/2018 07:34:19	12s	Misc Warning CM1AV34_CMHOME Cleaning module CM - not in home position                                       warning
PM1MM	RPIMTTR_INAUTO	12/11/2018 07:34:31	12/11/2018 10:05:58	2h31m27s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1MM	ST1RB02_ADVICE	12/11/2018 07:34:56	12/11/2018 07:35:01	5s	Electric Warning ST1RB02_ADVICE Transponder St. 1 BC Transponder 2 not readable                                 warning
PM1MM	RPIMTTR_INPROD	12/11/2018 07:34:58	12/11/2018 08:24:49	49m51s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1MM	ST1RB06_ADVICE	12/11/2018 07:35:26	12/11/2018 07:35:30	4s	Electric Warning ST1RB06_ADVICE Transponder St. 1 BC Transponder 6 not readable                                 warning
PM1MM	ST1RF12_ADVICE	12/11/2018 07:35:40	12/11/2018 07:35:43	3s	Electric Warning ST1RF12_ADVICE Transponder St. 1 FC Transponder 12 not readable                                warning
PM1MM	PM1TIME_TOHIGH	12/11/2018 07:35:43	12/11/2018 07:36:32	49s	Misc Warning PM1TIME_TOHIGH PM Cycle time exceeded ideal time                                               warning
PM1MM	MM1TIME_TOHIGH	12/11/2018 07:35:43	12/11/2018 08:56:04	1h20m21s	Misc Warning MM1TIME_TOHIGH MM Cycle time exceeded ideal time                                               warning
PM1MM	ST1RB06_ADVICE	12/11/2018 07:45:08	12/11/2018 07:45:12	4s	Electric Warning ST1RB06_ADVICE Transponder St. 1 BC Transponder 6 not readable                                 warning
PM1MM	ST1RF12_ADVICE	12/11/2018 07:45:18	12/11/2018 07:45:22	4s	Electric Warning ST1RF12_ADVICE Transponder St. 1 FC Transponder 12 not readable                                warning
PM1MM	ST1RF11_ADVICE	12/11/2018 07:45:23	12/11/2018 07:45:28	5s	Electric Warning ST1RF11_ADVICE Transponder St. 1 FC Transponder 11 not readable                                warning
PM1MM	ST1RB03_ADVICE	12/11/2018 07:45:35	12/11/2018 07:45:39	4s	Electric Warning ST1RB03_ADVICE Transponder St. 1 BC Transponder 3 not readable                                 warning
PM1MM	ST1RF12_ADVICE	12/11/2018 07:45:41	12/11/2018 07:45:43	2s	Electric Warning ST1RF12_ADVICE Transponder St. 1 FC Transponder 12 not readable                                warning
");

			// MM normal fault: Full Case
			var e = Utility.BuildEvent(events, 6);
			Assert.AreEqual("Electric DO1DI02_0521M1 Axiserror Transport FC 1                                                        fault", e.Message);
			Assert.AreEqual("DO1DI02_0521M1", e.Tag);
			Assert.AreEqual(new TimeSpan(0, 0, 54), e.OPRespTime);
			Assert.AreEqual(new TimeSpan(0, 4, 58), e.RecoveryTime);
			Assert.AreEqual(new TimeSpan(0, 0, 0), e.EMRespTime);
			Assert.AreEqual(new TimeSpan(0, 0, 0), e.RepairTime);

			// Unknown Fault
			events = StringToEvents(@"
PM1MM	ST1RB03_ADVICE	12/11/2018 17:09:10	12/11/2018 17:09:13	3s	Electric Warning ST1RB03_ADVICE Transponder St. 1 BC Transponder 3 not readable                                 warning
PM1MM	RPIMTTR_FAULTD	12/11/2018 17:20:53	12/11/2018 17:31:27	11m19s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM1MM	ME1DI41_I776S7	12/11/2018 17:20:55	12/11/2018 17:35:33	14m38s	Misc Warning ME1DI41_I776S7 SCE drawer BC is not empty                                                      warning
PM1MM	RPIMTTR_RECOVE	12/11/2018 17:20:55	12/11/2018 17:31:27	10m32s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM1MM	RPIMTTR_INAUTO	12/11/2018 17:21:27	12/11/2018 17:35:33	14m6s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1MM	RPIMTTR_INPROD	12/11/2018 17:21:35	12/11/2018 17:35:33	13m58s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1MM	ST1RB02_ADVICE	12/11/2018 17:24:00	12/11/2018 17:24:03	3s	Electric Warning ST1RB02_ADVICE Transponder St. 1 BC Transponder 2 not readable                                 warning");
			e = Utility.BuildEvent(events, 1);
			Assert.AreEqual("Unknown MM Fault", e.Message);
			Assert.AreEqual("Unknown MM Fault", e.Tag);

			// Unknown Fault
			events = StringToEvents(@"
PM1MM	ST3TRAN_NOTREL	12/11/2018 19:41:43	12/11/2018 19:47:18	5m35s	Misc Warning ST3TRAN_NOTREL No transport release station 3                                                  warning
PM1MM	TW2TRAN_NOTREL	12/11/2018 19:41:43	12/11/2018 19:41:47	4s	Misc Warning TW2TRAN_NOTREL No transport release station TW2                                                warning
PM1MM	CB1AV13_LATMOS	12/11/2018 19:47:18	12/11/2018 19:48:54	1m36s	Mediamissing Warning CB1AV13_LATMOS Protective atmosphere - error                                                   warning
PM1MM	RPIMTTR_OPRESP	12/11/2018 19:47:18	12/11/2018 19:48:02	44s	Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning
PM1MM	RPIMTTR_FAULTD	12/11/2018 19:47:18	12/11/2018 20:00:14	12m56s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM1MM	RPIMTTR_RECOVE	12/11/2018 19:48:02	12/11/2018 20:00:14	12m12s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM1MM	CB1AV18_LATMOS	12/11/2018 19:48:07	12/11/2018 19:49:31	1m24s	Mediamissing Warning CB1AV18_LATMOS Protective atmosphere not ok and dosing not active                              warning
PM1MM	ME1DI41_I776S5	12/11/2018 19:48:07	12/11/2018 19:52:11	4m4s	Misc Warning ME1DI41_I776S5 SCE drawer FC is not empty                                                      warning
PM1MM	CB1AV15_LATMOS	12/11/2018 19:49:00	12/11/2018 19:49:15	15s	Mediamissing Warning CB1AV15_LATMOS Protective atmosphere - standby - no error                                      warning
PM1MM	DO1OR03_OHNELA	12/11/2018 19:49:40	12/11/2018 19:49:40	0s	Testrun w/o LA is active deselection NARTEIS1");
			e = Utility.BuildEvent(events, 4);
			Assert.AreEqual("Mediamissing Warning CB1AV13_LATMOS Protective atmosphere - error                                                   warning", e.Message);
			Assert.AreEqual("CB1AV13_LATMOS", e.Tag);

			// Finds the MTTR fault itself
			events = StringToEvents(@"
PM8MM	CB1DI13_CLEANM	12/11/2018 07:12:44	12/11/2018 07:12:50	6s	Cleaning Module CM - Error
PM8MM	CM1AV34_CMHOME	12/11/2018 07:13:03	12/11/2018 07:13:04	1s	Cleaning module CM - not in home position
PM8MM	RPIMTTR_FAULTD	12/11/2018 07:19:16	12/11/2018 07:32:43	13m27s	MTTR: Machine in Faulted State (Downtime Tracked)
PM8MM	RPIMTTR_RECOVE	12/11/2018 07:19:52	12/11/2018 07:32:43	12m51s	MTTR: Recovery State
PM8MM	AV1AV11_ADVICE	12/11/2018 07:19:53	12/11/2018 07:59:50	39m57s	Start new Lot from SCADA
PM8MM	CB1WRN1_PNOZ01	12/11/2018 07:19:53	12/11/2018 07:59:50	39m57s	PNOZ Multi main device reports a  warning (e.g. safety element ready to confirm...)
PM8MM	VM2DI04_0715S3	12/11/2018 07:22:42	12/11/2018 07:59:50	37m8s	Disinfection - active");
			e = Utility.BuildEvent(events, 2);
			Assert.AreEqual("Unknown MM Fault", e.Message);
			Assert.AreEqual("Unknown MM Fault", e.Tag);

			// Finds the MTTR fault itself
			events = StringToEvents(@"
PM8MM	CB1A081_ADVICE	12/11/2018 17:00:28	12/11/2018 18:14:57	1h14m29s	Autostart active
PM8MM	ME1LK27_966B05	12/11/2018 17:17:02	12/11/2018 18:14:57	57m55s	Error RFID-Reader 13 female at SCE
PM8MM	RPIMTTR_FAULTD	12/11/2018 18:14:57	12/11/2018 19:03:00	48m3s	MTTR: Machine in Faulted State (Downtime Tracked)
PM8MM	RPIMTTR_RECOVE	12/11/2018 18:17:59	12/11/2018 19:03:00	45m1s	MTTR: Recovery State
PM8MM	AV1AV10_ADVICE	12/11/2018 18:21:03	12/11/2018 18:31:00	9m57s	Lot end message to SCADA
PM8MM	CB1AV50_ACDATA	12/11/2018 18:21:03	12/11/2018 18:31:00	9m57s	Lot change happens w/o actual production data
PM8MM	CB1WRN1_PNOZ01	12/11/2018 18:21:03	12/11/2018 18:31:01	9m58s	PNOZ Multi main device reports a  warning (e.g. safety element ready to confirm...)
PM8MM	MESPROD_RELPMS	12/11/2018 18:21:22	12/11/2018 18:31:01	9m39s	No procuction release from production management system (SCADA)
PM8MM	VM2DI04_0715S3	12/11/2018 18:21:22	12/11/2018 18:31:00	9m38s	Disinfection - active
PM8MM	RPIMTTR_INAUTO	12/11/2018 18:21:24	12/11/2018 18:31:00	9m36s	MTTR: Machine is Cycling
PM8MM	CB1A081_ADVICE	12/11/2018 18:21:24	12/11/2018 18:31:01	9m37s	Autostart active");
			e = Utility.BuildEvent(events, 2);
			Assert.AreEqual("Unknown MM Fault", e.Message);
			Assert.AreEqual("Unknown MM Fault", e.Tag);

			// Doesn't find the right fault
			events = StringToEvents(@"
PM1MM	ST1RB02_ADVICE	12/12/2018 17:34:21	12/12/2018 17:34:23	2s	Electric Warning ST1RB02_ADVICE Transponder St. 1 BC Transponder 2 not readable                                 warning
PM1MM	ME1DI34_I796E3	12/12/2018 17:34:23	12/12/2018 17:34:49	26s	Misc Warning ME1DI34_I796E3 SCE drawer FC is not locked in place I796S3                                     warning
PM1MM	ME1DI35_I796E4	12/12/2018 17:34:29	12/12/2018 17:34:49	20s	Misc Warning ME1DI35_I796E4 SCE drawer BC is not locked in place I796S4                                     warning
PM1MM	ST1RF11_ADVICE	12/12/2018 17:34:36	12/12/2018 17:34:49	13s	Electric Warning ST1RF11_ADVICE Transponder St. 1 FC Transponder 11 not readable                                warning
PM1MM	ME2ST06_995B02	12/12/2018 17:34:45	12/12/2018 17:35:22	37s	Electric ME2ST06_995B02 error RFID reading BC                                                           fault
PM1MM	RPIMTTR_OPRESP	12/12/2018 17:34:49	12/12/2018 17:35:22	33s	Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning
PM1MM	RPIMTTR_FAULTD	12/12/2018 17:34:49	12/12/2018 17:46:53	12m4s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM1MM	RPIMTTR_RECOVE	12/12/2018 17:35:22	12/12/2018 17:46:53	11m31s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM1MM	SCEFUNC_RESET1	12/12/2018 17:35:43	12/12/2018 17:35:43	0s	Reset all flags from SCE Selection HICKSJA5
PM1MM	SCEFUNC_RESET1	12/12/2018 17:35:48	12/12/2018 17:35:48	0s	Reset all flags from SCE Deselection HICKSJA5
PM1MM	CM1AV34_CMHOME	12/12/2018 17:36:36	12/12/2018 17:36:46	10s	Misc Warning CM1AV34_CMHOME Cleaning module CM - not in home position                                       warning");
			e = Utility.BuildEvent(events, 6);
			Assert.AreEqual("Electric ME2ST06_995B02 error RFID reading BC                                                           fault", e.Message);
			Assert.AreEqual("ME2ST06_995B02", e.Tag);
		}

		[TestMethod]
		public void Test_FindFault_EM() {
			var events = StringToEvents(@"
PM7EM	16RMESG_M62750	12/12/2018 19:37:21	12/12/2018 19:39:14	1m53s	IM failure
PM7EM	RPIMTTR_INPROD	12/12/2018 19:39:16	12/12/2018 20:00:09	20m53s	MTTR: Machine is Productive
PM7EM	GENWARN_W00581	12/12/2018 20:00:07	12/12/2018 20:52:58	52m51s	Tankfarm: error
PM7EM	RPIMTTR_OPRESP	12/12/2018 20:00:07	12/12/2018 20:45:14	45m7s	MTTR: Operator Acknowledge State
PM7EM	GENMESG_M00758	12/12/2018 20:00:09	12/12/2018 20:53:00	52m51s	Tankfarm: not ready
PM7EM	RPIMTTR_FAULTD	12/12/2018 20:00:09	12/12/2018 21:15:06	1h14m57s	MTTR: Machine in Faulted State (Downtime Tracked)
PM7EM	01FMESG_M71750	12/12/2018 20:07:15	12/12/2018 20:07:18	3s	ADS error invalid carrier data from MM
PM7EM	02MSENS_815021	12/12/2018 20:17:24	12/12/2018 21:47:10	1h29m46s	concentration
PM7EM	RPIMTTR_RECOVE	12/12/2018 20:45:14	12/12/2018 21:15:06	29m52s	MTTR: Recovery State
PM7EM	RPIMTTR_INPROD	12/12/2018 20:53:00	12/12/2018 21:00:26	7m26s	MTTR: Machine is Productive
PM7EM	RPIMTTR_INAUTO	12/12/2018 20:53:00	12/12/2018 21:00:28	7m28s	MTTR: Machine is Cycling
PM7EM	01TEROR_E51254	12/12/2018 21:00:26	12/12/2018 21:04:56	4m30s	reading error RFID reader (please remove carrier)
PM7EM	GENMESG_M00754	12/12/2018 21:04:20	12/12/2018 21:05:01	41s	key-switch permission active
PM7EM	RPIMTTR_INPROD	12/12/2018 21:05:07	12/13/2018 00:54:09	3h49m2s	MTTR: Machine is Productive
PM7EM	RPIMTTR_INAUTO	12/12/2018 21:05:07	12/13/2018 00:54:42	3h49m35s	MTTR: Machine is Cycling
PM7EM	01FMESG_M71750	12/12/2018 21:05:18	12/12/2018 21:05:22	4s	ADS error invalid carrier data from MM
PM7EM	16TWARN_W66511	12/12/2018 21:19:53	12/12/2018 21:57:09	37m16s	carrier missed
PM7EM	16TWARN_W66512	12/12/2018 21:19:55	12/12/2018 21:57:09	37m14s	unexpected carrier detected
PM7EM	09MWARN_W09518	12/12/2018 21:23:54	12/12/2018 21:57:11	33m17s	concentration sensor outlet measured value overrange
PM7EM	09MSENS_850021	12/12/2018 21:24:02	12/12/2018 21:57:09	33m7s	concentration outlet
PM7EM	01FMESG_M71757	12/12/2018 21:24:20	12/12/2018 21:28:35	4m15s	MM has error
PM7EM	02MSENS_815021	12/12/2018 21:47:40	12/12/2018 21:57:09	9m29s	concentration
");
			var e = Utility.BuildEvent(events, 5);
			Assert.AreEqual("Tankfarm: error", e.Message);
			Assert.AreEqual("GENWARN_W00581", e.Tag);

			// Lack of CTs (mapped from warn to fault)
			events = StringToEvents(@"
PM1TM	PL1OR01_SpcSp1	12/12/2018 14:38:27	12/12/2018 14:38:27	0s	SPC sampling lot 1 START MORASAB1
PM1TM	PL3OR03_SpcSp3	12/12/2018 14:38:30	12/12/2018 14:38:30	0s	SPC sampling lot 3 START MORASAB1
PM1TM	PL5OR05_SpcSp5	12/12/2018 14:38:32	12/12/2018 14:38:32	0s	SPC sampling lot 5 START MORASAB1
PM1TM	PL7OR07_SpcSp7	12/12/2018 14:38:34	12/12/2018 14:38:34	0s	SPC sampling lot 7 START MORASAB1
PM1TM	PL2OR02_SpcSp2	12/12/2018 14:38:40	12/12/2018 14:38:40	0s	SPC sampling lot 2 START MORASAB1
PM1TM	PL4OR04_SpcSp4	12/12/2018 14:38:42	12/12/2018 14:38:42	0s	SPC sampling lot 4 START MORASAB1
PM1TM	PL6OR06_SpcSp6	12/12/2018 14:38:44	12/12/2018 14:38:44	0s	SPC sampling lot 6 START MORASAB1
PM1TM	LLIFA01_Deckel	12/12/2018 14:55:39	12/12/2018 14:59:59	4m20s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	CLIFA01_LeerWT	12/12/2018 14:59:28	12/12/2018 14:59:47	19s	Electric CLIFA01_LeerWT CT Lifter - Lack of CTs (PLC: 1334)                                             warning
PM1TM	RPIMTTR_FAULTD	12/12/2018 14:59:28	12/12/2018 15:09:47	10m19s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM1TM	RPIMTTR_OPRESP	12/12/2018 14:59:28	12/12/2018 15:00:01	33s	Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning
PM1TM	RPIMTTR_INPROD	12/12/2018 14:59:47	12/12/2018 20:38:56	5h39m9s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1TM	RPIMTTR_RECOVE	12/12/2018 15:00:01	12/12/2018 15:09:47	9m46s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:00:49	12/12/2018 15:01:04	15s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:01:55	12/12/2018 15:01:57	2s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:02:47	12/12/2018 15:02:56	9s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:03:46	12/12/2018 15:04:32	46s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:05:22	12/12/2018 15:05:24	2s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:06:52	12/12/2018 15:06:53	1s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	PL1OR01_SpcSp1	12/12/2018 16:32:12	12/12/2018 16:32:12	0s	SPC sampling lot 1 START HICKSJA5
PM1TM	PL3OR03_SpcSp3	12/12/2018 16:32:14	12/12/2018 16:32:14	0s	SPC sampling lot 3 START HICKSJA5
PM1TM	PL5OR05_SpcSp5	12/12/2018 16:32:15	12/12/2018 16:32:15	0s	SPC sampling lot 5 STOP HICKSJA5");
			//e = Utility.BuildEvent(events, 9);
			//Assert.AreEqual("Electric CLIFA01_LeerWT CT Lifter - Lack of CTs (PLC: 1334)                                             warning", e.Message);
			//Assert.AreEqual("CLIFA01_LeerWT", e.Tag);
		}

		[TestMethod]
		public void Test_FindFault_IM() {
			var events = StringToEvents(@"
PM1IM	RPIMTTR_INAUTO	12/11/2018 10:12:34	12/11/2018 10:33:06	20m32s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1IM	RPIMTTR_INPROD	12/11/2018 10:12:34	12/11/2018 10:33:06	20m32s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	IM1SACR_STx161	12/11/2018 10:31:29	12/11/2018 10:33:06	1m37s	Electric Warning IM1SACR_STx161 PM1_One of Safety Doors IM1 Open                                                warning
PM1IM	CB1CB01_ADVICE	12/11/2018 10:33:06	12/11/2018 10:33:06	0s	Misc Warning CB1CB01_ADVICE Control voltage is switched off! (PLC: 3)                                       warning
PM1IM	CB1CB02_ADVICE	12/11/2018 10:33:06	12/11/2018 10:33:07	1s	Misc Warning CB1CB02_ADVICE Compressed air complete line missing (PLC: 72)                                  warning
PM1IM	RPIMTTR_FAULTD	12/11/2018 10:33:06	12/11/2018 10:59:01	25m55s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM1IM	RPIMTTR_RECOVE	12/11/2018 10:33:06	12/11/2018 10:59:01	25m55s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM1IM	OM1CT05_ADVICE	12/11/2018 10:33:06	12/11/2018 10:33:06	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	CB1FA50_ADVICE	12/11/2018 10:33:07	12/11/2018 10:33:19	12s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM1IM	CPPFA88_ADVICE	12/11/2018 10:33:07	12/11/2018 10:34:38	1m31s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:33:07	12/11/2018 10:34:38	1m31s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:33:07	12/11/2018 10:34:38	1m31s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:33:07	12/11/2018 10:34:38	1m31s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:33:13	12/11/2018 10:33:22	9s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CB1FA50_ADVICE	12/11/2018 10:33:20	12/11/2018 10:34:38	1m18s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:33:22	12/11/2018 10:33:28	6s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:33:28	12/11/2018 10:33:28	0s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:33:29	12/11/2018 10:33:29	0s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:33:29	12/11/2018 10:33:30	1s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CB1FA01_ADVICE	12/11/2018 10:33:30	12/11/2018 10:34:38	1m8s	Electric CB1FA01_ADVICE Inspection Module - Profibus error (PLC: 24)                                    fault
PM1IM	CC1GW01_0020A1	12/11/2018 10:33:30	12/11/2018 10:34:37	1m7s	Electric CC1GW01_0020A1 AEG identification system                                                       bus error
PM1IM	CC1GW01_0036Y1	12/11/2018 10:33:30	12/11/2018 10:34:37	1m7s	Electric CC1GW01_0036Y1 Profibus coupler Festo valve terminal                                           bus error
PM1IM	CPPFA33_ADVICE	12/11/2018 10:33:30	12/11/2018 10:33:34	4s	Electric CPPFA33_ADVICE CT Pick and Place - Nc axis hor. servo error! (PLC: 389)                        fault
PM1IM	CPPFA35_ADVICE	12/11/2018 10:33:30	12/11/2018 10:33:34	4s	Electric CPPFA35_ADVICE CT Pick and Place - Nc axis CT lifter ver. servo error! (PLC: 405)              fault
PM1IM	CTPFA01_ADVICE	12/11/2018 10:33:30	12/11/2018 10:33:34	4s	Electric CTPFA01_ADVICE CT Transport - Nc axis transport servo error! (PLC: 341)                        fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:33:30	12/11/2018 10:34:38	1m8s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA03_ADVICE	12/11/2018 10:33:30	12/11/2018 10:33:34	4s	Electric IVSFA03_ADVICE Inspection Vision System - Nc axis camera servo error! (PLC: 357)               fault
PM1IM	IVSFA05_ADVICE	12/11/2018 10:33:30	12/11/2018 10:33:34	4s	Electric IVSFA05_ADVICE Inspection Vision System - Nc axis camera light servo error! (PLC: 373)         fault
PM1IM	LTRFA61_ADVICE	12/11/2018 10:33:30	12/11/2018 10:33:34	4s	Electric LTRFA61_ADVICE Lens Transfer Robot - NC axis servo fault (PLC: 573)                            fault
PM1IM	PO1GW01_0001X1	12/11/2018 10:33:30	12/11/2018 10:34:37	1m7s	Electric PO1GW01_0001X1 Profibus coupler IP link box                                                    bus error
PM1IM	PO1GW01_0041Y2	12/11/2018 10:33:30	12/11/2018 10:34:37	1m7s	Electric PO1GW01_0041Y2 Profibus coupler Festo valve terminal                                           bus error
PM1IM	IM1SACR_STx161	12/11/2018 10:33:37	12/11/2018 10:34:16	39s	Electric Warning IM1SACR_STx161 PM1_One of Safety Doors IM1 Open                                                warning
PM1IM	IM1SACR_STx161	12/11/2018 10:34:17	12/11/2018 10:34:20	3s	Electric Warning IM1SACR_STx161 PM1_One of Safety Doors IM1 Open                                                warning
PM1IM	IM1SACR_STx161	12/11/2018 10:34:21	12/11/2018 10:35:35	1m14s	Electric Warning IM1SACR_STx161 PM1_One of Safety Doors IM1 Open                                                warning
PM1IM	CB1FU15_132FD0	12/11/2018 10:34:37	12/11/2018 10:35:35	58s	Electric CB1FU15_132FD0 Overload control voltage (24V) (PLC: 1)                                         tripped
PM1IM	BA1SA01_TR0x50	12/11/2018 10:34:45	12/11/2018 10:35:35	50s	Mechanic Warning BA1SA01_TR0x50 PM1_Safety Door (561S4) IM1 SD-150                                              open
PM1IM	CB1CB02_ADVICE	12/11/2018 10:35:35	12/11/2018 10:35:37	2s	Misc Warning CB1CB02_ADVICE Compressed air complete line missing (PLC: 72)                                  warning
PM1IM	CB1FA50_ADVICE	12/11/2018 10:35:36	12/11/2018 10:35:48	12s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM1IM	CPPFA88_ADVICE	12/11/2018 10:35:36	12/11/2018 10:35:54	18s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:35:36	12/11/2018 10:35:53	17s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:35:36	12/11/2018 10:35:37	1s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:35:36	12/11/2018 10:36:02	26s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:35:36	12/11/2018 10:36:03	27s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:35:37	12/11/2018 10:35:39	2s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:35:39	12/11/2018 10:35:42	3s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:35:42	12/11/2018 10:35:52	10s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:35:52	12/11/2018 10:36:06	14s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:35:53	12/11/2018 10:35:54	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:35:54	12/11/2018 10:36:03	9s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	IVSFA69_ADVICE	12/11/2018 10:35:54	12/11/2018 10:36:02	8s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	OM1CT07_ADVICE	12/11/2018 10:36:07	12/11/2018 10:36:07	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT08_ADVICE	12/11/2018 10:36:07	12/11/2018 10:36:07	0s	H Inspection module in automatic START                                                                          SPS
PM1IM	RPIMTTR_INAUTO	12/11/2018 10:36:08	12/11/2018 10:36:22	14s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1IM	IM1CYCL_TIME01	12/11/2018 10:36:11	12/11/2018 10:36:22	11s	Misc Warning IM1CYCL_TIME01 IM cycle time exceeded ideal time                                               warning
PM1IM	IOSFA49_0737S0	12/11/2018 10:36:22	12/11/2018 10:36:33	11s	Electric IOSFA49_0737S0 Infeed/Outfeed Station - Outfeed stopper 1 CT not moved out. (PLC: 297)         fault
PM1IM	OM1CT07_ADVICE	12/11/2018 10:36:22	12/11/2018 10:36:22	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT05_ADVICE	12/11/2018 10:36:29	12/11/2018 10:36:29	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	BA1SA01_TR0x56	12/11/2018 10:36:33	12/11/2018 10:36:57	24s	Mechanic Warning BA1SA01_TR0x56 PM1_Safety Door (571S4) IM1 SD-156                                              open
PM1IM	IM1SACR_STx161	12/11/2018 10:36:33	12/11/2018 10:36:57	24s	Electric Warning IM1SACR_STx161 PM1_One of Safety Doors IM1 Open                                                warning
PM1IM	CB1CB02_ADVICE	12/11/2018 10:36:33	12/11/2018 10:36:58	25s	Misc Warning CB1CB02_ADVICE Compressed air complete line missing (PLC: 72)                                  warning
PM1IM	CB1FA50_ADVICE	12/11/2018 10:36:58	12/11/2018 10:37:03	5s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM1IM	CPPFA88_ADVICE	12/11/2018 10:36:58	12/11/2018 10:37:02	4s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:36:58	12/11/2018 10:37:00	2s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:36:58	12/11/2018 10:37:09	11s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:36:58	12/11/2018 10:37:00	2s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:36:58	12/11/2018 10:37:00	2s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:37:00	12/11/2018 10:37:02	2s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	OM1CT07_ADVICE	12/11/2018 10:37:08	12/11/2018 10:37:08	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	IM1CYCL_TIME01	12/11/2018 10:37:09	12/11/2018 10:37:27	18s	Misc Warning IM1CYCL_TIME01 IM cycle time exceeded ideal time                                               warning
PM1IM	RPIMTTR_INAUTO	12/11/2018 10:37:09	12/11/2018 10:37:27	18s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1IM	OM1CT08_ADVICE	12/11/2018 10:37:09	12/11/2018 10:37:09	0s	H Inspection module in automatic START                                                                          SPS
PM1IM	RPIMTTR_INPROD	12/11/2018 10:37:18	12/11/2018 10:37:27	9s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	OM1CT07_ADVICE	12/11/2018 10:37:27	12/11/2018 10:37:27	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	IM1CYCL_TIME01	12/11/2018 10:37:36	12/11/2018 10:37:39	3s	Misc Warning IM1CYCL_TIME01 IM cycle time exceeded ideal time                                               warning
PM1IM	RPIMTTR_INAUTO	12/11/2018 10:37:36	12/11/2018 10:37:39	3s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1IM	RPIMTTR_INPROD	12/11/2018 10:37:36	12/11/2018 10:37:39	3s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	OM1CT08_ADVICE	12/11/2018 10:37:36	12/11/2018 10:37:36	0s	H Inspection module in automatic START                                                                          SPS
PM1IM	CTPFA23_ADVICE	12/11/2018 10:37:39	12/11/2018 10:37:47	8s	Electric CTPFA23_ADVICE CT Transport - Error in shift register! Please unload line. (PLC: 145)          fault
PM1IM	OM1CT07_ADVICE	12/11/2018 10:37:39	12/11/2018 10:37:39	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT05_ADVICE	12/11/2018 10:37:45	12/11/2018 10:37:45	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	BA1SA01_TR0x59	12/11/2018 10:37:47	12/11/2018 10:38:01	14s	Mechanic Warning BA1SA01_TR0x59 PM1_Safety Door (573S1) IM1 SD-159                                              open
PM1IM	IM1SACR_STx161	12/11/2018 10:37:47	12/11/2018 10:38:01	14s	Electric Warning IM1SACR_STx161 PM1_One of Safety Doors IM1 Open                                                warning
PM1IM	CB1CB02_ADVICE	12/11/2018 10:37:48	12/11/2018 10:38:02	14s	Misc Warning CB1CB02_ADVICE Compressed air complete line missing (PLC: 72)                                  warning
PM1IM	CPPFA88_ADVICE	12/11/2018 10:38:03	12/11/2018 10:38:07	4s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:38:03	12/11/2018 10:38:05	2s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:38:03	12/11/2018 10:38:11	8s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:38:03	12/11/2018 10:38:05	2s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:38:03	12/11/2018 10:38:05	2s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	CB1FA50_ADVICE	12/11/2018 10:38:03	12/11/2018 10:41:36	3m33s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM1IM	CPPFA98_ADVICE	12/11/2018 10:38:05	12/11/2018 10:38:05	0s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA69_ADVICE	12/11/2018 10:38:05	12/11/2018 10:38:05	0s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:38:05	12/11/2018 10:38:05	0s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:38:06	12/11/2018 10:38:07	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	CPPFA88_ADVICE	12/11/2018 10:38:20	12/11/2018 10:38:25	5s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:38:20	12/11/2018 10:38:23	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:38:20	12/11/2018 10:38:40	20s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:38:20	12/11/2018 10:38:23	3s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:38:20	12/11/2018 10:38:23	3s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CPPFA98_ADVICE	12/11/2018 10:38:22	12/11/2018 10:38:23	1s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA69_ADVICE	12/11/2018 10:38:22	12/11/2018 10:38:23	1s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:38:22	12/11/2018 10:38:23	1s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:38:23	12/11/2018 10:38:24	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:38:41	12/11/2018 10:38:44	3s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CPPFA88_ADVICE	12/11/2018 10:38:41	12/11/2018 10:38:45	4s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:38:41	12/11/2018 10:38:44	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:38:41	12/11/2018 10:38:51	10s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:38:41	12/11/2018 10:38:44	3s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA69_ADVICE	12/11/2018 10:38:43	12/11/2018 10:38:44	1s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:38:43	12/11/2018 10:38:44	1s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA98_ADVICE	12/11/2018 10:38:43	12/11/2018 10:38:44	1s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:38:44	12/11/2018 10:38:45	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	CPPFA88_ADVICE	12/11/2018 10:40:36	12/11/2018 10:40:40	4s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:40:36	12/11/2018 10:40:39	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:40:36	12/11/2018 10:40:46	10s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:40:36	12/11/2018 10:40:39	3s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:40:36	12/11/2018 10:40:39	3s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CPPFA98_ADVICE	12/11/2018 10:40:38	12/11/2018 10:40:39	1s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA69_ADVICE	12/11/2018 10:40:38	12/11/2018 10:40:39	1s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:40:38	12/11/2018 10:40:39	1s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:40:39	12/11/2018 10:40:40	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	OM1CT07_ADVICE	12/11/2018 10:41:00	12/11/2018 10:41:00	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT05_ADVICE	12/11/2018 10:41:11	12/11/2018 10:41:11	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	IVSFA77_ADVICE	12/11/2018 10:41:13	12/11/2018 10:41:16	3s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CPPFA88_ADVICE	12/11/2018 10:41:13	12/11/2018 10:41:18	5s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:41:13	12/11/2018 10:41:16	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:41:13	12/11/2018 10:41:23	10s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:41:13	12/11/2018 10:41:16	3s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	CPPFA98_ADVICE	12/11/2018 10:41:16	12/11/2018 10:41:16	0s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA69_ADVICE	12/11/2018 10:41:16	12/11/2018 10:41:16	0s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:41:16	12/11/2018 10:41:16	0s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:41:17	12/11/2018 10:41:18	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	IS1ES01_NA0x07	12/11/2018 10:41:36	12/11/2018 10:43:37	2m1s	Electric IS1ES01_NA0x07 PM1_Estop Robot on IM1                                                          activated
PM1IM	PM1ESCR_ESPMX1	12/11/2018 10:41:36	12/11/2018 10:43:37	2m1s	Electric Warning PM1ESCR_ESPMX1 PM1_Estop Circuit IM1 Actuated                                                  warning
PM1IM	CB1CB01_ADVICE	12/11/2018 10:41:36	12/11/2018 10:43:55	2m19s	Misc Warning CB1CB01_ADVICE Control voltage is switched off! (PLC: 3)                                       warning
PM1IM	IM1SACR_STx161	12/11/2018 10:41:37	12/11/2018 10:43:55	2m18s	Electric Warning IM1SACR_STx161 PM1_One of Safety Doors IM1 Open                                                warning
PM1IM	BA1SA01_TR0x54	12/11/2018 10:43:39	12/11/2018 10:43:54	15s	Mechanic Warning BA1SA01_TR0x54 PM1_Safety Door (582S8) IM1 SD-154                                              open
PM1IM	CB1CB02_ADVICE	12/11/2018 10:43:55	12/11/2018 10:43:56	1s	Misc Warning CB1CB02_ADVICE Compressed air complete line missing (PLC: 72)                                  warning
PM1IM	CB1FA50_ADVICE	12/11/2018 10:43:56	12/11/2018 10:48:53	4m57s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM1IM	EM1EM01_ADVICE	12/11/2018 10:43:56	12/11/2018 10:44:20	24s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	CPPFA88_ADVICE	12/11/2018 10:44:03	12/11/2018 10:44:07	4s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:44:03	12/11/2018 10:44:06	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:44:03	12/11/2018 10:44:14	11s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:44:03	12/11/2018 10:44:06	3s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:44:03	12/11/2018 10:44:06	3s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CPPFA98_ADVICE	12/11/2018 10:44:05	12/11/2018 10:44:06	1s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA69_ADVICE	12/11/2018 10:44:05	12/11/2018 10:44:06	1s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:44:05	12/11/2018 10:44:06	1s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:44:06	12/11/2018 10:44:07	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	OM1CT07_ADVICE	12/11/2018 10:45:23	12/11/2018 10:45:23	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT05_ADVICE	12/11/2018 10:45:57	12/11/2018 10:45:57	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	CPPFA88_ADVICE	12/11/2018 10:46:01	12/11/2018 10:46:05	4s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:46:01	12/11/2018 10:46:04	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:46:01	12/11/2018 10:46:12	11s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:46:01	12/11/2018 10:46:04	3s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:46:01	12/11/2018 10:46:04	3s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CPPFA98_ADVICE	12/11/2018 10:46:03	12/11/2018 10:46:04	1s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA69_ADVICE	12/11/2018 10:46:03	12/11/2018 10:46:04	1s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:46:03	12/11/2018 10:46:04	1s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:46:04	12/11/2018 10:46:05	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	CPPFA88_ADVICE	12/11/2018 10:46:51	12/11/2018 10:46:55	4s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:46:51	12/11/2018 10:46:54	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:46:51	12/11/2018 10:48:47	1m56s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:46:51	12/11/2018 10:46:54	3s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:46:51	12/11/2018 10:46:54	3s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	CPPFA98_ADVICE	12/11/2018 10:46:53	12/11/2018 10:46:54	1s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA69_ADVICE	12/11/2018 10:46:53	12/11/2018 10:46:54	1s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:46:53	12/11/2018 10:46:54	1s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:46:54	12/11/2018 10:46:55	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	OM1CT07_ADVICE	12/11/2018 10:48:44	12/11/2018 10:48:44	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT05_ADVICE	12/11/2018 10:48:47	12/11/2018 10:48:47	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	CPPFA88_ADVICE	12/11/2018 10:48:49	12/11/2018 10:48:54	5s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 10:48:49	12/11/2018 10:48:52	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 10:48:49	12/11/2018 10:48:59	10s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 10:48:49	12/11/2018 10:48:52	3s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 10:48:49	12/11/2018 10:48:52	3s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CPPFA98_ADVICE	12/11/2018 10:48:52	12/11/2018 10:48:52	0s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA78_ADVICE	12/11/2018 10:48:52	12/11/2018 10:48:52	0s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 10:48:52	12/11/2018 10:48:53	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	OM1CT07_ADVICE	12/11/2018 10:48:59	12/11/2018 10:48:59	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	RPIMTTR_INPROD	12/11/2018 10:49:01	12/11/2018 10:51:37	2m36s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	OM1CT08_ADVICE	12/11/2018 10:49:01	12/11/2018 10:49:01	0s	H Inspection module in automatic START                                                                          SPS
PM1IM	IM1CYCL_TIME01	12/11/2018 10:49:01	12/11/2018 11:33:26	44m25s	Misc Warning IM1CYCL_TIME01 IM cycle time exceeded ideal time                                               warning
PM1IM	RPIMTTR_INAUTO	12/11/2018 10:49:01	12/11/2018 11:33:26	44m25s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1IM	RPIMTTR_INPROD	12/11/2018 10:52:20	12/11/2018 10:52:30	10s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 10:54:07	12/11/2018 10:54:20	13s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 10:55:46	12/11/2018 10:56:00	14s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 10:56:15	12/11/2018 10:56:30	15s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 11:02:24	12/11/2018 11:02:38	14s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 11:03:16	12/11/2018 11:03:30	14s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 11:03:50	12/11/2018 11:04:04	14s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 11:06:53	12/11/2018 11:31:23	24m30s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 11:32:25	12/11/2018 11:32:34	9s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 11:32:58	12/11/2018 11:33:08	10s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	OM1CT07_ADVICE	12/11/2018 11:33:26	12/11/2018 11:33:26	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	RPIMTTR_FAULTD	12/11/2018 11:33:26	12/11/2018 11:45:03	11m37s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM1IM	CPPCR01_SK0021	12/11/2018 11:33:26	12/11/2018 11:34:05	39s	Mechanic CPPCR01_SK0021 Seq. control CT Pick and Place                                                  fault
PM1IM	RPIMTTR_OPRESP	12/11/2018 11:33:26	12/11/2018 11:34:05	39s	Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning
PM1IM	IOSFA56_TMLOCK	12/11/2018 11:33:34	12/11/2018 11:34:18	44s	Electric IOSFA56_TMLOCK Infeed/Outfeed Station - Disturbed sequence with transfer module. Check TM!!! (Pfault
PM1IM	RPIMTTR_RECOVE	12/11/2018 11:34:05	12/11/2018 11:45:03	10m58s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM1IM	OM1CT05_ADVICE	12/11/2018 11:34:06	12/11/2018 11:34:06	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	CB1FA50_ADVICE	12/11/2018 11:34:07	12/11/2018 11:34:14	7s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM1IM	CPPFA88_ADVICE	12/11/2018 11:34:07	12/11/2018 11:34:12	5s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 11:34:07	12/11/2018 11:34:10	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 11:34:07	12/11/2018 11:34:18	11s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 11:34:07	12/11/2018 11:34:57	50s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 11:34:07	12/11/2018 11:34:58	51s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CPPFA98_ADVICE	12/11/2018 11:34:10	12/11/2018 11:34:10	0s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA69_ADVICE	12/11/2018 11:34:10	12/11/2018 11:34:20	10s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 11:34:10	12/11/2018 11:34:20	10s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 11:34:11	12/11/2018 11:34:12	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	OM1CT07_ADVICE	12/11/2018 11:34:17	12/11/2018 11:34:17	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	IVSFA26_ADVICE	12/11/2018 11:34:17	12/11/2018 11:34:18	1s	Electric IVSFA26_ADVICE Inspection Vision System - Inspection camera not in home position! (PLC: 162)   fault
PM1IM	CTPFA24_ADVICE	12/11/2018 11:34:19	12/11/2018 11:34:19	0s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA26_ADVICE	12/11/2018 11:34:19	12/11/2018 11:34:19	0s	Electric IVSFA26_ADVICE Inspection Vision System - Inspection camera not in home position! (PLC: 162)   fault
PM1IM	CTPFA24_ADVICE	12/11/2018 11:34:19	12/11/2018 11:34:20	1s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA26_ADVICE	12/11/2018 11:34:19	12/11/2018 11:34:20	1s	Electric IVSFA26_ADVICE Inspection Vision System - Inspection camera not in home position! (PLC: 162)   fault
PM1IM	OM1CT05_ADVICE	12/11/2018 11:34:20	12/11/2018 11:34:20	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	CTPFA24_ADVICE	12/11/2018 11:34:20	12/11/2018 11:34:42	22s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSCR01_SK0014	12/11/2018 11:34:40	12/11/2018 11:34:42	2s	Mechanic IVSCR01_SK0014 Seq. control home position Inspection Vision System                             fault
PM1IM	CB1FA50_ADVICE	12/11/2018 11:34:42	12/11/2018 11:34:54	12s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM1IM	CPPFA88_ADVICE	12/11/2018 11:34:42	12/11/2018 11:34:54	12s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 11:34:42	12/11/2018 11:34:52	10s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 11:34:42	12/11/2018 11:34:59	17s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	CPPFA98_ADVICE	12/11/2018 11:34:52	12/11/2018 11:34:52	0s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	IVSFA69_ADVICE	12/11/2018 11:34:52	12/11/2018 11:34:57	5s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 11:34:52	12/11/2018 11:34:57	5s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	CPPFA89_ADVICE	12/11/2018 11:34:53	12/11/2018 11:34:54	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	CTPFA24_ADVICE	12/11/2018 11:34:59	12/11/2018 11:35:03	4s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	OM1CT07_ADVICE	12/11/2018 11:35:02	12/11/2018 11:35:02	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT08_ADVICE	12/11/2018 11:35:03	12/11/2018 11:35:03	0s	H Inspection module in automatic START                                                                          SPS
PM1IM	RPIMTTR_INPROD	12/11/2018 11:35:03	12/11/2018 12:36:41	1h1m38s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	IM1CYCL_TIME01	12/11/2018 11:35:03	12/11/2018 17:24:10	5h49m7s	Misc Warning IM1CYCL_TIME01 IM cycle time exceeded ideal time                                               warning
PM1IM	RPIMTTR_INAUTO	12/11/2018 11:35:03	12/11/2018 17:24:10	5h49m7s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1IM	RPIMTTR_INPROD	12/11/2018 12:37:14	12/11/2018 12:40:19	3m5s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 12:55:51	12/11/2018 13:01:19	5m28s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 13:01:41	12/11/2018 13:02:19	38s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 13:02:25	12/11/2018 13:04:00	1m35s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 13:04:28	12/11/2018 13:06:56	2m28s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 13:56:14	12/11/2018 13:57:43	1m29s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 15:10:34	12/11/2018 17:24:10	2h13m36s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 15:49:50	12/11/2018 15:54:32	4m42s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	OM1CT07_ADVICE	12/11/2018 17:24:09	12/11/2018 17:24:09	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT05_ADVICE	12/11/2018 17:24:17	12/11/2018 17:24:17	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	BA1SA01_TR0x54	12/11/2018 17:24:19	12/11/2018 17:24:46	27s	Mechanic Warning BA1SA01_TR0x54 PM1_Safety Door (582S8) IM1 SD-154                                              open
PM1IM	IM1SACR_STx161	12/11/2018 17:24:19	12/11/2018 17:24:46	27s	Electric Warning IM1SACR_STx161 PM1_One of Safety Doors IM1 Open                                                warning
PM1IM	CB1CB02_ADVICE	12/11/2018 17:24:20	12/11/2018 17:24:47	27s	Misc Warning CB1CB02_ADVICE Compressed air complete line missing (PLC: 72)                                  warning
PM1IM	OM1CT07_ADVICE	12/11/2018 17:24:46	12/11/2018 17:24:46	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	IVSFA03_ADVICE	12/11/2018 17:24:47	12/11/2018 17:24:53	6s	Electric IVSFA03_ADVICE Inspection Vision System - Nc axis camera servo error! (PLC: 357)               fault
PM1IM	OM1CT05_ADVICE	12/11/2018 17:24:51	12/11/2018 17:24:51	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	CPPFA88_ADVICE	12/11/2018 17:24:52	12/11/2018 17:24:57	5s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM1IM	CPPFA97_ADVICE	12/11/2018 17:24:52	12/11/2018 17:24:56	4s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM1IM	CTPFA24_ADVICE	12/11/2018 17:24:52	12/11/2018 17:25:01	9s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM1IM	IVSFA68_ADVICE	12/11/2018 17:24:52	12/11/2018 17:24:57	5s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM1IM	IVSFA77_ADVICE	12/11/2018 17:24:52	12/11/2018 17:24:57	5s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM1IM	CB1FA50_ADVICE	12/11/2018 17:24:52	12/11/2018 17:24:58	6s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM1IM	CPPFA98_ADVICE	12/11/2018 17:24:55	12/11/2018 17:24:56	1s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM1IM	CPPFA89_ADVICE	12/11/2018 17:24:56	12/11/2018 17:24:57	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM1IM	IVSFA69_ADVICE	12/11/2018 17:24:56	12/11/2018 17:24:57	1s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM1IM	IVSFA78_ADVICE	12/11/2018 17:24:56	12/11/2018 17:24:57	1s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM1IM	OM1CT07_ADVICE	12/11/2018 17:25:02	12/11/2018 17:25:02	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT08_ADVICE	12/11/2018 17:25:02	12/11/2018 17:25:02	0s	H Inspection module in automatic START                                                                          SPS
PM1IM	IM1CYCL_TIME01	12/11/2018 17:25:03	12/11/2018 17:35:11	10m8s	Misc Warning IM1CYCL_TIME01 IM cycle time exceeded ideal time                                               warning
PM1IM	RPIMTTR_INAUTO	12/11/2018 17:25:03	12/11/2018 17:35:11	10m8s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1IM	RPIMTTR_INPROD	12/11/2018 17:25:03	12/11/2018 17:35:11	10m8s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	OM1CT07_ADVICE	12/11/2018 17:35:11	12/11/2018 17:35:11	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT05_ADVICE	12/11/2018 17:35:21	12/11/2018 17:35:21	0s	H Inspection module in setup mode                                                                               SPS
PM1IM	BA1SA01_TR0x54	12/11/2018 17:35:24	12/11/2018 17:35:37	13s	Mechanic Warning BA1SA01_TR0x54 PM1_Safety Door (582S8) IM1 SD-154                                              open
PM1IM	IM1SACR_STx161	12/11/2018 17:35:24	12/11/2018 17:35:37	13s	Electric Warning IM1SACR_STx161 PM1_One of Safety Doors IM1 Open                                                warning
PM1IM	CB1CB02_ADVICE	12/11/2018 17:35:24	12/11/2018 17:35:38	14s	Misc Warning CB1CB02_ADVICE Compressed air complete line missing (PLC: 72)                                  warning
PM1IM	OM1CT07_ADVICE	12/11/2018 17:35:38	12/11/2018 17:35:38	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT08_ADVICE	12/11/2018 17:35:39	12/11/2018 17:35:39	0s	H Inspection module in automatic START                                                                          SPS
PM1IM	RPIMTTR_INPROD	12/11/2018 17:35:39	12/11/2018 21:24:06	3h48m27s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 17:47:39	12/11/2018 17:48:17	38s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 17:48:23	12/11/2018 17:49:30	1m7s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 19:41:51	12/11/2018 19:42:33	42s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 19:42:39	12/11/2018 19:47:30	4m51s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 19:47:37	12/11/2018 19:49:00	1m23s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	BV1MRI1_FKT001	12/11/2018 19:53:40	12/11/2018 19:53:40	0s	MRI START START NARTEIS1
PM1IM	RPIMTTR_INPROD	12/11/2018 21:24:24	12/11/2018 21:25:12	48s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	RPIMTTR_INPROD	12/11/2018 21:28:24	12/12/2018 01:06:45	3h38m21s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	EM1EM01_ADVICE	12/11/2018 23:20:58	12/11/2018 23:25:41	4m43s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	OM1CT07_ADVICE	12/12/2018 01:10:21	12/12/2018 01:10:21	0s	H Inspection module in automatic mode                                                                           SPS
PM1IM	OM1CT08_ADVICE	12/12/2018 01:12:29	12/12/2018 01:12:29	0s	H Inspection module in automatic START                                                                          SPS
PM1IM	RPIMTTR_INAUTO	12/12/2018 01:12:29	12/12/2018 06:58:08	5h45m39s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1IM	IM1CYCL_TIME01	12/12/2018 01:12:31	12/12/2018 06:58:08	5h45m37s	Misc Warning IM1CYCL_TIME01 IM cycle time exceeded ideal time                                               warning
PM1IM	EM1EM01_ADVICE	12/12/2018 01:37:05	12/12/2018 01:41:29	4m24s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 01:41:41	12/12/2018 01:44:39	2m58s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	RPIMTTR_INPROD	12/12/2018 02:13:29	12/12/2018 03:56:17	1h42m48s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 02:22:13	12/12/2018 02:24:42	2m29s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 03:02:25	12/12/2018 03:03:44	1m19s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 03:03:50	12/12/2018 03:04:03	13s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 03:04:09	12/12/2018 03:05:21	1m12s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 03:05:27	12/12/2018 03:06:34	1m7s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 03:06:48	12/12/2018 03:09:52	3m4s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	AV1AV11_ADVICE	12/12/2018 03:52:19	12/12/2018 03:52:19	0s	H Lot end IM                                                                                                    SPS
PM1IM	RPIMTTR_INPROD	12/12/2018 03:56:52	12/12/2018 06:58:08	3h1m16s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 04:07:07	12/12/2018 04:07:50	43s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 04:07:56	12/12/2018 04:08:57	1m1s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 05:43:22	12/12/2018 05:44:55	1m33s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 05:45:01	12/12/2018 05:45:23	22s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM1IM	EM1EM01_ADVICE	12/12/2018 05:45:29	12/12/2018 05:51:20	5m51s	Misc Warning EM1EM01_ADVICE EM is not ready! (PLC: 436)                                                     warning
PM2IM	CB1FA50_ADVICE	12/11/2018 05:55:17	12/11/2018 05:55:23	6s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM2IM	ISTCY02_0855Y8	12/11/2018 06:58:56	12/11/2018 07:00:17	1m21s	Mechanic ISTCY02_0855Y8 Inverting Station - Drip pan cylinder 2                                         not in operation position
PM2IM	ISTCR01_SK0017	12/11/2018 06:58:56	12/11/2018 07:00:10	1m13s	Mechanic ISTCR01_SK0017 Seq. control Inverting Station                                                  fault
PM2IM	RPIMTTR_OPRESP	12/11/2018 06:58:56	12/11/2018 07:00:10	1m14s	Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning
PM2IM	RPIMTTR_FAULTD	12/11/2018 06:58:56	12/11/2018 07:11:41	12m45s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM2IM	RPIMTTR_RECOVE	12/11/2018 07:00:10	12/11/2018 07:11:41	11m31s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM2IM	OM1CT05_ADVICE	12/11/2018 07:00:12	12/11/2018 07:00:12	0s	H Inspection module in setup mode                                                                               SPS
PM2IM	CB1FA50_ADVICE	12/11/2018 07:00:17	12/11/2018 07:00:23	6s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM2IM	IM1CYCL_TIME01	12/11/2018 07:01:41	12/11/2018 07:19:32	17m51s	Misc Warning IM1CYCL_TIME01 IM cycle time exceeded ideal time                                               warning
PM2IM	RPIMTTR_INAUTO	12/11/2018 07:01:41	12/11/2018 07:19:32	17m51s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM2IM	RPIMTTR_INPROD	12/11/2018 07:01:41	12/11/2018 07:19:15	17m34s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM2IM	CPPCR01_SK0021	12/11/2018 07:19:32	12/11/2018 07:21:12	1m40s	Mechanic CPPCR01_SK0021 Seq. control CT Pick and Place                                                  fault
PM2IM	RPIMTTR_OPRESP	12/11/2018 07:19:32	12/11/2018 07:21:11	1m39s	Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning
PM2IM	RPIMTTR_FAULTD	12/11/2018 07:19:32	12/11/2018 07:31:30	11m58s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM2IM	IOSFA56_TMLOCK	12/11/2018 07:19:38	12/11/2018 07:21:12	1m34s	Electric IOSFA56_TMLOCK Infeed/Outfeed Station - Disturbed sequence with transfer module. Check TM!!! (Pfault
PM2IM	IOSFA44_Sensor	12/11/2018 07:20:20	12/11/2018 07:21:12	52s	Misc IOSFA44_Sensor Infeed/Outfeed Station-CT missing. Look for sensors 739BG0/1/3/4/736BG0/737BG0/3fault
PM2IM	RPIMTTR_RECOVE	12/11/2018 07:21:11	12/11/2018 07:31:30	10m19s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM2IM	CB1FA50_ADVICE	12/11/2018 07:21:13	12/11/2018 07:21:17	4s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM2IM	CPPFA88_ADVICE	12/11/2018 07:21:13	12/11/2018 07:21:18	5s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM2IM	CPPFA97_ADVICE	12/11/2018 07:21:13	12/11/2018 07:21:16	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM2IM	CTPFA24_ADVICE	12/11/2018 07:21:13	12/11/2018 07:21:28	15s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM2IM	IVSFA68_ADVICE	12/11/2018 07:21:13	12/11/2018 07:21:16	3s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM2IM	IVSFA77_ADVICE	12/11/2018 07:21:13	12/11/2018 07:21:16	3s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM2IM	CPPFA98_ADVICE	12/11/2018 07:21:15	12/11/2018 07:21:16	1s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM2IM	IVSFA69_ADVICE	12/11/2018 07:21:15	12/11/2018 07:21:16	1s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM2IM	IVSFA78_ADVICE	12/11/2018 07:21:15	12/11/2018 07:21:16	1s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM2IM	CPPFA89_ADVICE	12/11/2018 07:21:16	12/11/2018 07:21:17	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM2IM	RPIMTTR_INPROD	12/11/2018 07:21:30	12/11/2018 07:21:47	17s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM2IM	RPIMTTR_INPROD	12/11/2018 07:22:31	12/11/2018 07:23:00	29s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM2IM	RPIMTTR_INPROD	12/11/2018 07:23:13	12/11/2018 07:33:45	10m32s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM2IM	RPIMTTR_INPROD	12/11/2018 07:38:31	12/11/2018 08:10:58	32m27s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM2IM	CPPCR01_SK0021	12/11/2018 14:43:27	12/11/2018 14:43:53	26s	Mechanic CPPCR01_SK0021 Seq. control CT Pick and Place                                                  fault
PM2IM	RPIMTTR_FAULTD	12/11/2018 14:43:27	12/11/2018 14:54:19	10m52s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM2IM	RPIMTTR_OPRESP	12/11/2018 14:43:27	12/11/2018 14:43:52	25s	Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning
PM2IM	OM1CT07_ADVICE	12/11/2018 14:43:27	12/11/2018 14:43:27	0s	H Inspection module in automatic mode                                                                           SPS
PM2IM	IOSFA56_TMLOCK	12/11/2018 14:43:34	12/11/2018 14:44:17	43s	Electric IOSFA56_TMLOCK Infeed/Outfeed Station - Disturbed sequence with transfer module. Check TM!!! (Pfault
PM2IM	RPIMTTR_RECOVE	12/11/2018 14:43:52	12/11/2018 14:54:19	10m27s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM2IM	OM1CT05_ADVICE	12/11/2018 14:43:52	12/11/2018 14:43:52	0s	H Inspection module in setup mode                                                                               SPS
PM2IM	CB1FA50_ADVICE	12/11/2018 14:43:54	12/11/2018 14:44:00	6s	Electric CB1FA50_ADVICE Lens Transfer Robot - NC axis not calibrated (PLC: 562)                         fault
PM2IM	CPPFA97_ADVICE	12/11/2018 14:43:54	12/11/2018 14:43:57	3s	Electric CPPFA97_ADVICE CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)                 fault
PM2IM	CTPFA24_ADVICE	12/11/2018 14:43:54	12/11/2018 14:44:17	23s	Electric CTPFA24_ADVICE CT Transport - Not in home position! (PLC: 139)                                 fault
PM2IM	IVSFA68_ADVICE	12/11/2018 14:43:54	12/11/2018 14:44:12	18s	Electric IVSFA68_ADVICE Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)            fault
PM2IM	IVSFA77_ADVICE	12/11/2018 14:43:54	12/11/2018 14:44:12	18s	Electric IVSFA77_ADVICE Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)      fault
PM2IM	CPPFA88_ADVICE	12/11/2018 14:43:54	12/11/2018 14:43:59	5s	Electric CPPFA88_ADVICE CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)               fault
PM2IM	CPPFA98_ADVICE	12/11/2018 14:43:56	12/11/2018 14:43:57	1s	Electric CPPFA98_ADVICE CT Pick and Place - Vertical NC axis reference drive (PLC: 395)                 fault
PM2IM	IVSFA69_ADVICE	12/11/2018 14:43:56	12/11/2018 14:44:11	15s	Electric IVSFA69_ADVICE Inspection Vision System - Camera NC axis reference drive (PLC: 347)            fault
PM2IM	IVSFA78_ADVICE	12/11/2018 14:43:56	12/11/2018 14:44:12	16s	Electric IVSFA78_ADVICE Inspection Vision System - Camera light NC axis reference drive (PLC: 363)      fault
PM2IM	CPPFA89_ADVICE	12/11/2018 14:43:57	12/11/2018 14:43:58	1s	Electric CPPFA89_ADVICE CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)               fault
PM8IM	VFSFA20_511TF2	12/12/2018 12:36:32	12/12/2018 10:36:38	6s	-- Buffer Fault! --
PM8IM	VFSFA20_511TF2	12/12/2018 12:36:32	12/12/2018 12:36:38	6s	Vacuum/Fill Station - Vacuum error pos. 3 (PLC: 111)
PM8IM	RPIMTTR_INPROD	12/12/2018 12:36:45	12/12/2018 12:52:07	15m22s	MTTR: Machine is Productive
PM8IM	RPIMTTR_RECOVE	12/12/2018 12:52:07	12/12/2018 13:02:46	10m39s	MTTR: Recovery State
PM8IM	RPIMTTR_FAULTD	12/12/2018 12:52:07	12/12/2018 13:02:46	10m39s	MTTR: Machine in Faulted State (Downtime Tracked)
PM8IM	SC1SD0_9014FZ7	12/12/2018 12:52:07	12/12/2018 12:52:09	2s	Safety Door 8 (IOS)
PM8IM	OM1CT07_ADVICE	12/12/2018 12:52:07	12/12/2018 12:52:26	19s	Inspection module in automatic mode
PM8IM	LTRFA18_ADVICE	12/12/2018 12:52:07	12/12/2018 12:52:31	24s	Lens Transfer Robot - Not in home position! (PLC: 81)
PM8IM	CC1CT07_ADVICE	12/12/2018 12:52:07	12/12/2018 12:52:27	20s	Inspection module in automatic mode
PM8IM	CC1CT05_ADVICE	12/12/2018 12:52:26	12/12/2018 12:52:45	19s	Inspection module in setup mode
PM8IM	IVSFA77_ADVICE	12/12/2018 12:52:33	12/12/2018 12:52:36	3s	Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)
PM8IM	CPPFA97_ADVICE	12/12/2018 12:52:33	12/12/2018 12:52:36	3s	CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)
PM8IM	CB1FA50_ADVICE	12/12/2018 12:52:33	12/12/2018 12:52:40	7s	Lens Transfer Robot - NC axis not calibrated (PLC: 562)
");

			// IM Special Case
			var e = Utility.BuildEvent(events, 37 - 32);
			Assert.AreEqual("Misc Warning CB1CB01_ADVICE Control voltage is switched off! (PLC: 3)                                       warning", e.Message);
			Assert.AreEqual("CB1CB01_ADVICE", e.Tag);
			// IM Normal Case
			e = Utility.BuildEvent(events, 226 - 32);
			Assert.AreEqual("Mechanic CPPCR01_SK0021 Seq. control CT Pick and Place                                                  fault", e.Message);
			Assert.AreEqual("CPPCR01_SK0021", e.Tag);
			// IM text before fault
			e = Utility.BuildEvent(events, 337 - 32);
			Assert.AreEqual("Mechanic ISTCY02_0855Y8 Inverting Station - Drip pan cylinder 2                                         not in operation position", e.Message);
			Assert.AreEqual("ISTCY02_0855Y8", e.Tag);
			// IM text before fault
			e = Utility.BuildEvent(events, 346 - 32);
			Assert.AreEqual("Mechanic CPPCR01_SK0021 Seq. control CT Pick and Place                                                  fault", e.Message);
			Assert.AreEqual("CPPCR01_SK0021", e.Tag);
			// IM text before fault
			e = Utility.BuildEvent(events, 365 - 32);
			Assert.AreEqual("Electric IOSFA56_TMLOCK Infeed/Outfeed Station - Disturbed sequence with transfer module. Check TM!!! (Pfault", e.Message);
			Assert.AreEqual("IOSFA56_TMLOCK", e.Tag);
			// Safety Door Fault (Removed Safety from Warning Check)
			e = Utility.BuildEvent(events, 599 - 246);
			Assert.AreEqual("Safety Door 8 (IOS)", e.Message);
			Assert.AreEqual("SC1SD0_9014FZ7", e.Tag);

			// Camera System Offline (Added to Allowed Warnings)
			events = StringToEvents(@"
PM9IM	RPIMTTR_INPROD	12/12/2018 07:09:10	12/12/2018 07:46:53	37m43s	MTTR: Machine is Productive
PM9IM	CC1CT07_ADVICE	12/12/2018 07:46:53	12/12/2018 07:50:51	3m58s	Inspection module in automatic mode
PM9IM	IVSFA29_ADVICE	12/12/2018 07:46:53	12/12/2018 07:50:57	4m4s	Inspection Vision System - Camera offline! (PLC: 165)
PM9IM	OM1CT07_ADVICE	12/12/2018 07:46:53	12/12/2018 07:50:51	3m58s	Inspection module in automatic mode
PM9IM	RPIMTTR_RECOVE	12/12/2018 07:46:53	12/12/2018 08:11:22	24m29s	MTTR: Recovery State
PM9IM	RPIMTTR_FAULTD	12/12/2018 07:46:53	12/12/2018 08:11:22	24m29s	MTTR: Machine in Faulted State (Downtime Tracked)
PM9IM	IVSFA30_ADVICE	12/12/2018 07:46:55	12/12/2018 07:50:48	3m53s	Inspection Vision System - Faulty data with lot change. (PLC: 166)
PM9IM	IVSFA30_ADVICE	12/12/2018 07:50:50	12/12/2018 07:50:55	5s	Inspection Vision System - Faulty data with lot change. (PLC: 166)
PM9IM	CC1CT05_ADVICE	12/12/2018 07:50:51	12/12/2018 07:51:11	20s	Inspection module in setup mode
PM9IM	IVSFA68_ADVICE	12/12/2018 07:50:57	12/12/2018 07:51:00	3s	Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)
PM9IM	CPPFA88_ADVICE	12/12/2018 07:50:57	12/12/2018 07:51:02	5s	CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)
PM9IM	CB1FA50_ADVICE	12/12/2018 07:50:57	12/12/2018 07:51:01	4s	Lens Transfer Robot - NC axis not calibrated (PLC: 562)
PM9IM	CPPFA97_ADVICE	12/12/2018 07:50:57	12/12/2018 07:51:00	3s	CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)
PM9IM	IVSFA77_ADVICE	12/12/2018 07:50:57	12/12/2018 07:51:00	3s	Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)
PM9IM	IVSFA69_ADVICE	12/12/2018 07:51:00	12/12/2018 07:51:00	0s	Inspection Vision System - Camera NC axis reference drive (PLC: 347)
PM9IM	IVSFA78_ADVICE	12/12/2018 07:51:00	12/12/2018 07:51:00	0s	Inspection Vision System - Camera light NC axis reference drive (PLC: 363)
PM9IM	CPPFA98_ADVICE	12/12/2018 07:51:00	12/12/2018 07:51:00	0s	CT Pick and Place - Vertical NC axis reference drive (PLC: 395)
PM9IM	CPPFA89_ADVICE	12/12/2018 07:51:01	12/12/2018 07:51:02	1s	CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)");
			e = Utility.BuildEvent(events, 5);
			Assert.AreEqual("Inspection Vision System - Camera offline! (PLC: 165)", e.Message);
			Assert.AreEqual("IVSFA29_ADVICE", e.Tag);

			// TODO: This is a special case. Im faults when after 3 conseuctive Vacuum Error pos. X errors, but the message may not go off then come back
			// e.x., "Vacuum/Fill Station - Vacuum error pos. 2 (PLC: 110)" happens first, that's #1
			//       "Vacuum/Fill Station - Vacuum error pos. 1 (PLC: 109)" becomes active, that's #2
			//       "Vacuum/Fill Station - Vacuum error pos. 1 (PLC: 109)" happens again, that's #3 - Line faults, but the message didn't go off then come back on
			//      Need a good way to notice these faults
			events = StringToEvents(@"
PM9IM	VFSFA19_511TF1	12/12/2018 11:57:03	12/12/2018 11:57:10	7s	Vacuum/Fill Station - Vacuum error pos. 2 (PLC: 110)
PM9IM	RPIMTTR_INPROD	12/12/2018 11:57:16	12/12/2018 12:07:06	9m50s	MTTR: Machine is Productive
PM9IM	VFSFA18_511TF1	12/12/2018 12:07:05	12/12/2018 12:14:48	7m43s	Vacuum/Fill Station - Vacuum error pos. 1 (PLC: 109)
PM9IM	OM1CT07_ADVICE	12/12/2018 12:07:20	12/12/2018 12:14:48	7m28s	Inspection module in automatic mode
PM9IM	RPIMTTR_RECOVE	12/12/2018 12:07:20	12/12/2018 12:25:12	17m52s	MTTR: Recovery State
PM9IM	CC1CT07_ADVICE	12/12/2018 12:07:21	12/12/2018 12:14:48	7m27s	Inspection module in automatic mode
PM9IM	RPIMTTR_FAULTD	12/12/2018 12:07:21	12/12/2018 12:25:12	17m51s	MTTR: Machine in Faulted State (Downtime Tracked)
PM9IM	CC1CT05_ADVICE	12/12/2018 12:14:48	12/12/2018 12:15:12	24s	Inspection module in setup mode
PM9IM	IVSFA68_ADVICE	12/12/2018 12:14:49	12/12/2018 12:15:08	19s	Inspection Vision System - Camera NC axis not calibrated! (PLC: 346)
PM9IM	CB1FA50_ADVICE	12/12/2018 12:14:49	12/12/2018 12:14:53	4s	Lens Transfer Robot - NC axis not calibrated (PLC: 562)
PM9IM	CPPFA97_ADVICE	12/12/2018 12:14:49	12/12/2018 12:14:52	3s	CT Pick and Place - Vertical NC axis not calibrated! (PLC: 394)
PM9IM	IVSFA77_ADVICE	12/12/2018 12:14:49	12/12/2018 12:15:08	19s	Inspection Vision System - Camera light NC axis not calibrated! (PLC: 362)
PM9IM	CPPFA88_ADVICE	12/12/2018 12:14:50	12/12/2018 12:14:55	5s	CT Pick and Place - Horizontal NC axis not calibrated! (PLC: 378)
PM9IM	IVSFA78_ADVICE	12/12/2018 12:14:52	12/12/2018 12:15:07	15s	Inspection Vision System - Camera light NC axis reference drive (PLC: 363)
PM9IM	IVSFA69_ADVICE	12/12/2018 12:14:52	12/12/2018 12:15:08	16s	Inspection Vision System - Camera NC axis reference drive (PLC: 347)
PM9IM	CPPFA89_ADVICE	12/12/2018 12:14:53	12/12/2018 12:14:55	2s	CT Pick and Place - Horizontal NC axis reference drive (PLC: 379)
PM9IM	CC1CT07_ADVICE	12/12/2018 12:15:12	12/12/2018 12:15:13	1s	Inspection module in automatic mode
PM9IM	OM1CT07_ADVICE	12/12/2018 12:15:12	12/12/2018 12:15:13	1s	Inspection module in automatic mode
PM9IM	RPIMTTR_INAUTO	12/12/2018 12:15:12	12/12/2018 15:55:13	3h40m1s	MTTR: Machine is Cycling");
			//e = Utility.BuildEvent(events, 6);
			//Assert.AreEqual("Vacuum/Fill Station - Vacuum error pos. 1 (PLC: 109)", e.Message);
			//Assert.AreEqual("VFSFA18_511TF1", e.Tag);
		}

		[TestMethod]
		public void Test_FindFault_TM() {
			var events = StringToEvents(@"
PM1TM	CB1FA01_VakBeh	12/12/2018 07:42:54	12/12/2018 07:43:00	6s	Electric Warning CB1FA01_VakBeh Knock-out vessel is emptied (PLC: 0014)                                         warning
PM1TM	CS1CY01_8605Y8	12/12/2018 07:43:01	12/12/2018 07:43:01	0s	Cam Stopper Track 1 - Turning cylinder selection HICKSJA5
PM1TM	CS1FA02_RotStp	12/12/2018 07:43:03	12/12/2018 07:43:13	10s	Electric CS1FA02_RotStp Cam Stopper Track 1 - Movement excecuted in Set Up mode. CT may be in wrong posifault
PM1TM	CS1CY01_8605Y8	12/12/2018 07:43:16	12/12/2018 07:43:16	0s	Cam Stopper Track 1 - Turning cylinder deselection HICKSJA5
PM1TM	RPIMTTR_FAULTD	12/12/2018 08:14:47	12/12/2018 08:43:35	28m48s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM1TM	RPIMTTR_RECOVE	12/12/2018 08:15:35	12/12/2018 08:43:35	28m	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM1TM	SU1SA01_TR0x76	12/12/2018 08:23:26	12/12/2018 08:28:27	5m1s	Mechanic Warning SU1SA01_TR0x76 PM1_Safety Door (592S4) TM1 SD-176                                              open
PM1TM	TM1SACR_SATMX1	12/12/2018 08:23:26	12/12/2018 08:28:27	5m1s	Electric Warning TM1SACR_SATMX1 PM1_One of Safety Doors TM1 Open                                                warning
PM1TM	CB1FA01_150QA1	12/12/2018 08:23:27	12/12/2018 08:28:27	5m	Electric CB1FA01_150QA1 Doors not closed (PLC: 0005)                                                    fault
PM1TM	CB1FA01_S10422	12/12/2018 08:23:27	12/12/2018 08:28:38	5m11s	Mediamissing CB1FA01_S10422 Compressed air missing. Please acknowledge with CLR (PLC: 0008)                 warning
PM1TM	SU1SA01_TR0x67	12/12/2018 08:24:18	12/12/2018 08:28:27	4m9s	Mechanic Warning SU1SA01_TR0x67 PM1_Safety Door (591S1) TM1 SD-167                                              open
PM1TM	CB1FA01_VakBeh	12/12/2018 08:28:28	12/12/2018 08:28:34	6s	Electric Warning CB1FA01_VakBeh Knock-out vessel is emptied (PLC: 0014)                                         warning
PM1TM	RPIMTTR_INAUTO	12/12/2018 08:28:40	12/12/2018 08:29:13	33s	Misc Warning RPIMTTR_INAUTO MTTR: Machine is Cycling                                                        warning
PM1TM	RPIMTTR_INPROD	12/12/2018 08:28:40	12/12/2018 08:29:13	33s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1TM	PL7FA49_6057S7	12/12/2018 08:28:53	12/12/2018 08:28:59	6s	Electric PL7FA49_6057S7 P Loader Tr 7 - Gripper 2 sticking lens warning (PLC: 1455)                     fault
PM1TM	PL1FA36_6039S0	12/12/2018 08:28:59	12/12/2018 08:29:04	5s	Electric PL1FA36_6039S0 P Loader Tr 1 - Gripper 1 sticking lens warning  (PLC: 1442)                    fault
PM1TM	PL2FA38_6039S5	12/12/2018 08:28:59	12/12/2018 08:29:04	5s	Electric PL2FA38_6039S5 P Loader Tr 2 - Gripper 1 sticking lens warning (PLC: 1444)                     fault
PM1TM	PL4FA43_6045S7	12/12/2018 08:28:59	12/12/2018 08:29:04	5s	Electric PL4FA43_6045S7 P Loader Tr 4 - Gripper 2 sticking lens warning (PLC: 1449)                     fault
PM1TM	PL0FA01_SENSOR	12/12/2018 08:28:59	12/12/2018 08:29:03	4s	Electric PL0FA01_SENSOR Product Loader - Lens is sticking on one of the suction grippers (PLC: 1470)    fault
PM1TM	PL7FA02_6057S7	12/12/2018 08:28:59	12/12/2018 08:29:03	4s	Electric PL7FA02_6057S7 P Loader Tr 7 - Gripper 2 sticking lens error (PLC: 1469)                       fault
PM1TM	PL0FA01_SENSOR	12/12/2018 08:29:04	12/12/2018 08:31:37	2m33s	Electric PL0FA01_SENSOR Product Loader - Lens is sticking on one of the suction grippers (PLC: 1470)    fault
PM1TM	PL1FA02_6039S0	12/12/2018 08:29:04	12/12/2018 08:31:37	2m33s	Electric PL1FA02_6039S0 P Loader Tr 1 - Gripper 1 sticking lens error  (PLC: 1456)                      fault
PM1TM	PL2FA02_6039S5	12/12/2018 08:29:04	12/12/2018 08:31:37	2m33s	Electric PL2FA02_6039S5 P Loader Tr 2 - Gripper 1 sticking lens error (PLC: 1458)                       fault
");
			var e = Utility.BuildEvent(events, 4);
			Assert.AreEqual("Unknown TM Fault", e.Message);
			Assert.AreEqual("Unknown TM Fault", e.Tag);

			// Lack of CTs (mapped from warn to fault)
			events = StringToEvents(@"
PM1TM	PL1OR01_SpcSp1	12/12/2018 14:38:27	12/12/2018 14:38:27	0s	SPC sampling lot 1 START MORASAB1
PM1TM	PL3OR03_SpcSp3	12/12/2018 14:38:30	12/12/2018 14:38:30	0s	SPC sampling lot 3 START MORASAB1
PM1TM	PL5OR05_SpcSp5	12/12/2018 14:38:32	12/12/2018 14:38:32	0s	SPC sampling lot 5 START MORASAB1
PM1TM	PL7OR07_SpcSp7	12/12/2018 14:38:34	12/12/2018 14:38:34	0s	SPC sampling lot 7 START MORASAB1
PM1TM	PL2OR02_SpcSp2	12/12/2018 14:38:40	12/12/2018 14:38:40	0s	SPC sampling lot 2 START MORASAB1
PM1TM	PL4OR04_SpcSp4	12/12/2018 14:38:42	12/12/2018 14:38:42	0s	SPC sampling lot 4 START MORASAB1
PM1TM	PL6OR06_SpcSp6	12/12/2018 14:38:44	12/12/2018 14:38:44	0s	SPC sampling lot 6 START MORASAB1
PM1TM	LLIFA01_Deckel	12/12/2018 14:55:39	12/12/2018 14:59:59	4m20s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	CLIFA01_LeerWT	12/12/2018 14:59:28	12/12/2018 14:59:47	19s	Electric CLIFA01_LeerWT CT Lifter - Lack of CTs (PLC: 1334)                                             warning
PM1TM	RPIMTTR_FAULTD	12/12/2018 14:59:28	12/12/2018 15:09:47	10m19s	Misc Warning RPIMTTR_FAULTD MTTR: Machine in Faulted State (Downtime Tracked)                               warning
PM1TM	RPIMTTR_OPRESP	12/12/2018 14:59:28	12/12/2018 15:00:01	33s	Misc Warning RPIMTTR_OPRESP MTTR: Operator Acknowledge State                                                warning
PM1TM	RPIMTTR_INPROD	12/12/2018 14:59:47	12/12/2018 20:38:56	5h39m9s	Misc Warning RPIMTTR_INPROD MTTR: Machine is Productive                                                     warning
PM1TM	RPIMTTR_RECOVE	12/12/2018 15:00:01	12/12/2018 15:09:47	9m46s	Misc Warning RPIMTTR_RECOVE MTTR: Recovery State                                                            warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:00:49	12/12/2018 15:01:04	15s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:01:55	12/12/2018 15:01:57	2s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:02:47	12/12/2018 15:02:56	9s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:03:46	12/12/2018 15:04:32	46s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:05:22	12/12/2018 15:05:24	2s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	LLIFA01_Deckel	12/12/2018 15:06:52	12/12/2018 15:06:53	1s	Electric LLIFA01_Deckel Lid Lifter - Lack of lids (PLC: 1374)                                           warning
PM1TM	PL1OR01_SpcSp1	12/12/2018 16:32:12	12/12/2018 16:32:12	0s	SPC sampling lot 1 START HICKSJA5
PM1TM	PL3OR03_SpcSp3	12/12/2018 16:32:14	12/12/2018 16:32:14	0s	SPC sampling lot 3 START HICKSJA5
PM1TM	PL5OR05_SpcSp5	12/12/2018 16:32:15	12/12/2018 16:32:15	0s	SPC sampling lot 5 STOP HICKSJA5");
			e = Utility.BuildEvent(events, 9);
			Assert.AreEqual("Electric CLIFA01_LeerWT CT Lifter - Lack of CTs (PLC: 1334)                                             warning", e.Message);
			Assert.AreEqual("CLIFA01_LeerWT", e.Tag);
		}

		[TestMethod]
		public void Test_GroupFaults() {
			// Easy to group
			var simpleGrouping = new List<Fault> {
				new Fault(new DateTime(2019, 06, 01, 12, 00, 00, 00), new DateTime(2019, 06, 01, 12, 12, 00, 00), "MM10", "test unrelated 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 00, 00), new DateTime(2019, 06, 01, 12, 05, 01, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 02, 00), new DateTime(2019, 06, 01, 12, 05, 03, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 04, 00), new DateTime(2019, 06, 01, 12, 05, 04, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 06, 00), new DateTime(2019, 06, 01, 12, 05, 08, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 10, 00), new DateTime(2019, 06, 01, 12, 05, 12, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 06, 00, 00), new DateTime(2019, 06, 01, 12, 07, 00, 00), "MM10", "test unrelated 2"),
			};
			var simpleGroupingAnswerKey = new List<Fault> {
				new Fault(new DateTime(2019, 06, 01, 12, 00, 00, 00), new DateTime(2019, 06, 01, 12, 12, 00, 00), "MM10", "test unrelated 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 00, 00), new DateTime(2019, 06, 01, 12, 05, 12, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 06, 00, 00), new DateTime(2019, 06, 01, 12, 07, 00, 00), "MM10", "test unrelated 2"),
			};
			var simpleGroupingResult = Utility.GroupFaults(simpleGrouping, 5);
			Assert.IsTrue(FaultListsEqual(simpleGroupingResult, simpleGroupingAnswerKey), "Simple Grouping Failed");

			// With one grouping from different PM thrown in
			var simpleGrouping2 = new List<Fault> {
				new Fault(new DateTime(2019, 06, 01, 12, 00, 00, 00), new DateTime(2019, 06, 01, 12, 12, 00, 00), "MM10", "test unrelated 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 00, 00), new DateTime(2019, 06, 01, 12, 05, 01, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 02, 00), new DateTime(2019, 06, 01, 12, 05, 03, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 04, 00), new DateTime(2019, 06, 01, 12, 05, 04, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 06, 00), new DateTime(2019, 06, 01, 12, 05, 08, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 10, 00), new DateTime(2019, 06, 01, 12, 05, 12, 00), "MM11", "test 1"),	// Wrong PM!
				new Fault(new DateTime(2019, 06, 01, 12, 06, 00, 00), new DateTime(2019, 06, 01, 12, 07, 00, 00), "MM10", "test unrelated 2"),
			};
			var simpleGroupingAnswerKey2 = new List<Fault> {
				new Fault(new DateTime(2019, 06, 01, 12, 00, 00, 00), new DateTime(2019, 06, 01, 12, 12, 00, 00), "MM10", "test unrelated 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 00, 00), new DateTime(2019, 06, 01, 12, 05, 08, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 10, 00), new DateTime(2019, 06, 01, 12, 05, 12, 00), "MM11", "test 1"),	// Wrong PM!
				new Fault(new DateTime(2019, 06, 01, 12, 06, 00, 00), new DateTime(2019, 06, 01, 12, 07, 00, 00), "MM10", "test unrelated 2"),
			};
			var simpleGroupingResult2 = Utility.GroupFaults(simpleGrouping2, 5);
			Assert.IsTrue(FaultListsEqual(simpleGroupingResult2, simpleGroupingAnswerKey2), "Simple Grouping 2 Failed");

			// Should find no grouping (<= 5 minutes)
			var noGrouping = new List<Fault> {
				new Fault(new DateTime(2019, 06, 01, 12, 00, 01, 00), new DateTime(2019, 06, 01, 12, 01, 00, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 06, 01, 00), new DateTime(2019, 06, 01, 12, 07, 00, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 12, 01, 00), new DateTime(2019, 06, 01, 12, 13, 00, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 18, 01, 00), new DateTime(2019, 06, 01, 12, 19, 00, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 24, 01, 00), new DateTime(2019, 06, 01, 12, 25, 00, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 30, 01, 00), new DateTime(2019, 06, 01, 12, 31, 00, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 36, 01, 00), new DateTime(2019, 06, 01, 12, 37, 00, 00), "MM10", "test 1"),
			};
			var noGroupingResult = Utility.GroupFaults(noGrouping, 5);
			Assert.IsTrue(FaultListsEqual(noGroupingResult, noGrouping), "No Grouping Failed (@ 5 minutes)");

			// Test grouping with 6m, should group them all
			Assert.IsTrue(Utility.GroupFaults(noGrouping, 6).Count == 1, "No Grouping Failed (@ 6 minutes)");

			// Test Faults that have resp time being added
			var respGrouping = new List<Fault> {
				new Fault(new DateTime(2019, 06, 01, 12, 00, 00, 00), new DateTime(2019, 06, 01, 12, 12, 00, 00), "MM10", "test unrelated 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 00, 00), new DateTime(2019, 06, 01, 12, 05, 01, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 02, 00), new DateTime(2019, 06, 01, 12, 05, 03, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 04, 00), new DateTime(2019, 06, 01, 12, 05, 04, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 06, 00), new DateTime(2019, 06, 01, 12, 05, 08, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 10, 00), new DateTime(2019, 06, 01, 12, 05, 12, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 06, 00, 00), new DateTime(2019, 06, 01, 12, 07, 00, 00), "MM10", "test unrelated 2"),
			};
			respGrouping[6].OpRespTime = 0.8;
			var respGroupingAnswerKey = new List<Fault> {
				new Fault(new DateTime(2019, 06, 01, 12, 00, 00, 00), new DateTime(2019, 06, 01, 12, 12, 00, 00), "MM10", "test unrelated 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 05, 00, 00), new DateTime(2019, 06, 01, 12, 05, 12, 00), "MM10", "test 1"),
				new Fault(new DateTime(2019, 06, 01, 12, 06, 00, 00), new DateTime(2019, 06, 01, 12, 07, 00, 00), "MM10", "test unrelated 2"),
			};
			var respGroupingResult = Utility.GroupFaults(respGrouping, 5);
			Assert.IsTrue(FaultListsEqual(respGroupingResult, respGroupingAnswerKey), "Skip MTTR faults in Grouping Failed");
		}

		[TestMethod]
		public void Test_GroupEvents() {
			// Easy to group
			var test = new List<Event> {
				new Event() { Start = DateTime.Parse("09/09/2019 06:56:11"), End = DateTime.Parse("09/09/2019 06:56:11"), Area = "PP1", Message = "test unrelated 1" },
				new Event() { Start = DateTime.Parse("09/09/2019 06:56:34"), End = DateTime.Parse("09/09/2019 06:56:35"), Area = "PP1", Message = "test" },
				new Event() { Start = DateTime.Parse("09/09/2019 06:56:38"), End = DateTime.Parse("09/09/2019 06:56:42"), Area = "PP1", Message = "test" },
				new Event() { Start = DateTime.Parse("09/09/2019 06:56:45"), End = DateTime.Parse("09/09/2019 06:56:45"), Area = "PP1", Message = "test unrelated 2" },
				new Event() { Start = DateTime.Parse("09/09/2019 06:56:45"), End = DateTime.Parse("09/09/2019 06:57:17"), Area = "PP1", Message = "test" },
			};
			var key = new List<Event> {
				new Event() { Start = DateTime.Parse("09/09/2019 06:56:11"), End = DateTime.Parse("09/09/2019 06:56:11"), Area = "PP1", Message = "test unrelated 1" },
				new Event() { Start = DateTime.Parse("09/09/2019 06:56:34"), End = DateTime.Parse("09/09/2019 06:57:17"), Area = "PP1", Message = "test" },
				new Event() { Start = DateTime.Parse("09/09/2019 06:56:45"), End = DateTime.Parse("09/09/2019 06:56:45"), Area = "PP1", Message = "test unrelated 2" },
			};
			var result = Utility.GroupFaults(test, 5);
			Assert.IsTrue(EventListsEqual(key, result), "Simple Event List Grouping Failed");
		}
	}
}
