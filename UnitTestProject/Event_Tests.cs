﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTTROverview;
using System;

namespace UnitTestProject
{
	[TestClass]
	public class Event_Tests
	{
		// Area helpers
		[TestMethod]
		public void TestArea() {
			var e = new Event();

			// PMXMM
			e.Area = "PM1MM";
			Assert.IsTrue(e.Machine == "MM", "PM1MM.Machine != MM");
			e.Area = "PM10MM";
			Assert.IsTrue(e.Machine == "MM", "PM10MM.Machine != MM");
			// EMX
			e.Area = "EM1";
			Assert.IsTrue(e.Machine == "EM", "EM1.Machine != EM");
			e.Area = "EM10";
			Assert.IsTrue(e.Machine == "EM", "EM10.Machine != EM");
			// PP1TX
			e.Area = "PP1T2";
			Assert.IsTrue(e.Machine == "PP", "PP1T2.Machine != EM");
			e.Area = "PP2";
			Assert.IsTrue(e.Machine == "PP", "PP2.Machine != EM");
		}

		[TestMethod]
		public void TestAPL() {
			var e = new Event();
			var frmt = string.Empty;

			// PMXMM
			frmt = "PM1MM";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 1, frmt);
			frmt = "PM3MM";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 1, frmt);
			frmt = "PM4MM";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 2, frmt);
			frmt = "PM6MM";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 2, frmt);
			frmt = "PM7MM";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 3, frmt);
			frmt = "PM9MM";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 3, frmt);
			frmt = "PM10MM";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 4, frmt);
			frmt = "PM12MM";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 4, frmt);

			// EMX
			frmt = "EM1";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 1, frmt);
			frmt = "EM3";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 1, frmt);
			frmt = "EM4";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 2, frmt);
			frmt = "EM6";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 2, frmt);
			frmt = "EM7";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 3, frmt);
			frmt = "EM9";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 3, frmt);
			frmt = "EM10";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 4, frmt);
			frmt = "EM11";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 4, frmt);

			// PPX
			frmt = "PP1";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 1, frmt);
			frmt = "PP2";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 1, frmt);
			frmt = "PP3";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 2, frmt);
			frmt = "PP4";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 2, frmt);
			frmt = "PP5";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 3, frmt);
			frmt = "PP6";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 3, frmt);
			frmt = "PP7";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 4, frmt);
			frmt = "PP8";
			Assert.IsTrue(new Event() { Area = frmt }.APL == 4, frmt);
		}

		[TestMethod]
		public void TestTrimMessage() {
			// Test 6- normal message
			Assert.AreEqual(
				"CT Lifter - Lack of CTs (PLC: 1334)",
				new Event() { Area = "PM1MM", Tag = "CLIFA01_LeerWT", Message = "Electric CLIFA01_LeerWT CT Lifter - Lack of CTs (PLC: 1334)" }.TrimmedMessage,
				"6- Normal Message (Trimming Needed)"
				);

			// Test 7+ normal Message
			Assert.AreEqual(
				"CT Lifter - Lack of CTs (PLC: 1334)",
				new Event() { Area = "PM7MM", Tag = "", Message = "CT Lifter - Lack of CTs (PLC: 1334)" }.TrimmedMessage,
				"7+ Normal Message (No Trimmed Needed)"
				);

			// Additional Test Message
			Assert.AreEqual(
				"CT Lifter - Lack of CTs (PLC: 1334)",
				new Event() { Area = "PM1MM", Tag = "CLIFA01_LeerWT", Message = "Electricssssssss CLIFA01_LeerWT CT Lifter - Lack of CTs (PLC: 1334)" }.TrimmedMessage,
				"6- Normal Message - Extra Characters at front"
				);

			// Chomp end off too
			Assert.AreEqual(
				"Carrier station 1 FC-side not in position (732S1)",
				new Event() { Area = "PM1MM", Tag = "PR1PAKW_I732S1", Message = "Electric PR1PAKW_I732S1 Carrier station 1 FC-side not in position (732S1)                               fault" }.TrimmedMessage,
				"6- Full Message - Status at the end"
				);

			// PP Line Faults
			Assert.AreEqual(
				"Shell Feeding 1/3 - Backup operation Shell Feeding track 1/3 active (PLC: 1358)",
				new Event() { Area = "PP2", Tag = "SF1FA01_W00050", Message = "MISC Warning SF1FA01_W00050 Shell Feeding 1/3 - Backup operation Shell Feeding track 1/3 active (PLC: 1358) warning" }.TrimmedMessage,
				"6- PP line fault"
				);
		}

		// Tag Clarity
		[TestMethod]
		public void TestTag_Full() {
			var evnt = new Event() {
				Tag = "APL03_PM1MM_RM2FA01_ToABS1_ala"
			};
			Assert.IsTrue(evnt.Tag == "RM2FA01_ToABS1");
		}

		[TestMethod]
		public void TestTag_AlreadyChomped() {
			var evnt = new Event() {
				Tag = "RM2FA01_ToABS1"
			};
			Assert.IsTrue(evnt.Tag == "RM2FA01_ToABS1");
		}

		[TestMethod]
		public void TestTag_FrontHalfChomped() {
			var evnt = new Event() {
				Tag = "RM2FA01_ToABS1_ala"
			};
			Assert.IsTrue(evnt.Tag == "RM2FA01_ToABS1");
		}

		[TestMethod]
		public void TestTag_LastHalfChomped() {
			var evnt = new Event() {
				Tag = "APL03_PM1MM_RM2FA01_ToABS1"
			};
			Assert.IsTrue(evnt.Tag == "RM2FA01_ToABS1");
		}

		[TestMethod]
		public void TestTag_Short() {
			var evnt = new Event() {
				Tag = "APL03_PM1MM_RM2FA01_ToABS"
			};
			Assert.IsTrue(evnt.Tag == "RM2FA01_ToABS");
		}

		[TestMethod]
		public void TestTag_ShortAndAlreadyChomped() {
			var evnt = new Event() {
				Tag = "RM2FA01_ToABS"
			};
			Assert.IsTrue(evnt.Tag == "RM2FA01_ToABS");
		}

		// -- Response Times
		[TestMethod]
		public void TestOpFaults_OnlyOpRespTime() {
			var evnt = new Event() {
				in_OPRespTime = new TimeSpan(0, 10, 0),
			};
			Assert.IsTrue(evnt.IsOpEvent);
			Assert.IsFalse(evnt.IsEMEvent);
		}

		[TestMethod]
		public void TestOpFaults_OnlyOpRespAndRecoveryTime() {
			var evnt = new Event() {
				in_OPRespTime = new TimeSpan(0, 10, 0),
				in_RecoveryTime = new TimeSpan(0, 10, 0),
			};
			Assert.IsTrue(evnt.IsOpEvent);
			Assert.IsFalse(evnt.IsEMEvent);
		}

		[TestMethod]
		public void TestOpFaults_OnlyRecoveryTime() {
			var evnt = new Event() {
				in_RecoveryTime = new TimeSpan(0, 10, 0),
			};
			Assert.IsTrue(evnt.IsOpEvent);
			Assert.IsFalse(evnt.IsEMEvent);
		}

		// -- Shift Assignment
		[TestMethod]
		public void TestShifts_LessThan1Hour() {
			var evnt = new Event() {
				Start = new DateTime(2018, 9, 26, 6, 00, 00),
				End = new DateTime(2018, 9, 26, 6, 01, 00),
			};
			Assert.IsTrue(evnt.Shift == "K", "Before 0630 is previous shift, should be K shift");

			evnt.Start = new DateTime(2018, 9, 26, 6, 30, 00);
			evnt.End = new DateTime(2018, 9, 26, 6, 31, 00);
			Assert.IsTrue(evnt.Shift == "J", "After 0630 is day shift, should be J shift");

			evnt.Start = new DateTime(2018, 9, 26, 18, 30, 00);
			evnt.End = new DateTime(2018, 9, 26, 18, 31, 00);
			Assert.IsTrue(evnt.Shift == "I", "After 1830 is the next night shift, should be I shift");

			evnt.Start = new DateTime(2018, 9, 27, 06, 30, 00);
			evnt.End = new DateTime(2018, 9, 27, 06, 31, 00);
			Assert.IsTrue(evnt.Shift == "H", "After 0630 is the next day shift, should be H shift");
		}

		[TestMethod]
		public void TestShifts_2Hours() {
			var evnt = new Event() {
				Start = new DateTime(2018, 9, 26, 4, 29, 59),
				End = new DateTime(2018, 9, 26, 6, 29, 59),
			};
			Assert.IsTrue(evnt.Shift == "K", "Before 0630 is previous shift, should be K shift");

			evnt.Start = new DateTime(2018, 9, 26, 8, 00, 00);
			evnt.End = new DateTime(2018, 9, 26, 10, 00, 00);
			Assert.IsTrue(evnt.Shift == "J", "After 0630 is day shift, should be J shift");

			evnt.Start = new DateTime(2018, 9, 26, 18, 30, 00);
			evnt.End = new DateTime(2018, 9, 26, 20, 30, 00);
			Assert.IsTrue(evnt.Shift == "I", "After 1830 is the next night shift, should be I shift");

			evnt.Start = new DateTime(2018, 9, 27, 06, 30, 00);
			evnt.End = new DateTime(2018, 9, 27, 08, 30, 00);
			Assert.IsTrue(evnt.Shift == "H", "After 0630 is the next day shift, should be H shift");
		}

		[TestMethod]
		public void TestShifts_LongFaultsAcrossShifts() {
			// Previous Night to day shift
			var evnt = new Event() {
				Start = new DateTime(2018, 9, 26, 5, 00, 00),
				End = new DateTime(2018, 9, 26, 8, 00, 00),
			};
			Assert.IsTrue(evnt.Shift == "K -> J", "Fault running from morning to day shift, Expected (K -> J), got ({0})", evnt.Shift);

			evnt.Start = new DateTime(2018, 9, 26, 17, 00, 00);
			evnt.End = new DateTime(2018, 9, 26, 19, 00, 00);
			Assert.IsTrue(evnt.Shift == "J -> I", "Fault running from days to incomming night shift");

			evnt.Start = new DateTime(2018, 9, 26, 5, 00, 00);
			evnt.End = new DateTime(2018, 9, 26, 20, 00, 00);
			Assert.IsTrue(evnt.Shift == "K -> J -> I", "Fault runnning from morning across days and incoming nights");

			evnt.Start = new DateTime(2018, 9, 26, 05, 00, 00);
			evnt.End = new DateTime(2018, 9, 27, 08, 30, 00);
			Assert.IsTrue(evnt.Shift == "K -> J -> I -> H", "Fault running across all 4 shifts");

			evnt.Start = new DateTime(2018, 9, 27, 05, 00, 00);
			evnt.End = new DateTime(2018, 9, 28, 08, 30, 00);
			Assert.IsTrue(evnt.Shift == "I -> H", "Fault running across all 4 shifts, but 2 of them are the same");
		}

		// PP Faults
		[TestMethod]
		public void TestPPFaults_ExcludeActionMessages() {
			var ev = new Event() {
				Area = "PP1T2",
				Tag = "OM2AV03_ADVISE",
				Start = DateTime.Parse("09/09/2019 07:19:10"),
				End = DateTime.Parse("09/09/2019 07:20:06"),
				Message = "H Safety Zone 2 in automatic mode                                                                               SPS"
			};
			Assert.IsFalse(ev.IsAFaultMessage);

			ev.Message = "Safety Zone 2 in automatic mode                                                                               SPS";
			Assert.IsTrue(ev.IsAFaultMessage);
		}

		[TestMethod]
		public void TestPPFaults_ExcludeOperatorEvents() {
			var ev = new Event() {
				Area = "PP1T2",
				Tag = "TC1MW01_PP1DWT",
				Start = DateTime.Parse("09/09/2019 07:19:10"),
				End = DateTime.Parse("09/09/2019 07:20:06"),
				Message = "Count let pass CT to PP1T1 from PP1T2  FORDNA3"
			};
			Assert.IsTrue(ev.IsAnActionMessage);
			Assert.IsFalse(ev.IsAFaultMessage);
		}
	}
}
