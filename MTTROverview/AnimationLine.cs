﻿using System;
using System.Windows.Forms;

namespace MTTROverview
{
	public class AnimationLine
	{
		public int X = -1;  // Position of this line
		public int Alpha = 0; // Output
		public bool Done = false;
		public Control Parent = null;

		private TimeSpan AnimationTime = new TimeSpan(0, 0, 0, 0, 50);
		private TimeSpan ShowTime = new TimeSpan(0, 0, 4);
		private const int AnimationSteps = 10;

		private Timer Timer = new Timer();
		private int State = 0;

		public void Start() {
			Done = false;
			Timer.Interval = (int)AnimationTime.TotalMilliseconds;
			Timer.Tick += Update;
			Timer.Start();
		}

		private void Update(object sender, EventArgs e) {
			switch (State) {
				case 0: // Fade In Line
					if (Alpha < 255) {
						Alpha = Math.Min(255, Alpha + (255 / AnimationSteps));
					} else {
						Timer.Interval = 4 * (int)AnimationTime.TotalMilliseconds;
						State++;
					}
					break;

				case 1: // Wait to fade out
					Alpha = 255;
					State++;
					Timer.Interval = (int)AnimationTime.TotalMilliseconds;
					break;

				case 2: // Fade Out
					if (Alpha > 0) {
						Alpha = Math.Max(0, Alpha - (255 / AnimationSteps));
					} else {
						State++;
					}
					break;

				case 3:
				default:
					// Do Nothing, we're done
					Done = true;
					Alpha = 0;
					Timer.Stop();
					break;
			}

			// Refresh the parent
			if (Parent != null) {
				Parent.Refresh();
			}
		}
	}
}
