﻿namespace MTTROverview
{
    partial class ReportSelectionWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.ReportTypeThisShift = new System.Windows.Forms.Button();
			this.ReportTypeLastShift = new System.Windows.Forms.Button();
			this.ReportTypeLast2Shifts = new System.Windows.Forms.Button();
			this.ReportTypeTesting = new System.Windows.Forms.Button();
			this.ReportTypeToday = new System.Windows.Forms.Button();
			this.EndTimeBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.StartTimeBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.ReportTitleBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.ReportTypeThisMonth = new System.Windows.Forms.Button();
			this.ReportTypeLast30Days = new System.Windows.Forms.Button();
			this.ReportTypeLast15Days = new System.Windows.Forms.Button();
			this.ReportTypeLast7Days = new System.Windows.Forms.Button();
			this.ReportTypeThisWeek = new System.Windows.Forms.Button();
			this.ReportTypeLast12Hours = new System.Windows.Forms.Button();
			this.ReportTypeLast24Hours = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.StatusCheckBtn = new System.Windows.Forms.Button();
			this.PMAvailabilityBtn = new System.Windows.Forms.Button();
			this.RunMessageQueryBtn = new System.Windows.Forms.Button();
			this.RunMonthlyReport = new System.Windows.Forms.Button();
			this.RunDetailReport = new System.Windows.Forms.Button();
			this.RunOverviewReport = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.ReportTypeThisShift);
			this.groupBox1.Controls.Add(this.ReportTypeLastShift);
			this.groupBox1.Controls.Add(this.ReportTypeLast2Shifts);
			this.groupBox1.Controls.Add(this.ReportTypeTesting);
			this.groupBox1.Controls.Add(this.ReportTypeToday);
			this.groupBox1.Controls.Add(this.EndTimeBox);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.StartTimeBox);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.ReportTitleBox);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.ReportTypeThisMonth);
			this.groupBox1.Controls.Add(this.ReportTypeLast30Days);
			this.groupBox1.Controls.Add(this.ReportTypeLast15Days);
			this.groupBox1.Controls.Add(this.ReportTypeLast7Days);
			this.groupBox1.Controls.Add(this.ReportTypeThisWeek);
			this.groupBox1.Controls.Add(this.ReportTypeLast12Hours);
			this.groupBox1.Controls.Add(this.ReportTypeLast24Hours);
			this.groupBox1.Location = new System.Drawing.Point(2, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(578, 144);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Report Type";
			// 
			// ReportTypeThisShift
			// 
			this.ReportTypeThisShift.Location = new System.Drawing.Point(342, 19);
			this.ReportTypeThisShift.Name = "ReportTypeThisShift";
			this.ReportTypeThisShift.Size = new System.Drawing.Size(162, 23);
			this.ReportTypeThisShift.TabIndex = 19;
			this.ReportTypeThisShift.Text = "This Shift";
			this.ReportTypeThisShift.UseVisualStyleBackColor = true;
			this.ReportTypeThisShift.Click += new System.EventHandler(this.ReportTypeThisShift_Click);
			// 
			// ReportTypeLastShift
			// 
			this.ReportTypeLastShift.Location = new System.Drawing.Point(174, 19);
			this.ReportTypeLastShift.Name = "ReportTypeLastShift";
			this.ReportTypeLastShift.Size = new System.Drawing.Size(162, 23);
			this.ReportTypeLastShift.TabIndex = 18;
			this.ReportTypeLastShift.Text = "Last Shift (12 Hours)";
			this.ReportTypeLastShift.UseVisualStyleBackColor = true;
			this.ReportTypeLastShift.Click += new System.EventHandler(this.ReportTypeLastShift_Click);
			// 
			// ReportTypeLast2Shifts
			// 
			this.ReportTypeLast2Shifts.Location = new System.Drawing.Point(6, 19);
			this.ReportTypeLast2Shifts.Name = "ReportTypeLast2Shifts";
			this.ReportTypeLast2Shifts.Size = new System.Drawing.Size(162, 23);
			this.ReportTypeLast2Shifts.TabIndex = 17;
			this.ReportTypeLast2Shifts.Text = "Last 2 Shifts (24 Hours)";
			this.ReportTypeLast2Shifts.UseVisualStyleBackColor = true;
			this.ReportTypeLast2Shifts.Click += new System.EventHandler(this.ReportTypeLast2Shifts_Click);
			// 
			// ReportTypeTesting
			// 
			this.ReportTypeTesting.Location = new System.Drawing.Point(510, 19);
			this.ReportTypeTesting.Name = "ReportTypeTesting";
			this.ReportTypeTesting.Size = new System.Drawing.Size(63, 23);
			this.ReportTypeTesting.TabIndex = 16;
			this.ReportTypeTesting.Text = "Testing";
			this.ReportTypeTesting.UseVisualStyleBackColor = true;
			this.ReportTypeTesting.Visible = false;
			this.ReportTypeTesting.Click += new System.EventHandler(this.ReportTypeTesting_Click);
			// 
			// ReportTypeToday
			// 
			this.ReportTypeToday.Location = new System.Drawing.Point(336, 60);
			this.ReportTypeToday.Name = "ReportTypeToday";
			this.ReportTypeToday.Size = new System.Drawing.Size(75, 23);
			this.ReportTypeToday.TabIndex = 15;
			this.ReportTypeToday.Text = "Today";
			this.ReportTypeToday.UseVisualStyleBackColor = true;
			this.ReportTypeToday.Click += new System.EventHandler(this.ReportTypeToday_Click);
			// 
			// EndTimeBox
			// 
			this.EndTimeBox.Location = new System.Drawing.Point(403, 118);
			this.EndTimeBox.Name = "EndTimeBox";
			this.EndTimeBox.Size = new System.Drawing.Size(169, 20);
			this.EndTimeBox.TabIndex = 14;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(330, 121);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 13);
			this.label2.TabIndex = 13;
			this.label2.Text = "Report End:";
			// 
			// StartTimeBox
			// 
			this.StartTimeBox.Location = new System.Drawing.Point(403, 91);
			this.StartTimeBox.Name = "StartTimeBox";
			this.StartTimeBox.Size = new System.Drawing.Size(169, 20);
			this.StartTimeBox.TabIndex = 12;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(330, 94);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(67, 13);
			this.label3.TabIndex = 11;
			this.label3.Text = "Report Start:";
			// 
			// ReportTitleBox
			// 
			this.ReportTitleBox.Location = new System.Drawing.Point(80, 118);
			this.ReportTitleBox.Name = "ReportTitleBox";
			this.ReportTitleBox.Size = new System.Drawing.Size(244, 20);
			this.ReportTitleBox.TabIndex = 8;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 121);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(68, 13);
			this.label1.TabIndex = 7;
			this.label1.Text = "Report Title: ";
			// 
			// ReportTypeThisMonth
			// 
			this.ReportTypeThisMonth.Location = new System.Drawing.Point(498, 60);
			this.ReportTypeThisMonth.Name = "ReportTypeThisMonth";
			this.ReportTypeThisMonth.Size = new System.Drawing.Size(75, 23);
			this.ReportTypeThisMonth.TabIndex = 6;
			this.ReportTypeThisMonth.Text = "This Month";
			this.ReportTypeThisMonth.UseVisualStyleBackColor = true;
			this.ReportTypeThisMonth.Click += new System.EventHandler(this.ReportTypeThisMonth_Click);
			// 
			// ReportTypeLast30Days
			// 
			this.ReportTypeLast30Days.Location = new System.Drawing.Point(171, 89);
			this.ReportTypeLast30Days.Name = "ReportTypeLast30Days";
			this.ReportTypeLast30Days.Size = new System.Drawing.Size(81, 23);
			this.ReportTypeLast30Days.TabIndex = 5;
			this.ReportTypeLast30Days.Text = "Last 30 Days";
			this.ReportTypeLast30Days.UseVisualStyleBackColor = true;
			this.ReportTypeLast30Days.Click += new System.EventHandler(this.ReportTypeLast30Days_Click);
			// 
			// ReportTypeLast15Days
			// 
			this.ReportTypeLast15Days.Location = new System.Drawing.Point(87, 89);
			this.ReportTypeLast15Days.Name = "ReportTypeLast15Days";
			this.ReportTypeLast15Days.Size = new System.Drawing.Size(81, 23);
			this.ReportTypeLast15Days.TabIndex = 4;
			this.ReportTypeLast15Days.Text = "Last 15 Days";
			this.ReportTypeLast15Days.UseVisualStyleBackColor = true;
			this.ReportTypeLast15Days.Click += new System.EventHandler(this.ReportTypeLast15Days_Click);
			// 
			// ReportTypeLast7Days
			// 
			this.ReportTypeLast7Days.Location = new System.Drawing.Point(6, 89);
			this.ReportTypeLast7Days.Name = "ReportTypeLast7Days";
			this.ReportTypeLast7Days.Size = new System.Drawing.Size(75, 23);
			this.ReportTypeLast7Days.TabIndex = 3;
			this.ReportTypeLast7Days.Text = "Last 7 Days";
			this.ReportTypeLast7Days.UseVisualStyleBackColor = true;
			this.ReportTypeLast7Days.Click += new System.EventHandler(this.ReportTypeLast7Days_Click);
			// 
			// ReportTypeThisWeek
			// 
			this.ReportTypeThisWeek.Location = new System.Drawing.Point(417, 60);
			this.ReportTypeThisWeek.Name = "ReportTypeThisWeek";
			this.ReportTypeThisWeek.Size = new System.Drawing.Size(75, 23);
			this.ReportTypeThisWeek.TabIndex = 2;
			this.ReportTypeThisWeek.Text = "This Week";
			this.ReportTypeThisWeek.UseVisualStyleBackColor = true;
			this.ReportTypeThisWeek.Click += new System.EventHandler(this.ReportTypeThisWeek_Click);
			// 
			// ReportTypeLast12Hours
			// 
			this.ReportTypeLast12Hours.Location = new System.Drawing.Point(87, 60);
			this.ReportTypeLast12Hours.Name = "ReportTypeLast12Hours";
			this.ReportTypeLast12Hours.Size = new System.Drawing.Size(75, 23);
			this.ReportTypeLast12Hours.TabIndex = 1;
			this.ReportTypeLast12Hours.Text = "Last 12 hrs";
			this.ReportTypeLast12Hours.UseVisualStyleBackColor = true;
			this.ReportTypeLast12Hours.Click += new System.EventHandler(this.ReportTypeLast12Hours_Click);
			// 
			// ReportTypeLast24Hours
			// 
			this.ReportTypeLast24Hours.Location = new System.Drawing.Point(6, 60);
			this.ReportTypeLast24Hours.Name = "ReportTypeLast24Hours";
			this.ReportTypeLast24Hours.Size = new System.Drawing.Size(75, 23);
			this.ReportTypeLast24Hours.TabIndex = 0;
			this.ReportTypeLast24Hours.Text = "Last 24 hrs";
			this.ReportTypeLast24Hours.UseVisualStyleBackColor = true;
			this.ReportTypeLast24Hours.Click += new System.EventHandler(this.ReportTypeLast24Hours_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.StatusCheckBtn);
			this.groupBox2.Controls.Add(this.PMAvailabilityBtn);
			this.groupBox2.Controls.Add(this.RunMessageQueryBtn);
			this.groupBox2.Controls.Add(this.RunMonthlyReport);
			this.groupBox2.Controls.Add(this.RunDetailReport);
			this.groupBox2.Controls.Add(this.RunOverviewReport);
			this.groupBox2.Location = new System.Drawing.Point(2, 147);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(578, 74);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Run Report...";
			// 
			// StatusCheckBtn
			// 
			this.StatusCheckBtn.Location = new System.Drawing.Point(6, 15);
			this.StatusCheckBtn.Name = "StatusCheckBtn";
			this.StatusCheckBtn.Size = new System.Drawing.Size(175, 23);
			this.StatusCheckBtn.TabIndex = 5;
			this.StatusCheckBtn.Text = "Check SCADA Files...";
			this.StatusCheckBtn.UseVisualStyleBackColor = true;
			this.StatusCheckBtn.Click += new System.EventHandler(this.StatusCheckBtn_Click);
			// 
			// PMAvailabilityBtn
			// 
			this.PMAvailabilityBtn.Location = new System.Drawing.Point(396, 15);
			this.PMAvailabilityBtn.Name = "PMAvailabilityBtn";
			this.PMAvailabilityBtn.Size = new System.Drawing.Size(175, 23);
			this.PMAvailabilityBtn.TabIndex = 4;
			this.PMAvailabilityBtn.Text = "Get PM Availability...";
			this.PMAvailabilityBtn.UseVisualStyleBackColor = true;
			this.PMAvailabilityBtn.Click += new System.EventHandler(this.GetPMAvailabityBtn_Click);
			// 
			// RunMessageQueryBtn
			// 
			this.RunMessageQueryBtn.Location = new System.Drawing.Point(201, 15);
			this.RunMessageQueryBtn.Name = "RunMessageQueryBtn";
			this.RunMessageQueryBtn.Size = new System.Drawing.Size(175, 23);
			this.RunMessageQueryBtn.TabIndex = 3;
			this.RunMessageQueryBtn.Text = "Run Message Query...";
			this.RunMessageQueryBtn.UseVisualStyleBackColor = true;
			this.RunMessageQueryBtn.Click += new System.EventHandler(this.RunMessageQueryBtn_Click);
			// 
			// RunMonthlyReport
			// 
			this.RunMonthlyReport.Location = new System.Drawing.Point(396, 41);
			this.RunMonthlyReport.Name = "RunMonthlyReport";
			this.RunMonthlyReport.Size = new System.Drawing.Size(175, 23);
			this.RunMonthlyReport.TabIndex = 2;
			this.RunMonthlyReport.Text = "Run Top Downtime Report...";
			this.RunMonthlyReport.UseVisualStyleBackColor = true;
			this.RunMonthlyReport.Click += new System.EventHandler(this.RunMonthlyReport_Click);
			// 
			// RunDetailReport
			// 
			this.RunDetailReport.Location = new System.Drawing.Point(201, 41);
			this.RunDetailReport.Name = "RunDetailReport";
			this.RunDetailReport.Size = new System.Drawing.Size(175, 23);
			this.RunDetailReport.TabIndex = 1;
			this.RunDetailReport.Text = "Run Detailed Report...";
			this.RunDetailReport.UseVisualStyleBackColor = true;
			this.RunDetailReport.Click += new System.EventHandler(this.RunDetailReport_Click);
			// 
			// RunOverviewReport
			// 
			this.RunOverviewReport.Location = new System.Drawing.Point(6, 41);
			this.RunOverviewReport.Name = "RunOverviewReport";
			this.RunOverviewReport.Size = new System.Drawing.Size(175, 23);
			this.RunOverviewReport.TabIndex = 0;
			this.RunOverviewReport.Text = "Run Overview Report...";
			this.RunOverviewReport.UseVisualStyleBackColor = true;
			this.RunOverviewReport.Click += new System.EventHandler(this.RunOverviewReport_Click);
			// 
			// ReportSelectionWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(583, 221);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "ReportSelectionWindow";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Select Time Range and Type of Report to Run";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox ReportTitleBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ReportTypeThisMonth;
        private System.Windows.Forms.Button ReportTypeLast30Days;
        private System.Windows.Forms.Button ReportTypeLast15Days;
        private System.Windows.Forms.Button ReportTypeLast7Days;
        private System.Windows.Forms.Button ReportTypeThisWeek;
        private System.Windows.Forms.Button ReportTypeLast12Hours;
        private System.Windows.Forms.Button ReportTypeLast24Hours;
        private System.Windows.Forms.TextBox EndTimeBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox StartTimeBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button RunMonthlyReport;
        private System.Windows.Forms.Button RunDetailReport;
        private System.Windows.Forms.Button RunOverviewReport;
        private System.Windows.Forms.Button ReportTypeToday;
        private System.Windows.Forms.Button ReportTypeTesting;
        private System.Windows.Forms.Button ReportTypeLastShift;
        private System.Windows.Forms.Button ReportTypeLast2Shifts;
        private System.Windows.Forms.Button ReportTypeThisShift;
        private System.Windows.Forms.Button RunMessageQueryBtn;
        private System.Windows.Forms.Button PMAvailabilityBtn;
		private System.Windows.Forms.Button StatusCheckBtn;
	}
}