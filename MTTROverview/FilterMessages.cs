﻿using System.Collections.Generic;
using System.Linq;

namespace MTTROverview
{
	public class FilterMessages
	{
		// Typical Exlcudes
		public static bool FilterRFIDMsgs = true;

		public static bool FilterDoorMsgs = true;
		public static bool FilterCycleMsgs = true;
		public static bool FilterSCEMsgs = true;
		public static bool FilterMTTRMsgs = false;
		public static bool FilterAllWarnings = false;
		public static string FilterKey = string.Empty;

		// Machine
		//public static bool FilterIncludeMM = true;
		//public static bool FilterIncludeEM = false;
		//public static bool FilterIncludeIM = false;
		//public static bool FilterIncludeTM = false;

		public static List<string> ExcludeModules = new List<string>();

		public static List<Event> Filter(List<Event> msgs) {
			// -- Machine Filter

			// TODO Rewrite this to only do a single loop, this is terribly ineffecient (Or is it, because LINQ has lazy evaluation?)

			var ret = msgs.AsParallel().Where(x => !ExcludeModules.Contains(x.Area)).Select(x => x);

			// -- Msgs Filters
			// Filter ALL Warnings
			if (FilterAllWarnings) {
				ret = ret.Where(x => !x.Message.Contains("Warning")).Select(x => x);
			}
			// Filter out SCE
			if (FilterSCEMsgs) {
				ret = ret.Where(x => !x.Message.Contains("SCE") && !x.Message.Contains("CB1AV24") && !x.Message.Contains("CB1AV25")).Select(x => x);
			}
			// Filter out Cycle Messages
			if (FilterCycleMsgs) {
				ret = ret.Where(x => !x.Area.EndsWith("MM") ||
					(!x.Message.Contains("TRAN_NOTREL") && !x.Message.Contains("RM2FA01_ToABS") &&
					!x.Message.Contains("TIME_TOHIGH"))
				).Select(x => x);
			}
			// Filter out Door Messages
			if (FilterDoorMsgs) {
				ret = ret.Where(x =>
					// MM
					!x.Area.EndsWith("MM") ||
						(!x.Message.Contains("Safety Door") &&
						 !x.Message.Contains("SC1SE01_301A46") &&
						 !x.Message.Contains("CB1DI08_2423LP") &&
						 !x.Message.Contains("CB1DI10_2425LP") &&
						 !x.Message.Contains("CB1ST01_2427LP") &&
						 !x.Message.Contains("CB1D279_DRIVES"))
				).Select(x => x);
			}
			// Filter out RFID [Warning] Messages
			if (FilterRFIDMsgs) {
				ret = ret.Where(x => !x.Area.EndsWith("MM") || !x.Message.Contains("Transponder") || !x.Message.Contains("Warning")).Select(x => x);
			}
			// Filter out MTTR messages
			if (FilterMTTRMsgs) {
				ret = ret.Where(x => !x.Message.Contains("MTTR")).Select(x => x);
			}
			if (!string.IsNullOrWhiteSpace(FilterKey)) {
				// TODO Allow for some prefixes and advanced filtering here
				//ret = ret.Where(x => x.Message.Contains(FilterKey)).Select(x => x);

				// new
				var split = FilterKey.Split(' ');   // Split by spaces
				ret = ret.Where(x => split.All(y => x.Message.Contains(y))).Select(x => x);
			}

			return ret.ToList();
		}
	}
}
