﻿namespace MTTROverview
{
    partial class DetailReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetailReportForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.TimeSeriesBox = new System.Windows.Forms.GroupBox();
            this.TopRightTabControl = new System.Windows.Forms.TabControl();
            this.TopFaultAreasTab = new System.Windows.Forms.TabPage();
            this.TopFaultAreas = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFIDTab = new System.Windows.Forms.TabPage();
            this.RFIDGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.FaultsTab = new System.Windows.Forms.TabPage();
            this.FaultList = new System.Windows.Forms.DataGridView();
            this.Module = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Start = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Message = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MessagesTab = new System.Windows.Forms.TabPage();
            this.MessagesList = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.FilterRFIDMsgsBtn = new System.Windows.Forms.ToolStripButton();
            this.FilterDoorMsgsBtn = new System.Windows.Forms.ToolStripButton();
            this.FilterCycleMsgsBtn = new System.Windows.Forms.ToolStripButton();
            this.FilterSCEMsgsBtn = new System.Windows.Forms.ToolStripButton();
            this.FilterMTTRMessagesBtn = new System.Windows.Forms.ToolStripButton();
            this.FilterAllWarningsBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ModuleVisibilityBtn = new System.Windows.Forms.ToolStripDropDownButton();
            this.IncludeApl01PM1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.FilterTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.TopFaultsTab = new System.Windows.Forms.TabPage();
            this.TopFaultsGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OverviewTab = new System.Windows.Forms.TabPage();
            this.OverviewGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusBarText = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.TopRightTabControl.SuspendLayout();
            this.TopFaultAreasTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopFaultAreas)).BeginInit();
            this.RFIDTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RFIDGrid)).BeginInit();
            this.TabControl1.SuspendLayout();
            this.FaultsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FaultList)).BeginInit();
            this.MessagesTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MessagesList)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.TopFaultsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopFaultsGrid)).BeginInit();
            this.OverviewTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OverviewGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(907, 532);
            this.splitContainer1.SplitterDistance = 233;
            this.splitContainer1.TabIndex = 5;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.TimeSeriesBox);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.TopRightTabControl);
            this.splitContainer2.Size = new System.Drawing.Size(907, 233);
            this.splitContainer2.SplitterDistance = 697;
            this.splitContainer2.TabIndex = 0;
            // 
            // TimeSeriesBox
            // 
            this.TimeSeriesBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimeSeriesBox.Location = new System.Drawing.Point(0, 0);
            this.TimeSeriesBox.Name = "TimeSeriesBox";
            this.TimeSeriesBox.Size = new System.Drawing.Size(697, 233);
            this.TimeSeriesBox.TabIndex = 7;
            this.TimeSeriesBox.TabStop = false;
            this.TimeSeriesBox.Text = "Time Visualization";
            // 
            // TopRightTabControl
            // 
            this.TopRightTabControl.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.TopRightTabControl.Controls.Add(this.TopFaultAreasTab);
            this.TopRightTabControl.Controls.Add(this.RFIDTab);
            this.TopRightTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TopRightTabControl.Location = new System.Drawing.Point(0, 0);
            this.TopRightTabControl.Multiline = true;
            this.TopRightTabControl.Name = "TopRightTabControl";
            this.TopRightTabControl.SelectedIndex = 0;
            this.TopRightTabControl.Size = new System.Drawing.Size(206, 233);
            this.TopRightTabControl.TabIndex = 0;
            // 
            // TopFaultAreasTab
            // 
            this.TopFaultAreasTab.Controls.Add(this.TopFaultAreas);
            this.TopFaultAreasTab.Location = new System.Drawing.Point(23, 4);
            this.TopFaultAreasTab.Name = "TopFaultAreasTab";
            this.TopFaultAreasTab.Padding = new System.Windows.Forms.Padding(3);
            this.TopFaultAreasTab.Size = new System.Drawing.Size(179, 225);
            this.TopFaultAreasTab.TabIndex = 0;
            this.TopFaultAreasTab.Text = "Top Fault Areas";
            this.TopFaultAreasTab.UseVisualStyleBackColor = true;
            // 
            // TopFaultAreas
            // 
            this.TopFaultAreas.AllowUserToAddRows = false;
            this.TopFaultAreas.AllowUserToDeleteRows = false;
            this.TopFaultAreas.AllowUserToOrderColumns = true;
            this.TopFaultAreas.AllowUserToResizeRows = false;
            this.TopFaultAreas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TopFaultAreas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TopFaultAreas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TopFaultAreas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TopFaultAreas.DefaultCellStyle = dataGridViewCellStyle2;
            this.TopFaultAreas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TopFaultAreas.Location = new System.Drawing.Point(3, 3);
            this.TopFaultAreas.MultiSelect = false;
            this.TopFaultAreas.Name = "TopFaultAreas";
            this.TopFaultAreas.RowHeadersVisible = false;
            this.TopFaultAreas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TopFaultAreas.Size = new System.Drawing.Size(173, 219);
            this.TopFaultAreas.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.FillWeight = 50F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Area";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.FillWeight = 25F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Time";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.FillWeight = 25F;
            this.dataGridViewTextBoxColumn6.HeaderText = "Count";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // RFIDTab
            // 
            this.RFIDTab.Controls.Add(this.RFIDGrid);
            this.RFIDTab.Location = new System.Drawing.Point(23, 4);
            this.RFIDTab.Name = "RFIDTab";
            this.RFIDTab.Padding = new System.Windows.Forms.Padding(3);
            this.RFIDTab.Size = new System.Drawing.Size(179, 210);
            this.RFIDTab.TabIndex = 1;
            this.RFIDTab.Text = "RFID Errors";
            this.RFIDTab.UseVisualStyleBackColor = true;
            // 
            // RFIDGrid
            // 
            this.RFIDGrid.AllowUserToAddRows = false;
            this.RFIDGrid.AllowUserToDeleteRows = false;
            this.RFIDGrid.AllowUserToOrderColumns = true;
            this.RFIDGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.AliceBlue;
            this.RFIDGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.RFIDGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.RFIDGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.RFIDGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RFIDGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn11,
            this.FC});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RFIDGrid.DefaultCellStyle = dataGridViewCellStyle5;
            this.RFIDGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RFIDGrid.Location = new System.Drawing.Point(3, 3);
            this.RFIDGrid.MultiSelect = false;
            this.RFIDGrid.Name = "RFIDGrid";
            this.RFIDGrid.RowHeadersVisible = false;
            this.RFIDGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.RFIDGrid.Size = new System.Drawing.Size(173, 204);
            this.RFIDGrid.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.FillWeight = 20F;
            this.dataGridViewTextBoxColumn15.HeaderText = "Pos";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.FillWeight = 30F;
            this.dataGridViewTextBoxColumn11.HeaderText = "FC";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // FC
            // 
            this.FC.FillWeight = 30F;
            this.FC.HeaderText = "BC";
            this.FC.Name = "FC";
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.FaultsTab);
            this.TabControl1.Controls.Add(this.MessagesTab);
            this.TabControl1.Controls.Add(this.TopFaultsTab);
            this.TabControl1.Controls.Add(this.OverviewTab);
            this.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl1.Location = new System.Drawing.Point(0, 0);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(907, 295);
            this.TabControl1.TabIndex = 0;
            // 
            // FaultsTab
            // 
            this.FaultsTab.Controls.Add(this.FaultList);
            this.FaultsTab.Location = new System.Drawing.Point(4, 22);
            this.FaultsTab.Name = "FaultsTab";
            this.FaultsTab.Padding = new System.Windows.Forms.Padding(3);
            this.FaultsTab.Size = new System.Drawing.Size(899, 269);
            this.FaultsTab.TabIndex = 0;
            this.FaultsTab.Text = "Fault Events";
            this.FaultsTab.UseVisualStyleBackColor = true;
            // 
            // FaultList
            // 
            this.FaultList.AllowUserToAddRows = false;
            this.FaultList.AllowUserToDeleteRows = false;
            this.FaultList.AllowUserToOrderColumns = true;
            this.FaultList.AllowUserToResizeRows = false;
            this.FaultList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.FaultList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FaultList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Module,
            this.Start,
            this.Duration,
            this.Message,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.FaultList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FaultList.Location = new System.Drawing.Point(3, 3);
            this.FaultList.MultiSelect = false;
            this.FaultList.Name = "FaultList";
            this.FaultList.ReadOnly = true;
            this.FaultList.RowHeadersVisible = false;
            this.FaultList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.FaultList.Size = new System.Drawing.Size(893, 263);
            this.FaultList.TabIndex = 1;
            this.FaultList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.FaultList_CellDoubleClick);
            // 
            // Module
            // 
            this.Module.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Module.FillWeight = 25F;
            this.Module.HeaderText = "Module";
            this.Module.Name = "Module";
            this.Module.ReadOnly = true;
            this.Module.Width = 50;
            // 
            // Start
            // 
            this.Start.FillWeight = 25F;
            this.Start.HeaderText = "Start";
            this.Start.Name = "Start";
            this.Start.ReadOnly = true;
            // 
            // Duration
            // 
            this.Duration.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Duration.FillWeight = 25F;
            this.Duration.HeaderText = "Duration";
            this.Duration.Name = "Duration";
            this.Duration.ReadOnly = true;
            this.Duration.Width = 50;
            // 
            // Message
            // 
            this.Message.HeaderText = "Message";
            this.Message.Name = "Message";
            this.Message.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column7.HeaderText = "OP Resp";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 75;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column8.HeaderText = "Recovery";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 75;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column9.HeaderText = "EM Resp";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 75;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column10.HeaderText = "Repair";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 75;
            // 
            // MessagesTab
            // 
            this.MessagesTab.Controls.Add(this.MessagesList);
            this.MessagesTab.Controls.Add(this.toolStrip1);
            this.MessagesTab.Location = new System.Drawing.Point(4, 22);
            this.MessagesTab.Name = "MessagesTab";
            this.MessagesTab.Padding = new System.Windows.Forms.Padding(3);
            this.MessagesTab.Size = new System.Drawing.Size(899, 249);
            this.MessagesTab.TabIndex = 1;
            this.MessagesTab.Text = "Messages (By Time)";
            this.MessagesTab.UseVisualStyleBackColor = true;
            // 
            // MessagesList
            // 
            this.MessagesList.AllowUserToAddRows = false;
            this.MessagesList.AllowUserToDeleteRows = false;
            this.MessagesList.AllowUserToOrderColumns = true;
            this.MessagesList.AllowUserToResizeRows = false;
            this.MessagesList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.MessagesList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MessagesList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.MessagesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessagesList.Location = new System.Drawing.Point(3, 28);
            this.MessagesList.MultiSelect = false;
            this.MessagesList.Name = "MessagesList";
            this.MessagesList.ReadOnly = true;
            this.MessagesList.RowHeadersVisible = false;
            this.MessagesList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MessagesList.Size = new System.Drawing.Size(893, 218);
            this.MessagesList.TabIndex = 3;
            this.MessagesList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MessagesList_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Area";
            this.dataGridViewTextBoxColumn1.FillWeight = 25F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Module";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Start";
            this.dataGridViewTextBoxColumn2.FillWeight = 75F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Start";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "End";
            this.dataGridViewTextBoxColumn3.FillWeight = 75F;
            this.dataGridViewTextBoxColumn3.HeaderText = "End";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "PrettyDuration";
            this.dataGridViewTextBoxColumn7.FillWeight = 25F;
            this.dataGridViewTextBoxColumn7.HeaderText = "Duration";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Message";
            this.dataGridViewTextBoxColumn8.FillWeight = 200F;
            this.dataGridViewTextBoxColumn8.HeaderText = "Message";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.FilterRFIDMsgsBtn,
            this.FilterDoorMsgsBtn,
            this.FilterCycleMsgsBtn,
            this.FilterSCEMsgsBtn,
            this.FilterMTTRMessagesBtn,
            this.FilterAllWarningsBtn,
            this.toolStripSeparator1,
            this.ModuleVisibilityBtn,
            this.toolStripSeparator2,
            this.toolStripLabel2,
            this.FilterTextBox});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(893, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(50, 22);
            this.toolStripLabel1.Text = "Exclude:";
            // 
            // FilterRFIDMsgsBtn
            // 
            this.FilterRFIDMsgsBtn.Checked = true;
            this.FilterRFIDMsgsBtn.CheckOnClick = true;
            this.FilterRFIDMsgsBtn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FilterRFIDMsgsBtn.Image = ((System.Drawing.Image)(resources.GetObject("FilterRFIDMsgsBtn.Image")));
            this.FilterRFIDMsgsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FilterRFIDMsgsBtn.Name = "FilterRFIDMsgsBtn";
            this.FilterRFIDMsgsBtn.Size = new System.Drawing.Size(56, 22);
            this.FilterRFIDMsgsBtn.Text = "RFIDs";
            this.FilterRFIDMsgsBtn.ToolTipText = "Filter Out all RFID Messages";
            this.FilterRFIDMsgsBtn.Click += new System.EventHandler(this.FilterMsgsBtn_Click);
            // 
            // FilterDoorMsgsBtn
            // 
            this.FilterDoorMsgsBtn.Checked = true;
            this.FilterDoorMsgsBtn.CheckOnClick = true;
            this.FilterDoorMsgsBtn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FilterDoorMsgsBtn.Image = ((System.Drawing.Image)(resources.GetObject("FilterDoorMsgsBtn.Image")));
            this.FilterDoorMsgsBtn.ImageTransparentColor = System.Drawing.Color.White;
            this.FilterDoorMsgsBtn.Name = "FilterDoorMsgsBtn";
            this.FilterDoorMsgsBtn.Size = new System.Drawing.Size(53, 22);
            this.FilterDoorMsgsBtn.Text = "Door";
            this.FilterDoorMsgsBtn.ToolTipText = "Filter Out all Door Messages";
            this.FilterDoorMsgsBtn.Click += new System.EventHandler(this.FilterMsgsBtn_Click);
            // 
            // FilterCycleMsgsBtn
            // 
            this.FilterCycleMsgsBtn.Checked = true;
            this.FilterCycleMsgsBtn.CheckOnClick = true;
            this.FilterCycleMsgsBtn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FilterCycleMsgsBtn.Image = ((System.Drawing.Image)(resources.GetObject("FilterCycleMsgsBtn.Image")));
            this.FilterCycleMsgsBtn.ImageTransparentColor = System.Drawing.Color.White;
            this.FilterCycleMsgsBtn.Name = "FilterCycleMsgsBtn";
            this.FilterCycleMsgsBtn.Size = new System.Drawing.Size(74, 22);
            this.FilterCycleMsgsBtn.Text = "Trans Rel";
            this.FilterCycleMsgsBtn.ToolTipText = "Filter Out All Transport Release (Waiting for Station X) messages";
            this.FilterCycleMsgsBtn.Click += new System.EventHandler(this.FilterMsgsBtn_Click);
            // 
            // FilterSCEMsgsBtn
            // 
            this.FilterSCEMsgsBtn.Checked = true;
            this.FilterSCEMsgsBtn.CheckOnClick = true;
            this.FilterSCEMsgsBtn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FilterSCEMsgsBtn.Image = ((System.Drawing.Image)(resources.GetObject("FilterSCEMsgsBtn.Image")));
            this.FilterSCEMsgsBtn.ImageTransparentColor = System.Drawing.Color.White;
            this.FilterSCEMsgsBtn.Name = "FilterSCEMsgsBtn";
            this.FilterSCEMsgsBtn.Size = new System.Drawing.Size(47, 22);
            this.FilterSCEMsgsBtn.Text = "SCE";
            this.FilterSCEMsgsBtn.ToolTipText = "Filter Out All Other (SCE, RMM, ";
            this.FilterSCEMsgsBtn.Click += new System.EventHandler(this.FilterMsgsBtn_Click);
            // 
            // FilterMTTRMessagesBtn
            // 
            this.FilterMTTRMessagesBtn.CheckOnClick = true;
            this.FilterMTTRMessagesBtn.ImageTransparentColor = System.Drawing.Color.White;
            this.FilterMTTRMessagesBtn.Name = "FilterMTTRMessagesBtn";
            this.FilterMTTRMessagesBtn.Size = new System.Drawing.Size(43, 22);
            this.FilterMTTRMessagesBtn.Text = "MTTR";
            this.FilterMTTRMessagesBtn.ToolTipText = "Filter Out All MTTR Messages";
            this.FilterMTTRMessagesBtn.Click += new System.EventHandler(this.FilterMsgsBtn_Click);
            // 
            // FilterAllWarningsBtn
            // 
            this.FilterAllWarningsBtn.CheckOnClick = true;
            this.FilterAllWarningsBtn.Image = ((System.Drawing.Image)(resources.GetObject("FilterAllWarningsBtn.Image")));
            this.FilterAllWarningsBtn.ImageTransparentColor = System.Drawing.Color.White;
            this.FilterAllWarningsBtn.Name = "FilterAllWarningsBtn";
            this.FilterAllWarningsBtn.Size = new System.Drawing.Size(77, 22);
            this.FilterAllWarningsBtn.Text = "Warnings";
            this.FilterAllWarningsBtn.ToolTipText = "Filter Out All Warning Messages";
            this.FilterAllWarningsBtn.Click += new System.EventHandler(this.FilterMsgsBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ModuleVisibilityBtn
            // 
            this.ModuleVisibilityBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IncludeApl01PM1,
            this.toolStripMenuItem2});
            this.ModuleVisibilityBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ModuleVisibilityBtn.Name = "ModuleVisibilityBtn";
            this.ModuleVisibilityBtn.Size = new System.Drawing.Size(117, 22);
            this.ModuleVisibilityBtn.Text = "Include Modules...";
            // 
            // IncludeApl01PM1
            // 
            this.IncludeApl01PM1.Checked = true;
            this.IncludeApl01PM1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IncludeApl01PM1.Name = "IncludeApl01PM1";
            this.IncludeApl01PM1.Size = new System.Drawing.Size(180, 22);
            this.IncludeApl01PM1.Text = "toolStripMenuItem1";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem2.Text = "toolStripMenuItem2";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(36, 22);
            this.toolStripLabel2.Text = "Filter:";
            // 
            // FilterTextBox
            // 
            this.FilterTextBox.Name = "FilterTextBox";
            this.FilterTextBox.Size = new System.Drawing.Size(200, 25);
            this.FilterTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FilterTextBox_KeyDown);
            // 
            // TopFaultsTab
            // 
            this.TopFaultsTab.Controls.Add(this.TopFaultsGrid);
            this.TopFaultsTab.Location = new System.Drawing.Point(4, 22);
            this.TopFaultsTab.Name = "TopFaultsTab";
            this.TopFaultsTab.Padding = new System.Windows.Forms.Padding(3);
            this.TopFaultsTab.Size = new System.Drawing.Size(899, 249);
            this.TopFaultsTab.TabIndex = 2;
            this.TopFaultsTab.Text = "Faults By Count / Duration";
            this.TopFaultsTab.UseVisualStyleBackColor = true;
            // 
            // TopFaultsGrid
            // 
            this.TopFaultsGrid.AllowUserToAddRows = false;
            this.TopFaultsGrid.AllowUserToDeleteRows = false;
            this.TopFaultsGrid.AllowUserToOrderColumns = true;
            this.TopFaultsGrid.AllowUserToResizeRows = false;
            this.TopFaultsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TopFaultsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TopFaultsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn13});
            this.TopFaultsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TopFaultsGrid.Location = new System.Drawing.Point(3, 3);
            this.TopFaultsGrid.MultiSelect = false;
            this.TopFaultsGrid.Name = "TopFaultsGrid";
            this.TopFaultsGrid.ReadOnly = true;
            this.TopFaultsGrid.RowHeadersVisible = false;
            this.TopFaultsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TopFaultsGrid.Size = new System.Drawing.Size(893, 243);
            this.TopFaultsGrid.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.FillWeight = 25F;
            this.dataGridViewTextBoxColumn9.HeaderText = "Module";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.FillWeight = 25F;
            this.dataGridViewTextBoxColumn12.HeaderText = "Duration";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.FillWeight = 25F;
            this.dataGridViewTextBoxColumn14.HeaderText = "Count";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.FillWeight = 200F;
            this.dataGridViewTextBoxColumn13.HeaderText = "Message";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // OverviewTab
            // 
            this.OverviewTab.Controls.Add(this.OverviewGrid);
            this.OverviewTab.Location = new System.Drawing.Point(4, 22);
            this.OverviewTab.Name = "OverviewTab";
            this.OverviewTab.Padding = new System.Windows.Forms.Padding(3);
            this.OverviewTab.Size = new System.Drawing.Size(899, 249);
            this.OverviewTab.TabIndex = 3;
            this.OverviewTab.Text = "Overview";
            this.OverviewTab.UseVisualStyleBackColor = true;
            // 
            // OverviewGrid
            // 
            this.OverviewGrid.AllowUserToAddRows = false;
            this.OverviewGrid.AllowUserToDeleteRows = false;
            this.OverviewGrid.AllowUserToResizeRows = false;
            this.OverviewGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.OverviewGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.Column11,
            this.Column12,
            this.Column15,
            this.Column13,
            this.Column14,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.Column4,
            this.Column5,
            this.dataGridViewTextBoxColumn19,
            this.Column6,
            this.Column1,
            this.Column2,
            this.Column3});
            this.OverviewGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OverviewGrid.Location = new System.Drawing.Point(3, 3);
            this.OverviewGrid.MultiSelect = false;
            this.OverviewGrid.Name = "OverviewGrid";
            this.OverviewGrid.ReadOnly = true;
            this.OverviewGrid.RowHeadersVisible = false;
            this.OverviewGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.OverviewGrid.Size = new System.Drawing.Size(893, 243);
            this.OverviewGrid.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn10.DividerWidth = 3;
            this.dataGridViewTextBoxColumn10.FillWeight = 30F;
            this.dataGridViewTextBoxColumn10.HeaderText = "Module";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 80;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Avg MTTR";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 85;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Avg OP Resp";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 97;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Avg Recovery";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Avg EM Resp";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 98;
            // 
            // Column14
            // 
            this.Column14.DividerWidth = 3;
            this.Column14.HeaderText = "Avg Repair";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 88;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.FillWeight = 20F;
            this.dataGridViewTextBoxColumn16.HeaderText = "Avail %";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.ToolTipText = "% of time module was cycling (not faulted)";
            this.dataGridViewTextBoxColumn16.Width = 66;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.FillWeight = 20F;
            this.dataGridViewTextBoxColumn17.HeaderText = "Prod %";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.ToolTipText = "% of time module was productive";
            this.dataGridViewTextBoxColumn17.Width = 65;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DividerWidth = 3;
            this.dataGridViewTextBoxColumn18.FillWeight = 20F;
            this.dataGridViewTextBoxColumn18.HeaderText = "Fault %";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.ToolTipText = "% of time module in faulted state";
            this.dataGridViewTextBoxColumn18.Width = 69;
            // 
            // Column4
            // 
            this.Column4.FillWeight = 20F;
            this.Column4.HeaderText = "Wasted %";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.ToolTipText = "% of time the module was cycling but not productive";
            this.Column4.Width = 80;
            // 
            // Column5
            // 
            this.Column5.DividerWidth = 3;
            this.Column5.FillWeight = 20F;
            this.Column5.HeaderText = "Lot Chg %";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.ToolTipText = "% of time the module was in a lot change state";
            this.Column5.Width = 83;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.FillWeight = 20F;
            this.dataGridViewTextBoxColumn19.HeaderText = "Stops";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.ToolTipText = "Count of times the module stopped cycling";
            this.dataGridViewTextBoxColumn19.Width = 59;
            // 
            // Column6
            // 
            this.Column6.DividerWidth = 3;
            this.Column6.FillWeight = 20F;
            this.Column6.HeaderText = "Faults";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 63;
            // 
            // Column1
            // 
            this.Column1.FillWeight = 20F;
            this.Column1.HeaderText = "Avg Cycl";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.ToolTipText = "Average Cycling Period Time";
            this.Column1.Width = 74;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 20F;
            this.Column2.HeaderText = "Avg Prod";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.ToolTipText = "Average Productive Period";
            this.Column2.Width = 76;
            // 
            // Column3
            // 
            this.Column3.FillWeight = 20F;
            this.Column3.HeaderText = "Avg Fault";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.ToolTipText = "Average Fault Duration (Period)";
            this.Column3.Width = 77;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBarText,
            this.ProgressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 532);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(907, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusBarText
            // 
            this.StatusBarText.Name = "StatusBarText";
            this.StatusBarText.Size = new System.Drawing.Size(790, 17);
            this.StatusBarText.Spring = true;
            this.StatusBarText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ProgressBar
            // 
            this.ProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ProgressBar.AutoSize = false;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 554);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "Form1";
            this.Text = "Detail Report";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.TopRightTabControl.ResumeLayout(false);
            this.TopFaultAreasTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TopFaultAreas)).EndInit();
            this.RFIDTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RFIDGrid)).EndInit();
            this.TabControl1.ResumeLayout(false);
            this.FaultsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FaultList)).EndInit();
            this.MessagesTab.ResumeLayout(false);
            this.MessagesTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MessagesList)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.TopFaultsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TopFaultsGrid)).EndInit();
            this.OverviewTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OverviewGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl TabControl1;
        private System.Windows.Forms.TabPage FaultsTab;
        private System.Windows.Forms.TabPage MessagesTab;
        private System.Windows.Forms.DataGridView FaultList;
        private System.Windows.Forms.TabPage TopFaultsTab;
        private System.Windows.Forms.DataGridView TopFaultsGrid;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarText;
        private System.Windows.Forms.ToolStripProgressBar ProgressBar;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox TimeSeriesBox;
        private System.Windows.Forms.TabControl TopRightTabControl;
        private System.Windows.Forms.TabPage TopFaultAreasTab;
        private System.Windows.Forms.DataGridView TopFaultAreas;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.TabPage RFIDTab;
        private System.Windows.Forms.DataGridView RFIDGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC;
        private System.Windows.Forms.DataGridView MessagesList;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton FilterRFIDMsgsBtn;
        private System.Windows.Forms.ToolStripButton FilterCycleMsgsBtn;
        private System.Windows.Forms.ToolStripButton FilterDoorMsgsBtn;
        private System.Windows.Forms.ToolStripButton FilterSCEMsgsBtn;
        private System.Windows.Forms.ToolStripButton FilterAllWarningsBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.TabPage OverviewTab;
        private System.Windows.Forms.DataGridView OverviewGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Module;
        private System.Windows.Forms.DataGridViewTextBoxColumn Start;
        private System.Windows.Forms.DataGridViewTextBoxColumn Duration;
        private System.Windows.Forms.DataGridViewTextBoxColumn Message;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.ToolStripDropDownButton ModuleVisibilityBtn;
        private System.Windows.Forms.ToolStripMenuItem IncludeApl01PM1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripTextBox FilterTextBox;
        private System.Windows.Forms.ToolStripButton FilterMTTRMessagesBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    }
}

