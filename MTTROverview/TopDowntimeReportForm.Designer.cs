﻿namespace MTTROverview
{
    partial class TopDowntimeReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.FaultList = new System.Windows.Forms.DataGridView();
			this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.MsgText = new System.Windows.Forms.TextBox();
			this.MsgTag = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.MsgArea = new System.Windows.Forms.TextBox();
			this.ApplyBtn = new System.Windows.Forms.Button();
			this.CategoriesBox = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.RenameKeyBtn = new System.Windows.Forms.Button();
			this.TopTenKeysBtn = new System.Windows.Forms.Button();
			this.BusyPanel = new System.Windows.Forms.Panel();
			this.BusyProgressLabel = new System.Windows.Forms.Label();
			this.BusyPercentLabel = new System.Windows.Forms.Label();
			this.BusyShreeLabel = new System.Windows.Forms.Label();
			this.BusyProgressBar = new System.Windows.Forms.ProgressBar();
			this.BusyStatusLabel = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.FaultList)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.BusyPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// FaultList
			// 
			this.FaultList.AllowUserToAddRows = false;
			this.FaultList.AllowUserToDeleteRows = false;
			this.FaultList.AllowUserToResizeRows = false;
			this.FaultList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FaultList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.FaultList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column9,
            this.Column10,
            this.Column2,
            this.Column8,
            this.Column11,
            this.Column12,
            this.Column1,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
			this.FaultList.Location = new System.Drawing.Point(3, 34);
			this.FaultList.Name = "FaultList";
			this.FaultList.ReadOnly = true;
			this.FaultList.RowHeadersVisible = false;
			this.FaultList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.FaultList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.FaultList.Size = new System.Drawing.Size(730, 385);
			this.FaultList.TabIndex = 9;
			// 
			// Column9
			// 
			this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.Column9.DataPropertyName = "Start";
			this.Column9.FillWeight = 10F;
			this.Column9.HeaderText = "Start";
			this.Column9.Name = "Column9";
			this.Column9.ReadOnly = true;
			this.Column9.Width = 54;
			// 
			// Column10
			// 
			this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.Column10.DataPropertyName = "End";
			this.Column10.FillWeight = 10F;
			this.Column10.HeaderText = "End";
			this.Column10.Name = "Column10";
			this.Column10.ReadOnly = true;
			this.Column10.Width = 51;
			// 
			// Column2
			// 
			this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.Column2.DataPropertyName = "PM";
			this.Column2.HeaderText = "PM";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Width = 48;
			// 
			// Column8
			// 
			this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.Column8.DataPropertyName = "Shift";
			this.Column8.FillWeight = 10F;
			this.Column8.HeaderText = "Shift";
			this.Column8.Name = "Column8";
			this.Column8.ReadOnly = true;
			this.Column8.Width = 40;
			// 
			// Column11
			// 
			this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.Column11.DataPropertyName = "Duration";
			this.Column11.FillWeight = 10F;
			this.Column11.HeaderText = "Dur (m)";
			this.Column11.Name = "Column11";
			this.Column11.ReadOnly = true;
			this.Column11.ToolTipText = "In Minutes";
			this.Column11.Width = 66;
			// 
			// Column12
			// 
			this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.Column12.DataPropertyName = "Message";
			this.Column12.HeaderText = "Message Text";
			this.Column12.Name = "Column12";
			this.Column12.ReadOnly = true;
			// 
			// Column1
			// 
			this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.Column1.DataPropertyName = "Key";
			this.Column1.HeaderText = "Key";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.Column1.Width = 50;
			// 
			// Column3
			// 
			this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.Column3.DataPropertyName = "OpRespTime";
			this.Column3.HeaderText = "OpResp";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			this.Column3.ToolTipText = "In Minutes";
			this.Column3.Width = 50;
			// 
			// Column4
			// 
			this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.Column4.DataPropertyName = "RecoveryTime";
			this.Column4.HeaderText = "Recovery";
			this.Column4.Name = "Column4";
			this.Column4.ReadOnly = true;
			this.Column4.ToolTipText = "In Minutes";
			this.Column4.Width = 50;
			// 
			// Column5
			// 
			this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.Column5.DataPropertyName = "EMRespTime";
			this.Column5.HeaderText = "EMResp";
			this.Column5.Name = "Column5";
			this.Column5.ReadOnly = true;
			this.Column5.ToolTipText = "In Minutes";
			this.Column5.Width = 50;
			// 
			// Column6
			// 
			this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.Column6.DataPropertyName = "RepairTime";
			this.Column6.HeaderText = "Repair";
			this.Column6.Name = "Column6";
			this.Column6.ReadOnly = true;
			this.Column6.ToolTipText = "In Minutes";
			this.Column6.Width = 50;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.tableLayoutPanel1);
			this.groupBox1.Location = new System.Drawing.Point(3, 425);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(730, 68);
			this.groupBox1.TabIndex = 10;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Assign With Same Tag to: ";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 7;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 88F));
			this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.MsgText, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.MsgTag, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label4, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.MsgArea, 3, 1);
			this.tableLayoutPanel1.Controls.Add(this.ApplyBtn, 6, 0);
			this.tableLayoutPanel1.Controls.Add(this.CategoriesBox, 5, 1);
			this.tableLayoutPanel1.Controls.Add(this.label3, 4, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(724, 49);
			this.tableLayoutPanel1.TabIndex = 12;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(3, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(54, 25);
			this.label2.TabIndex = 15;
			this.label2.Text = "Tag:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(54, 24);
			this.label1.TabIndex = 13;
			this.label1.Text = "Message:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// MsgText
			// 
			this.MsgText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.SetColumnSpan(this.MsgText, 5);
			this.MsgText.Location = new System.Drawing.Point(63, 3);
			this.MsgText.Name = "MsgText";
			this.MsgText.ReadOnly = true;
			this.MsgText.Size = new System.Drawing.Size(570, 20);
			this.MsgText.TabIndex = 14;
			// 
			// MsgTag
			// 
			this.MsgTag.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.MsgTag.Location = new System.Drawing.Point(63, 27);
			this.MsgTag.Name = "MsgTag";
			this.MsgTag.ReadOnly = true;
			this.MsgTag.Size = new System.Drawing.Size(149, 20);
			this.MsgTag.TabIndex = 16;
			// 
			// label4
			// 
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Location = new System.Drawing.Point(218, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(34, 25);
			this.label4.TabIndex = 17;
			this.label4.Text = "Area:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// MsgArea
			// 
			this.MsgArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.MsgArea.Location = new System.Drawing.Point(258, 27);
			this.MsgArea.Name = "MsgArea";
			this.MsgArea.ReadOnly = true;
			this.MsgArea.Size = new System.Drawing.Size(149, 20);
			this.MsgArea.TabIndex = 18;
			// 
			// ApplyBtn
			// 
			this.ApplyBtn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ApplyBtn.Location = new System.Drawing.Point(639, 3);
			this.ApplyBtn.Name = "ApplyBtn";
			this.tableLayoutPanel1.SetRowSpan(this.ApplyBtn, 2);
			this.ApplyBtn.Size = new System.Drawing.Size(82, 43);
			this.ApplyBtn.TabIndex = 19;
			this.ApplyBtn.Text = "<Apply>";
			this.ApplyBtn.UseVisualStyleBackColor = true;
			this.ApplyBtn.Click += new System.EventHandler(this.ApplyBtn_Click);
			// 
			// CategoriesBox
			// 
			this.CategoriesBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CategoriesBox.FormattingEnabled = true;
			this.CategoriesBox.Location = new System.Drawing.Point(453, 27);
			this.CategoriesBox.MaxDropDownItems = 12;
			this.CategoriesBox.Name = "CategoriesBox";
			this.CategoriesBox.Size = new System.Drawing.Size(180, 21);
			this.CategoriesBox.TabIndex = 20;
			this.CategoriesBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CategoriesBox_KeyDown);
			// 
			// label3
			// 
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(413, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(34, 25);
			this.label3.TabIndex = 21;
			this.label3.Text = "Key:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// RenameKeyBtn
			// 
			this.RenameKeyBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.RenameKeyBtn.Location = new System.Drawing.Point(613, 5);
			this.RenameKeyBtn.Name = "RenameKeyBtn";
			this.RenameKeyBtn.Size = new System.Drawing.Size(120, 23);
			this.RenameKeyBtn.TabIndex = 11;
			this.RenameKeyBtn.Text = "Rename Key...";
			this.RenameKeyBtn.UseVisualStyleBackColor = true;
			this.RenameKeyBtn.Visible = false;
			this.RenameKeyBtn.Click += new System.EventHandler(this.RenameKeyBtn_Click);
			// 
			// TopTenKeysBtn
			// 
			this.TopTenKeysBtn.Location = new System.Drawing.Point(3, 5);
			this.TopTenKeysBtn.Name = "TopTenKeysBtn";
			this.TopTenKeysBtn.Size = new System.Drawing.Size(120, 23);
			this.TopTenKeysBtn.TabIndex = 12;
			this.TopTenKeysBtn.Text = "Show Top 10 Keys...";
			this.TopTenKeysBtn.UseVisualStyleBackColor = true;
			this.TopTenKeysBtn.Click += new System.EventHandler(this.TopTenKeysBtn_Click);
			// 
			// BusyPanel
			// 
			this.BusyPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.BusyPanel.Controls.Add(this.BusyProgressLabel);
			this.BusyPanel.Controls.Add(this.BusyPercentLabel);
			this.BusyPanel.Controls.Add(this.BusyShreeLabel);
			this.BusyPanel.Controls.Add(this.BusyProgressBar);
			this.BusyPanel.Controls.Add(this.BusyStatusLabel);
			this.BusyPanel.Controls.Add(this.label5);
			this.BusyPanel.Location = new System.Drawing.Point(120, 105);
			this.BusyPanel.Name = "BusyPanel";
			this.BusyPanel.Size = new System.Drawing.Size(496, 186);
			this.BusyPanel.TabIndex = 13;
			// 
			// BusyProgressLabel
			// 
			this.BusyProgressLabel.Font = new System.Drawing.Font("Open Sans", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BusyProgressLabel.Location = new System.Drawing.Point(8, 136);
			this.BusyProgressLabel.Name = "BusyProgressLabel";
			this.BusyProgressLabel.Size = new System.Drawing.Size(130, 17);
			this.BusyProgressLabel.TabIndex = 4;
			this.BusyProgressLabel.Text = "0 / 0";
			this.BusyProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.BusyProgressLabel.Visible = false;
			// 
			// BusyPercentLabel
			// 
			this.BusyPercentLabel.Font = new System.Drawing.Font("Open Sans", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BusyPercentLabel.Location = new System.Drawing.Point(388, 136);
			this.BusyPercentLabel.Name = "BusyPercentLabel";
			this.BusyPercentLabel.Size = new System.Drawing.Size(100, 17);
			this.BusyPercentLabel.TabIndex = 3;
			this.BusyPercentLabel.Text = "0.0%";
			this.BusyPercentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.BusyPercentLabel.Visible = false;
			// 
			// BusyShreeLabel
			// 
			this.BusyShreeLabel.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BusyShreeLabel.Location = new System.Drawing.Point(7, 115);
			this.BusyShreeLabel.Name = "BusyShreeLabel";
			this.BusyShreeLabel.Size = new System.Drawing.Size(481, 38);
			this.BusyShreeLabel.TabIndex = 5;
			this.BusyShreeLabel.Text = "Hello, Shree!\r\nGood Morning!";
			this.BusyShreeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// BusyProgressBar
			// 
			this.BusyProgressBar.ForeColor = System.Drawing.Color.DodgerBlue;
			this.BusyProgressBar.Location = new System.Drawing.Point(7, 156);
			this.BusyProgressBar.Name = "BusyProgressBar";
			this.BusyProgressBar.Size = new System.Drawing.Size(481, 23);
			this.BusyProgressBar.TabIndex = 2;
			// 
			// BusyStatusLabel
			// 
			this.BusyStatusLabel.Font = new System.Drawing.Font("Open Sans", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BusyStatusLabel.Location = new System.Drawing.Point(3, 65);
			this.BusyStatusLabel.Name = "BusyStatusLabel";
			this.BusyStatusLabel.Size = new System.Drawing.Size(489, 60);
			this.BusyStatusLabel.TabIndex = 1;
			this.BusyStatusLabel.Text = "<Not Started>";
			this.BusyStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Open Sans Semibold", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(4, 5);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(488, 60);
			this.label5.TabIndex = 0;
			this.label5.Text = "Loading Data...";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// TopDowntimeReportForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(736, 495);
			this.Controls.Add(this.BusyPanel);
			this.Controls.Add(this.TopTenKeysBtn);
			this.Controls.Add(this.RenameKeyBtn);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.FaultList);
			this.Name = "TopDowntimeReportForm";
			this.Text = "Downtime Overview";
			((System.ComponentModel.ISupportInitialize)(this.FaultList)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.BusyPanel.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView FaultList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MsgText;
        private System.Windows.Forms.TextBox MsgTag;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox MsgArea;
        private System.Windows.Forms.Button ApplyBtn;
        private System.Windows.Forms.ComboBox CategoriesBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button RenameKeyBtn;
        private System.Windows.Forms.Button TopTenKeysBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
		private System.Windows.Forms.Panel BusyPanel;
		private System.Windows.Forms.Label BusyProgressLabel;
		private System.Windows.Forms.Label BusyPercentLabel;
		private System.Windows.Forms.ProgressBar BusyProgressBar;
		private System.Windows.Forms.Label BusyStatusLabel;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label BusyShreeLabel;
	}
}