﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MTTROverview.Dialogs
{
	public partial class RenameDowntimeKeyDialog : Form
	{
		private List<string> ExistingKeys;
		public string OldKey => ToRenameBox.Text;
		public string NewKey => NewNameBox.Text;

		public RenameDowntimeKeyDialog(List<string> existingKeys) {
			InitializeComponent();
			ExistingKeys = existingKeys;
			ToRenameBox.Items.AddRange(ExistingKeys.ToArray());
		}

		private void NewNameBox_TextChanged(Object sender, EventArgs e) {
			ErrorLabel.Visible = !string.IsNullOrWhiteSpace(NewNameBox.Text) && ExistingKeys.Contains(NewNameBox.Text);
			OkayBtn.Enabled = !string.IsNullOrWhiteSpace(ToRenameBox.Text) && !string.IsNullOrWhiteSpace(NewNameBox.Text) && !ErrorLabel.Visible;
		}
	}
}
