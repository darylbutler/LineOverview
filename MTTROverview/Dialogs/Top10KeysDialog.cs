﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MTTROverview.Dialogs
{
	public partial class Top10KeysDialog : Form
	{
		public Top10KeysDialog(List<DowntimeDetail> data) {
			InitializeComponent();

			data.Sort((a, b) => b.TotalDowntime.CompareTo(a.TotalDowntime));
			foreach (var item in data) {
				//Grid.Rows.Add(item.Key, Utility.PrettyTimeSpan(item.TotalDowntime), item.Count);
				Grid.Rows.Add(item.Key, item.TotalDowntime, item.Count);
			}
		}

		private void DoneBtn_Click(Object sender, EventArgs e) {
			Close();
		}
	}

	public struct DowntimeDetail
	{
		public string Key;
		public TimeSpan TotalDowntime;
		public ulong Count;
	}
}
