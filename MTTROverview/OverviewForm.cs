﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace MTTROverview
{
	public partial class OverviewForm : Form
	{
		private String Title;
		private List<TimeSeriesRow> Data;
		private DateTime Start, End;

		// Time Series
		private TimeSeries TimeSeries = new TimeSeries();

		private string TimeSeriesSelectedModule = string.Empty;

		// HeatMap options
		private const int SquareWidth = 64;

		private const int SquareHeight = 48;
		private const int SquareSpacing = 2;    // Spacing Between panels
		private Color WorstColor = Color.Red;
		private Color BestColor = Color.Green;

		private Control Selected = null;

		// Property
		public double WorstTimeThreshold = 0.98; // In %

		private Color SelectedColor => Color.FromArgb(75, SystemColors.Highlight);
		private Color HoverColor => Color.FromArgb(75, SystemColors.HotTrack);

		public OverviewForm(string QueryTitle, DateTime start, DateTime end, List<TimeSeriesRow> rows) {
			InitializeComponent();

			// Remember Data
			Title = ReportTitle.Text = QueryTitle;
			Data = rows;
			if (start < end) {
				Start = start;
				End = end;
			} else {
				Start = end;
				End = start;
			}

			// Setup Timeseries Control
			TimeSeriesPanel.Controls.Add(TimeSeries);
			TimeSeries.BorderStyle = BorderStyle.FixedSingle;
			TimeSeries.Dock = DockStyle.Fill;
			TimeSeries.Start = Start;
			TimeSeries.End = End;
			//TimeSeries.TimeClicked += TimeSeriesTimeClicked;

			BuildHeatMap();
		}

		// -- MTTR ------------------------------------------------------------------------------------------
		private void LoadAfterSelection(string area = "") {
			LoadMTTRTable(area);
			LoadDetailsTable(area);
			LoadTimeSeries(area);
			LoadStatsTable(area);
		}

		private void LoadMTTRTable(string area = "") {
			MTTRGrid.Rows.Clear();

			// Build Data
			var AllFaults = GetDataFromArea(area).SelectMany(x => x.Faults);

			// Fill Table
			foreach (var shift in new List<string> { "J", "K", "H", "I" }) {
				var faults = AllFaults.Where(x => x.Shift.Contains(shift)).Select(x => x);
				if (faults.Count() > 0) {
					MTTRGrid.Rows.Add(
						shift,
						faults.Select(x => x.Shift.Contains(shift)).Count(),
						string.Format("{0:0.0}s", faults.Select(x => x.Duration.TotalSeconds).Average()),
						string.Format("{0:0.0}s", faults.Select(x => x.OPRespTime.TotalSeconds).Average()),
						string.Format("{0:0.0}s", faults.Select(x => x.RecoveryTime.TotalSeconds).Average()),
						string.Format("{0:0.0}s", faults.Select(x => x.EMRespTime.TotalSeconds).Average()),
						string.Format("{0:0.0}s", faults.Select(x => x.RepairTime.TotalSeconds).Average())
						);
				}
			}

			// Fault Count Labels
			var opFaults = AllFaults.Where(x => x.IsOpEvent).Select(x => x).Count();
			var emFaults = AllFaults.Where(x => x.IsEMEvent).Select(x => x).Count();
			OPFaultsLabel.Text = string.Format("Total Faults: {0}, Operator Faults: {1}, EM Faults: {2}", AllFaults.Count(), opFaults, emFaults);
		}

		private void LoadDetailsTable(string area = "") {
			var faults = GetDataFromArea(area).SelectMany(x => x.Faults);
			var faultCounts = new Dictionary<string, Tuple<ulong, TimeSpan>>();

			HeatMapFaultLog.Rows.Clear();
			foreach (var flt in faults) {
				HeatMapFaultLog.Rows.Add(
					flt.Shift,
					flt.Area,
					flt.Start,
					flt.End,
					flt.PrettyDuration,
					flt.Message);

				// While we iterate all the faults, count to fill the dictionary for HeatMapFaultStats
				if (faultCounts.ContainsKey(flt.Message)) {
					var orig = faultCounts[flt.Message];
					faultCounts[flt.Message] = new Tuple<ulong, TimeSpan>(orig.Item1 + 1, orig.Item2 + flt.Duration);
				} else {
					faultCounts.Add(flt.Message, new Tuple<ulong, TimeSpan>(1, flt.Duration));
				}
			}
			HeatMapFaultLog.Sort(HeatMapFaultLog.Columns[1], ListSortDirection.Descending);

			HeatMapFaultStats.Rows.Clear();
			foreach (var tpl in faultCounts) {
				HeatMapFaultStats.Rows.Add(
					tpl.Value.Item1,
					Utility.PrettyTimeSpan(tpl.Value.Item2),
					tpl.Key
					);
			}
			HeatMapFaultStats.Sort(HeatMapFaultStats.Columns[1], ListSortDirection.Ascending);
		}

		private void LoadTimeSeries(string area = "") {
			var rows = GetDataFromArea(area);

			TimeSeries.Series.Clear();
			TimeSeries.Series.AddRange(rows);
			TimeSeries.Redraw();
		}

		private void LoadStatsTable(string area = "") {
			var data = GetDataFromArea(area);
			StatsTable.Rows.Clear();
			string PercentFormat = "{0:0.}%";
			string HourFormat = "{0:0.0}h";

			// -- Run Time
			double runTime = 0d;
			if (area == "")
				runTime = (End - Start).TotalHours * 4 * 6;
			else if (area.StartsWith("APL"))
				runTime = (End - Start).TotalHours * 4 * 3;
			else if (area.StartsWith("PM") && area.Length < 5)
				runTime = (End - Start).TotalHours * 4;
			else
				runTime = (End - Start).TotalHours;
			StatsTable.Rows.Add(
				"Total Hours",
				string.Format("{0:0}h", runTime),
				"100%");

			// -- Cycling Time
			var cylcingTime = data.SelectMany(x => x.Cycling).Count() > 0 ? data.SelectMany(x => x.Cycling).Select(x => x.Duration).Aggregate((a, b) => a + b) : TimeSpan.Zero;
			var cyclingPerc = (cylcingTime.TotalHours / runTime) * 100.0d;
			StatsTable.Rows.Add(
				"Cycling",
				string.Format(HourFormat, cylcingTime.TotalHours),
				string.Format(PercentFormat, cyclingPerc));

			// -- Productive Time
			var prodTime = data.SelectMany(x => x.Productive).Count() > 0 ? data.SelectMany(x => x.Productive).Select(x => x.Duration).Aggregate((a, b) => a + b) : TimeSpan.Zero;
			var prodPerc = (prodTime.TotalHours / runTime) * 100.0d;
			StatsTable.Rows.Add(
				"Productive",
				string.Format(HourFormat, prodTime.TotalHours),
				string.Format(PercentFormat, prodPerc));

			// -- Faulted Time
			var faultTime = data.SelectMany(x => x.Faults).Count() > 0 ? data.SelectMany(x => x.Faults).Select(x => x.Duration).Aggregate((a, b) => a + b) : TimeSpan.Zero;
			var faultPerc = (faultTime.TotalHours / runTime) * 100.0d;
			StatsTable.Rows.Add(
				"Faulted",
				string.Format(HourFormat, faultTime.TotalHours),
				string.Format(PercentFormat, faultPerc));
		}

		private IEnumerable<TimeSeriesRow> GetDataFromArea(string area = "") {
			// Build Data
			IEnumerable<TimeSeriesRow> rows = null;
			if (area == "") {
				rows = Data.Select(x => x);
			} else if (area == "APL01") {
				rows = Data.Where(x => x.Title.StartsWith("PM1") || x.Title.StartsWith("PM2") || x.Title.StartsWith("PM3")).Select(x => x);
			} else if (area == "APL02") {
				rows = Data.Where(x => x.Title.StartsWith("PM4") || x.Title.StartsWith("PM5") || x.Title.StartsWith("PM6")).Select(x => x);
			} else {
				rows = Data.Where(x => x.Title.StartsWith(area)).Select(x => x);
			}
			return rows;
		}

		// -- Availability ----------------------------------------------------------------------------------
		private void BuildHeatMap() {
			var PanelMargin = new Padding(1);
			HeatMapTable.Controls.Clear();

			// -- All Panel -----------------------------------
			var allPanel = new Panel() {
				BackColor = Color.Gray,
				Dock = DockStyle.Fill,
				Margin = PanelMargin,
			};
			allPanel.Tag = allPanel.BackColor;
			var areaAll = new Label {
				AutoSize = false,
				Dock = DockStyle.Fill,
				Font = new Font("Calibri", 12.0f, FontStyle.Bold),
				TextAlign = ContentAlignment.MiddleCenter,
				Text = "< All >",
			};
			areaAll.Click += (o, e) => {
				if (Selected == allPanel) return;
				if (Selected != null) {
					Selected.BackColor = (Color)Selected.Tag;
				}
				Selected = allPanel;

				LoadAfterSelection();
			};
			areaAll.MouseEnter += (o, e) => {
				allPanel.BackColor = HoverColor;
			};
			areaAll.MouseLeave += (o, e) => {
				allPanel.BackColor = Selected == allPanel ? SelectedColor : (Color)allPanel.Tag;
			};
			allPanel.Controls.Add(areaAll);
			HeatMapTable.Controls.Add(allPanel, 1, 0);
			HeatMapTable.SetColumnSpan(allPanel, 11);
			toolTip.SetToolTip(areaAll, "Select and view statistics for All Lines");

			// -- APL01 Panel -----------------
			var apl01Panel = new Panel() {
				BackColor = Color.Silver,
				Dock = DockStyle.Fill,
				Margin = PanelMargin
			};
			apl01Panel.Tag = apl01Panel.BackColor;
			var areaAPL01 = new Label {
				AutoSize = false,
				Dock = DockStyle.Fill,
				Font = new Font("Calibri", 12.0f, FontStyle.Bold),
				TextAlign = ContentAlignment.MiddleCenter,
				Text = "< APL01 >"
			};
			areaAPL01.Click += (o, e) => {
				if (Selected == apl01Panel) return;
				if (Selected != null) {
					Selected.BackColor = (Color)Selected.Tag;
				}
				Selected = apl01Panel;

				LoadAfterSelection("APL01");
			};
			areaAPL01.MouseEnter += (o, e) => {
				apl01Panel.BackColor = HoverColor;
			};
			areaAPL01.MouseLeave += (o, e) => {
				apl01Panel.BackColor = Selected == apl01Panel ? SelectedColor : (Color)apl01Panel.Tag;
			};
			apl01Panel.Controls.Add(areaAPL01);
			HeatMapTable.Controls.Add(apl01Panel, 1, 1);
			HeatMapTable.SetColumnSpan(apl01Panel, 5);
			toolTip.SetToolTip(areaAPL01, "Select and view statistics for all of APL01");

			// -- APL02 Panel -----------------
			var apl02Panel = new Panel() {
				BackColor = Color.Silver,
				Dock = DockStyle.Fill,
				Margin = PanelMargin
			};
			apl02Panel.Tag = apl02Panel.BackColor;
			var areaAPL02 = new Label {
				AutoSize = false,
				Dock = DockStyle.Fill,
				Font = new Font("Calibri", 12.0f, FontStyle.Bold),
				TextAlign = ContentAlignment.MiddleCenter,
				Text = "< APL02 >"
			};
			areaAPL02.Click += (o, e) => {
				if (Selected == apl02Panel) return;
				if (Selected != null) {
					Selected.BackColor = (Color)Selected.Tag;
				}
				Selected = apl02Panel;

				LoadAfterSelection("APL02");
			};
			areaAPL02.MouseEnter += (o, e) => {
				apl02Panel.BackColor = HoverColor;
			};
			areaAPL02.MouseLeave += (o, e) => {
				apl02Panel.BackColor = Selected == apl02Panel ? SelectedColor : (Color)apl02Panel.Tag;
			};
			apl02Panel.Controls.Add(areaAPL02);
			HeatMapTable.Controls.Add(apl02Panel, 7, 1);
			HeatMapTable.SetColumnSpan(apl02Panel, 5);
			toolTip.SetToolTip(areaAPL02, "Select and view statistics for all of APL02");

			// Add the PMs
			var totalTime = (End - Start).TotalMinutes;
			int row = 0, col = 0;
			for (int i = 1; i <= 6; ++i) {
				//col = (i - 1) % 2 == 0 ? 1 : 7;
				//row = ((i - 1) / 2) + 2;
				col = i > 3 ? 7 : 1;
				row = i < 4 ? i + 1 : i - 2;

				// PM
				var module = string.Format("PM{0}", i);
				var time = FlattenEvents(Data.Where(x => x.Title.StartsWith(module)).SelectMany(x => x.Faults).ToList()).TotalMinutes;
				var PMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				PMPanel.Dock = DockStyle.Fill;
				//HeatMapPanel.Controls.Add(PMPanel);
				HeatMapTable.Controls.Add(PMPanel, col, row);

				// MM
				module = string.Format("PM{0}MM", i);
				time = Data.Where(x => x.Title == module).Select(x => x.Faults.Sum(s => s.Duration.TotalMinutes)).Sum();
				var MMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				MMPanel.Dock = DockStyle.Fill;
				HeatMapTable.Controls.Add(MMPanel, col + 1, row);

				// EM
				module = string.Format("PM{0}EM", i);
				time = Data.Where(x => x.Title == module).Select(x => x.Faults.Sum(s => s.Duration.TotalMinutes)).Sum();
				var EMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				EMPanel.Dock = DockStyle.Fill;
				HeatMapTable.Controls.Add(EMPanel, col + 2, row);

				// IM
				module = string.Format("PM{0}IM", i);
				time = Data.Where(x => x.Title == module).Select(x => x.Faults.Sum(s => s.Duration.TotalMinutes)).Sum();
				var IMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				IMPanel.Dock = DockStyle.Fill;
				HeatMapTable.Controls.Add(IMPanel, col + 3, row);

				// TM
				module = string.Format("PM{0}TM", i);
				time = Data.Where(x => x.Title == module).Select(x => x.Faults.Sum(s => s.Duration.TotalMinutes)).Sum();
				var TMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				TMPanel.Dock = DockStyle.Fill;
				HeatMapTable.Controls.Add(TMPanel, col + 4, row);
			}
		}

		private void BuildHeatMapOld() {
			HeatMapPanel.Controls.Clear();
			HeatMapPanel.Size = new Size(
				(SquareWidth * 5) + (SquareSpacing * 3),
				(SquareHeight * 6) + (SquareSpacing * 5));     // Include PP lines at bottom? or Right?

			int row = 0, col = 0;
			for (int i = 1; i <= 6; ++i) {
				row = i - 1;

				var totalTime = (End - Start).TotalMinutes;
				// PM
				col = 0;
				var module = string.Format("PM{0}", i);
				//var time = Data.Where(x => x.Title.StartsWith(module)).Select(x => x.Cycling.Sum(s => s.Duration.TotalMinutes)).Sum();
				var time = FlattenEvents(Data.Where(x => x.Title.StartsWith(module)).SelectMany(x => x.Faults).ToList()).TotalMinutes;
				var PMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				HeatMapPanel.Controls.Add(PMPanel);

				// MM
				col = 1;
				module = string.Format("PM{0}MM", i);
				//var time = (ulong)Data.Where(x => x.Title == module).Select(x => x.Faults.Sum(s => s.Duration.TotalMinutes)).Sum();
				time = Data.Where(x => x.Title == module).Select(x => x.Faults.Sum(s => s.Duration.TotalMinutes)).Sum();
				var MMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				HeatMapPanel.Controls.Add(MMPanel);
				// EM
				col = 2;
				module = string.Format("PM{0}EM", i);
				time = Data.Where(x => x.Title == module).Select(x => x.Faults.Sum(s => s.Duration.TotalMinutes)).Sum();
				var EMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				HeatMapPanel.Controls.Add(EMPanel);
				// IM
				col = 3;
				module = string.Format("PM{0}IM", i);
				time = Data.Where(x => x.Title == module).Select(x => x.Faults.Sum(s => s.Duration.TotalMinutes)).Sum();
				var IMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				HeatMapPanel.Controls.Add(IMPanel);
				// TM
				col = 4;
				module = string.Format("PM{0}TM", i);
				time = Data.Where(x => x.Title == module).Select(x => x.Faults.Sum(s => s.Duration.TotalMinutes)).Sum();
				var TMPanel = makeHeatMapPanel(module, time / totalTime, row, col);
				HeatMapPanel.Controls.Add(TMPanel);
			}
		}

		private Panel makeHeatMapPanel(string module, double faultedPercent, int row, int col) {
			var map = new Panel {
				Parent = HeatMapPanel,
				Padding = new Padding(0),
				Location = new Point(   // Left for makeHeatMapPanelOld
					col * (SquareWidth + SquareSpacing),      // x
					row * (SquareHeight + SquareSpacing)      // y
				),
				Size = new Size(SquareWidth, SquareHeight),     // Left for makeHeatMapPanelOld
				BackColor = ColorTime(faultedPercent)
			};
			map.Tag = map.BackColor;

			// Title Label
			var titleFontSize = module.Length > 3 ? 11.0f : 14.0f;
			var area = new Label {
				AutoSize = false,
				Dock = DockStyle.Top,
				Size = new Size(0, SquareHeight / 2),
				Font = new Font("Calibri", titleFontSize, FontStyle.Bold),
				TextAlign = ContentAlignment.TopCenter,
				Text = module.Length > 3 ? ShortArea(module) : module
			};

			// Count Label
			var val = new Label {
				AutoSize = false,
				Dock = DockStyle.Fill,
				Size = new Size(0, SquareHeight / 2),
				Font = new Font("Calibri", 11.0f, FontStyle.Italic),
				Text = string.Format("{0:0}%", (1d - faultedPercent) * 100d),
				TextAlign = ContentAlignment.MiddleCenter
			};
			map.Controls.Add(area);
			map.Controls.Add(val);
			val.BringToFront();

			// -- Add on click event to populate HeatMapDetails
			EventHandler loadHeatMapDeets = (obj, send) => {
				if (Selected == map) return;
				if (Selected != null) {
					Selected.BackColor = (Color)Selected.Tag;
				}
				Selected = map;
				Selected.BackColor = SelectedColor;

				LoadAfterSelection(module);
			};
			EventHandler mEnter = (o, e) => {
				map.BackColor = HoverColor;
			};
			EventHandler mLeave = (o, e) => {
				map.BackColor = Selected == map ? SelectedColor : (Color)map.Tag;
			};
			area.MouseEnter += mEnter;
			area.MouseLeave += mLeave;
			area.Click += loadHeatMapDeets;
			val.MouseEnter += mEnter;
			val.MouseLeave += mLeave;
			val.Click += loadHeatMapDeets;

			return map;
		}

		private Color ColorTime(double time) {
			//var ratio = Math.Min(1.0d, (double)time / (double)WorstTimeThreshold);

			//var h = (float)(Math.Floor((1 - ratio) * 120.0f));
			//var s = (float)(Math.Abs(ratio - 0.5d) / 0.5d);
			//var v = 1;
			//h = 120;
			//s = 1;

			//return ColorFromAhsb(255, h, s, v);
			if (time <= 1.0d - WorstTimeThreshold)
				return Color.FromArgb(0, WorstColor);
			return Color.FromArgb(Limit((int)(510.0d * time)), WorstColor);

			//return Color.FromArgb(
			//    BlendColors(ratio, WorstColor.R, BestColor.R),
			//    BlendColors(ratio, WorstColor.G, BestColor.G),
			//    BlendColors(ratio, WorstColor.B, BestColor.B));
		}

		private byte Limit(int input) {
			if (input < 0) return 0;
			if (input > 255) return 255;
			return (byte)input;
		}

		private byte BlendColors(double ratio, byte worst, byte best) {
			if (worst == best) return worst;

			var diff = (byte)Math.Abs(worst - best);
			if (worst > best) {
				return (byte)(best + Convert.ToByte(ratio * diff));
			}
			return (byte)(worst + Convert.ToByte(ratio * diff));
		}

		public static Color ColorFromAhsb(int a, float h, float s, float b) {
			if (0 > a || 255 < a) {
				throw new ArgumentOutOfRangeException("a", a, "Invalid Alpha");
			}
			if (0f > h || 360f < h) {
				throw new ArgumentOutOfRangeException("h", h, "Invalid Hue");
			}
			if (0f > s || 1f < s) {
				throw new ArgumentOutOfRangeException("s", s, "Invalid Saturation");
			}
			if (0f > b || 1f < b) {
				throw new ArgumentOutOfRangeException("b", b, "InvalidBrightness");
			}

			if (0 == s) {
				return Color.FromArgb(a, Convert.ToInt32(b * 255),
				  Convert.ToInt32(b * 255), Convert.ToInt32(b * 255));
			}

			float fMax, fMid, fMin;
			int iSextant, iMax, iMid, iMin;

			if (0.5 < b) {
				fMax = b - (b * s) + s;
				fMin = b + (b * s) - s;
			} else {
				fMax = b + (b * s);
				fMin = b - (b * s);
			}

			iSextant = (int)Math.Floor(h / 60f);
			if (300f <= h) {
				h -= 360f;
			}
			h /= 60f;
			h -= 2f * (float)Math.Floor(((iSextant + 1f) % 6f) / 2f);
			if (0 == iSextant % 2) {
				fMid = h * (fMax - fMin) + fMin;
			} else {
				fMid = fMin - h * (fMax - fMin);
			}

			iMax = Convert.ToInt32(fMax * 255);
			iMid = Convert.ToInt32(fMid * 255);
			iMin = Convert.ToInt32(fMin * 255);

			switch (iSextant) {
				case 1:
					return Color.FromArgb(a, iMid, iMax, iMin);

				case 2:
					return Color.FromArgb(a, iMin, iMax, iMid);

				case 3:
					return Color.FromArgb(a, iMin, iMid, iMax);

				case 4:
					return Color.FromArgb(a, iMid, iMin, iMax);

				case 5:
					return Color.FromArgb(a, iMax, iMin, iMid);

				default:
					return Color.FromArgb(a, iMax, iMid, iMin);
			}
		}

		private string ShortArea(string area) {
			return string.Format("{0}{1}", area.Substring(3, 2), area[2]);
		}

		private TimeSpan FlattenEvents(List<Event> faults) {
			var flattened = new List<Event>();  // Flattened faults

			if (faults.Count < 1)
				return TimeSpan.Zero;

			foreach (var fault in faults) {
				// See if this fault overlapps with any timeframes we've recorded already
				bool found = false;
				foreach (var e in flattened) {
					// If this fault start before our flattened event and overlaps some
					if (fault.Start < e.Start && fault.End >= e.Start) {
						e.Start = fault.Start;
						e.End = e.End > fault.End ? e.End : fault.End;  // Take the latest end
						found = true;
					}
					// If this fault ends after our flattened event and overlaps some
					else if (fault.End > e.End && fault.Start <= e.End) {
						e.End = fault.End;
						e.Start = e.Start < fault.Start ? e.Start : fault.Start;
						found = true;
					}

					if (found) break;
				}
				// If we didn't use this fault to extend a range we already found, then we need to add it
				if (!found) {
					flattened.Add(fault);
				}
			}

			// Return the Timespan covered
			return flattened.Select(x => x.Duration).Aggregate((a, b) => a + b);
		}
	}
}
