﻿namespace MTTROverview
{
    partial class OverviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.ReportTitle = new System.Windows.Forms.Label();
            this.MTTRGrid = new System.Windows.Forms.DataGridView();
            this.OPFaultsLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.HeatMapPanel = new System.Windows.Forms.Panel();
            this.HeatMapTable = new System.Windows.Forms.TableLayoutPanel();
            this.HeatMapDetails = new System.Windows.Forms.TabControl();
            this.FaultStatsPage = new System.Windows.Forms.TabPage();
            this.HeatMapFaultStats = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FaultLogPage = new System.Windows.Forms.TabPage();
            this.HeatMapFaultLog = new System.Windows.Forms.DataGridView();
            this.TimeSeriesPanel = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.StatsTable = new System.Windows.Forms.DataGridView();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.MTTRGrid)).BeginInit();
            this.HeatMapPanel.SuspendLayout();
            this.HeatMapDetails.SuspendLayout();
            this.FaultStatsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HeatMapFaultStats)).BeginInit();
            this.FaultLogPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HeatMapFaultLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // ReportTitle
            // 
            this.ReportTitle.AutoSize = true;
            this.ReportTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportTitle.Location = new System.Drawing.Point(5, 5);
            this.ReportTitle.Name = "ReportTitle";
            this.ReportTitle.Size = new System.Drawing.Size(155, 31);
            this.ReportTitle.TabIndex = 0;
            this.ReportTitle.Text = "Report Title";
            // 
            // MTTRGrid
            // 
            this.MTTRGrid.AllowUserToAddRows = false;
            this.MTTRGrid.AllowUserToDeleteRows = false;
            this.MTTRGrid.AllowUserToResizeRows = false;
            this.MTTRGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MTTRGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column7,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.MTTRGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MTTRGrid.Location = new System.Drawing.Point(0, 0);
            this.MTTRGrid.MultiSelect = false;
            this.MTTRGrid.Name = "MTTRGrid";
            this.MTTRGrid.ReadOnly = true;
            this.MTTRGrid.RowHeadersVisible = false;
            this.MTTRGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.MTTRGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MTTRGrid.Size = new System.Drawing.Size(471, 129);
            this.MTTRGrid.TabIndex = 1;
            // 
            // OPFaultsLabel
            // 
            this.OPFaultsLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.OPFaultsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OPFaultsLabel.Location = new System.Drawing.Point(0, 129);
            this.OPFaultsLabel.Name = "OPFaultsLabel";
            this.OPFaultsLabel.Size = new System.Drawing.Size(471, 23);
            this.OPFaultsLabel.TabIndex = 3;
            this.OPFaultsLabel.Text = "< Fault Description >";
            this.OPFaultsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(515, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Availability By Module";
            // 
            // HeatMapPanel
            // 
            this.HeatMapPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HeatMapPanel.Controls.Add(this.HeatMapTable);
            this.HeatMapPanel.Location = new System.Drawing.Point(5, 39);
            this.HeatMapPanel.Name = "HeatMapPanel";
            this.HeatMapPanel.Size = new System.Drawing.Size(669, 262);
            this.HeatMapPanel.TabIndex = 6;
            // 
            // HeatMapTable
            // 
            this.HeatMapTable.ColumnCount = 13;
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.HeatMapTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HeatMapTable.Location = new System.Drawing.Point(0, 0);
            this.HeatMapTable.Name = "HeatMapTable";
            this.HeatMapTable.RowCount = 6;
            this.HeatMapTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.HeatMapTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.HeatMapTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.HeatMapTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.HeatMapTable.Size = new System.Drawing.Size(669, 262);
            this.HeatMapTable.TabIndex = 0;
            // 
            // HeatMapDetails
            // 
            this.HeatMapDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HeatMapDetails.Controls.Add(this.FaultStatsPage);
            this.HeatMapDetails.Controls.Add(this.FaultLogPage);
            this.HeatMapDetails.Location = new System.Drawing.Point(5, 465);
            this.HeatMapDetails.Name = "HeatMapDetails";
            this.HeatMapDetails.SelectedIndex = 0;
            this.HeatMapDetails.Size = new System.Drawing.Size(889, 193);
            this.HeatMapDetails.TabIndex = 8;
            // 
            // FaultStatsPage
            // 
            this.FaultStatsPage.Controls.Add(this.HeatMapFaultStats);
            this.FaultStatsPage.Location = new System.Drawing.Point(4, 22);
            this.FaultStatsPage.Name = "FaultStatsPage";
            this.FaultStatsPage.Padding = new System.Windows.Forms.Padding(3);
            this.FaultStatsPage.Size = new System.Drawing.Size(881, 167);
            this.FaultStatsPage.TabIndex = 1;
            this.FaultStatsPage.Text = "Fault Statistics";
            this.FaultStatsPage.UseVisualStyleBackColor = true;
            // 
            // HeatMapFaultStats
            // 
            this.HeatMapFaultStats.AllowUserToAddRows = false;
            this.HeatMapFaultStats.AllowUserToDeleteRows = false;
            this.HeatMapFaultStats.AllowUserToResizeRows = false;
            this.HeatMapFaultStats.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HeatMapFaultStats.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn7});
            this.HeatMapFaultStats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HeatMapFaultStats.Location = new System.Drawing.Point(3, 3);
            this.HeatMapFaultStats.MultiSelect = false;
            this.HeatMapFaultStats.Name = "HeatMapFaultStats";
            this.HeatMapFaultStats.ReadOnly = true;
            this.HeatMapFaultStats.RowHeadersVisible = false;
            this.HeatMapFaultStats.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.HeatMapFaultStats.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HeatMapFaultStats.Size = new System.Drawing.Size(875, 161);
            this.HeatMapFaultStats.TabIndex = 9;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.FillWeight = 10F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Count";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 60;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.FillWeight = 10F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Total Duration";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 99;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.HeaderText = "Message Text";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // FaultLogPage
            // 
            this.FaultLogPage.Controls.Add(this.HeatMapFaultLog);
            this.FaultLogPage.Location = new System.Drawing.Point(4, 22);
            this.FaultLogPage.Name = "FaultLogPage";
            this.FaultLogPage.Padding = new System.Windows.Forms.Padding(3);
            this.FaultLogPage.Size = new System.Drawing.Size(881, 167);
            this.FaultLogPage.TabIndex = 0;
            this.FaultLogPage.Text = "Fault Log";
            this.FaultLogPage.UseVisualStyleBackColor = true;
            // 
            // HeatMapFaultLog
            // 
            this.HeatMapFaultLog.AllowUserToAddRows = false;
            this.HeatMapFaultLog.AllowUserToDeleteRows = false;
            this.HeatMapFaultLog.AllowUserToResizeRows = false;
            this.HeatMapFaultLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HeatMapFaultLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column16,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12});
            this.HeatMapFaultLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HeatMapFaultLog.Location = new System.Drawing.Point(3, 3);
            this.HeatMapFaultLog.MultiSelect = false;
            this.HeatMapFaultLog.Name = "HeatMapFaultLog";
            this.HeatMapFaultLog.ReadOnly = true;
            this.HeatMapFaultLog.RowHeadersVisible = false;
            this.HeatMapFaultLog.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.HeatMapFaultLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HeatMapFaultLog.Size = new System.Drawing.Size(875, 161);
            this.HeatMapFaultLog.TabIndex = 8;
            // 
            // TimeSeriesPanel
            // 
            this.TimeSeriesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimeSeriesPanel.Location = new System.Drawing.Point(0, 0);
            this.TimeSeriesPanel.Name = "TimeSeriesPanel";
            this.TimeSeriesPanel.Size = new System.Drawing.Size(412, 152);
            this.TimeSeriesPanel.TabIndex = 9;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(5, 307);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.MTTRGrid);
            this.splitContainer1.Panel1.Controls.Add(this.OPFaultsLabel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TimeSeriesPanel);
            this.splitContainer1.Size = new System.Drawing.Size(887, 152);
            this.splitContainer1.SplitterDistance = 471;
            this.splitContainer1.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.StatsTable);
            this.panel1.Location = new System.Drawing.Point(680, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(210, 262);
            this.panel1.TabIndex = 11;
            // 
            // StatsTable
            // 
            this.StatsTable.AllowUserToAddRows = false;
            this.StatsTable.AllowUserToDeleteRows = false;
            this.StatsTable.AllowUserToResizeRows = false;
            this.StatsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StatsTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column13,
            this.Column14,
            this.Column15});
            this.StatsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatsTable.Location = new System.Drawing.Point(0, 0);
            this.StatsTable.MultiSelect = false;
            this.StatsTable.Name = "StatsTable";
            this.StatsTable.ReadOnly = true;
            this.StatsTable.RowHeadersVisible = false;
            this.StatsTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.StatsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.StatsTable.Size = new System.Drawing.Size(210, 262);
            this.StatsTable.TabIndex = 2;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column13.HeaderText = "Stat";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column14.HeaderText = "Value";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 50;
            // 
            // Column15
            // 
            this.Column15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column15.HeaderText = "%";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 50;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column8.FillWeight = 10F;
            this.Column8.HeaderText = "S";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.ToolTipText = "Shift";
            this.Column8.Width = 25;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.HeaderText = "Area";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.ToolTipText = "Where Fault Occurred";
            this.Column16.Width = 54;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.FillWeight = 10F;
            this.Column9.HeaderText = "Start";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.ToolTipText = "Time Fault Started";
            this.Column9.Width = 54;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10.FillWeight = 10F;
            this.Column10.HeaderText = "End";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.ToolTipText = "Time Fault Ended";
            this.Column10.Width = 51;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11.FillWeight = 10F;
            this.Column11.HeaderText = "Dur";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.ToolTipText = "Duration";
            this.Column11.Width = 49;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column12.HeaderText = "Message Text";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.ToolTipText = "Fault / Message Text";
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.FillWeight = 10F;
            this.Column1.HeaderText = "Shift";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 40;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7.HeaderText = "# Faults";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 70;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.FillWeight = 20F;
            this.Column2.HeaderText = "MTTR";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.ToolTipText = "Average Total Fault Length";
            this.Column2.Width = 63;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.FillWeight = 20F;
            this.Column3.HeaderText = "OP Resp";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.ToolTipText = "Average Length of time between line stop to activity on the HMI panel (Keyswitch " +
    "or Reset btn press)";
            this.Column3.Width = 75;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.FillWeight = 20F;
            this.Column4.HeaderText = "Recovery";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.ToolTipText = "Average length of time between HMI activity and either a) machine running or b) E" +
    "M requested";
            this.Column4.Width = 78;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.FillWeight = 20F;
            this.Column5.HeaderText = "EM Resp";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.ToolTipText = "Average length of time between EM Request HMI btn press and EM Arrived btn presse" +
    "d";
            this.Column5.Width = 76;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.FillWeight = 20F;
            this.Column6.HeaderText = "Repair";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.ToolTipText = "Average length of time between EM Arrived and machine running";
            this.Column6.Width = 63;
            // 
            // OverviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 662);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.HeatMapDetails);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ReportTitle);
            this.Controls.Add(this.HeatMapPanel);
            this.DoubleBuffered = true;
            this.Name = "OverviewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Report: Overview";
            ((System.ComponentModel.ISupportInitialize)(this.MTTRGrid)).EndInit();
            this.HeatMapPanel.ResumeLayout(false);
            this.HeatMapDetails.ResumeLayout(false);
            this.FaultStatsPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HeatMapFaultStats)).EndInit();
            this.FaultLogPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HeatMapFaultLog)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StatsTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ReportTitle;
        private System.Windows.Forms.DataGridView MTTRGrid;
        private System.Windows.Forms.Label OPFaultsLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel HeatMapPanel;
        private System.Windows.Forms.TabControl HeatMapDetails;
        private System.Windows.Forms.TabPage FaultLogPage;
        private System.Windows.Forms.DataGridView HeatMapFaultLog;
        private System.Windows.Forms.TabPage FaultStatsPage;
        private System.Windows.Forms.DataGridView HeatMapFaultStats;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Panel TimeSeriesPanel;
        private System.Windows.Forms.TableLayoutPanel HeatMapTable;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView StatsTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.ToolTip toolTip;
    }
}