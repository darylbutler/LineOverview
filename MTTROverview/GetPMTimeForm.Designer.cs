﻿namespace MTTROverview
{
    partial class GetPMTimeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.IncludePm12Box = new System.Windows.Forms.CheckBox();
            this.IncludePm11Box = new System.Windows.Forms.CheckBox();
            this.IncludePm10Box = new System.Windows.Forms.CheckBox();
            this.IncludePm9Box = new System.Windows.Forms.CheckBox();
            this.IncludePm8Box = new System.Windows.Forms.CheckBox();
            this.IncludePm7Box = new System.Windows.Forms.CheckBox();
            this.IncludePm6Box = new System.Windows.Forms.CheckBox();
            this.IncludePm5Box = new System.Windows.Forms.CheckBox();
            this.IncludePm4Box = new System.Windows.Forms.CheckBox();
            this.IncludePm3Box = new System.Windows.Forms.CheckBox();
            this.IncludePm2Box = new System.Windows.Forms.CheckBox();
            this.IncludePm1Box = new System.Windows.Forms.CheckBox();
            this.RunQueryBtn = new System.Windows.Forms.Button();
            this.EndTimeBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.StartTimeBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PMGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ProgressLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PMGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.IncludePm12Box);
            this.groupBox1.Controls.Add(this.IncludePm11Box);
            this.groupBox1.Controls.Add(this.IncludePm10Box);
            this.groupBox1.Controls.Add(this.IncludePm9Box);
            this.groupBox1.Controls.Add(this.IncludePm8Box);
            this.groupBox1.Controls.Add(this.IncludePm7Box);
            this.groupBox1.Controls.Add(this.IncludePm6Box);
            this.groupBox1.Controls.Add(this.IncludePm5Box);
            this.groupBox1.Controls.Add(this.IncludePm4Box);
            this.groupBox1.Controls.Add(this.IncludePm3Box);
            this.groupBox1.Controls.Add(this.IncludePm2Box);
            this.groupBox1.Controls.Add(this.IncludePm1Box);
            this.groupBox1.Controls.Add(this.RunQueryBtn);
            this.groupBox1.Controls.Add(this.EndTimeBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.StartTimeBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(693, 79);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Query:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(66, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(251, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "(Shift-Click to Select All, Control-Click to Select None)";
            // 
            // IncludePm12Box
            // 
            this.IncludePm12Box.Location = new System.Drawing.Point(315, 38);
            this.IncludePm12Box.Name = "IncludePm12Box";
            this.IncludePm12Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm12Box.TabIndex = 35;
            this.IncludePm12Box.Text = "PM12";
            this.IncludePm12Box.UseVisualStyleBackColor = true;
            this.IncludePm12Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm11Box
            // 
            this.IncludePm11Box.Location = new System.Drawing.Point(254, 38);
            this.IncludePm11Box.Name = "IncludePm11Box";
            this.IncludePm11Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm11Box.TabIndex = 34;
            this.IncludePm11Box.Text = "PM11";
            this.IncludePm11Box.UseVisualStyleBackColor = true;
            this.IncludePm11Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm10Box
            // 
            this.IncludePm10Box.Location = new System.Drawing.Point(193, 38);
            this.IncludePm10Box.Name = "IncludePm10Box";
            this.IncludePm10Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm10Box.TabIndex = 33;
            this.IncludePm10Box.Text = "PM10";
            this.IncludePm10Box.UseVisualStyleBackColor = true;
            this.IncludePm10Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm9Box
            // 
            this.IncludePm9Box.Location = new System.Drawing.Point(132, 38);
            this.IncludePm9Box.Name = "IncludePm9Box";
            this.IncludePm9Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm9Box.TabIndex = 32;
            this.IncludePm9Box.Text = "PM9";
            this.IncludePm9Box.UseVisualStyleBackColor = true;
            this.IncludePm9Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm8Box
            // 
            this.IncludePm8Box.Location = new System.Drawing.Point(71, 38);
            this.IncludePm8Box.Name = "IncludePm8Box";
            this.IncludePm8Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm8Box.TabIndex = 31;
            this.IncludePm8Box.Text = "PM8";
            this.IncludePm8Box.UseVisualStyleBackColor = true;
            this.IncludePm8Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm7Box
            // 
            this.IncludePm7Box.Location = new System.Drawing.Point(10, 38);
            this.IncludePm7Box.Name = "IncludePm7Box";
            this.IncludePm7Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm7Box.TabIndex = 30;
            this.IncludePm7Box.Text = "PM7";
            this.IncludePm7Box.UseVisualStyleBackColor = true;
            this.IncludePm7Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm6Box
            // 
            this.IncludePm6Box.Location = new System.Drawing.Point(315, 16);
            this.IncludePm6Box.Name = "IncludePm6Box";
            this.IncludePm6Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm6Box.TabIndex = 29;
            this.IncludePm6Box.Text = "PM6";
            this.IncludePm6Box.UseVisualStyleBackColor = true;
            this.IncludePm6Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm5Box
            // 
            this.IncludePm5Box.Location = new System.Drawing.Point(254, 16);
            this.IncludePm5Box.Name = "IncludePm5Box";
            this.IncludePm5Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm5Box.TabIndex = 28;
            this.IncludePm5Box.Text = "PM5";
            this.IncludePm5Box.UseVisualStyleBackColor = true;
            this.IncludePm5Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm4Box
            // 
            this.IncludePm4Box.Location = new System.Drawing.Point(193, 16);
            this.IncludePm4Box.Name = "IncludePm4Box";
            this.IncludePm4Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm4Box.TabIndex = 27;
            this.IncludePm4Box.Text = "PM4";
            this.IncludePm4Box.UseVisualStyleBackColor = true;
            this.IncludePm4Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm3Box
            // 
            this.IncludePm3Box.Location = new System.Drawing.Point(132, 16);
            this.IncludePm3Box.Name = "IncludePm3Box";
            this.IncludePm3Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm3Box.TabIndex = 24;
            this.IncludePm3Box.Text = "PM3";
            this.IncludePm3Box.UseVisualStyleBackColor = true;
            this.IncludePm3Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm2Box
            // 
            this.IncludePm2Box.Location = new System.Drawing.Point(71, 16);
            this.IncludePm2Box.Name = "IncludePm2Box";
            this.IncludePm2Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm2Box.TabIndex = 23;
            this.IncludePm2Box.Text = "PM2";
            this.IncludePm2Box.UseVisualStyleBackColor = true;
            this.IncludePm2Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // IncludePm1Box
            // 
            this.IncludePm1Box.Checked = true;
            this.IncludePm1Box.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IncludePm1Box.Location = new System.Drawing.Point(10, 15);
            this.IncludePm1Box.Name = "IncludePm1Box";
            this.IncludePm1Box.Size = new System.Drawing.Size(55, 17);
            this.IncludePm1Box.TabIndex = 22;
            this.IncludePm1Box.Text = "PM1";
            this.IncludePm1Box.UseVisualStyleBackColor = true;
            this.IncludePm1Box.Click += new System.EventHandler(this.IncludePMCheckboxClick);
            // 
            // RunQueryBtn
            // 
            this.RunQueryBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RunQueryBtn.Location = new System.Drawing.Point(610, 13);
            this.RunQueryBtn.Name = "RunQueryBtn";
            this.RunQueryBtn.Size = new System.Drawing.Size(75, 57);
            this.RunQueryBtn.TabIndex = 21;
            this.RunQueryBtn.Text = "Query";
            this.RunQueryBtn.UseVisualStyleBackColor = true;
            this.RunQueryBtn.Click += new System.EventHandler(this.RunQueryBtn_Click);
            // 
            // EndTimeBox
            // 
            this.EndTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EndTimeBox.Location = new System.Drawing.Point(416, 46);
            this.EndTimeBox.Name = "EndTimeBox";
            this.EndTimeBox.Size = new System.Drawing.Size(188, 20);
            this.EndTimeBox.TabIndex = 18;
            this.EndTimeBox.TextChanged += new System.EventHandler(this.DateTimeBox_TextChanged);
            this.EndTimeBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateTimeBox_KeyDown);
            this.EndTimeBox.Leave += new System.EventHandler(this.DateTimeBox_DoneEditing);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(372, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "End:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // StartTimeBox
            // 
            this.StartTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StartTimeBox.Location = new System.Drawing.Point(416, 18);
            this.StartTimeBox.Name = "StartTimeBox";
            this.StartTimeBox.Size = new System.Drawing.Size(188, 20);
            this.StartTimeBox.TabIndex = 16;
            this.StartTimeBox.TextChanged += new System.EventHandler(this.DateTimeBox_TextChanged);
            this.StartTimeBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateTimeBox_KeyDown);
            this.StartTimeBox.Leave += new System.EventHandler(this.DateTimeBox_DoneEditing);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(372, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Start:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PMGrid
            // 
            this.PMGrid.AllowUserToAddRows = false;
            this.PMGrid.AllowUserToDeleteRows = false;
            this.PMGrid.AllowUserToOrderColumns = true;
            this.PMGrid.AllowUserToResizeRows = false;
            this.PMGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PMGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PMGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column7,
            this.Column4,
            this.Column8,
            this.Column5,
            this.Column9,
            this.Column6});
            this.PMGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PMGrid.Location = new System.Drawing.Point(0, 4);
            this.PMGrid.Name = "PMGrid";
            this.PMGrid.ReadOnly = true;
            this.PMGrid.RowHeadersVisible = false;
            this.PMGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PMGrid.Size = new System.Drawing.Size(693, 632);
            this.PMGrid.TabIndex = 5;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.FillWeight = 60.35671F;
            this.Column1.HeaderText = "Module";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column2.FillWeight = 285.593F;
            this.Column2.HeaderText = "Month";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 75;
            // 
            // Column3
            // 
            this.Column3.FillWeight = 42.91188F;
            this.Column3.HeaderText = "Cycling";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column7.FillWeight = 64.96184F;
            this.Column7.HeaderText = "%";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 55;
            // 
            // Column4
            // 
            this.Column4.FillWeight = 42.91188F;
            this.Column4.HeaderText = "Productive";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column8.FillWeight = 141.8878F;
            this.Column8.HeaderText = "%";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 55;
            // 
            // Column5
            // 
            this.Column5.FillWeight = 42.91188F;
            this.Column5.HeaderText = "Faulted";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column9.FillWeight = 175.5532F;
            this.Column9.HeaderText = "%";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 55;
            // 
            // Column6
            // 
            this.Column6.FillWeight = 42.91188F;
            this.Column6.HeaderText = "Total Time";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProgressLabel,
            this.ProgressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 715);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(693, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ProgressLabel
            // 
            this.ProgressLabel.AutoSize = false;
            this.ProgressLabel.Name = "ProgressLabel";
            this.ProgressLabel.Size = new System.Drawing.Size(556, 17);
            this.ProgressLabel.Spring = true;
            this.ProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ProgressBar
            // 
            this.ProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(120, 16);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PMGrid);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 79);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.panel1.Size = new System.Drawing.Size(693, 636);
            this.panel1.TabIndex = 7;
            // 
            // GetPMTimeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 737);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Name = "GetPMTimeForm";
            this.Text = "Get Available Time";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PMGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox IncludePm12Box;
        private System.Windows.Forms.CheckBox IncludePm11Box;
        private System.Windows.Forms.CheckBox IncludePm10Box;
        private System.Windows.Forms.CheckBox IncludePm9Box;
        private System.Windows.Forms.CheckBox IncludePm8Box;
        private System.Windows.Forms.CheckBox IncludePm7Box;
        private System.Windows.Forms.CheckBox IncludePm6Box;
        private System.Windows.Forms.CheckBox IncludePm5Box;
        private System.Windows.Forms.CheckBox IncludePm4Box;
        private System.Windows.Forms.CheckBox IncludePm3Box;
        private System.Windows.Forms.CheckBox IncludePm2Box;
        private System.Windows.Forms.CheckBox IncludePm1Box;
        private System.Windows.Forms.Button RunQueryBtn;
        private System.Windows.Forms.TextBox EndTimeBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox StartTimeBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView PMGrid;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel ProgressLabel;
        private System.Windows.Forms.ToolStripProgressBar ProgressBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}