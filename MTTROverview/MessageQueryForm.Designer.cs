﻿namespace MTTROverview
{
    partial class MessageQueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.GetMTTRBtn = new System.Windows.Forms.Button();
			this.IncludePP2Box = new System.Windows.Forms.CheckBox();
			this.IncludePm3Box = new System.Windows.Forms.CheckBox();
			this.IncludePm2Box = new System.Windows.Forms.CheckBox();
			this.IncludePm1Box = new System.Windows.Forms.CheckBox();
			this.RunQueryBtn = new System.Windows.Forms.Button();
			this.MessageKeyBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.EndTimeBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.StartTimeBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.IncludePP1Box = new System.Windows.Forms.CheckBox();
			this.IncludeTMBox = new System.Windows.Forms.CheckBox();
			this.IncludeIMBox = new System.Windows.Forms.CheckBox();
			this.IncludeEMBox = new System.Windows.Forms.CheckBox();
			this.IncludeMMBox = new System.Windows.Forms.CheckBox();
			this.IncludeApl04Box = new System.Windows.Forms.CheckBox();
			this.IncludeApl03Box = new System.Windows.Forms.CheckBox();
			this.IncludeApl02Box = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.IncludeApl01Box = new System.Windows.Forms.CheckBox();
			this.MessagesList = new System.Windows.Forms.DataGridView();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.ProgressLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
			this.GetPPBtn = new System.Windows.Forms.Button();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.MessagesList)).BeginInit();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.GetPPBtn);
			this.groupBox1.Controls.Add(this.GetMTTRBtn);
			this.groupBox1.Controls.Add(this.IncludePP2Box);
			this.groupBox1.Controls.Add(this.IncludePm3Box);
			this.groupBox1.Controls.Add(this.IncludePm2Box);
			this.groupBox1.Controls.Add(this.IncludePm1Box);
			this.groupBox1.Controls.Add(this.RunQueryBtn);
			this.groupBox1.Controls.Add(this.MessageKeyBox);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.EndTimeBox);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.StartTimeBox);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.IncludePP1Box);
			this.groupBox1.Controls.Add(this.IncludeTMBox);
			this.groupBox1.Controls.Add(this.IncludeIMBox);
			this.groupBox1.Controls.Add(this.IncludeEMBox);
			this.groupBox1.Controls.Add(this.IncludeMMBox);
			this.groupBox1.Controls.Add(this.IncludeApl04Box);
			this.groupBox1.Controls.Add(this.IncludeApl03Box);
			this.groupBox1.Controls.Add(this.IncludeApl02Box);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.IncludeApl01Box);
			this.groupBox1.Location = new System.Drawing.Point(4, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(626, 135);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Query:";
			// 
			// GetMTTRBtn
			// 
			this.GetMTTRBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.GetMTTRBtn.Location = new System.Drawing.Point(545, 64);
			this.GetMTTRBtn.Name = "GetMTTRBtn";
			this.GetMTTRBtn.Size = new System.Drawing.Size(75, 32);
			this.GetMTTRBtn.TabIndex = 26;
			this.GetMTTRBtn.Text = "Get MTTR";
			this.GetMTTRBtn.UseVisualStyleBackColor = true;
			this.GetMTTRBtn.Click += new System.EventHandler(this.GetMTTRBtn_Click);
			// 
			// IncludePP2Box
			// 
			this.IncludePP2Box.AutoSize = true;
			this.IncludePP2Box.Checked = true;
			this.IncludePP2Box.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludePP2Box.Location = new System.Drawing.Point(198, 84);
			this.IncludePP2Box.Name = "IncludePP2Box";
			this.IncludePP2Box.Size = new System.Drawing.Size(59, 17);
			this.IncludePP2Box.TabIndex = 25;
			this.IncludePP2Box.Text = "PP1T2";
			this.IncludePP2Box.UseVisualStyleBackColor = true;
			// 
			// IncludePm3Box
			// 
			this.IncludePm3Box.AutoSize = true;
			this.IncludePm3Box.Checked = true;
			this.IncludePm3Box.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludePm3Box.Location = new System.Drawing.Point(198, 38);
			this.IncludePm3Box.Name = "IncludePm3Box";
			this.IncludePm3Box.Size = new System.Drawing.Size(48, 17);
			this.IncludePm3Box.TabIndex = 24;
			this.IncludePm3Box.Text = "PM3";
			this.IncludePm3Box.UseVisualStyleBackColor = true;
			// 
			// IncludePm2Box
			// 
			this.IncludePm2Box.AutoSize = true;
			this.IncludePm2Box.Checked = true;
			this.IncludePm2Box.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludePm2Box.Location = new System.Drawing.Point(134, 38);
			this.IncludePm2Box.Name = "IncludePm2Box";
			this.IncludePm2Box.Size = new System.Drawing.Size(48, 17);
			this.IncludePm2Box.TabIndex = 23;
			this.IncludePm2Box.Text = "PM2";
			this.IncludePm2Box.UseVisualStyleBackColor = true;
			// 
			// IncludePm1Box
			// 
			this.IncludePm1Box.AutoSize = true;
			this.IncludePm1Box.Checked = true;
			this.IncludePm1Box.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludePm1Box.Location = new System.Drawing.Point(70, 38);
			this.IncludePm1Box.Name = "IncludePm1Box";
			this.IncludePm1Box.Size = new System.Drawing.Size(48, 17);
			this.IncludePm1Box.TabIndex = 22;
			this.IncludePm1Box.Text = "PM1";
			this.IncludePm1Box.UseVisualStyleBackColor = true;
			// 
			// RunQueryBtn
			// 
			this.RunQueryBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.RunQueryBtn.Location = new System.Drawing.Point(545, 12);
			this.RunQueryBtn.Name = "RunQueryBtn";
			this.RunQueryBtn.Size = new System.Drawing.Size(75, 52);
			this.RunQueryBtn.TabIndex = 21;
			this.RunQueryBtn.Text = "Get Messages";
			this.RunQueryBtn.UseVisualStyleBackColor = true;
			this.RunQueryBtn.Click += new System.EventHandler(this.RunQueryBtn_Click);
			// 
			// MessageKeyBox
			// 
			this.MessageKeyBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.MessageKeyBox.Location = new System.Drawing.Point(128, 107);
			this.MessageKeyBox.Name = "MessageKeyBox";
			this.MessageKeyBox.Size = new System.Drawing.Size(412, 20);
			this.MessageKeyBox.TabIndex = 20;
			this.MessageKeyBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MessageKeyBox_KeyDown);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(8, 110);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(114, 13);
			this.label4.TabIndex = 19;
			this.label4.Text = "Message Contains:";
			// 
			// EndTimeBox
			// 
			this.EndTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.EndTimeBox.Location = new System.Drawing.Point(371, 36);
			this.EndTimeBox.Name = "EndTimeBox";
			this.EndTimeBox.Size = new System.Drawing.Size(169, 20);
			this.EndTimeBox.TabIndex = 18;
			this.EndTimeBox.TextChanged += new System.EventHandler(this.StartTimeBox_TextChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(332, 39);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(33, 13);
			this.label2.TabIndex = 17;
			this.label2.Text = "End:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// StartTimeBox
			// 
			this.StartTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.StartTimeBox.Location = new System.Drawing.Point(371, 13);
			this.StartTimeBox.Name = "StartTimeBox";
			this.StartTimeBox.Size = new System.Drawing.Size(169, 20);
			this.StartTimeBox.TabIndex = 16;
			this.StartTimeBox.TextChanged += new System.EventHandler(this.StartTimeBox_TextChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(327, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(38, 13);
			this.label3.TabIndex = 15;
			this.label3.Text = "Start:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// IncludePP1Box
			// 
			this.IncludePP1Box.AutoSize = true;
			this.IncludePP1Box.Checked = true;
			this.IncludePP1Box.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludePP1Box.Location = new System.Drawing.Point(70, 84);
			this.IncludePP1Box.Name = "IncludePP1Box";
			this.IncludePP1Box.Size = new System.Drawing.Size(59, 17);
			this.IncludePP1Box.TabIndex = 9;
			this.IncludePP1Box.Text = "PP1T1";
			this.IncludePP1Box.UseVisualStyleBackColor = true;
			// 
			// IncludeTMBox
			// 
			this.IncludeTMBox.AutoSize = true;
			this.IncludeTMBox.Checked = true;
			this.IncludeTMBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludeTMBox.Location = new System.Drawing.Point(262, 61);
			this.IncludeTMBox.Name = "IncludeTMBox";
			this.IncludeTMBox.Size = new System.Drawing.Size(42, 17);
			this.IncludeTMBox.TabIndex = 8;
			this.IncludeTMBox.Text = "TM";
			this.IncludeTMBox.UseVisualStyleBackColor = true;
			// 
			// IncludeIMBox
			// 
			this.IncludeIMBox.AutoSize = true;
			this.IncludeIMBox.Checked = true;
			this.IncludeIMBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludeIMBox.Location = new System.Drawing.Point(198, 61);
			this.IncludeIMBox.Name = "IncludeIMBox";
			this.IncludeIMBox.Size = new System.Drawing.Size(38, 17);
			this.IncludeIMBox.TabIndex = 7;
			this.IncludeIMBox.Text = "IM";
			this.IncludeIMBox.UseVisualStyleBackColor = true;
			// 
			// IncludeEMBox
			// 
			this.IncludeEMBox.AutoSize = true;
			this.IncludeEMBox.Checked = true;
			this.IncludeEMBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludeEMBox.Location = new System.Drawing.Point(134, 61);
			this.IncludeEMBox.Name = "IncludeEMBox";
			this.IncludeEMBox.Size = new System.Drawing.Size(42, 17);
			this.IncludeEMBox.TabIndex = 6;
			this.IncludeEMBox.Text = "EM";
			this.IncludeEMBox.UseVisualStyleBackColor = true;
			// 
			// IncludeMMBox
			// 
			this.IncludeMMBox.AutoSize = true;
			this.IncludeMMBox.Checked = true;
			this.IncludeMMBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludeMMBox.Location = new System.Drawing.Point(70, 61);
			this.IncludeMMBox.Name = "IncludeMMBox";
			this.IncludeMMBox.Size = new System.Drawing.Size(44, 17);
			this.IncludeMMBox.TabIndex = 5;
			this.IncludeMMBox.Text = "MM";
			this.IncludeMMBox.UseVisualStyleBackColor = true;
			// 
			// IncludeApl04Box
			// 
			this.IncludeApl04Box.AutoSize = true;
			this.IncludeApl04Box.Checked = true;
			this.IncludeApl04Box.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludeApl04Box.Location = new System.Drawing.Point(262, 15);
			this.IncludeApl04Box.Name = "IncludeApl04Box";
			this.IncludeApl04Box.Size = new System.Drawing.Size(58, 17);
			this.IncludeApl04Box.TabIndex = 4;
			this.IncludeApl04Box.Text = "APL04";
			this.IncludeApl04Box.UseVisualStyleBackColor = true;
			// 
			// IncludeApl03Box
			// 
			this.IncludeApl03Box.AutoSize = true;
			this.IncludeApl03Box.Checked = true;
			this.IncludeApl03Box.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludeApl03Box.Location = new System.Drawing.Point(198, 15);
			this.IncludeApl03Box.Name = "IncludeApl03Box";
			this.IncludeApl03Box.Size = new System.Drawing.Size(58, 17);
			this.IncludeApl03Box.TabIndex = 3;
			this.IncludeApl03Box.Text = "APL03";
			this.IncludeApl03Box.UseVisualStyleBackColor = true;
			// 
			// IncludeApl02Box
			// 
			this.IncludeApl02Box.AutoSize = true;
			this.IncludeApl02Box.Checked = true;
			this.IncludeApl02Box.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludeApl02Box.Location = new System.Drawing.Point(134, 15);
			this.IncludeApl02Box.Name = "IncludeApl02Box";
			this.IncludeApl02Box.Size = new System.Drawing.Size(58, 17);
			this.IncludeApl02Box.TabIndex = 2;
			this.IncludeApl02Box.Text = "APL02";
			this.IncludeApl02Box.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Include: ";
			// 
			// IncludeApl01Box
			// 
			this.IncludeApl01Box.AutoSize = true;
			this.IncludeApl01Box.Checked = true;
			this.IncludeApl01Box.CheckState = System.Windows.Forms.CheckState.Checked;
			this.IncludeApl01Box.Location = new System.Drawing.Point(70, 15);
			this.IncludeApl01Box.Name = "IncludeApl01Box";
			this.IncludeApl01Box.Size = new System.Drawing.Size(58, 17);
			this.IncludeApl01Box.TabIndex = 0;
			this.IncludeApl01Box.Text = "APL01";
			this.IncludeApl01Box.UseVisualStyleBackColor = true;
			// 
			// MessagesList
			// 
			this.MessagesList.AllowUserToAddRows = false;
			this.MessagesList.AllowUserToDeleteRows = false;
			this.MessagesList.AllowUserToOrderColumns = true;
			this.MessagesList.AllowUserToResizeRows = false;
			this.MessagesList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.MessagesList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.MessagesList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.MessagesList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
			this.MessagesList.Location = new System.Drawing.Point(4, 145);
			this.MessagesList.Name = "MessagesList";
			this.MessagesList.ReadOnly = true;
			this.MessagesList.RowHeadersVisible = false;
			this.MessagesList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.MessagesList.Size = new System.Drawing.Size(626, 645);
			this.MessagesList.TabIndex = 4;
			this.MessagesList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MessagesList_KeyDown);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProgressLabel,
            this.ProgressBar});
			this.statusStrip1.Location = new System.Drawing.Point(0, 793);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(634, 22);
			this.statusStrip1.TabIndex = 5;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// ProgressLabel
			// 
			this.ProgressLabel.AutoSize = false;
			this.ProgressLabel.Name = "ProgressLabel";
			this.ProgressLabel.Size = new System.Drawing.Size(497, 17);
			this.ProgressLabel.Spring = true;
			this.ProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ProgressBar
			// 
			this.ProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.ProgressBar.Name = "ProgressBar";
			this.ProgressBar.Size = new System.Drawing.Size(120, 16);
			// 
			// GetPPBtn
			// 
			this.GetPPBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.GetPPBtn.Location = new System.Drawing.Point(545, 96);
			this.GetPPBtn.Name = "GetPPBtn";
			this.GetPPBtn.Size = new System.Drawing.Size(75, 32);
			this.GetPPBtn.TabIndex = 27;
			this.GetPPBtn.Text = "Get PP";
			this.GetPPBtn.UseVisualStyleBackColor = true;
			this.GetPPBtn.Click += new System.EventHandler(this.RunPPBtn_Click);
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.dataGridViewTextBoxColumn1.DataPropertyName = "Area";
			this.dataGridViewTextBoxColumn1.FillWeight = 82.48731F;
			this.dataGridViewTextBoxColumn1.HeaderText = "Module";
			this.dataGridViewTextBoxColumn1.MinimumWidth = 65;
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			this.dataGridViewTextBoxColumn1.Width = 65;
			// 
			// Column1
			// 
			this.Column1.DataPropertyName = "Tag";
			this.Column1.HeaderText = "Tag";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.Visible = false;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.dataGridViewTextBoxColumn2.DataPropertyName = "Start";
			this.dataGridViewTextBoxColumn2.FillWeight = 60.62817F;
			this.dataGridViewTextBoxColumn2.HeaderText = "Start";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.Width = 54;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.dataGridViewTextBoxColumn3.DataPropertyName = "End";
			this.dataGridViewTextBoxColumn3.FillWeight = 75F;
			this.dataGridViewTextBoxColumn3.HeaderText = "End";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Width = 51;
			// 
			// dataGridViewTextBoxColumn7
			// 
			this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.dataGridViewTextBoxColumn7.DataPropertyName = "PrettyDuration";
			this.dataGridViewTextBoxColumn7.FillWeight = 20.20939F;
			this.dataGridViewTextBoxColumn7.HeaderText = "Dur";
			this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
			this.dataGridViewTextBoxColumn7.ReadOnly = true;
			this.dataGridViewTextBoxColumn7.Width = 50;
			// 
			// dataGridViewTextBoxColumn8
			// 
			this.dataGridViewTextBoxColumn8.DataPropertyName = "Message";
			this.dataGridViewTextBoxColumn8.FillWeight = 161.6751F;
			this.dataGridViewTextBoxColumn8.HeaderText = "Message";
			this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
			this.dataGridViewTextBoxColumn8.ReadOnly = true;
			// 
			// Column2
			// 
			this.Column2.DataPropertyName = "OpRespTime";
			this.Column2.HeaderText = "OpResp";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Visible = false;
			// 
			// Column3
			// 
			this.Column3.DataPropertyName = "RecoveryTime";
			this.Column3.HeaderText = "Recovery";
			this.Column3.Name = "Column3";
			this.Column3.ReadOnly = true;
			this.Column3.Visible = false;
			// 
			// Column4
			// 
			this.Column4.DataPropertyName = "EmRespTime";
			this.Column4.HeaderText = "EMResp";
			this.Column4.Name = "Column4";
			this.Column4.ReadOnly = true;
			this.Column4.Visible = false;
			// 
			// Column5
			// 
			this.Column5.DataPropertyName = "RepairTime";
			this.Column5.HeaderText = "Repair";
			this.Column5.Name = "Column5";
			this.Column5.ReadOnly = true;
			this.Column5.Visible = false;
			// 
			// MessageQueryForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(634, 815);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.MessagesList);
			this.Controls.Add(this.groupBox1);
			this.Name = "MessageQueryForm";
			this.Text = "Message Query";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.MessagesList)).EndInit();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox IncludePP1Box;
        private System.Windows.Forms.CheckBox IncludeTMBox;
        private System.Windows.Forms.CheckBox IncludeIMBox;
        private System.Windows.Forms.CheckBox IncludeEMBox;
        private System.Windows.Forms.CheckBox IncludeMMBox;
        private System.Windows.Forms.CheckBox IncludeApl04Box;
        private System.Windows.Forms.CheckBox IncludeApl03Box;
        private System.Windows.Forms.CheckBox IncludeApl02Box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox IncludeApl01Box;
        private System.Windows.Forms.TextBox MessageKeyBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox EndTimeBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox StartTimeBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button RunQueryBtn;
        private System.Windows.Forms.DataGridView MessagesList;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel ProgressLabel;
        private System.Windows.Forms.ToolStripProgressBar ProgressBar;
        private System.Windows.Forms.CheckBox IncludePm3Box;
        private System.Windows.Forms.CheckBox IncludePm2Box;
        private System.Windows.Forms.CheckBox IncludePm1Box;
        private System.Windows.Forms.CheckBox IncludePP2Box;
        private System.Windows.Forms.Button GetMTTRBtn;
		private System.Windows.Forms.Button GetPPBtn;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
	}
}