﻿using System;
using System.Windows.Forms;

namespace MTTROverview
{
	public partial class ReportSelectionWindow : Form
	{
		private string ReportTitlePrefix = string.Empty;

		public ReportSelectionWindow() {
			InitializeComponent();
			Text = "MTTR Reporter v1.8 -- Select Time Range and Type of Report to Run ";
		}

		// -- Quick Time Frames
		private void ReportTypeLast24Hours_Click(Object sender, EventArgs e) {
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "Last 24 Hours");
			StartTimeBox.Text = DateTime.Now.AddHours(-24).ToString();
			EndTimeBox.Text = DateTime.Now.ToString();
		}

		private void ReportTypeLast12Hours_Click(Object sender, EventArgs e) {
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "Last 12 Hours");
			StartTimeBox.Text = DateTime.Now.AddHours(-12).ToString();
			EndTimeBox.Text = DateTime.Now.ToString();
		}

		private void ReportTypeLast7Days_Click(Object sender, EventArgs e) {
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "Last 7 Days");
			StartTimeBox.Text = DateTime.Now.AddDays(-7).ToString();
			EndTimeBox.Text = DateTime.Now.ToString();
		}

		private void ReportTypeLast15Days_Click(Object sender, EventArgs e) {
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "Last 15 Days");
			StartTimeBox.Text = DateTime.Now.AddDays(-15).ToString();
			EndTimeBox.Text = DateTime.Now.ToString();
		}

		private void ReportTypeLast30Days_Click(Object sender, EventArgs e) {
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "Last 30 Days");
			StartTimeBox.Text = DateTime.Now.AddDays(-30).ToString();
			EndTimeBox.Text = DateTime.Now.ToString();
		}

		private void ReportTypeToday_Click(Object sender, EventArgs e) {
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "Today");
			StartTimeBox.Text = DateTime.Now.Date.ToString();
			EndTimeBox.Text = DateTime.Now.ToString();
		}

		private void ReportTypeThisWeek_Click(Object sender, EventArgs e) {
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "This Week");
			// Find first day of the week
			var day = (int)DateTime.Now.DayOfWeek;

			StartTimeBox.Text = DateTime.Now.Date.AddDays(-day).ToString();
			EndTimeBox.Text = DateTime.Now.ToString();
		}

		private void ReportTypeThisMonth_Click(Object sender, EventArgs e) {
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "This Month");
			var date = DateTime.Now.Date.AddDays(-DateTime.Now.Day).ToString();
			StartTimeBox.Text = DateTime.Now.Date.AddDays(-(DateTime.Now.Day - 1)).ToString();
			EndTimeBox.Text = DateTime.Now.ToString();
		}

		private void ReportTypeLastShift_Click(Object sender, EventArgs e) {
			var now = DateTime.Now;
			var start = now;
			var end = now;

			if (now.Hour < 7) {
				// Last Day Shift
				end = now.Date.AddDays(-1).AddHours(18.5);
			} else if (now.Hour < 18.5) {
				// Last Night Shift
				end = now.Date.AddHours(6.5);
			} else {
				// Today's Day Shift
				end = now.Date.AddHours(18.5);
			}
			start = end.AddHours(-12);

			StartTimeBox.Text = start.ToString();
			EndTimeBox.Text = end.ToString();
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "Last Shift");
		}

		private void ReportTypeLast2Shifts_Click(Object sender, EventArgs e) {
			var now = DateTime.Now;
			var start = now;
			var end = now;

			if (now.Hour < 7) { // In Night Shift
								// Last Day -> Night Shift
				end = now.Date.AddDays(-1).AddHours(18.5);
			} else if (now.Hour < 18.5) { // In Day Shift
										  // Last Night -> Day Shift
				end = now.Date.AddHours(6.5);
			} else {
				// Today's Day -> Last Night's Night Shift
				end = now.Date.AddHours(18.5);
			}
			start = end.AddHours(-24);

			StartTimeBox.Text = start.ToString();
			EndTimeBox.Text = end.ToString();
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "Last 2 Shifts");
		}

		private void ReportTypeThisShift_Click(Object sender, EventArgs e) {
			var now = DateTime.Now;
			var start = now;
			var end = now;

			if (now.Hour < 7) { // In Night Shift
				start = now.Date.AddDays(-1).AddHours(18.5);
			} else if (now.Hour < 18.5) { // In Day Shift
				start = now.Date.AddHours(6.5);
			} else {
				start = now.Date.AddHours(18.5);
			}
			end = now;

			StartTimeBox.Text = start.ToString();
			EndTimeBox.Text = end.ToString();
			ReportTitleBox.Text = string.Format("{0}{1}", ReportTitlePrefix, "This Shift");
		}

		// -- Run Reports
		private void RunOverviewReport_Click(Object sender, EventArgs e) {
			VerifyQueryTimes(sender, e, out DateTime start, out DateTime end);
			if (string.IsNullOrWhiteSpace(ReportTitleBox.Text)) {
				ReportTitleBox.Text = CreateReportTitle(start, end);
			}

			var data = Utility.QueryCSVFilesForRows(start, end);
			var win = new OverviewForm(ReportTitleBox.Text, start, end, data);
			win.Show();
		}

		private void RunDetailReport_Click(Object sender, EventArgs e) {
			VerifyQueryTimes(sender, e, out DateTime start, out DateTime end);
			if (string.IsNullOrWhiteSpace(ReportTitleBox.Text)) {
				ReportTitleBox.Text = CreateReportTitle(start, end);
			}

			var data = Utility.QueryCSVFilesForMessages(start, end);
			var sorted = Utility.SortLists(data);
			var rows = Utility.CreateRowsFromQuery(start, end, sorted);
			var win = new DetailReportForm(ReportTitleBox.Text, data, rows);
			win.Show();
		}

		private void RunMonthlyReport_Click(Object sender, EventArgs e) {
			VerifyQueryTimes(sender, e, out DateTime start, out DateTime end);
			
			var win = new TopDowntimeReportForm(start, end);
			win.Show();
		}

		private void RunMessageQueryBtn_Click(Object sender, EventArgs e) {
			VerifyQueryTimes(sender, e, out DateTime start, out DateTime end);
			if (string.IsNullOrWhiteSpace(ReportTitleBox.Text)) {
				ReportTitleBox.Text = CreateReportTitle(start, end);
			}

			var win = new MessageQueryForm(start, end);
			win.Show();
		}

		private void GetPMAvailabityBtn_Click(Object sender, EventArgs e) {
			VerifyQueryTimes(sender, e, out DateTime start, out DateTime end);
			if (string.IsNullOrWhiteSpace(ReportTitleBox.Text)) {
				ReportTitleBox.Text = CreateReportTitle(start, end);
			}

			var win = new GetPMTimeForm(start, end);
			win.Show();
		}

		// -- Utility ---
		private string CreateReportTitle(DateTime start, DateTime end) {
			var diff = (end - start);

			if (diff.TotalHours < 37) {
				return string.Format("{0} + {1} hours", start, diff.TotalHours);
			}
			if (diff.TotalDays < 7) {
				return string.Format("{0} + {1} days", start, diff.TotalDays);
			}
			return string.Format("{0} to {1}", start, end);
		}

		private void VerifyQueryTimes(Object sender, EventArgs e, out DateTime start, out DateTime end) {
			if (!DateTime.TryParse(StartTimeBox.Text, out start) ||
							!DateTime.TryParse(EndTimeBox.Text, out end)) {
				// If we can't parse the dates, parse them as 24 hours (Default Report)
				ReportTypeLast12Hours_Click(sender, e);
				start = DateTime.Parse(StartTimeBox.Text);
				end = DateTime.Parse(EndTimeBox.Text);
			}
		}

		private void ReportTypeTesting_Click(Object sender, EventArgs e) {
			StartTimeBox.Text = "8/16/2018 8:00:00 AM";
			EndTimeBox.Text = "8/16/2018 8:00:00 PM";

			ReportTitleBox.Text = "Testing";
		}

		private void StatusCheckBtn_Click(Object sender, EventArgs e) {
			new FileStatusForm().Show();
		}
	}
}
