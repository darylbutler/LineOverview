﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace MTTROverview
{
	internal class TimeSeries : Panel
	{
		private Color CyclingColor = Color.Yellow;
		private Color CyclingBorder = Color.DarkGoldenrod;
		private Color ProductiveColor = Color.Green;
		private Color ProductiveBorder = Color.DarkGreen;
		private Color FaultedColor = Color.Red;
		private Color FaultedBorder = Color.DarkRed;
		private Color LotChangeColor = Color.CornflowerBlue;
		private Color LotChangeBorder = Color.DarkBlue;
		private Color DefaultColor = Color.Gray;
		private Color DefaultBorder = Color.DarkGray;
		private Color BackgroundColor = Color.AntiqueWhite;
		private Color TextColor = Color.Black;

		private Color MarkerColor = Color.DarkOrchid;

		private const bool SeperationBetweenGroups = true;
		private const float SeperationSpacing = 10.0f;
		private const int TileWidth = 10;
		private const int TileHeight = 16;
		private const float TileSpacing = 2.0f;
		private const float RowSpacing = 2.0f;
		private const float TileBorderWidth = 2.0f;
		private const int RightMargin = 100;
		private const int BottomMargin = 20;

		private int TimeInterval = 5;   // interval for dates
		private int LeftMargin = 100;   // Calculated in DrawImage()
		private Bitmap Image;
		private List<ToolTipArea> HotSpots = new List<ToolTipArea>();
		private List<ToolTip> ToolTips = new List<ToolTip>();
		private bool VerticalCursorMouseIsOver = false;
		private int VerticalCursorMousePosition = -1;
		private List<AnimationLine> Markers = new List<AnimationLine>();

		public DateTime Start = DateTime.MaxValue, End = DateTime.MinValue;
		public List<TimeSeriesRow> Series = new List<TimeSeriesRow>();

		public delegate void TimeClick(DateTime dt);

		public TimeClick TimeClicked;

		public TimeSeries() {
			AutoScroll = true;
			DoubleBuffered = true;

			MouseMove += TimeSeries_MouseMove;
			MouseLeave += TimeSeries_MouseLeave;
			MouseDown += TimeSeries_MouseDown;
		}

		// -- Tooltips / Mouse Movement -------------------------------------------
		private ToolTip ToolTip;

		private ToolTipArea CurrentToolTipArea;

		private void TimeSeries_MouseDown(object sender, MouseEventArgs e) {
			//if (TimeClicked == null)
			//    return;

			int x = e.X + Math.Abs(AutoScrollPosition.X);
			int y = e.Y + Math.Abs(AutoScrollPosition.Y);

			x -= 110;   // Subtract left margin
			int seg = (int)(x / (TileWidth + TileSpacing));

			DateTime clicked = Start.AddMinutes(seg * TimeInterval);
			TimeClicked?.Invoke(clicked);
		}

		private void TimeSeries_MouseLeave(object sender, EventArgs e) {
			// Clear the current hotspot
			ToolTip?.Dispose();
			ToolTip = null;
			CurrentToolTipArea = null;

			if (VerticalCursorMouseIsOver) {
				VerticalCursorMouseIsOver = false;
				VerticalCursorMousePosition = -1;
				Refresh();
			}
		}

		private void TimeSeries_MouseMove(object sender, MouseEventArgs e) {
			int x = e.X + Math.Abs(AutoScrollPosition.X);
			int y = e.Y + Math.Abs(AutoScrollPosition.Y);

			if (x > 110 && x < AutoScrollMinSize.Width &&
				y >= 0 && y < AutoScrollMinSize.Height + 20) {
				VerticalCursorMouseIsOver = true;
				VerticalCursorMousePosition = x;
				Refresh();
			} else if (VerticalCursorMouseIsOver) {
				VerticalCursorMouseIsOver = false;
				Refresh();
			}

			if (ToolTip != null && CurrentToolTipArea?.HotSpot.Contains(x, y) == true) {
				return;
			}

			// Clear the current hotspot
			ToolTip?.Hide(this);
			ToolTip = null;
			CurrentToolTipArea = null;

			// See if this is over another hotspot
			foreach (var area in HotSpots) {
				if (area.HotSpot.Contains(x, y)) {
					ToolTip = new ToolTip() {
						ToolTipTitle = area.Title,
						ToolTipIcon = ToolTipIcon.Info,
					};
					CurrentToolTipArea = area;
					ToolTip.Show(area.ToolTip, this, e.X, (int)area.HotSpot.Y + TileWidth + 2 + AutoScrollPosition.Y);
					return;
				}
			}
		}

		// -- Vertical Cursor Marekrs ---------------------------------------------
		public void CreateMarker(DateTime time) {
			// Convert DateTime into an x
			int x = 110;   // Start at left margin
			var minutes = (int)((time - Start).TotalMinutes / TimeInterval);    // Distance from Start in Minutes
			x += minutes * (int)(TileWidth + TileSpacing);

			if (x < 110) x = 110;
			if (x > AutoScrollMinSize.Width) x = AutoScrollMinSize.Width;

			// Prevent multiple markers for the same X
			if (Markers.Find(t => t.X == x) != null) return;

			// Brind the X into view if it is out
			if (x < Math.Abs(AutoScrollPosition.X) || x > (Math.Abs(AutoScrollPosition.X) + Width)) {
				AutoScrollPosition = new Point(
						Math.Min(Math.Max(0, x - (Width / 2)), AutoScrollMinSize.Width - Width),
						AutoScrollPosition.Y
					);
			}

			var line = new AnimationLine() {
				Parent = this,
				X = x
			};
			line.Start();
			Markers.Add(line);
		}

		// -- Public Update -------------------------------------------------------
		public void Redraw() {
			if (Image != null) {
				Image.Dispose();
				Image = null;
			}
			Refresh();
		}

		// -- Updating / Painting -------------------------------------------------
		public void UpdateSpan() {
			if (Start == DateTime.MaxValue && End == DateTime.MinValue) {
				// Find the Ranges
				Start = DateTime.MaxValue;
				End = DateTime.MinValue;
				if (Series.Count > 0) {
					foreach (var s in Series) {
						foreach (var l in s.Productive.Concat(s.Cycling).Concat(s.Faults)) {
							if (l.Start < Start)
								Start = l.Start;
							if (l.Start > End)
								End = l.Start;
						}
					}
				}

				if (Start == DateTime.MaxValue || End == DateTime.MinValue) {
					Start = End = DateTime.Now;
				}
			}

			End = new DateTime(
				End.Year,
				End.Month,
				End.Day,
				End.Hour,
				(End.Minute / TimeInterval) * TimeInterval,
				0);
			Start = new DateTime(
				Start.Year,
				Start.Month,
				Start.Day,
				Start.Hour,
				(Start.Minute / TimeInterval) * TimeInterval,
				0);

			int intervals = (int)(((End - Start).TotalMinutes / TimeInterval) + 0.5f);

			// -- Figure out Left Margin
			if (Series.Count > 0) {
				var maxTitleLength = Series.Max(x => x.Title.Length);
				var maxTitle = Series.Where(x => x.Title.Length == maxTitleLength).Select(x => x.Title).FirstOrDefault();

				var t = new Bitmap(100, 100);
				Graphics g = Graphics.FromImage(t);
				LeftMargin = (int)g.MeasureString(maxTitle, DefaultFont).Width + 10;
			} else {
				LeftMargin = 100;
			}

			// -- Create the Area
			if (!SeperationBetweenGroups) {
				AutoScrollMinSize = new Size(
						LeftMargin + ((int)(TileWidth + TileSpacing) * intervals) + RightMargin,       // Width
						70 + ((int)(TileHeight + RowSpacing) * Series.Count) + BottomMargin     // Height
					);
			} else {
				float extraSeperation = (Series.Select(x => x.Group).Distinct().Count() - 1) * SeperationSpacing;
				AutoScrollMinSize = new Size(
						LeftMargin + ((int)(TileWidth + TileSpacing) * intervals) + RightMargin,                           // Width
						70 + ((int)(TileHeight + RowSpacing) * Series.Count) + BottomMargin + (int)extraSeperation  // Height
					);
			}
		}

		private void DrawToImage() {
			UpdateSpan();
			ToolTips = new List<ToolTip>();
			HotSpots = new List<ToolTipArea>();
			Image = new Bitmap(AutoScrollMinSize.Width, AutoScrollMinSize.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

			Graphics g = Graphics.FromImage(Image);

			g.FillRectangle(new SolidBrush(BackgroundColor), new Rectangle(0, 0, AutoScrollMinSize.Width, AutoScrollMinSize.Height));
			float y = 10, x = 10;

			// -- Draw the dates & times
			x = LeftMargin + 10;    // Left is for axis titles
			Pen AxisPen = new Pen(Color.Gray);
			for (DateTime time = Start; time <= End; time = time.AddMinutes(TimeInterval)) {
				if ((time.Hour == 0 && time.Minute == 0 && time.Second == 0) || time == Start || time >= End) {
					// Draw the date
					var dim = CenterString(time.ToShortDateString(), x, 10, DefaultFont, Brushes.Gray, g);
				}
				if ((time.Minute == 0 && time.Second == 0) || time == Start || time >= End) {
					// Draw the hour
					var dim = CenterString(time.ToShortTimeString(), x, 25, DefaultFont, Brushes.Gray, g);
					// Draw a line since we don't draw a 0
					g.DrawLine(AxisPen, x, 40, x, 40 + dim.Height + 2);
				} else if (time.Minute % 15 == 0) {
					// Draw the time
					var dim = CenterString(time.Minute.ToString(), x, 40, DefaultFont, Brushes.Gray, g);
					y = 40 + dim.Height + 1;

					// Draw the line
					g.DrawLine(AxisPen, x - 1 - (TileWidth * 0.5f), y, x + 1 + (TileWidth * 0.5f), y);
				} else {
					// Don't draw the time
					var dim = g.MeasureString(time.Minute.ToString(), DefaultFont);
					y = 40 + dim.Height + 1;

					// Draw the line
					g.DrawLine(AxisPen, x - 1 - (TileWidth * 0.5f), y, x + 1 + (TileWidth * 0.5f), y);
				}
				x += TileWidth + TileSpacing;
			}

			// Draw each series
			y += 10;    // Padding from top axis
			string currentGroup = string.Empty;
			foreach (var s in Series) {
				// Check if we need to move down (Show Seperation between groups)
				if (SeperationBetweenGroups && currentGroup != string.Empty && currentGroup != s.Group) {
					y += SeperationSpacing;
				}
				currentGroup = s.Group;

				// Draw title
				Font TitleFont = DefaultFont;
				var dim = g.MeasureString(s.Title, TitleFont);
				g.DrawString(s.Title,
					TitleFont,
					new SolidBrush(TextColor),
					10,
					y + ((TileHeight - dim.Height) * 0.5f));
				x = (LeftMargin + 10) - (TileWidth * 0.5f);

				// Traverse the time axis
				SeriesState State = SeriesState.NotRunning;
				DateTime StateTimeStart = Start;
				float StateTimeStartX = x;
				string StateFault = string.Empty;
				for (DateTime time = Start; time <= End; time = time.AddMinutes(TimeInterval)) {
					Color fill, border;
					string tooltip = string.Empty;
					SeriesState currState;

					if (ContainedIn(time, s.LotChange)) {   // TODO - Moved lot change to cover faults, do we like this?
						fill = LotChangeColor;
						border = LotChangeBorder;
						currState = SeriesState.LotChange;
					} else if (ContainedIn(time, s.Faults)) {
						fill = FaultedColor;
						border = FaultedBorder;
						currState = SeriesState.Faulted;
					} else if (ContainedIn(time, s.Productive)) {
						fill = ProductiveColor;
						border = ProductiveBorder;
						currState = SeriesState.Productive;
					} else if (ContainedIn(time, s.Cycling)) {
						fill = CyclingColor;
						border = CyclingBorder;
						currState = SeriesState.Cycling;
					} else {
						fill = DefaultColor;
						border = DefaultBorder;
						currState = SeriesState.NotRunning;
					}

					// Connect the tiles together if we're not changing states
					if (State != currState || time == Start) {
						// Draw the Tile as disconnected
						g.FillRectangle(new SolidBrush(fill),
						x,
						y,
						TileWidth,
						TileHeight);
						g.DrawRectangle(new Pen(border, TileBorderWidth),
							x + (TileBorderWidth * 0.5f),
							y + (TileBorderWidth * 0.5f),
							TileWidth - TileBorderWidth,
							TileHeight - TileBorderWidth);
					} else {
						// Connect this tile with the last
						g.DrawRectangle(new Pen(border, TileBorderWidth),
							x + (TileBorderWidth * 0.5f) - TileSpacing,
							y + (TileBorderWidth * 0.5f),
							TileWidth - TileBorderWidth + TileSpacing,
							TileHeight - TileBorderWidth);
						g.FillRectangle(new SolidBrush(fill),
							x - TileSpacing - TileBorderWidth,
							y + TileBorderWidth,
							TileWidth + TileSpacing,
							TileHeight - (TileBorderWidth * 2));
					}

					// Catch the end of a run
					if (State != currState || time == End) {
						var fault = FindEventIn(time.AddMinutes(-TimeInterval), s.Faults);
						string TimeStr = string.Format("\n\tOp Response Time: {0}\n\tRecoveryTime: {1}{2}",
							Utility.PrettyTimeSpan(fault?.OPRespTime),
							Utility.PrettyTimeSpan(fault?.RecoveryTime),
							(fault?.IsOpEvent == true) ? string.Empty : string.Format("\n\tEM Response Time: {0}\n\tRepair Time: {1}",
								Utility.PrettyTimeSpan(fault?.EMRespTime),
								Utility.PrettyTimeSpan(fault?.RepairTime))
							);
						HotSpots.Add(new ToolTipArea() {
							HotSpot = new RectangleF(StateTimeStartX, y, (x - TileSpacing) - StateTimeStartX, TileWidth),
							ToolTip = string.Format("{0}\n{1}: {2} to {3}\nDuration: {4}\n{5}{6}{7}",
														State.ToString(),
														StateTimeStart.ToShortDateString(), // djb: Added Date to tooltip
														fault != null ? fault.Start.ToShortTimeString() : StateTimeStart.ToShortTimeString(),
														fault != null ? fault.End.ToShortTimeString() : time.ToShortTimeString(),
														Utility.PrettyTimeSpan(fault != null ? fault?.Duration : time - StateTimeStart),
														fault != null ? string.Format("Shift: {0}", fault.Shift) : string.Empty,
														State == SeriesState.Faulted ? string.Format("\n{0}", fault?.Message) : string.Empty,
														State == SeriesState.Faulted ? TimeStr : string.Empty
														),
							Title = s.Title,
						});
						State = currState;
						StateTimeStart = time;
						StateTimeStartX = x;
					}

					x += TileWidth + TileSpacing;
				}

				// Move to new Row
				y += TileHeight + RowSpacing;
			}
		}

		protected override void OnPaint(PaintEventArgs e) {
			// Redraw everything if we need to
			if (Image == null)
				DrawToImage();

			// Translate the image to the scroll area
			e.Graphics.TranslateTransform(AutoScrollPosition.X, AutoScrollPosition.Y);
			if (Image != null) e.Graphics.DrawImage(Image, 0, 0);

			// Vertical Mouse Cursor
			if (VerticalCursorMouseIsOver) {
				e.Graphics.TranslateTransform(0, 0);
				e.Graphics.DrawLine(new Pen(Color.DarkGray),
					VerticalCursorMousePosition,
					55,
					VerticalCursorMousePosition,
					AutoScrollMinSize.Height - BottomMargin);
			}

			// Draw any markers
			List<AnimationLine> doneMarkers = new List<AnimationLine>();
			foreach (var marker in Markers) {
				if (!marker.Done) {
					e.Graphics.TranslateTransform(0, 0);
					e.Graphics.DrawLine(new Pen(Color.FromArgb(marker.Alpha, MarkerColor), 3.0f),
						marker.X,
						55,
						marker.X,
						AutoScrollMinSize.Height - BottomMargin);
				} else {
					doneMarkers.Add(marker);
				}
			}
			foreach (var m in doneMarkers) {
				Markers.Remove(m);
			}

			base.OnPaint(e);
		}

		// -- Helpers -------------------------------------------------------------
		private SizeF CenterString(string text, float x, float y, Font font, Brush brush, Graphics g) {
			// x, y should be mid points
			var dim = g.MeasureString(text, font);
			g.DrawString(text, font, brush, x - (dim.Width / 2), y);
			return dim;
		}

		private Event FindEventIn(DateTime time, List<Event> events) {
			foreach (var e in events) {
				// Round the times down
				var start = new DateTime(e.Start.Year, e.Start.Month, e.Start.Day, e.Start.Hour, TimeInterval * (e.Start.Minute / TimeInterval), 0);
				var end = new DateTime(e.End.Year, e.End.Month, e.End.Day, e.End.Hour, TimeInterval * (e.End.Minute / TimeInterval), 0);
				if (start <= time && end >= time)
					return e;
			}
			return null;
		}

		private bool ContainedIn(DateTime time, List<Event> events) {
			return FindEventIn(time, events) != null;
		}
	}

	internal enum SeriesState
	{ NotRunning, Faulted, LotChange, Productive, Cycling }

	public class TimeSeriesRow
	{
		public string Title;
		public string Group;

		public List<Event> Cycling = new List<Event>(),
							Productive = new List<Event>(),
							Faults = new List<Event>(),
							LotChange = new List<Event>();
	}

	internal class ToolTipArea
	{
		public RectangleF HotSpot;
		public String ToolTip, Title;
	}
}
