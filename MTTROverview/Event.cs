﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MTTROverview
{
	public class Event
	{
		public string Area { get; set; }
		public string Message { get; set; }

		public string TrimmedMessage {
			get {
				if (Message.StartsWith("Unknown"))
					return Message;

				// If 6-, trim the front of the string (e.x.: "Electric CLIFA01_LeerWT CT Lifter - Lack of CTs (PLC: 1334)" into "CT Lifter - Lack of CTs (PLC: 1334)"
				if (APL == 1 || APL == 2) {
					var msg = Message.Substring(Message.IndexOf(Tag) + Tag.Length);
					if (msg.EndsWith("fault", StringComparison.OrdinalIgnoreCase))
						msg = msg.Substring(0, msg.Length - 5);
					if (msg.EndsWith("warning", StringComparison.OrdinalIgnoreCase))
						msg = msg.Substring(0, msg.Length - 7);
					return msg.Trim();
				}

				return Message;
			}
		}

		public int APL {
			get {
				if (Area.StartsWith("PM") && int.TryParse(Area.Substring(2, Area.Length - 4), out int pm)) {
					return ((pm - 1) / 3) + 1;
				}
				if (Area.StartsWith("PP") && int.TryParse(Area.Substring(2), out int pp)) {
					return ((pp - 1) / 2) + 1;
				}
				if (Area.Length > 1 && Area[1] == 'M' && int.TryParse(Area.Substring(2), out int mm)) {
					return ((mm - 1) / 3) + 1;
				}
				return 0;
			}
		}

		public string Machine {
			get {
				if (Area.StartsWith("PM"))  // PM10MM
					return Area.Substring(Area.Length - 2);
				if (Area.StartsWith("PP"))  // PP1T2
					return "PP";
				if (Area.Length > 1 && Area[1] == 'M') // MM10
					return Area.Substring(0, 2);

				return "??";
			}
		}

		private DateTime _start, _end;

		public DateTime Start {
			get {
				return _start;
			}
			set {
				_start = value;
				shift_inner = string.Empty;
			}
		}

		public DateTime End {
			get {
				return _end;
			}
			set {
				_end = value;
				shift_inner = string.Empty;
			}
		}

		public TimeSpan in_OPRespTime, in_RecoveryTime, in_EMRespTime, in_RepairTime;

		public string Tag {
			get {
				return _tag;
			}
			set {
				if (value == null) {
					_tag = string.Empty;
				} else if (value.Length <= 14 || value.StartsWith("Unknown ")) {
					_tag = value;
				} else if (value.StartsWith("APL0")) {
					if (value.Length > 26) {
						_tag = value.Trim().Substring(12, 14);  // Chomp off both sides of the tag
					} else {
						_tag = value.Trim().Substring(12);      // Chmp off the Module specific parts of the tag
					}
				} else {
					if (value.Length > 14) {
						_tag = value.Trim().Substring(0, 14); // Chomp off any post text like '_ala' or '_bad'
					} else {
						_tag = value.Trim();
					}
				}
			}
		}

		private string _tag = string.Empty;

		public TimeSpan Duration => End - Start;
		public string PrettyDuration => Utility.PrettyTimeSpan(Duration);

		public TimeSpan OPRespTime {
			get {
				if (in_RecoveryTime.TotalMilliseconds < 1 && in_EMRespTime.TotalMilliseconds < 1 && in_RepairTime.TotalMilliseconds < 1)
					return PreventNegitive(in_OPRespTime - new TimeSpan(0, 10, 0));
				return PreventNegitive(in_OPRespTime);
			}
		}

		public TimeSpan RecoveryTime {
			get {
				if (IsOpEvent)
					return PreventNegitive(in_RecoveryTime - new TimeSpan(0, 10, 0));
				return PreventNegitive(in_RecoveryTime);
			}
		}

		public TimeSpan EMRespTime => PreventNegitive(in_EMRespTime);

		public TimeSpan RepairTime {
			get {
				if (in_RepairTime.TotalMilliseconds > 0) {
					return PreventNegitive(in_RepairTime - new TimeSpan(0, 10, 0));
				}
				return PreventNegitive(in_RepairTime);
			}
		}

		public bool IsOpEvent => !IsEMEvent;

		public bool IsEMEvent => EMRespTime.TotalMilliseconds > 0 ||
									RepairTime.TotalMilliseconds > 0;

		private TimeSpan PreventNegitive(TimeSpan time) {
			if (time.TotalMilliseconds < 0)
				return TimeSpan.Zero;
			return time;
		}

		// -- Message Classification --------------------------------------------------------------
		public bool IsMTTRMsg => _tag.StartsWith("RPIMTTR");

		public bool IsMTTRFaultEventActiveMsg => _tag == "RPIMTTR_FAULTD";

		public bool IsCyclingMsg => Tag.StartsWith("RPIMTTR_INAUTO") ||
									Tag.StartsWith("RPIMTTR_T1AUTO") ||
									Tag.StartsWith("RPIMTTR_T2AUTO") ||
									Tag.StartsWith("RPIMTTR_T3AUTO");

		public bool IsProductiveMsg => Tag.StartsWith("RPIMTTR_INPROD") ||
										Tag.StartsWith("RPIMTTR_T1PROD") ||
										Tag.StartsWith("RPIMTTR_T2PROD") ||
										Tag.StartsWith("RPIMTTR_T3PROD");

		public bool IsLotChangeMsg => _tag.StartsWith("H ") || _tag == "AV1AV10_ADVICE"; // Lot end Message to Scada

		public bool IsAFaultMessage {
			get {
				if (Machine == "PP") {
					return !IsMTTRMsg
						&& !Message.Trim().StartsWith("H ")
						&& !IsAnActionMessage
						&& !Utility.ExcludeTags.Contains(Tag)
						&& Message.IndexOf("reserve", StringComparison.OrdinalIgnoreCase) < 0               // PP uses some unnamed faults for stopping the line, ignroe these
						&& !Message.Contains("Safety Door") && !Message.Contains("Failure to Lock Door")     // Exclude Safety door faults
						&& !Utility.SkipPPTags.Contains(Tag)
						&& !Tag.StartsWith("TK1SD0_") && !Tag.StartsWith("TK2SD0_") && !Tag.StartsWith("TK3SD0_")
						&& true;
				} else {
					var SecondaryFaults = new List<string> {
                    // 6-
                    "CB1DI13_CLEANM",   // MISC CB1DI13_CLEANM Cleaning Module CM - Error
                    "CB1DI14_CLEANM",   // MISC CB1DI14_CLEANM Cleaning module CM - not ready
                    "CB1DI02_SDOORS",   // Safety Doors SD - Not Locked
                    "SC1SE01_301A46",   // All Safety Contactors Closed
                    "CB1GW03_2425LP",   // Safety
                    "CB1FA01_150QA1",   // Doors Not Closed
                    "CB1FA01_S10422",   // TM MediaMissing

                    // 7+
                    "SC1CB01_I09F04",   // Circuit breaker 24VDC outputs: 9F4
                };
					var AllowedWarnings = new List<string> {
					"GENWARN_W00520",   // 7+ Leakage Sensor Warnings -- We explicitley map this from warning to fault with MTTR
                    "GENWARN_W00521",   // 7+ Leakage Sensor Warnings -- We explicitley map this from warning to fault with MTTR
                    "GENWARN_W00522",   // 7+ Leakage Sensor Warnings -- We explicitley map this from warning to fault with MTTR
                    "GENWARN_W00523",   // 7+ Leakage Sensor Warnings -- We explicitley map this from warning to fault with MTTR
                    "GENWARN_W00524",   // 7+ Leakage Sensor Warnings -- We explicitley map this from warning to fault with MTTR
                    "GENWARN_W00525",   // 7+ Leakage Sensor Warnings -- We explicitley map this from warning to fault with MTTR
                    "GENWARN_W00581",   // 7+ Tank Farm Error -- We explicitley map this from warning to fault with MTTR
                    "GENMESG_M00758",   // 7+ Tank Farm Not Ready -- We explicitley map this from warning to fault with MTTR
                    "IVSFA29_ADVICE",   // Camera System offline
                    "CLIFA01_LeerWT",   // TM's Lack of CTs (6-) -- We explicitley map this from warning to fault with MTTR
                    "CB1CB01_ADVICE",   // IM Control Voltage Off - This warning comes up when the EM door is opened while the IM is running
                    "CB1AV13_LATMOS",   // MM -  LA Error (Does fault the line)
                };

					return ((!Message.StartsWith("H ") && !IsAWarningMessage && !IsAnActionMessage) || AllowedWarnings.Contains(_tag))
							&& !IsMTTRMsg
							&& !SecondaryFaults.Contains(_tag);
				}
			}
		}

		public bool IsAnActionMessage {
			get {
				var split = Message.Split(' ');
				if (split.Length < 2) return false;

				var name = split.Last();

				if (name.Length < 1) return false;
				if (name == "SYSTEM" || name == "WILLIKEM") return true;

				// Last character must be a number
				if (!char.IsDigit(name.Last())) return false;

				foreach (var c in name.Substring(0, name.Length - 1)) {
					if (!char.IsLetter(c)) return false;
				}
				return true;

				// xxxxxxxxxxx fivetwo1
				//            123456789
				//if (Message.Length > 9 && Message.Last() >= '0' && Message.Last() <= '9' && Message[Message.Length - 9] == ' ') {
				//	// Check that the user string is all caps
				//	string user = Message.Substring(Message.Length - 8, 7);
				//	return user.ToUpper() == user;
				//}
				//return false;
			}
		}

		public bool IsAWarningMessage => Message.Contains("Warning") || _tag.EndsWith("ADVICE") || (APL > 2 && Utility.WarningTags.Contains(_tag));

		// -- Shifts ------------------------------------------------------------------------------
		private string shift_inner = string.Empty;

		public string Shift {
			get {
				if (shift_inner == string.Empty) {
					var shiftStart = GetShiftFromTime(Start).ToString().ToUpper();
					var shiftEnd = GetShiftFromTime(End).ToString().ToUpper();

					// Time less than 1 hour, just return the shift of the start time
					if (Duration.TotalHours < 24 && shiftStart == shiftEnd) {
						shift_inner = shiftStart;
					} else {
						// TODO should we waste resources on multiple shifts? -- Yes, we should
						var t = Start;
						var shifts = new List<char> { GetShiftFromTime(t) };
						while (t <= End) {
							t = t.AddHours(11.5);
							var s = GetShiftFromTime(t);
							if (!shifts.Contains(s)) {
								shifts.Add(s);
							}
						}
						shift_inner = string.Join(" -> ", shifts);
					}
				}
				return shift_inner;
			}
		}

		private char GetShiftFromTime(DateTime time) {
			TimeSpan DayShiftBegin = new TimeSpan(6, 30, 0);
			TimeSpan DayShiftEnd = new TimeSpan(18, 30, 0);

			if (time.TimeOfDay < DayShiftBegin) {
				time = time.Date.AddDays(-1).AddHours(22);  // Make time the previous day in the night shift for this calculation
			}

			char[] days_w1 = { 'H', 'H', 'J', 'J', 'H', 'H', 'J' };
			char[] days_w2 = { 'J', 'J', 'H', 'H', 'J', 'J', 'H' };
			char[] nights_w1 = { 'I', 'K', 'K', 'I', 'I', 'K', 'K' };
			char[] nights_w2 = { 'K', 'I', 'I', 'K', 'K', 'I', 'I' };

			int weeks = ((int)(time.Date - new DateTime(2018, 1, 14)).TotalDays) / 7;

			if (weeks % 2 == 0) {
				return time.TimeOfDay >= DayShiftBegin && time.TimeOfDay < DayShiftEnd ?
					days_w1[(int)time.DayOfWeek] : nights_w1[(int)time.DayOfWeek];
			} else {
				return time.TimeOfDay >= DayShiftBegin && time.TimeOfDay < DayShiftEnd ?
					days_w2[(int)time.DayOfWeek] : nights_w2[(int)time.DayOfWeek];
			}
		}

		public override Int32 GetHashCode() => GetHash().GetHashCode();
		public string GetHash() => string.Format("{0}{1}{2}", Tag, Start.ToBinary(), Message);
		public override String ToString() => string.Format("{0}-{1}__{2} ({3})", Start, End, TrimmedMessage, Tag);
	}
}
