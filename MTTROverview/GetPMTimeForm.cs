﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MTTROverview
{
	public partial class GetPMTimeForm : Form
	{
		private Task<List<Utility.PMInstance>> QueryTask = null;
		private CancellationTokenSource CancelTask;

		public GetPMTimeForm(DateTime start, DateTime end) {
			InitializeComponent();

			StartTimeBox.Text = start.ToString();
			EndTimeBox.Text = end.ToString();
			DateTimeBox_DoneEditing(StartTimeBox, null);
			DateTimeBox_DoneEditing(EndTimeBox, null);
			PMGrid.AutoGenerateColumns = false;
		}

		private void DateTimeBox_TextChanged(Object sender, EventArgs e) {
			var box = (sender as TextBox);

			if (DateTime.TryParse(box.Text, out DateTime r)) {
				box.BackColor = Color.LightGreen;
				box.Tag = r.Date;
			} else {
				box.BackColor = Color.LightPink;
				box.Tag = null;
			}
		}

		private void DateTimeBox_DoneEditing(Object sender, EventArgs e) {
			var box = (sender as TextBox);

			if (box.Tag != null) {
				var d = (DateTime)box.Tag;

				if (box == StartTimeBox) {
					box.Tag = new DateTime(d.Year, d.Month, 1);
				} else {
					box.Tag = new DateTime(d.Year, d.Month, 1).AddMonths(1).AddMilliseconds(-1);
				}
				box.Text = ((DateTime)box.Tag).ToShortDateString();
			}
		}

		private void DateTimeBox_KeyDown(Object sender, KeyEventArgs e) {
			if (e.KeyData == Keys.Enter) {
				DateTimeBox_DoneEditing(sender, null);
			}
		}

		private void IncludePMCheckboxClick(Object sender, EventArgs e) {
			var state = true;
			if (ModifierKeys == Keys.Shift) {
				state = true;  // Select All
			} else if (ModifierKeys == Keys.Control) {
				state = false;   // Select None
			} else return;

			IncludePm1Box.Checked =
			IncludePm2Box.Checked =
			IncludePm3Box.Checked =
			IncludePm4Box.Checked =
			IncludePm5Box.Checked =
			IncludePm6Box.Checked =
			IncludePm7Box.Checked =
			IncludePm8Box.Checked =
			IncludePm9Box.Checked =
			IncludePm10Box.Checked =
			IncludePm11Box.Checked =
			IncludePm12Box.Checked =
				state;
		}

		private async void RunQueryBtn_Click(Object sender, EventArgs e) {
			if (QueryTask != null && CancelTask != null) {
				CancelTask.Cancel();
				return;
			}

			// Check for Correct Dates
			if (StartTimeBox.Tag == null || EndTimeBox.Tag == null) {
				MessageBox.Show("Cannot parse query times.");
				return;
			}
			var start = (DateTime)StartTimeBox.Tag;
			var end = (DateTime)EndTimeBox.Tag;

			// Build the PM Module List
			var modules = GetSelectedModules();

			// Mark UI As Busy
			RunQueryBtn.Text = "Cancel...";
			ProgressLabel.Text = "Query In Progress...";
			ProgressBar.Style = ProgressBarStyle.Marquee;

			// Setup the task to get the Data
			CancelTask = new CancellationTokenSource();
			QueryTask = Task.Run(async () => {
				return await Task.Run(() => {
					return Utility.GetPMTimes(start, end, modules, CancelTask.Token);
				}, CancelTask.Token);
			}, CancelTask.Token);

			// Await the data
			var sw = System.Diagnostics.Stopwatch.StartNew();
			var res = await QueryTask;
			sw.Stop();

			// Update UI
			ProgressLabel.Text = "Updating UI...";
			Application.DoEvents();
			PopulateGrid(res);

			// Mark UI As Done
			RunQueryBtn.Text = "Query";
			if (!CancelTask.IsCancellationRequested) {
				ProgressLabel.Text = string.Format("Query Completed. Took {0}m ({1}s).", sw.Elapsed.TotalMinutes, sw.Elapsed.TotalSeconds);
			} else {
				ProgressLabel.Text = string.Format("Query Canceled. Took {0}m ({1}s).", sw.Elapsed.TotalMinutes, sw.Elapsed.TotalSeconds);
			}
			ProgressBar.Style = ProgressBarStyle.Continuous;
			ProgressBar.Value = 0;

			// Cleanup
			QueryTask.Dispose();
			QueryTask = null;
			CancelTask.Dispose();
			CancelTask = null;
		}

		private void PopulateGrid(List<Utility.PMInstance> times) {
			PMGrid.Rows.Clear();

			// Sort the results by date
			times.Sort((x, y) => x.Date.CompareTo(y.Date));

			// Fill in each PM
			for (int i = 1; i < 15; i++) {
				var module = string.Format("PM{0}", i);     // format as PM1 - PM15

				foreach (var mod in times.Where(x => x.PM == module).Select(x => x)) {
					if (mod.TotalTime <= TimeSpan.Zero) {
						PMGrid.Rows.Add(mod.PM, GetMonthName(mod.Date), mod.CyclingTime, "0%", mod.ProductiveTime, "0%", mod.FaultedTime, "0%", mod.TotalTime);
					} else {
						PMGrid.Rows.Add(mod.PM, GetMonthName(mod.Date),
							mod.CyclingTime, string.Format("{0:0.00}%", (mod.CyclingTime.TotalSeconds / mod.TotalTime.TotalSeconds) * 100.0),
							mod.ProductiveTime, string.Format("{0:0.00}%", (mod.ProductiveTime.TotalSeconds / mod.TotalTime.TotalSeconds) * 100.0),
							mod.FaultedTime, string.Format("{0:0.00}%", (mod.FaultedTime.TotalSeconds / mod.TotalTime.TotalSeconds) * 100.0),
							mod.TotalTime);
					}
				}
			}
		}

		private string GetMonthName(DateTime date) {
			switch (date.Month) {
				case 1: return "January";
				case 2: return "February";
				case 3: return "March";
				case 4: return "April";
				case 5: return "May";
				case 6: return "June";
				case 7: return "July";
				case 8: return "August";
				case 9: return "September";
				case 10: return "October";
				case 11: return "November";
				case 12: return "December";
			}
			return "Unkown Month";
		}

		private List<string> GetSelectedModules() {
			var list = new List<string>(15);

			if (IncludePm1Box.Checked) list.Add("PM1");
			if (IncludePm2Box.Checked) list.Add("PM2");
			if (IncludePm3Box.Checked) list.Add("PM3");
			if (IncludePm4Box.Checked) list.Add("PM4");
			if (IncludePm5Box.Checked) list.Add("PM5");
			if (IncludePm6Box.Checked) list.Add("PM6");
			if (IncludePm7Box.Checked) list.Add("PM7");
			if (IncludePm8Box.Checked) list.Add("PM8");
			if (IncludePm9Box.Checked) list.Add("PM9");
			if (IncludePm10Box.Checked) list.Add("PM10");
			if (IncludePm11Box.Checked) list.Add("PM11");
			if (IncludePm12Box.Checked) list.Add("PM12");

			return list;
		}
	}
}
