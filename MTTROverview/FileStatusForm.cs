﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MTTROverview
{
	public partial class FileStatusForm : Form
	{
		private readonly string FoundKey = "Found";
		private readonly string NotFoundKey = "Not Found";
		private readonly int HoursToPoll = 24;

		public FileStatusForm() {
			InitializeComponent();
			CheckStatus();
		}

		private void CheckStatus() {
			Grid.Rows.Clear();

			var files = Directory.EnumerateFiles(Utility.CSVPath)
						.Where(x => x.EndsWith("1.csv") || x.EndsWith("2.csv") || x.EndsWith("3.csv") || x.EndsWith("4.csv") || x.EndsWith("5.csv"))
						.OrderByDescending(x => x)  // Want the newest file first
						.Select(x => x)
						.ToList();

			// -- Determine the minutes of the hour when SCADA is dumping files
			var lastfileTime = DateTime.ParseExact(Path.GetFileName(files[0]).Substring(0, 15), "yyyy-MM-dd_HHmm", System.Globalization.CultureInfo.InvariantCulture);
			var dumpingTime = lastfileTime.Minute;
			DumpingTimeLabel.Text = string.Format("Dumping Time: On the {0}{1} minute of the hour.", dumpingTime, GetNumberSuffix(dumpingTime));

			// -- Start at the last dumping time, go back X hours and determine if we have a file from each APL
			var lastDumpTime = DateTime.Now.AddMinutes(dumpingTime - DateTime.Now.Minute);
			if (lastDumpTime > DateTime.Now) lastDumpTime = lastDumpTime.AddHours(-1);

			for (int h = 0; h <= HoursToPoll; h++) {  // h = hours
				var apl = new bool[5];  // For each APL, if file exists

				var time = lastDumpTime.ToString("yyyy-MM-dd_HHmm");
				var nextTime = lastDumpTime.AddHours(-1).ToString("yyyy-MM-dd_HHmm");
				foreach (var f in files) {
					var path = Path.GetFileName(f).Substring(0, 15);
					if (path == time) {
						if (f.EndsWith("1.csv")) apl[0] = true;
						else if (f.EndsWith("2.csv")) apl[1] = true;
						else if (f.EndsWith("3.csv")) apl[2] = true;
						else if (f.EndsWith("4.csv")) apl[3] = true;
						else if (f.EndsWith("5.csv")) apl[4] = true;
						continue;
					} else if (path == nextTime) {
						break;   // Exit, we've passed where this file should be
					}
				}

				// Add the row
				Grid.Rows.Add(
					time,
					apl[0] ? FoundKey : NotFoundKey,
					apl[1] ? FoundKey : NotFoundKey,
					apl[2] ? FoundKey : NotFoundKey,
					apl[3] ? FoundKey : NotFoundKey,
					apl[4] ? FoundKey : NotFoundKey
				);

				// Highlight cells
				var row = Grid.Rows[Grid.Rows.Count - 1];
				row.Cells[0].Style.BackColor = apl[0] && apl[1] && apl[2] && apl[3] && apl[4] ? Color.LightGreen : !apl[0] && !apl[1] && !apl[2] && !apl[3] && !apl[4] ? Color.PaleVioletRed : Color.LightGoldenrodYellow;
				row.Cells[1].Style.BackColor = apl[0] ? Color.LightGreen : Color.PaleVioletRed;
				row.Cells[2].Style.BackColor = apl[1] ? Color.LightGreen : Color.PaleVioletRed;
				row.Cells[3].Style.BackColor = apl[2] ? Color.LightGreen : Color.PaleVioletRed;
				row.Cells[4].Style.BackColor = apl[3] ? Color.LightGreen : Color.PaleVioletRed;
				row.Cells[5].Style.BackColor = apl[4] ? Color.LightGreen : Color.PaleVioletRed;

				// Move to the last hour
				lastDumpTime = lastDumpTime.AddHours(-1);
			}
		}

		private string GetNumberSuffix(int num) {
			if (num < 1) return string.Empty;
			var x = num.ToString();

			if (x.EndsWith("1")) return "st";
			if (x.EndsWith("2")) return "nd";
			if (x.EndsWith("3")) return "rd";
			return "th";
		}
	}
}
