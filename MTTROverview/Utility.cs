﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MTTROverview
{
	public static class Utility
	{
		// -- Constants
#if DEBUG
		public static string CSVPath = @"C:\Programming\testing\alarms\current";
		public static string CSVPathArchive = @"C:\Programming\testing\alarms\backup";
#else
		public static string CSVPath = @"G:\BusUnits\Manufacturing\LS3ZENON\SCADA_DATA\AlarmsData\Backup";
		public static string CSVPathArchive = @"G:\BusUnits\Manufacturing\LS3ZENON\SCADA_DATA_Archive\AlarmsData\Backup";
#endif
		public const bool MultiThreaded = true;
		public const int MaxFilesForMultiThreaded = 1000;
		public const string LogFile = "log";

		// Static Members
		public static List<string> ExcludeTags = new List<string> {
				"RM2FA01_ToABS2", "RM2FA01_ToABS1", // Waiting for Robot to load drawer X
                "COMOPC1_WD0001", "COMOPC1_WD0002", // OPC server messages
            };

		public static List<string> Module_Keys = new List<string> {
                // 6-
                "APL01_PM1MM", "APL01_PM1EM", "APL01_PM1IM", "APL01_PM1TM",
				"APL01_PM2MM", "APL01_PM2EM", "APL01_PM2IM", "APL01_PM2TM",
				"APL01_PM3MM", "APL01_PM3EM", "APL01_PM3IM", "APL01_PM3TM",
				"APL01_PP1T1", "APL01_PP1T2",
				"APL02_PM1MM", "APL02_PM1EM", "APL02_PM1IM", "APL02_PM1TM",
				"APL02_PM2MM", "APL02_PM2EM", "APL02_PM2IM", "APL02_PM2TM",
				"APL02_PM3MM", "APL02_PM3EM", "APL02_PM3IM", "APL02_PM3TM",
				"APL02_PP1T1", "APL02_PP1T2",
                // 7+
                "APL03_PM1MM", "APL03_PM1EM", "APL03_PM1IM", "APL03_PM1TM",
				"APL03_PM2MM", "APL03_PM2EM", "APL03_PM2IM", "APL03_PM2TM",
				"APL03_PM3MM", "APL03_PM3EM", "APL03_PM3IM", "APL03_PM3TM",
				"APL03_PP1T1", "APL03_PP1T2",
				"APL04_PM1MM", "APL04_PM1EM", "APL04_PM1IM", "APL04_PM1TM",
				"APL04_PM2MM", "APL04_PM2EM", "APL04_PM2IM", "APL04_PM2TM",
				"APL04_PM3MM", "APL04_PM3EM", "APL04_PM3IM", "APL04_PM3TM",
				"APL04_PP1T1", "APL04_PP1T2",
			};

		public static string PrettyTimeSpan(TimeSpan? t) {
			string ret = string.Empty;

			if (!t.HasValue) return ret;

			if (t.Value.Days > 0)
				ret = string.Format("{0}{1}d", ret, t.Value.Days);
			if (t.Value.Hours > 0)
				ret = string.Format("{0}{1}h", ret, t.Value.Hours);
			if (t.Value.Minutes > 0)
				ret = string.Format("{0}{1}m", ret, t.Value.Minutes);
			if (t.Value.Seconds > 0 || ret?.Length == 0)
				ret = string.Format("{0}{1}s", ret, t.Value.Seconds);
			return ret;
		}

		// --- Queries ---
		public static List<TimeSeriesRow> QueryForRows(DateTime start, DateTime end, IProgress<ProgressWithBarAndLabel> prog = null) {
			const int MaxFilesBeforeBuild = 300;

			// 1 - Find all the files for this query
			prog?.Report(new ProgressWithBarAndLabel() {
				Label = "Finding Files",
				Max = 0,
			});
			var files = FindFilesInQuery(start, end);

			//File.WriteAllLines("files", files);

			// 2 - Parse Files and occasionally build the fault list
			var processed = new List<string>();
			var builders = new Dictionary<string, IncrementalMTTRFaultBuilder>(150);

			var parseProgress = new Progress<int>(x => {
				prog?.Report(new ProgressWithBarAndLabel() {
					Label = "Parsing some files...",
					Max = files.Count,
					Min = 0,
					Current = processed.Count + x,
				});
			});

			for (int i = 0; i < files.Count;) {
				// -- Parse Files
				var f = files.Skip(i).Take(MaxFilesBeforeBuild);
				var buffer = ParseFiles(f, start, end, parseProgress);
				processed.AddRange(f);
				i += f.Count();

				// -- Throw away faults outside our timeframe				
				//var removedFaults = new List<Event>();
				//var iter = 0;
				//var max = buffer.Count;
				//for (int e = 0; e < buffer.Count; e++) {
				//	if (buffer[e].End < start || buffer[e].Start > end) {
				//		removedFaults.Add(buffer[e]);
				//		//buffer.RemoveAt(e);
				//		//e--;
				//	}

				//	if (iter % 200 == 0) {
				//		prog?.Report(new ProgressWithBarAndLabel() {
				//			Label = "Removing Irrelevant Entries...",
				//			Max = max,
				//			Min = 0,
				//			Current = iter,
				//		});
				//	}
				//	iter++;
				//}
				//File.WriteAllLines("outOfQuery", removedFaults);
				//var toRemove = new List<Event>(buffer.Count);
				//var iter = 0;
				//var iterLock = new Object();
				//Parallel.For(0, buffer.Count, e => {
				//	if (buffer[e].End < start || buffer[e].Start > end) toRemove.Add(buffer[e]);
				//	lock (iterLock) {
				//		iter++;
				//	}

				//	if (iter % 200 == 0) {
				//		prog?.Report(new ProgressWithBarAndLabel() {
				//			Label = "Finding Irrelevant Entries...",
				//			Max = buffer.Count,
				//			Min = 0,
				//			Current = iter,
				//		});
				//	}
				//});
				//iter = 0;
				//foreach (var e in toRemove) {
				//	buffer.Remove(e);
				//	iter++;
					
				//	if (iter % 200 == 0 ) {
				//		prog?.Report(new ProgressWithBarAndLabel() {
				//			Label = "Removing Irrelevant Entries...",
				//			Max = toRemove.Count,
				//			Min = 0,
				//			Current = iter,
				//		});
				//	}
				//}


				// -- Build
				var split = SortLists(buffer);
				for (int l = 0; l < split.Count; l++) {
					if (split[l].Count < 1) continue;
					var list = split[l];

					prog?.Report(new ProgressWithBarAndLabel() {
						Label = $"Incremental building... ({list[0].Area})",
						Max = split.Count,
						Min = 0,
						Current = l + 1,
					});

					if (!builders.ContainsKey(list[0].Area)) builders[list[0].Area] = new IncrementalMTTRFaultBuilder();
					builders[list[0].Area].AddEvents(list, true);
				}
			}
			Debug.Assert(processed.Count == files.Count, "Didn't process the correct # of files");
			for (int i = 0; i < files.Count; i++) {
				Debug.Assert(processed[i] == files[i], $"Processed index {i} doesn't match files index");
			}

			//prog?.Report(new ProgressWithBarAndLabel() {
			//	Label = "Finalizing Fault Generation",
			//	Max = 0,
			//});
			//var ret = new List<TimeSeriesRow>();
			//foreach (var bldr in builders.Values) {
			//	bldr.FinishBuildingFaults();
			//	//ret.Add(new TimeSeriesRow() {
			//	//	Faults = bldr.FinishBuildingFaults()
			//	//});
			//}

			// -- Group the faults
			var ret = new List<TimeSeriesRow>();
			var groupingDone = 0;
			Object groupingDoneLock = new object();
			Parallel.For(0, builders.Count, i => {
				var bldr = builders.ElementAt(i).Value;

				var faults = bldr.FinishBuildingFaults();
				faults = GroupFaults(faults);

				lock (groupingDoneLock) {
					ret.Add(new TimeSeriesRow() { Faults = faults });
					groupingDone++;
				}

				prog?.Report(new ProgressWithBarAndLabel() {
					Label = $"Finalizing and Grouping Faults...",
					Max = builders.Count,
					Min = 0,
					Current = groupingDone,
				});
			});

			return ret;
		}
		public static List<TimeSeriesRow> QueryCSVFilesForRows(DateTime start, DateTime end, IProgress<ProgressWithBarAndLabel> prog = null) {
			// 1 - Get messages in this time frame
			var messages = QueryCSVFilesForMessages(start, end, prog);

			// 2 - Sort the lists in order
			var lists = SortLists(messages);

			// 3 - Create the Series
			var parseProgress = new Progress<int>(x => {
				prog?.Report(new ProgressWithBarAndLabel() {
					Label = "Building Faults",
					Max = lists.Count,
					Min = 0,
					Current = x,
				});
			});
			var series = CreateRowsFromQuery(start, end, lists, parseProgress);

			return series;
		}

		public static List<Event> QueryCSVFilesForMessages(DateTime start, DateTime end, IProgress<ProgressWithBarAndLabel> prog = null) {
			// 1 - Find all the files for this query
			prog?.Report(new ProgressWithBarAndLabel() {
				Label = "Finding Files",
				Max = 0,
			});
			var files = FindFilesInQuery(start, end);

			// 2 - Parse Each File
			var parseProgress = new Progress<int>(x => {
				prog?.Report(new ProgressWithBarAndLabel() {
					Label = "Parsing Files",
					Max = files.Count,
					Min = 0,
					Current = x,
				});
			});
			var messages = ParseFiles(files, start, end, parseProgress);

			// 3 - Trim Messages to Query Time
			prog?.Report(new ProgressWithBarAndLabel() {
				Label = "Organizing Results",
				Max = 0,
			});
			return TrimMessageOutOfQueryTime(messages, start, end);
		}

		// -- Steps --
		public static List<TimeSeriesRow> CreateRowsFromQuery(DateTime start, DateTime end, List<List<Event>> lists, IProgress<int> progress = null) {
			var series = new List<TimeSeriesRow>();
			if (!MultiThreaded) {
				foreach (var area in lists) {
					// Setup Row
					TimeSeriesRow row = BuildTimeSeriesRow(area, start, end);
					series.Add(row);
					progress?.Report(series.Count);
				}
			} else {
				var done = 0;
				var rowTasks = new List<Task<TimeSeriesRow>>();
				foreach (var area in lists) {
					rowTasks.Add(Task.Run(() => {
						var r = BuildTimeSeriesRow(area, start, end);
						done++;
						progress?.Report(done);
						return r;
					}));
				}
				Task.WaitAll(rowTasks.ToArray());
				foreach (var row in rowTasks) {
					series.Add(row.Result);
				}
			}

			return series;
		}

		public static List<Event> TrimMessageOutOfQueryTime(List<Event> msgs, DateTime start, DateTime end) {
			var outList = new List<Event>(msgs.Count);
			foreach (var msg in msgs) {
				if (msg.Start >= start && msg.End <= end) {
					outList.Add(msg);
				} else if (msg.Start < start && msg.End > start.AddMinutes(10)) {
					msg.Start = start;
					outList.Add(msg);
				} else if (msg.End > end && msg.Start < end.AddMinutes(-10)) {
					msg.End = end;
					outList.Add(msg);
				}
			}
			return outList;
		}

		public static List<List<Event>> SortLists(List<Event> messages) {
			List<List<Event>> lists = new List<List<Event>>();
			if (!MultiThreaded) {
				for (int i = 1; i < 15; i++) {
					foreach (var module in new List<string> { "MM", "EM", "IM", "TM" }) {   // This allows us to order the lists how I like
						var list = messages.AsParallel().Where(x => x.Area == string.Format("PM{0}{1}", i, module)).OrderBy(x => x.Start).Select(x => x).ToList();

						if (list?.Count > 0)
							lists.Add(list);
					}
				}
			} else {
				var Modules = new List<string> {
                    // 6-
                    "PM1MM", "PM1EM", "PM1IM", "PM1TM",
					"PM2MM", "PM2EM", "PM2IM", "PM2TM",
					"PM3MM", "PM3EM", "PM3IM", "PM3TM",
					"PP1", "PP2",
					"PM4MM", "PM4EM", "PM4IM", "PM4TM",
					"PM5MM", "PM5EM", "PM5IM", "PM5TM",
					"PM6MM", "PM6EM", "PM6IM", "PM6TM",
					"PP3", "PP4",
                    // 7+
                    "PM7MM", "PM7EM", "PM7IM", "PM7TM",
					"PM8MM", "PM8EM", "PM8IM", "PM8TM",
					"PM9MM", "PM9EM", "PM9IM", "PM9TM",
					"PP5", "PP6",
					"PM10MM", "PM10EM", "PM10IM", "PM10TM",
					"PM11MM", "PM11EM", "PM11IM", "PM11TM",
					"PM12MM", "PM12EM", "PM12IM", "PM12TM",
					"PP7", "PP8",
			};
				var splitTasks = new List<Task<List<Event>>>();
				foreach (var module in Modules) {
					var t = Task.Run(() => {
						var msgs = new List<Event>();
						foreach (var msg in messages) {
							if (msg.Area == module)
								msgs.Add(msg);
						}

						msgs.Sort((a, b) => a.Start.CompareTo(b.Start));
						return msgs;
					});

					splitTasks.Add(t);
				}
				Task.WaitAll(splitTasks.ToArray());
				var c = 0;
				var all = new List<Event>();
				foreach (var l in splitTasks) {
					if (l.Result?.Count > 0) {
						lists.Add(l.Result);
						all.AddRange(l.Result);
						c += l.Result.Count;
					}
				}
				var except = messages.Except(all);
				Debug.Assert(c == messages.Count, "Not All messages were captured during the Module Split!");
			}

			return lists;
		}

		public static List<Event> ParseFiles(IEnumerable<String> files, DateTime? start = null, DateTime? end = null, IProgress<int> progress = null) {
			var messages = new List<Event>(files.Count() * 1500); // rough estimate based on lines in file I have open now
			var done = 0;

			if (!MultiThreaded || files.Count() > MaxFilesForMultiThreaded) {
				foreach (var file in files) {
					var e = ParseFile(file, start, end);
					messages.AddRange(e);
					done++;
					progress?.Report(done);
				}
			} else {
				// Multithreaded
				var tasks = new List<Task<List<Event>>>();
				foreach (var file in files) {
					tasks.Add(Task.Run(() => {
						var r = ParseFile(file, start, end);
						done++;
						progress?.Report(done);
						return r;
					}));
				}
				Task.WaitAll(tasks.ToArray());
				foreach (var e in tasks) {
					messages.AddRange(e.Result);
				}
			}

			return messages;
		}

		public static List<string> FindFilesInQuery(DateTime start, DateTime end) {
			const int HourPadding = 4;
			const int DaysOfArchive = 0;   // Update: Non-archived data should hold atleast this many days

			Func<IEnumerable<string>, List<string>> GetFilesInQuery = (li) => {
				return li.Where(x => {
					if (!x.EndsWith("1.csv") && !x.EndsWith("2.csv") && !x.EndsWith("3.csv") && !x.EndsWith("4.csv"))
						return false;
					var s = Path.GetFileName(x).Substring(0, 15);
					var t = DateTime.ParseExact(s, "yyyy-MM-dd_HHmm", System.Globalization.CultureInfo.InvariantCulture);
					return start.AddHours(-HourPadding) <= t && t <= end.AddHours(HourPadding);
				}).Select(x => x).ToList();
			};

			// Query the nonArchive
			var files = GetFilesInQuery(Directory.EnumerateFiles(CSVPath, "*.csv"));

			// Include the archive path if query is old
			if ((DateTime.Now.Date - start.Date).TotalDays > DaysOfArchive) {
				foreach (var path in Directory.EnumerateDirectories(CSVPathArchive)) {
					var dir = Path.GetFileNameWithoutExtension(path);

					// ex: Alarms-201807
					if (dir.StartsWith("Alarms-") && int.TryParse(dir.Substring(7, 4), out int year)
													  && int.TryParse(dir.Substring(11, 2), out int month)
													  && new DateTime(year, month, start.Day).Date >= start.Date) {
						files.AddRange(GetFilesInQuery(Directory.EnumerateFiles(path, "*.csv")));
					}
				}
			}

			files.Sort((x, y) => new FileInfo(x).CreationTime.CompareTo(new FileInfo(y).CreationTime));
			Debug.Assert(new FileInfo(files[0]).CreationTime < new FileInfo(files.Last()).CreationTime, "File Sort order reversed");

			return files;
		}

		public static List<Event> ParseFile(string path, DateTime? start = null, DateTime? end = null) {
			var events = new List<Event>(1500);
			var text = File.ReadAllLines(path);

			int startIndex = -1;
			bool newVersion = false;

			if (text[1] == "Session altered.") {
				startIndex = 6;
				newVersion = false;
			} else {
				startIndex = 3;
				newVersion = true;
			}

			string areaStr = string.Empty,
					   startStr = string.Empty,
					   endStr = string.Empty,
					   tagStr = string.Empty,
					   msgStr = string.Empty;

			for (int i = startIndex; i < text.Length; ++i) {
				var split = CSVSplitLine(text[i]);

				if (newVersion && split.Count == 15) {
					areaStr = split[0];
					startStr = split[4];
					endStr = split[6];
					tagStr = split[1];
					msgStr = split[9];
				} else if (newVersion && split.Count == 12) {
					areaStr = split[0];
					startStr = split[4];
					endStr = split[6];
					tagStr = split[1];
					msgStr = split[8];
				} else if (split.Count == 26) {
					areaStr = split[0];
					startStr = split[6];
					endStr = split[8];
					tagStr = split[2];
					msgStr = split[11];
				} else {
					continue;   // Skip this line
				}

				// Skip unknown areas
				if (!Module_Keys.Contains(areaStr))
					continue;
				if (ExcludeTags.Contains(tagStr))
					continue;

				// Convert Area to readable format
				string area = "unknown";
				var apl = int.Parse(areaStr[4].ToString());
				if (areaStr[7] == 'M') {           // MM, EM, IM, TM
												   // APL01_PM1MM
					var line = ((apl - 1) * 3) + int.Parse(areaStr[8].ToString());
					var machine = areaStr.Substring(9, 2);

					// For 7+, wrap UV into MM messages
					if (machine == "UV") machine = "MM";

					area = string.Format("PM{1}{0}", machine, line);
				} else if (areaStr[7] == 'P') {      // PP
													 // APL02_PP1T2
					var line = ((apl - 1) * 2) + int.Parse(areaStr[10].ToString());
					area = string.Format("PP{0}", line);
				}

				if (!DateTime.TryParse(startStr.Trim(), System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime start_time)) {
					Log("Error processing line - Start DateTime format not recognized.\nFile: {0}\nLine #: {1}\nLine: {2}\nStart Date Token: {3}",
						path, i, text[i], startStr.Trim());
					continue;
				}
				if (!DateTime.TryParse(endStr.Trim(), System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime end_time)) {
					Log("Error processing line - End DateTime format not recognized.\nFile: {0}\nLine #: {1}\nLine: {2}\nEnd Date Token: {3}",
						path, i, text[i], endStr.Trim());
					continue;
				}

				// Skip times outside of query range
				if ((end.HasValue && start_time > end.Value) || (start.HasValue && end_time < start.Value)) continue;

				events.Add(new Event() {
					Area = area,
					Start = start_time,
					End = end_time,
					Tag = tagStr,
					Message = msgStr.Trim()
				});
			}

			return events;
		}

		private static List<string> CSVSplitLine(string line) {
			// Split a string via commas, allowing " to escape tokens
			List<string> results = new List<string>();
			int start = 0;
			bool Escaped = false;
			bool WasEscaped = false;
			for (int i = 0; i < line.Length; ++i) {
				if (line[i] == '"') {
					Escaped = !Escaped;
					WasEscaped = true;
				} else if (!Escaped && line[i] == ',') {
					var str = line.Substring(start, i - start);
					if (WasEscaped) {
						str = str.TrimStart('"', ' ').TrimEnd(' ', '"');
					}
					results.Add(str.Trim());
					start = i + 1;
				}
			}

			// Grab last (unfinished) token
			if (start < line.Length - 1) {
				var str = line.Substring(start);
				if (WasEscaped) {
					str = str.TrimStart('"', ' ').TrimEnd(' ', '"');
				}
				results.Add(str.Trim());
			}

			return results;
		}

		public static TimeSeriesRow BuildTimeSeriesRow(List<Event> events, DateTime start, DateTime end, bool clampTime = false) {
			if (events?.Count < 2) return new TimeSeriesRow();
			List<long> times = new List<long>();

			Debug.Assert(events[0].Start <= events[1].Start && events[0].Start <= events.Last().Start, "Build Time Series Row called with a list of faults not sorted!");
			Debug.Assert(events[0].Area == events[1].Area && events[0].Area == events.Last().Area, "Build Time Series Row called with multiple Areas in same list!");

			var row = new TimeSeriesRow() {
				Title = events[0].Area,
				Group = events[0].Area.Substring(0, 3)
			};

			var knownKeys = new HashSet<string>(); // Prevent duplicate events in fault set
			bool isPP = events[0].Machine == "PP";
			bool IsEM = events[0].Machine == "EM";  //(row.Title.Length > 3 && row.Title[3] == 'E');  // PMXEM
			Event lotStart = null;

			for (int i = 0; i < events.Count; ++i) {
				Debug.Assert(i == (events.Count - 1) || events[i].Start <= events[i + 1].Start, "Message List input to BuildTimeSeriesRow not in order!");
				var msg = events[i];

				// Clamp Time to query time
				if (clampTime) {
					if (msg.Start < start) {
						msg.Start = start;
					}
					if (msg.End > end) {
						msg.End = end;
					}
				}

				// Classify the fault
				if (msg.IsCyclingMsg) {                     // Cycling Messages
					row.Cycling.Add(msg);
				} else if (msg.IsProductiveMsg) {           // Productive Messages
					row.Productive.Add(msg);
				} else if (msg.IsLotChangeMsg) {            // Lot Change Messages
					bool isLotStartMsg = IsEM ? msg.Message.IndexOf("end", StringComparison.OrdinalIgnoreCase) >= 0 : msg.Message.IndexOf("start", StringComparison.OrdinalIgnoreCase) >= 0;
					bool isLotEndMsg = IsEM ? msg.Message.IndexOf("start", StringComparison.OrdinalIgnoreCase) >= 0 : msg.Message.IndexOf("end", StringComparison.OrdinalIgnoreCase) >= 0;

					if (isLotStartMsg) {
						lotStart = msg;
					} else if (lotStart != null && isLotEndMsg) {
						// Create a lot change event for this time
						row.LotChange.Add(new Event() {
							Area = msg.Area,
							Start = lotStart.Start,
							End = msg.End,
							Message = "Lot Change Active"
						});
						lotStart = null;
					}
				} else if (!isPP && msg.IsMTTRMsg) {        // Fault Related
					if (msg.Tag.StartsWith("RPIMTTR_FAULTD")) {
						var e = BuildEvent(events, i);
						row.Faults.Add(e);
					}
				} else if (isPP) {
					if (msg.Tag == "CB1FA01_3041B7" || msg.Tag == "CB1FA01_3343B3" || msg.Tag == "CB1FA01_4041B9") {
						msg.Message = "PP is Locked Out";
						msg.Tag = "PPLockOut";

						// Skip all faults until the lockout is over
						for (int sub_i = i + 1; sub_i < events.Count; sub_i++) {
							// if we hit any other lockout msgs, just extend our lockout time
							if (events[sub_i].Tag == "CB1FA01_3041B7" || events[sub_i].Tag == "CB1FA01_3343B3" || events[sub_i].Tag == "CB1FA01_4041B9") {
								msg.End = events[sub_i].End;
							} else if (events[sub_i].Start > msg.End) {
								i = sub_i - 1;  // process this event next iteration
								break;
							}
						}
					} else if (msg.IsAFaultMessage) {
						var hash = string.Format("{0}{1}{2}", msg.Tag, msg.Start.ToBinary(), msg.Message);
						if (knownKeys.Add(hash))
							row.Faults.Add(msg);
					}
				}
			}

			if (isPP) {
				row.Faults = GroupFaults(row.Faults);
			}

			// TODO What to do if we didn't find an end to a lot change? We'll ignore it here (Sound okay to me!)
			return row;
		}

		public static List<Event> QueryMessages(List<string> Areas, DateTime start, DateTime end, string msgKey, IProgress<ProgressWithBarAndLabel> prog = null) {
			var files = FindFilesInQuery(start, end);
			msgKey = msgKey.Trim();
			var startQueryTime = DateTime.Now;  // Progress
			var currIndex = 0;

			// Check each one for messages
			var events = new List<Event>(1500);
			foreach (var file in files) {
				var text = File.ReadAllLines(file);
				currIndex++;    // Progress

				int startIndex = -1;
				bool newVersion = false;

				if (text[1] == "Session altered.") {
					startIndex = 6;
					newVersion = false;
				} else {
					startIndex = 3;
					newVersion = true;
				}

				string areaStr = string.Empty,
						   startStr = string.Empty,
						   endStr = string.Empty,
						   tagStr = string.Empty,
						   msgStr = string.Empty;

				for (int i = startIndex; i < text.Length; ++i) {
					var split = CSVSplitLine(text[i]);

					if (newVersion && split.Count == 15) {
						areaStr = split[0];
						startStr = split[4];
						endStr = split[6];
						tagStr = split[1];
						msgStr = split[9];
					} else if (newVersion && split.Count == 12) {
						areaStr = split[0];
						startStr = split[4];
						endStr = split[6];
						tagStr = split[1];
						msgStr = split[8];
					} else if (split.Count == 26) {
						areaStr = split[0];
						startStr = split[6];
						endStr = split[8];
						tagStr = split[2];
						msgStr = split[11];
					} else {
						continue;   // Skip this line
					}

					// Skip unwanted areas
					if (!Areas.Contains(areaStr))
						continue;

					// Convert Area to readable format
					string area = "unknown";
					var apl = int.Parse(areaStr[4].ToString());
					if (areaStr[7] == 'M') {           // MM, EM, IM, TM
													   // APL01_PM1MM
						var line = ((apl - 1) * 3) + int.Parse(areaStr[8].ToString());
						var machine = areaStr.Substring(9, 2);

						// For 7+, wrap UV into MM messages
						if (machine == "UV") machine = "MM";

						area = string.Format("PM{1}{0}", machine, line);
					} else if (areaStr[7] == 'P') {      // PP
														 // APL02_PP1T2
						var line = ((apl - 1) * 2) + int.Parse(areaStr[10].ToString());
						area = string.Format("PP{0}", line);
					}

					if (!DateTime.TryParse(startStr.Trim(), System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime s)) {
						System.Windows.Forms.MessageBox.Show(
								string.Format("Error processing line - Start DateTime format not recognized.\nFile: {0}\nLine #: {1}\nLine: {2}\nStart Date Token: {3}",
								file, i, text[i], startStr.Trim()),
								"Error Parsing file - Start Date Time Token!");
						continue;
					}
					if (!DateTime.TryParse(endStr.Trim(), System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime e)) {
						System.Windows.Forms.MessageBox.Show(
								string.Format("Error processing line - End DateTime format not recognized.\nFile: {0}\nLine #: {1}\nLine: {2}\nEnd Date Token: {3}",
								file, i, text[i], endStr.Trim()),
								"Error Parsing file - End Date Time Token!");
						continue;
					}

					Event ev = new Event() {
						Area = area,
						Start = s,
						End = e,
						Tag = tagStr,
						Message = msgStr.Trim()
					};

					if (!ExcludeTags.Contains(ev.Tag)
						&& ev.End >= start && ev.Start <= end
						&& (msgKey?.Length == 0 || ev.Message.IndexOf(msgKey, StringComparison.OrdinalIgnoreCase) >= 0)) {
						events.Add(ev);
					}
				}

				// -- Update Progress (per File?)
				if (prog != null) {
					var rate = (double)currIndex / (double)(DateTime.Now - startQueryTime).TotalMinutes; // Files per minute
					var perc = ((double)currIndex / (double)files.Count) * 100.0d;
					var eta = (files.Count - currIndex) / rate;

					prog.Report(new ProgressWithBarAndLabel() {
						Max = files.Count,
						Current = currIndex,
						Label = string.Format("Querying ({0:0.0}%, file {1} of {2}, Rate: {3:0.0}/m, ETA: {4:0.0}{5})", perc, currIndex, files.Count, rate,
											eta > 1 ? eta : eta * 60, eta > 1 ? "m" : "s")
					});
				}
			}

			return events.OrderBy(x => x.Start).GroupBy(x => x.Area).SelectMany(x => x).ToList();
		}

		public static Event BuildEvent(List<Event> events, int fltID) {
			const ulong BufferSeconds = 8;

			Debug.Assert(events[fltID].IsMTTRFaultEventActiveMsg, "fltID must point to the ID of the MTTR_FaultEventActive event!");

			var fault = new Event() {
				Area = events[fltID].Area,
				Start = events[fltID].Start,
				End = events[fltID].End,
				Message = string.Format("Unknown {0} Fault", events[fltID].Machine), // Default value
			};
			fault.Tag = fault.Message;  // 2018-09-10 -- Use the tag of the fault for the Shree report

			// Search earlier in time to BufferSeconds before this event started
			var startIndex = fltID;
			for (int y = fltID - 1; y >= 0; --y) {  // search earlier
				if ((fault.Start - events[y].Start).TotalSeconds > BufferSeconds)
					break;  // Loop is done
				startIndex = y;
			}

			Event fltText = null;
			for (int y = startIndex; y < events.Count; ++y) {  // search through event's faults
				var msg = events[y];
				if (msg.Start > fault.End) // Past fault duration
					break;  // Loop is done

				// Check if this event is a better fault description
				if (msg.IsAFaultMessage && (msg.Start - fault.Start).TotalSeconds < BufferSeconds && (fltText == null || (fltText.Duration < msg.Duration))) {
					fltText = msg;
				} else if (msg.Tag.StartsWith("RPIMTTR_OPRESP")) {
					fault.in_OPRespTime = msg.Duration;
				} else if (msg.Tag.StartsWith("RPIMTTR_RECOVE")) {
					fault.in_RecoveryTime = msg.Duration;
				} else if (msg.Tag.StartsWith("RPIMTTR_EMRESP")) {
					fault.in_EMRespTime = msg.Duration;
				} else if (msg.Tag.StartsWith("RPIMTTR_REPAIR")) {
					fault.in_RepairTime = msg.Duration;
				}
			}
			if (fltText != null) {
				fault.Message = fltText.Message;
				fault.Tag = fltText.Tag;        // 2018-09-10 -- Use the tag of the fault for the Shree report
			}

			return fault;
		}

		// -- Time
		public static void TimeFrame_GetLastTwoShifts(out DateTime start, out DateTime end) {
			var now = DateTime.Now;

			if (now.Hour < 7) { // In Night Shift
								// Last Day -> Night Shift
				end = now.Date.AddDays(-1).AddHours(18.5);
			} else if (now.Hour < 18.5) { // In Day Shift
										  // Last Night -> Day Shift
				end = now.Date.AddHours(6.5);
			} else {
				// Today's Day -> Last Night's Night Shift
				end = now.Date.AddHours(18.5);
			}
			start = end.AddHours(-24);
		}

		public static List<string> WarningTags = new List<string> {
			"-- MM Warnings --",
			"CB1AV50_ACDATA",
			"MESPROD_RELPMS",
			"CB1EMPT_NOTEMP",
			"CB2AV01_MULTIS",
			"CB2AV02_MULTIS",
			"CB2AV03_MULTIS",
			"CB2AV04_MULTIS",
			"CB2AV05_MULTIS",
			"CB2AV06_MULTIS",
			"CB2AV07_MULTIS",
			"CB2AV08_MULTIS",
			"CB2AV09_MULTIS",
			"CB2AV10_MULTIS",
			"CB2AV11_MULTIS",
			"CB2AV12_MULTIS",
			"CB2AV13_MULTIS",
			"CB2AV14_MULTIS",
			"CM1AV34_CMHOME",
			"VM2DI04_0715S3",
			"WF1AV66_EMDISA",
			"CB1INI2_INIFIL",
			"CB1INI3_INIFIL",
			"CB1INI4_INIFIL",
			"CB1GW04_0530A1",
			"CB1GW05_0530A1",
			"CB1GW06_0530A1",
			"CB1GW07_0530A1",
			"CB1GW04_0518A0",
			"CB1GW05_0518A0",
			"CB1GW06_0518A0",
			"CB1GW07_0518A0",
			"CB1GW04_0549A0",
			"CB1GW05_0549A0",
			"CB1GW06_0549A0",
			"CB1GW07_0549A0",
			"CB1AV13_LATMOS",
			"CB1AV14_LATMOS",
			"CB1AV15_LATMOS",
			"CB1AV16_LATMOS",
			"CB1AV10_LATMOS",
			"CB1AV18_LATMOS",
			"CB1AV11_LATMOS",
			"CB1AV12_LATMOS",
			"CB1AV17_LATMOS",
			"CB1AV21_HWSERR",
			"CB1AV22_HWSERR",
			"CB1AV23_HWSERR",
			"CB2BE01_IONISI",
			"CB2BE02_IONISI",
			"CB2BE03_IONISI",
			"CB2BE04_IONISI",
			"CB2BE05_IONISI",
			"CB2BE06_IONISI",
			"CB2BE07_IONISI",
			"UPSN200_INVDAT",
			"UPSN202_REPBAT",
			"UPSN203_RUNBAT",
			"CB1NO78_ADVICE",
			"PR1STDS_ADVICE",
			"LK1STDS_ADVICE",
			"DO1STDS_ADVICE",
			"MA1STDS_ADVICE",
			"FO1STDS_ADVICE",
			"UV1STDS_ADVICE",
			"DF1STDS_ADVICE",
			"RI1STDS_ADVICE",
			"WF1STDS_ADVICE",
			"SCESTDS_ADVICE",
			"CB1CLN1_ADVICE",
			"CB1EMPT_ADVICE",
			"CB1LOT1_DOUBLE",
			"RI1RINS_ADJUST",
			"WF1WAFL_ADJUST",
			"CB1WRN1_PNOZ01",
			"CB1CM01_NOTRDY",
			"ME1DI30_0796S3",
			"ME1DI31_0796S4",
			"ME1DI34_I796E3",
			"ME1DI35_I796E4",
			"ME1DI41_I670S5",
			"ME1DI41_I670S7",
			"CB1AV24_RFIDBC",
			"CB1AV25_RFIDFC",
			"ME2AV01_DRAWER",
			"ME2ST06_995B02",
			"ME2ST07_995B01",
			"ME1LK01_967B01",
			"ME1LK02_967B02",
			"ME1LK03_967B03",
			"ME1LK04_967B04",
			"ME1LK05_967B05",
			"ME1LK06_967B06",
			"ME1LK07_967B07",
			"ME1LK08_967B08",
			"ME1LK09_968B01",
			"ME1LK10_968B02",
			"ME1LK11_968B03",
			"ME1LK12_968B04",
			"ME1LK13_968B05",
			"ME1LK14_968B06",
			"ME1LK15_965B01",
			"ME1LK16_965B02",
			"ME1LK17_965B03",
			"ME1LK18_965B04",
			"ME1LK19_965B05",
			"ME1LK20_965B06",
			"ME1LK21_965B07",
			"ME1LK22_965B08",
			"ME1LK23_966B01",
			"ME1LK24_966B02",
			"ME1LK25_966B03",
			"ME1LK26_966B04",
			"ME1LK27_966B05",
			"ME1LK28_966B06",
			"TW1AV51_0540M1",
			"TW1AV52_0540M1",
			"TW1AV53_0516M1",
			"TW1AV54_STOPER",
			"TW1AV55_I617S3",
			"TW1AV62_MECNPR",
			"TW1A026_TW1DIS",
			"TW1AV63_0618S5",
			"TW1AV64_CAREXC",
			"TW1AV50_0504M1",
			"TW2A015_TW2DIS",
			"TW2NO72_NOTPOS",
			"TW2NO73_NOTUPZ",
			"TW2NO74_CPNOHP",
			"TW2NO75_RESERV",
			"DO1DSW2_DStMM2",
			"DO1DSW3_0NOREL",
			"DO1DSW4_TESTDS",
			"DO1AV71_ADVICE",
			"DO1AV72_ADVICE",
			"DO1AV73_ADVICE",
			"DO1AV74_ADVICE",
			"DO1AV75_ADVICE",
			"DO1AV76_ADVICE",
			"DO1AV77_ADVICE",
			"CB1AV26_DOSSUP",
			"DO1AV78_ADVICE",
			"DO1AV79_ADVICE",
			"FO1AV01_ADVICE",
			"UV1BE01_NIOBAY",
			"UV1BE02_NIOBAY",
			"UV1BE03_NIOBAY",
			"UV1BE04_NIOBAY",
			"UV1BE05_NIOBAY",
			"UV1BE06_NIOBAY",
			"DF1REVE_SCHUNK",
			"RI1AV52_ADVICE",
			"RI1AV53_ADVICE",
			"RI1AV54_ADVICE",
			"RI1TEMP_HWRFCM",
			"RI1TEMP_HWRBCM",
			"RI1BE01_SAFDOR",
			"RI1TEMP_HIGHFC",
			"RI1TEMP_HIGHBC",
			"WF1Di27_TIMEOU",
			"WF1AV63_ADVICE",
			"WF1AV64_ADVICE",
			"WF1AV65_ADVICE",
			"WF1TEMP_HWRFCM",
			"WF1TEMP_HWRBCM",
			"WF1EMR1_CLEAEM",
			"WF1EMR2_CLEAEM",
			"CB1AV18_IPMACT",
			"CB1AV19_IPMACT",
			"CB1AV20_IPMACT",
			"WF1BE01_SAFDOR",
			"WF1TEMP_HIGHFC",
			"WF1TEMP_HIGHBC",
			"TW1SLMI_0505M1",
			"TW1SLMA_0505M1",
			"TW1SLMI_0505M4",
			"TW1SLMA_0505M4",
			"TW1SLMI_0504M4",
			"TW1SLMA_0504M4",
			"TW1SLMI_0504M1",
			"TW1SLMA_0504M1",
			"TW2SLMI_0512M1",
			"TW2SLMA_0512M1",
			"TW2SLMI_0512M4",
			"TW2SLMA_0512M4",
			"TW2SLMI_0513M1",
			"TW2SLMA_0513M1",
			"TW2SLMI_0513M4",
			"TW2SLMA_0513M4",
			"TR1SLMI_0515M1",
			"TR1SLMA_0515M1",
			"TR1SLMI_0516M1",
			"TR1SLMA_0516M1",
			"TR1SLMI_0518M1",
			"TR1SLMA_0518M1",
			"TR1SLMI_0517M1",
			"TR1SLMA_0517M1",
			"TR2SLMI_0515M4",
			"TR2SLMA_0515M4",
			"TR2SLMI_0516M4",
			"TR2SLMA_0516M4",
			"TR2SLMI_0517M4",
			"TR2SLMA_0517M4",
			"DO1SLMI_0506M1",
			"DO1SLMA_0506M1",
			"DO1SLMI_0506M4",
			"DO1SLMA_0506M4",
			"DO1SPEC_IONVOL",
			"DO1SPEC_IONCUR",
			"MA1SLMI_0508M4",
			"MA1SLMA_0508M4",
			"MA1SLMI_0508M1",
			"MA1SLMA_0508M1",
			"FO1SLMI_0510M1",
			"FO1SLMA_0510M1",
			"SCEAV08_ADVICE",
			"SCEAV09_ADVICE",
			"WF1AV21_ADVICE",
			"MM1PILZ_DOOR01",
			"MM1PILZ_DOOR09",
			"MM1PILZ_DOOR21",
			"MM1PILZ_DOOR22",
			"MM1PILZ_DOOR24",
			"DO1DSS3_DStMM2",
			"DF1DII1_0511M4",
			"DF1DII2_0511M4",
			"DF1DII3_0511M1",
			"DF1DII4_0511M1",
			"TW1SLMA_0509M1",
			"TW1SLMA_0509M1",
			"TW1SLMA_0509M4",
			"TW1SLMA_0509M4",
			"TW2SCE1_ONLYHW",
			"-- UV Warnings --",
			"UV1WA01_DESE01",
			"UV1WA01_SLIG01",
			"UV1WA01_SLIG03",
			"UV1WA01_SLIG05",
			"UV1WA01_SLIG07",
			"UV1WA01_SLIG09",
			"UV1WA01_SLIG11",
			"UV1WA01_SLIG13",
			"UV1WA01_LANS01",
			"UV1WA01_LANS02",
			"UV1WA01_LANS03",
			"UV1WA01_LAAD01",
			"UV1WA01_LAAD02",
			"UV1WA01_LAAD03",
			"UV1WA01_LAFD01",
			"UV1WA01_LAFD02",
			"UV1WA01_LAFD03",
			"UV1WA01_LAFD04",
			"UV1WA01_LAFD05",
			"UV1WA01_LAFD06",
			"UV1WA01_LAFD07",
			"UV1WA01_LAFD08",
			"UV1WA01_LAFD09",
			"UV1WA01_LAFD10",
			"UV1WA01_LAFD11",
			"UV1WA01_LAFD12",
			"UV1WA01_LAFD13",
			"UV1WA01_LAFD14",
			"UV2WA01_DESE01",
			"UV2WA01_SLIG01",
			"UV2WA01_SLIG03",
			"UV2WA01_SLIG05",
			"UV2WA01_SLIG07",
			"UV2WA01_SLIG09",
			"UV2WA01_SLIG11",
			"UV2WA01_SLIG13",
			"UV2WA01_LANS01",
			"UV2WA01_LANS02",
			"UV2WA01_LANS03",
			"UV2WA01_LAAD01",
			"UV2WA01_LAAD02",
			"UV2WA01_LAAD03",
			"UV2WA01_LAFD01",
			"UV2WA01_LAFD02",
			"UV2WA01_LAFD03",
			"UV2WA01_LAFD04",
			"UV2WA01_LAFD05",
			"UV2WA01_LAFD06",
			"UV2WA01_LAFD07",
			"UV2WA01_LAFD08",
			"UV2WA01_LAFD09",
			"UV2WA01_LAFD10",
			"UV2WA01_LAFD11",
			"UV2WA01_LAFD12",
			"UV2WA01_LAFD13",
			"UV2WA01_LAFD14",
			"UV3WA01_DESE01",
			"UV3WA01_SLIG01",
			"UV3WA01_SLIG03",
			"UV3WA01_SLIG05",
			"UV3WA01_SLIG07",
			"UV3WA01_SLIG09",
			"UV3WA01_SLIG11",
			"UV3WA01_SLIG13",
			"UV3WA01_LANS01",
			"UV3WA01_LANS02",
			"UV3WA01_LANS03",
			"UV3WA01_LAAD01",
			"UV3WA01_LAAD02",
			"UV3WA01_LAAD03",
			"UV3WA01_LAFD01",
			"UV3WA01_LAFD02",
			"UV3WA01_LAFD03",
			"UV3WA01_LAFD04",
			"UV3WA01_LAFD05",
			"UV3WA01_LAFD06",
			"UV3WA01_LAFD07",
			"UV3WA01_LAFD08",
			"UV3WA01_LAFD09",
			"UV3WA01_LAFD10",
			"UV3WA01_LAFD11",
			"UV3WA01_LAFD12",
			"UV3WA01_LAFD13",
			"UV3WA01_LAFD14",
			"UV4WA01_DESE01",
			"UV4WA01_SLIG01",
			"UV4WA01_SLIG03",
			"UV4WA01_SLIG05",
			"UV4WA01_SLIG07",
			"UV4WA01_SLIG09",
			"UV4WA01_SLIG11",
			"UV4WA01_SLIG13",
			"UV4WA01_LANS01",
			"UV4WA01_LANS02",
			"UV4WA01_LANS03",
			"UV4WA01_LAAD01",
			"UV4WA01_LAAD02",
			"UV4WA01_LAAD03",
			"UV4WA01_LAFD01",
			"UV4WA01_LAFD02",
			"UV4WA01_LAFD03",
			"UV4WA01_LAFD04",
			"UV4WA01_LAFD05",
			"UV4WA01_LAFD06",
			"UV4WA01_LAFD07",
			"UV4WA01_LAFD08",
			"UV4WA01_LAFD09",
			"UV4WA01_LAFD10",
			"UV4WA01_LAFD11",
			"UV4WA01_LAFD12",
			"UV4WA01_LAFD13",
			"UV4WA01_LAFD14",
			"UV5WA01_DESE01",
			"UV5WA01_SLIG01",
			"UV5WA01_SLIG03",
			"UV5WA01_SLIG05",
			"UV5WA01_SLIG07",
			"UV5WA01_SLIG09",
			"UV5WA01_SLIG11",
			"UV5WA01_SLIG13",
			"UV5WA01_LANS01",
			"UV5WA01_LANS02",
			"UV5WA01_LANS03",
			"UV5WA01_LAAD01",
			"UV5WA01_LAAD02",
			"UV5WA01_LAAD03",
			"UV5WA01_LAFD01",
			"UV5WA01_LAFD02",
			"UV5WA01_LAFD03",
			"UV5WA01_LAFD04",
			"UV5WA01_LAFD05",
			"UV5WA01_LAFD06",
			"UV5WA01_LAFD07",
			"UV5WA01_LAFD08",
			"UV5WA01_LAFD09",
			"UV5WA01_LAFD10",
			"UV5WA01_LAFD11",
			"UV5WA01_LAFD12",
			"UV5WA01_LAFD13",
			"UV5WA01_LAFD14",
			"UV6WA01_DESE01",
			"UV6WA01_SLIG01",
			"UV6WA01_SLIG03",
			"UV6WA01_SLIG05",
			"UV6WA01_SLIG07",
			"UV6WA01_SLIG09",
			"UV6WA01_SLIG11",
			"UV6WA01_SLIG13",
			"UV6WA01_LANS01",
			"UV6WA01_LANS02",
			"UV6WA01_LANS03",
			"UV6WA01_LAAD01",
			"UV6WA01_LAAD02",
			"UV6WA01_LAAD03",
			"UV6WA01_LAFD01",
			"UV6WA01_LAFD02",
			"UV6WA01_LAFD03",
			"UV6WA01_LAFD04",
			"UV6WA01_LAFD05",
			"UV6WA01_LAFD06",
			"UV6WA01_LAFD07",
			"UV6WA01_LAFD08",
			"UV6WA01_LAFD09",
			"UV6WA01_LAFD10",
			"UV6WA01_LAFD11",
			"UV6WA01_LAFD12",
			"UV6WA01_LAFD13",
			"UV6WA01_LAFD14",
			"-- RMM Warnings --",
			"RM2FA01_VerrB1",
			"RM2FA01_VerrB2",
			"RM2FA01_VerrB3",
			"RM2FA01_VerrB4",
			"RM2FA01_VerrB5",
			"RM2FA01_VerrB6",
			"RM2FA01_MessAk",
			"RM2FA01_0166E3",
			"RM2FA01_ToUvB1",
			"RM2FA01_ToUvB2",
			"RM2FA01_ToUvB3",
			"RM2FA01_ToUvB4",
			"RM2FA01_ToUvB5",
			"RM2FA01_ToUvB6",
			"RM2FA01_ToABS1",
			"RM2FA01_ToABS2",
			"RM2FA01_ToAES1",
			"RM2FA01_ToAES2",
			"RM2FA01_By1wAk",
			"RM2FA01_By2wAk",
			"RM2FA01_By3wAk",
			"RM2FA01_By4wAk",
			"RM2FA01_By5wAk",
			"RM2FA01_By6wAk",
			"RM2FA01_PU1Adj",
			"RM2FA01_PU2Adj",
			"RM2FA01_PU3Adj",
			"RM2FA01_PU4Adj",
			"RM2FA01_PU5Adj",
			"RM2FA01_PU6Adj",
			"RM2FA01_UV1nRe",
			"RM2FA01_UV2nRe",
			"RM2FA01_UV3nRe",
			"RM2FA01_UV4nRe",
			"RM2FA01_UV5nRe",
			"RM2FA01_UV6nRe",
			"RM2FA01_UV1CnR",
			"RM2FA01_UV2CnR",
			"RM2FA01_UV3CnR",
			"RM2FA01_UV4CnR",
			"RM2FA01_UV5CnR",
			"RM2FA01_UV6CnR",
			"RM2FA01_UV1AnR",
			"RM2FA01_UV2AnR",
			"RM2FA01_UV3AnR",
			"RM2FA01_UV4AnR",
			"RM2FA01_UV5AnR",
			"RM2FA01_UV6AnR",
			"RM2FA01_UV1HnR",
			"RM2FA01_UV2HnR",
			"RM2FA01_UV3HnR",
			"RM2FA01_UV4HnR",
			"RM2FA01_UV5HnR",
			"RM2FA01_UV6HnR",
			"RM2FA01_XAxOff",
			"RM2FA01_Z1AxOf",
			"RM2FA01_Z2AxOf",
			"RM2FA01_Y1AxOf",
			"RM2FA01_Y2AxOf",
			"RM2FA01_B1AxOf",
			"RM2FA01_B2AxOf",
			"RM2FA01_B3AxOf",
			"RM2FA01_B4AxOf",
			"RM2FA01_B5AxOf",
			"RM2FA01_B6AxOf",
			"RO1FA01_RoManu",
			"RO1FA01_RoTpOn",
			"-- CM Warnings --",
			"TB1WA01_180S03",
			"TB1WA01_180S07",
			"DO1AD01_031U11",
			"DO1AD01_032U12",
			"SC1GW01_005A01",
			"SC1WA01_005G11",
			"SC1WA01_005G12",
			"SC1WA01_005G13",
			"-- DS Warnings --",
			"DS1WA01_WARN01",
			"DS1WA01_WARN02",
			"DS1WA01_WARN03",
			"DS1WA01_WARN04",
			"DS1WA01_WARN05",
			"DS1WA01_WARN06",
			"DS1WA01_WARN07",
			"DS1WA01_WARN08",
			"DS1WA01_WARN09",
			"DS1WA01_WARN10",
			"DS1WA01_WARN11",
			"DS1WA01_WARN12",
			"DS1WA01_WARN13",
			"DS1WA01_WARN14",
			"DS1WA01_WARN15",
			"DS1WA01_WARN16",
			"DS1WA01_WARN17",
			"DS1WA01_WARN18",
			"DS1WA01_WARN19",
			"DS1WA01_WARN20",
			"DS1WA01_WARN21",
			"DS1WA01_WARN22",
			"DS1WA01_WARN23",
			"DS1WA01_WARN24",
			"DS1WA01_WARN25",
			"DS1WA01_WARN26",
			"DS1WA01_WARN27",
			"DS1WA01_WARN28",
			"DS1WA01_WARN29",
			"DS1WA01_WARN30",
			"DS1WA01_WARN31",
			"DS1WA01_WARN32",
			"DS1WA01_WARN33",
			"-- EM Warnings --",
			"GENMESG_M00750",
			"GENMESG_M00751",
			"GENMESG_M00753",
			"GENMESG_M00754",
			"GENMESG_M00758",
			"GENMESG_M00762",
			"GENMESG_M00764",
			"GENMESG_M00765",
			"GENWARN_W00517",
			"GENWARN_W00518",
			"GENWARN_W00520",
			"GENWARN_W00521",
			"GENWARN_W00522",
			"GENWARN_W00523",
			"GENWARN_W00524",
			"GENWARN_W00525",
			"GENWARN_W00526",
			"GENWARN_W00536",
			"GENWARN_W00537",
			"GENWARN_W00538",
			"GENWARN_W00539",
			"GENWARN_W00540",
			"GENWARN_W00541",
			"GENWARN_W00545",
			"GENWARN_W00546",
			"GENWARN_W00557",
			"GENWARN_W00558",
			"GENWARN_W00559",
			"GENWARN_W00560",
			"GENWARN_W00562",
			"GENWARN_W00563",
			"GENWARN_W00579",
			"GENWARN_W00580",
			"GENWARN_W00581",
			"GENWARN_W00582",
			"GENWARN_W00585",
			"GENWARN_W00586",
			"GENWARN_W00587",
			"GENWARN_W00588",
			"GENWARN_W00589",
			"LI1WARN_W70500",
			"01MMESG_M01750",
			"01MWARN_W01500",
			"01MWARN_W01502",
			"01MWARN_W01513",
			"01MWARN_W01514",
			"01TMESG_M51750",
			"01TWARN_W51502",
			"01TWARN_W51503",
			"01TWARN_W51505",
			"01TWARN_W51506",
			"01TWARN_W51513",
			"01FMESG_M71750",
			"01FMESG_M71751",
			"01FMESG_M71752",
			"01FMESG_M71756",
			"01FMESG_M71757",
			"01FWARN_W71504",
			"01FWARN_W71505",
			"02MMESG_M02750",
			"02MMESG_M02751",
			"02MWARN_W02500",
			"02MWARN_W02502",
			"02MWARN_W02509",
			"02MWARN_W02510",
			"02MWARN_W02511",
			"02MWARN_W02513",
			"02MWARN_W02514",
			"02TWARN_W52500",
			"02TWARN_W52501",
			"02TWARN_W52502",
			"02TWARN_W52503",
			"02TWARN_W52504",
			"02TWARN_W52505",
			"02TWARN_W52506",
			"02TWARN_W52507",
			"02TWARN_W52508",
			"02TWARN_W52509",
			"02TWARN_W52510",
			"02TWARN_W52511",
			"02TWARN_W52512",
			"02TWARN_W52513",
			"02TWARN_W52514",
			"02TWARN_W52515",
			"02TWARN_W52516",
			"03MMESG_M03750",
			"03MWARN_W03500",
			"03MWARN_W03502",
			"03MWARN_W03512",
			"03MWARN_W03513",
			"03MWARN_W03514",
			"03TWARN_W53502",
			"03TWARN_W53503",
			"03TWARN_W53504",
			"03TWARN_W53505",
			"03TWARN_W53506",
			"03TWARN_W53507",
			"03TWARN_W53510",
			"03TWARN_W53512",
			"03TWARN_W53513",
			"04MMESG_M04750",
			"04MWARN_W04500",
			"04MWARN_W04502",
			"04MWARN_W04512",
			"04MWARN_W04513",
			"04MWARN_W04514",
			"04TWARN_W54502",
			"04TWARN_W54503",
			"04TWARN_W54504",
			"04TWARN_W54505",
			"04TWARN_W54506",
			"04TWARN_W54507",
			"04TWARN_W54510",
			"04TWARN_W54512",
			"04TWARN_W54513",
			"05MMESG_M05750",
			"05MWARN_W05500",
			"05MWARN_W05502",
			"05MWARN_W05512",
			"05MWARN_W05513",
			"05MWARN_W05514",
			"05TWARN_W55502",
			"05TWARN_W55503",
			"05TWARN_W55504",
			"05TWARN_W55505",
			"05TWARN_W55506",
			"05TWARN_W55507",
			"05TWARN_W55510",
			"05TWARN_W55512",
			"05TWARN_W55513",
			"06MMESG_M06750",
			"06MWARN_W06500",
			"06MWARN_W06502",
			"06MWARN_W06512",
			"06MWARN_W06513",
			"06MWARN_W06514",
			"06TWARN_W56502",
			"06TWARN_W56503",
			"06TWARN_W56504",
			"06TWARN_W56505",
			"06TWARN_W56506",
			"06TWARN_W56507",
			"06TWARN_W56510",
			"06TWARN_W56512",
			"06TWARN_W56513",
			"07MMESG_M07750",
			"07MWARN_W07500",
			"07MWARN_W07502",
			"07MWARN_W07510",
			"07MWARN_W07511",
			"07MWARN_W07512",
			"07MWARN_W07513",
			"07MWARN_W07514",
			"07TWARN_W57502",
			"07TWARN_W57503",
			"07TWARN_W57504",
			"07TWARN_W57505",
			"07TWARN_W57506",
			"07TWARN_W57507",
			"07TWARN_W57510",
			"07TWARN_W57512",
			"07TWARN_W57513",
			"08MMESG_M08750",
			"08MWARN_W08500",
			"08MWARN_W08502",
			"08MWARN_W08513",
			"08MWARN_W08514",
			"08TWARN_W58502",
			"08TWARN_W58503",
			"08TWARN_W58504",
			"08TWARN_W58505",
			"08TWARN_W58506",
			"08TWARN_W58507",
			"08TWARN_W58510",
			"08TWARN_W58512",
			"08TWARN_W58513",
			"09MMESG_M09750",
			"09MMESG_M09751",
			"09MWARN_W09500",
			"09MWARN_W09502",
			"09MWARN_W09510",
			"09MWARN_W09511",
			"09MWARN_W09513",
			"09MWARN_W09514",
			"09MWARN_W09517",
			"09MWARN_W09518",
			"09MWARN_W09519",
			"09TWARN_W59500",
			"09TWARN_W59501",
			"09TWARN_W59502",
			"09TWARN_W59503",
			"09TWARN_W59504",
			"09TWARN_W59505",
			"09TWARN_W59506",
			"09TWARN_W59507",
			"09TWARN_W59508",
			"09TWARN_W59509",
			"09TWARN_W59510",
			"09TWARN_W59511",
			"09TWARN_W59512",
			"09TWARN_W59513",
			"09TWARN_W59514",
			"09TWARN_W59515",
			"09TWARN_W59516",
			"10MMESG_M10750",
			"10MWARN_W10500",
			"10MWARN_W10502",
			"10MWARN_W10510",
			"10MWARN_W10511",
			"10MWARN_W10513",
			"10MWARN_W10514",
			"10MWARN_W10517",
			"10MWARN_W10518",
			"10MWARN_W10519",
			"10TWARN_W60502",
			"10TWARN_W60503",
			"10TWARN_W60504",
			"10TWARN_W60505",
			"10TWARN_W60506",
			"10TWARN_W60507",
			"10TWARN_W60510",
			"10TWARN_W60512",
			"10TWARN_W60513",
			"11MMESG_M11750",
			"11MWARN_W11500",
			"11MWARN_W11502",
			"11MWARN_W11513",
			"11MWARN_W11514",
			"11TWARN_W61502",
			"11TWARN_W61503",
			"11TWARN_W61504",
			"11TWARN_W61505",
			"11TWARN_W61506",
			"11TWARN_W61507",
			"11TWARN_W61510",
			"11TWARN_W61512",
			"11TWARN_W61513",
			"12MMESG_M12750",
			"12MMESG_M12751",
			"12MWARN_W12500",
			"12MWARN_W12502",
			"12MWARN_W12509",
			"12MWARN_W12510",
			"12MWARN_W12513",
			"12MWARN_W12514",
			"12TWARN_W62500",
			"12TWARN_W62501",
			"12TWARN_W62502",
			"12TWARN_W62503",
			"12TWARN_W62504",
			"12TWARN_W62505",
			"12TWARN_W62506",
			"12TWARN_W62507",
			"12TWARN_W62508",
			"12TWARN_W62509",
			"12TWARN_W62510",
			"12TWARN_W62511",
			"12TWARN_W62512",
			"12TWARN_W62513",
			"12TWARN_W62514",
			"12TWARN_W62515",
			"12TWARN_W62516",
			"13MWARN_W13500",
			"13MWARN_W13502",
			"13MWARN_W13509",
			"13MWARN_W13510",
			"13MWARN_W13513",
			"13MWARN_W13514",
			"13TMESG_M63750",
			"13TWARN_W63502",
			"13TWARN_W63503",
			"13TWARN_W63504",
			"13TWARN_W63505",
			"13TWARN_W63506",
			"13TWARN_W63507",
			"13TWARN_W63510",
			"13TWARN_W63512",
			"13TWARN_W63513",
			"14MWARN_W14500",
			"14MWARN_W14502",
			"14MWARN_W14509",
			"14MWARN_W14510",
			"14MWARN_W14513",
			"14MWARN_W14514",
			"14TMESG_M64750",
			"14TWARN_W64502",
			"14TWARN_W64503",
			"14TWARN_W64504",
			"14TWARN_W64505",
			"14TWARN_W64506",
			"14TWARN_W64507",
			"14TWARN_W64510",
			"14TWARN_W64512",
			"14TWARN_W64513",
			"15MMESG_M15750",
			"15MWARN_W15500",
			"15MWARN_W15502",
			"15MWARN_W15503",
			"15MWARN_W15511",
			"15TWARN_W65502",
			"15TWARN_W65503",
			"15TWARN_W65504",
			"15TWARN_W65505",
			"15TWARN_W65506",
			"15TWARN_W65507",
			"15TWARN_W65510",
			"15TWARN_W65512",
			"15TWARN_W65513",
			"16MWARN_W16500",
			"16MWARN_W16502",
			"16MWARN_W16513",
			"16MWARN_W16514",
			"16TMESG_M66750",
			"16TWARN_W66500",
			"16TWARN_W66501",
			"16TWARN_W66502",
			"16TWARN_W66504",
			"16TWARN_W66505",
			"16TWARN_W66506",
			"16TWARN_W66508",
			"16TWARN_W66509",
			"16TWARN_W66511",
			"16TWARN_W66512",
			"16TWARN_W66513",
			"16TWARN_W66514",
			"16TWARN_W66515",
			"16RMESG_M62750",
			"16RMESG_M62751",
			"16RMESG_M62752",
			"16RWARN_W62500",
			"16RWARN_W62501",
			"16RWARN_W62502",
			"16SMESG_M73750",
			"16SWARN_W73500",
			"LI2WARN_W74500",
			"MSPMESG_M80750",
			"MSPMESG_M80751",
			"MSPMESG_M80753",
			"MSPMESG_M80754",
			"MSPMESG_M80755",
			"MSPMESG_M80756",
			"MSPWARN_W80500",
			"MSPWARN_W80501",
			"MSPWARN_W80503",
			"MSPWARN_W80504",
			"MSPWARN_W80505",
			"CRCWARN_W75501",
			"CRCWARN_W75502",
			"-- IM Warnings --",
			"CB1CB01_ADVICE",
			"CB1CB02_ADVICE",
			"CB1CB03_ADVICE",
			"CB1SenR_Res004",
			"CB1SenR_Res005",
			"EM1EM01_ADVICE",
			"VM1VM01_ADVICE",
			"VM2VM02_ADVICE",
			"RESSenR_Res009",
			"TP1TP01_ADVICE",
			"IS1IS01_ADVICE",
			"IS1IS02_ADVICE",
			"SU1SU01_ADVICE",
			"SU1SU02_ADVICE",
			"RESSenR_Res015",
			"LAMLAM2_LAMINA",
			"RE1SenR_Res017",
			"RE2SenR_Res018",
			"RE3SenR_Res019",
			"ALGLOTS_LOTSTA",
			"ALGLOTS_LOTEND",
			"CB1LOTS_ADVICE",
			"AL2SenR_Res023",
			"AL3SenR_Res024",
			"ALGADS1_ADSERR",
			"RESSenR_Res026",
			"IS1IS11_ADVICE",
			"IS1IS10_ADVICE",
			"LTRDS01_ADVICE",
			"LTRDS02_ADVICE",
			"VFSDS03_ADVICE",
			"IVSDS04_ADVICE",
			"ISTDS05_ADVICE",
			"CPPDS06_ADVICE",
			"CB1SEL1_ADVICE",
			"CB1SEL2_ADVICE",
			"CB1SEL3_ADVICE",
			"CB1SEL4_ADVICE",
			"CB1SEL5_ADVICE",
			"RO1EM01_KEINEK",
			"RO1TM01_NOPICK",
			"T01SenR_Res042",
			"T02SenR_Res043",
			"T03SenR_Res044",
			"T04SenR_Res045",
			"T05SenR_Res046",
			"T06SenR_Res047",
			"CD1DS07_ADVICE",
			"T08SenR_Res049",
			"ISTTIME_ADV001",
			"ISTTIME_ADV002",
			"ISTTIME_ADV003",
			"ISTTIME_ADV004",
			"ISTTIME_ADV005",
			"ISTTIME_ADV006",
			"ISTTIME_ADV007",
			"ISTTIME_ADV008",
			"ISTTIME_ADV009",
			"ISTTIME_ADV010",
			"ISTTIME_ADV011",
			"ISTTIME_ADV012",
			"ISTTIME_ADV013",
			"ISTTIME_ADV014",
			"CDBSel6_ADVICE",
			"-- TM Warnings --",
			"CB1FA01_RE0000",
			"CB1FA01_S10422",
			"CB1FA01_VakBeh",
			"CLIFA01_LeerWT",
			"LLIFA01_Deckel",
			"CB1FA01_LotWe1",
			"CB1FA01_LotWe2",
			"CB1FA01_VakWar",
			"CB1FA01_DesWar",
			"CB1FA01_ReiWar",
			"CS0FA01_AbdPla",
			"PL0FA01_AbStur",
			"CB1FA01_Klinik",
			"PDOFA01_ABVORD",
			"CB1FA01_Lamin2",
			"SC1FA01_WARN15",
			"SC1FA01_RE0016",
			"SC1FA01_RE0017",
			"SC1FA01_RE0018",
			"SC1FA01_RE0019",
			"SC1FA01_RE0020",
			"SC1FA01_RE0021",
			"SC1FA01_RE0022",
			"CB1FA01_WARN23",
			"CB1FA01_WARN24",
			"SC1FA01_RE0025",
			"SC1FA01_WARN26",
			"SC1FA01_WARN27",
			"SC1FA01_WARN28",
			"SC1FA01_WARN29",
			"SC1FA01_WARN30",
			"SC1FA01_WARN31",
			"SC1FA01_WARN32",
			"SC1FA01_WARN33",
			"SC1FA01_RE0034",
			"SC1FA01_RE0035",
			"SC1FA01_RE0036",
			"SC1FA01_RE0037",
			"SC1FA01_RE0038",
			"SC1FA01_RE0039",
			"SC1FA01_RE0040",
			"SC1FA01_RE0041",
			"SC1FA01_RE0042",
			"SC1FA01_RE0043",
			"SC1FA01_RE0044",
			"SC1FA01_RE0045",
			"SC1FA01_RE0046",
			"SC1FA01_RE0047",
			"SC1FA01_RE0048",
			"SC1FA01_RE0049",
			"SC1FA01_RE0050",
			"-- PP Warnings --",
			"ResWARN_Res001",
			"QAHFA01_I7647B",
			"QAHFA02_I7647B",
			"QAHFA01_W00004",
			"QAHFA02_I4042F",
			"QAHFA01_W00006",
			"QAHFA01_W00007",
			"ResWARN_Res008",
			"MAIFA01_W00009",
			"MAIFA01_W00010",
			"MG1FA01_W00011",
			"MG2FA01_W00012",
			"MG3FA01_W00013",
			"MG4FA01_W00014",
			"MG5FA01_W00015",
			"MG1FA01_20VS65",
			"MG2FA01_20VS66",
			"MG3FA01_20VS67",
			"MG4FA01_20VS68",
			"MG5FA01_20VS69",
			"PRCFA01_W00021",
			"PRCFA01_W00022",
			"PRCFA01_W00023",
			"PRCFA01_W00024",
			"LPRFA01_W00025",
			"SPLFA01_W00026",
			"SHCFA01_W00027",
			"MAIWARN_W00028",
			"MAIWARN_W00029",
			"SHCFA01_W00030",
			"SHCFA01_W00031",
			"CLRFA01_W00032",
			"ESFFA01_W00033",
			"ResWARN_Res034",
			"ResWARN_Res035",
			"SDOFA01_W00036",
			"SDOFA01_W00037",
			"SF1FA01_W00038",
			"SF1FA01_W00039",
			"SS1FA01_W00040",
			"SF2FA01_W00041",
			"SF2FA01_W00042",
			"SS1FA01_W00043",
			"SF1FA01_W00044",
			"SF1FA01_W00045",
			"SS1FA01_W00046",
			"SF2FA01_W00047",
			"SF2FA01_W00048",
			"SS1FA01_W00049",
			"SF1FA01_W00050",
			"SF2FA01_W00051",
			"SLBFA01_0130N0",
			"SLBFA01_0131N0",
			"SLBFA01_0132N0",
			"SLBFA01_0133N0",
			"DB4FA01_0134N0",
			"DB3FA01_0135N0",
			"DB2FA01_0136N0",
			"DB1FA01_0137N0",
			"C2TFA01_0137N0",
			"MS1WARN_Res052",
			"MS1WARN_Res053",
			"ResWARN_Res054",
			"FOFFA01_W00055",
			"FOFFA01_W00056",
			"FOFFA01_W00057",
			"FOFFA01_W00058",
			"FOFFA01_W00059",
			"FOFFA01_W00060",
			"FOFFA01_W00061",
			"FOFFA01_W00062",
			"FOFFA01_W00063",
			"FOFFA01_W00064",
			"FOFFA01_W00065",
			"CB1FA01_INFO01",
			"CB1FA01_INFO02",
			"CB1FA01_INFO03",
			"CB1FA01_INFO04",
			"SF1WA01_DEAKTI",
			"SF2WA02_DEAKTI",
			"SF3WA03_DEAKTI",
			"SF4WA04_DEAKTI",
			"SF1WA01_MATERI",
			"SF2WA02_MATERI",
			"SF3WA03_MATERI",
			"SF4WA04_MATERI",
			"SF1FA01_I5359B",
			"SF2FA01_I5359B",
			"COPFA01_W00067",
			"COPFA01_I5637B",
			"SSTFA01_Calibr",
			"LPRWA02_INFORM",
			"SSTWA01_INFORM",
		};

		public static List<string> SkipPPTags = new List<string> {	// These PP Tags don't cause PP Downtime
				// Compressed Air Missing
				"CB1FA01_INFO01", "CB1FA01_INFO02", "CB1FA01_INFO03", "CB1FA01_INFO04", "CB1FA02_3041S6", "CB1FA02_S13411",

				// Waste Bin Full
				"QAHFA01_I76469",
				// No CTs/Lids Available
				"TC1FA69_0631S0", "COPFA01_advise", "CLPFA01_advise", "CLPFA04_INFORM",
				// Zone Homing
				"CLRFA01_SERVO1", "SPLFA01_SERVO1", "SPLFA01_SERVO2", "SDOFA01_SERVO1", "SSTFA01_SERVO1",
				"FOFFA01_SERVO1", "FOFFA01_SERVO2", "FOPFA01_SERVO3", "PRCFA01_SERVO1",
				"MF1FA01_SERVO1", "MF2FA01_SERVO2", "COPFA01_SERVO1",
				// Stations Not At Home Position
				"CDBFA01_STA536", "CDBFA01_STA596", "CDTFA01_STA535", "CLPFA01_STA521", "CLRFA01_STA530", "COPFA01_STA514", "CP1FA01_STA520",
				"EMAFA04_STA595", "EMAFA07_8331S5", "EMAFA98_4021Z1", "ESFFA01_STA550", "FOFFA01_STA571", "FOPFA01_STA570", "LSPFA01_STA511",
				"MAIFA01_STA591", "MAIFA02_STA591", "MF1FA01_STA590", "MF2FA02_STA590", "MG1FA01_STA592", "MG2FA02_STA592", "MG3FA03_STA592",
				"MG4FA04_STA592", "MG5FA05_STA592", "PRCFA01_STA560", "QAHFA01_STA585", "QAHFA02_STA585", "QAHFA03_STA585", "QAHFA04_STA585",
				"RBHFA04_STA590", "RMHFA03_STA590", "SDOFA01_STA540", "SF1FA01_STA518", "SF2FA02_STA518", "SF3FA03_STA518", "SF4FA04_STA518",
				"SHCFA01_STA512", "SHCFA02_STA513", "SPLFA01_S00031", "SPLFA01_S02077", "SPLFA01_STA517", "SSTFA01_STA575",
				// Safety Zone X in Auto Start
				"OM1AV04_ADVICE", "OM2AV04_ADVICE", "OM3AV04_ADVICE",	// Autostart
				"OM1AV03_ADVICE", "OM2AV03_ADVICE", "OM3AV03_ADVICE",	// Automatic
				"OM1AV01_ADVICE", "OM2AV01_ADVICE", "OM3AV01_ADVICE",	// Setup

				// Zone 1
				// Shell Placer tracks not in home position
				"SL1CY02_5451Y1", "SL1CY02_5451Y3", "SL1CY02_5451Y5", "SL1CY02_5451Y8", "SL1CY02_5452Y1", "SL1CY02_5452Y3", "SL1CY02_5452Y8",
				"SL2CY02_5457Y1", "SL2CY02_5457Y3", "SL2CY02_5457Y5", "SL2CY02_5457Y8", "SL2CY02_5458Y1", "SL2CY02_5458Y3", "SL2CY02_5458Y8",
				"SL3CY02_5453Y1", "SL3CY02_5453Y5", "SL3CY02_5453Y8", "SL3CY02_5454Y1", "SL3CY02_5454Y3", "SL3CY02_5454Y8", "SL4CY02_5459Y1",
				"SL4CY02_5459Y3", "SL4CY02_5459Y5", "SL4CY02_5460Y1", "SL4CY02_5460Y3", "SL4CY02_5460Y8", "SL4CY02_5459Y8", "SL3CY02_5453Y3",
				// Shell Feeding Waste Boxes full
				"SS1FA01_W00040", "SS1FA01_W00043", "SS1FA01_W00046", "SS1FA01_W00049",
				// CT Outpusher stacking almost full
				"COPFA01_W00067",

				// Zone 2
				// Saline Dosing (Deslected, empty)
				"SDOFA01_W00036", "SDOFA01_W00037",
				// Lens On Shell
				"FPBFA01_PM1LO1", "FPBFA01_PM1LO2", "FPBFA01_PM1LO3", "FPBFA01_PM1LO4", "FPBFA01_PM1LO5", "FPBFA01_PM1LO6", "FPBFA01_PM1LO7",
				"FPBFA01_PM2LO1", "FPBFA01_PM2LO2", "FPBFA01_PM2LO3", "FPBFA01_PM2LO4", "FPBFA01_PM2LO5", "FPBFA01_PM2LO6", "FPBFA01_PM2LO7",
				"FPBFA01_PM3LO1", "FPBFA01_PM3LO2", "FPBFA01_PM3LO3", "FPBFA01_PM3LO4", "FPBFA01_PM3LO5", "FPBFA01_PM3LO6", "FPBFA01_PM3LO7",
				"FPBWARN_DESEL0",
				// Foil Feeding Magazines [Almost] Empty
				"FOFFA01_W00055", "FOFFA01_W00056", "FOFFA01_W00057", "FOFFA01_W00058", "FOFFA01_W00059",
				"FOFFA01_W00060", "FOFFA01_W00061", "FOFFA01_W00062", "FOFFA01_W00063", "FOFFA01_W00064",
				"FOFFA01_W00065",
				// Laser Printer Malfunction
				"LPRFA01_S00002", "LPRFA01_S00006", "LPRFA01_S00010",
				// Sealhead / lasermask cleaning Soon
				"LPRWA02_INFORM", "SSTWA01_INFORM",
				// Sealing station being removed so It can be cleaned
				"SSTCY01_7455KH", "SSTCY02_7455KH", "SSTFA01_S02062", "SSTFA01_S02061",

				// Zone 3
				// MRI Boxes Full
				"QAHFA01_I40427", "QAHFA01_I76469", "QAHFA01_W00003", "QAHFA01_W00004", "QAHFA02_I40427", "QAHFA02_I76470", "QAHFA01_W00006",
				// Out of Empty Magazines for Blister Filling
				"MAIWARN_W00028", "MAIWARN_W00029",
				// SRIS blah balh
				"SRIFA01_ADVICE", "SRIFA02_ADVICE", "SRIFA03_ADVICE", "SRIFA04_ADVICE", "SRIFA05_ADVICE", "SRIFA06_ADVICE", "SRIFA07_ADVICE",
				"SRIFA08_ADVICE", "SRIFA09_ADVICE", "SRIFA10_ADVICE", "SRIFA11_ADVICE", "SRIFA12_ADVICE", "SRIFA13_ADVICE", "SRIFA14_ADVICE",
				"SRIFA15_ADVICE", "SRIFA16_ADVICE", "SRIFA17_ADVICE", "SRIFA18_ADVICE", "SRIFA19_ADVICE", "SRIFA20_ADVICE", "SRIFA21_ADVICE",
				"SRIFA22_ADVICE", "SRIFA23_ADVICE", "SRIFA24_ADVICE", "SRIFA25_ADVICE", "SRIFA26_ADVICE", "SRIFA27_ADVICE", "SRIFA28_ADVICE",
				"SRIFA29_ADVICE", "SRIFA30_ADVICE", "SRIFA31_ADVICE", "SRIFA32_ADVICE", "SRIFA33_ADVICE", "SRIFA34_ADVICE", "SRIFA35_ADVICE",
				"SRIFA36_ADVICE", "SRIFA37_ADVICE", "SRIFA38_ADVICE", "SRIFA39_ADVICE", "SRIFA40_ADVICE", "SRIFA41_ADVICE", "SRIFA42_ADVICE",
				"SRIFA43_ADVICE", "SRIFA44_ADVICE", "SRIFA45_ADVICE", "SRIFA46_ADVICE", "SRIFA47_ADVICE", "SRIFA48_ADVICE", "SRIFA49_ADVICE",
				"SRIFA50_ADVICE", "SRIFA51_ADVICE", "SRIFA52_ADVICE", "SRIFA53_ADVICE", "SRIFA54_ADVICE", "SRIFA55_ADVICE", "SRIFA56_ADVICE",
				"SRIFA57_ADVICE", "SRIFA58_ADVICE", "SRIFA59_ADVICE", "SRIFA60_ADVICE", "SRIFA61_ADVICE", "SRIFA62_ADVICE", "SRIFA63_ADVICE",
				"SRIFA67_ADVICE", "SRIFA70_ADVICE",
				// Gondola X - Almost full
				"MG1FA01_W00011", "MG2FA01_W00012", "MG3FA01_W00013", "MG2FA04_W00014", "MG5FA01_W00015",
				// Gondola X - Full
				"MG1FA01_S00048", "MG2FA01_S00049", "MG3FA01_S00050", "MG4FA01_S00051", "MG5FA01_S00052",
				// Gondola 5 - Stored Magazines time exceeded
				"MG5FA01_20VS69",
				// CT Unload
				"CTUMSG1_UNLOAD", "CTUMSG2_UNLOAD", "CTUMSG3_UNLOAD", "CTUMSG4_UNLOAD",
			};

		// -- Grouping Faults
		public static List<Fault> GroupFaults(List<Fault> faults, int WindowInMinutes = 5) {
			var ret = new List<Fault>(faults.Count);
			var toSkip = new List<int>();

			// Sort the faults by time
			faults.Sort((a, b) => a.Start.CompareTo(b.Start));

			// For each message, see if the next chronological fault (For the same area) has a start time within X of our end time
			// If so, join them and mark the joined fault for removal
			// If we go too far, then stop
			for (int i = 0; i < faults.Count; i++) {
				var flt = faults[i];

				// Skip any fault we've already joined to another
				if (toSkip.Contains(i)) continue;

				// Skip MTTR faults (I think this is the best way to tell)
				if (flt.OpRespTime <= 0) {
					// Go forward and find related faults
					for (int x = i + 1; x < faults.Count; x++) {
						var iterFlt = faults[x];

						// Skip faults we've already merged (Do i need this here with x? )
						if (toSkip.Contains(x)) continue;

						// Skip ahead until messages are starting after us
						if (iterFlt.Start < flt.End) continue;

						// End search if we're more than Window away from end of fault
						if ((iterFlt.Start - flt.End).TotalMinutes > WindowInMinutes) break;

						// Otherwise, join faults that match up
						if (iterFlt.PM == flt.PM && iterFlt.Message == flt.Message
							&& (iterFlt.Start - flt.End).TotalMinutes <= WindowInMinutes) {
							flt.End = iterFlt.End;
							toSkip.Add(x);
						}   // Keep going
					}
				}
				ret.Add(flt);
			}
			return ret;
		}

		public static List<Event> GroupFaults(List<Event> faults, int WindowInMinutes = 5) {
			var ret = new List<Event>(faults.Count);
			var toSkip = new List<int>();

			// Sort the faults by time
			faults.Sort((a, b) => a.Start.CompareTo(b.Start));

			// For each message, see if the next chronological fault (For the same area) has a start time within X of our end time
			// If so, join them and mark the joined fault for removal
			// If we go too far, then stop
			for (int i = 0; i < faults.Count; i++) {
				var flt = faults[i];

				// Skip any fault we've already joined to another
				if (toSkip.Contains(i)) continue;

				// Skip MTTR faults (I think this is the best way to tell)
				if (!flt.IsMTTRMsg) {
					// Go forward and find related faults
					for (int x = i + 1; x < faults.Count; x++) {
						var iterFlt = faults[x];

						// Skip faults we've already merged (Do i need this here with x? )
						if (toSkip.Contains(x)) continue;

						// Skip ahead until messages are starting after us
						if (iterFlt.Start < flt.End) continue;

						// End search if we're more than Window away from end of fault
						if ((iterFlt.Start - flt.End).TotalMinutes > WindowInMinutes) break;

						// Otherwise, join faults that match up
						if (iterFlt.Area == flt.Area && iterFlt.Message == flt.Message) {
							flt.End = iterFlt.End;
							toSkip.Add(x);
						}
					}
				}
				ret.Add(flt);
			}
			return ret;
		}

		// -- Logging ----------------------
		private static readonly Object LogLock = new object();

		public static void Log(string msg, params object[] para) {
			msg = string.Format(msg, para);
			msg = $"{DateTime.Now.ToString()}:\t{msg}";
			lock (LogLock) {
				using (var f = File.AppendText(LogFile)) {
					f.WriteLine(msg);
				}
			}
		}

		// -- PM Time -- Get Available Time by PM by Month
		public class PMInstance
		{
			public DateTime Date;
			public int Month => Date.Month;
			public string PM;   // PM01 - PM15
			public TimeSpan CyclingTime, ProductiveTime, FaultedTime, TotalTime;
		}

		public static List<PMInstance> GetPMTimes(DateTime start, DateTime end, List<string> Areas, System.Threading.CancellationToken cancel) {
			// Trim the start times
			start = new DateTime(start.Year, start.Month, 1);
			end = new DateTime(end.Year, end.Month, 1).AddMonths(1).AddMilliseconds(-1);

			// Create the return array to match the Areas array
			var ret = new List<PMInstance>(Areas.Count * 5);

			// -- Iterate by month            git
			for (var s = start; s <= end; s = s.AddMonths(1)) {
				var e = new DateTime(s.Year, s.Month, 1).AddMonths(1).AddMilliseconds(-1);
				var totalTime = new TimeSpan((int)(e - s).Add(new TimeSpan(0, 0, 0, 0, 1)).TotalHours * 4, 0, 0);    // Add that millisecond back on

				// Find files in this month
				var files = FindFilesInQuery(s, e);

				// Setup the PM Instances for this month for each line
				var modules = new List<PMInstance>(Areas.Count);
				foreach (var area in Areas) {
					modules.Add(new PMInstance() {
						Date = s.Date,
						PM = area,
						TotalTime = totalTime
					});
				}

				// generic event placeholder (We want to use event methods, but not the event itself, so don't recreate it all the time)
				var ev = new Event();

				// -- Iterate through the events
				foreach (var file in files) {
					var text = File.ReadAllLines(file);

					int startIndex = -1;
					bool newVersion = false;

					if (text[1] == "Session altered.") {
						startIndex = 6;
						newVersion = false;
					} else {
						startIndex = 3;
						newVersion = true;
					}

					// -- Iterate the lines in the file
					for (int i = startIndex; i < text.Length; ++i) {
						var split = CSVSplitLine(text[i]);

						// Check for cancelation
						if (cancel.IsCancellationRequested)
							return ret;

						if (split.Count < 1 || !split[0].StartsWith("APL")) continue;

						if (newVersion && split.Count == 15) {
							ev.Area = split[0];
							ev.Start = DateTime.ParseExact(split[4].Trim(), "MM/dd/yyyy HH:mm:ss", null);
							ev.End = DateTime.ParseExact(split[6].Trim(), "MM/dd/yyyy HH:mm:ss", null);
							ev.Tag = split[1];
							ev.Message = split[9];
						} else if (split.Count == 26) {
							ev.Area = split[0];
							ev.Start = DateTime.ParseExact(split[6].Trim(), "MM/dd/yyyy HH:mm:ss", null);
							ev.End = DateTime.ParseExact(split[8].Trim(), "MM/dd/yyyy HH:mm:ss", null);
							ev.Tag = split[2];
							ev.Message = split[11];
						}

						// If the line was bad, or the date is our of range, skip this iter
						if (ev.Area?.Length == 0 || !Module_Keys.Contains(ev.Area) || ev.Start.Month != s.Month || ev.End.Month != s.Month)
							continue;

						// Convert Area string to a PM string
						var apl = int.Parse(ev.Area[4].ToString());
						if (ev.Area[7] == 'M') {           // MM, EM, IM, TM
														   // APL01_PM1MM
							var line = ((apl - 1) * 3) + int.Parse(ev.Area[8].ToString());
							var machine = ev.Area.Substring(9, 2);

							// For 7+, wrap UV into MM messages
							if (machine == "UV") machine = "MM";

							ev.Area = string.Format("PM{0}", line);
						} else if (ev.Area[7] == 'P') {      // PP
															 // APL02_PP1T2
							var line = ((apl - 1) * 2) + int.Parse(ev.Area[10].ToString());
							ev.Area = string.Format("PP{0}", line);
						}

						// Skip unwanted areas
						if (ev.Area.StartsWith("PP")) continue; // ignore PP. MM, EM, IM, and TM only

						// See if we have this area
						var idex = Areas.IndexOf(ev.Area);
						if (idex > -1) {
							// throw away faults not in range
							if (ev.Start > e || ev.End < s) continue;   // Events that start after we end | end before we start

							// Make sure we chomp the start and end times of this message
							if (ev.Start < s) ev.Start = s;
							if (ev.End > e) ev.End = e;

							// If found, increment the relavant time
							if (ev.IsCyclingMsg) {
								modules[idex].CyclingTime += ev.Duration;
							} else if (ev.IsProductiveMsg) {
								modules[idex].ProductiveTime += ev.Duration;
							} else if (ev.IsMTTRFaultEventActiveMsg) {
								modules[idex].FaultedTime += ev.Duration;
							}
						}

						ev.Area = "";
					}
				}

				// Add the times we found for this month to the array
				ret.AddRange(modules);
			}

			return ret;
		}

		// -- Progress Struct
		public class ProgressWithBarAndLabel
		{
			public string Label = string.Empty;
			public int Max = 0, Min = 0, Current = 0;    // Max == 0 is indeterminate
		}
	}

	public static class StringExtentions
	{
		public static bool Contains(this String str, string txt, bool ignoreCase) {
			return str.IndexOf(txt, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal) >= 0;
		}

		public static bool StartsWith(this String str, string txt, bool ignoreCase) {
			return str.StartsWith(txt, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal);
		}

		public static bool EndsWith(this String str, string txt, bool ignoreCase) {
			return str.EndsWith(txt, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal);
		}
	}
}
