﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace MTTROverview
{
	public class IncrementalMTTRFaultBuilder
	{
		#region Constants / Parameters

		private const double SafeInterateTimeDiffInHours = 4;   // How far from the End time of the last fault in Events can we be sure we have all faults present
		private const ulong FindFaultBufferSeconds = 8;             // Time to go backwards from a MTTR_Faulted event in search of the originating fault msg

		#endregion Constants / Parameters

		#region Private Members

		public List<Event> Events = new List<Event>(1500),                 // All Messages
							Faults = new List<Event>();                     // MTTR Faults we've created

		private HashSet<string> KnownFaultKeys = new HashSet<string>();     // Prevent duplicate events in fault set
																			//private int LastParsedEventIndex = -1;

		#endregion Private Members

		#region Public Members

		public List<Event> MTTRFaults { get => Faults; }
		public string Area { get => Events.Count < 1 ? string.Empty : Events[0].Area; }

		#endregion Public Members

		#region Private Methods

		private void BuildMTTRFaults(bool buildAll = true) {
			if (Events.Count < 1) return;

			var lastTime = buildAll ? Events.Last().End.AddHours(2) : Events.Last().End.AddHours(-SafeInterateTimeDiffInHours);
			var isPP = Events[0].Machine == "PP";

			var i = 0;
			for (i = 0; i < Events.Count; i++) {
				var msg = Events[i];

				// Looping complete, delete all processed events
				if (msg.Start > lastTime) {
					break;
				}

				if (isPP) {
					if (msg.IsAFaultMessage && KnownFaultKeys.Add(msg.GetHash())) Faults.Add(msg);
				} else if (msg.IsMTTRFaultEventActiveMsg) {
					if (buildAll || msg.End <= lastTime) {
						var e = BuildMTTRFault(i);
						if (KnownFaultKeys.Add(e.GetHash()))
							Faults.Add(e);
					} else {
						// Search backwards until we're a minute away from the fault start before we cull the faults
						var exitTime = msg.Start.AddMinutes(-1);
						for (i = i - 1; i >= 0; i--) {
							if (Events[i].Start < exitTime) {
								i++;
								break;
							}
						}
						break;
					}
				}
			}

			// Remove the processed faults
			if (i > 0)
				Events.RemoveRange(0, i);
		}

		private Event BuildMTTRFault(int fltID) {
			Debug.Assert(Events[fltID].IsMTTRFaultEventActiveMsg, "fltID must point to the ID of the MTTR_FaultEventActive event!");

			var fault = new Event() {
				Area = Events[fltID].Area,
				Start = Events[fltID].Start,
				End = Events[fltID].End,
				Message = string.Format("Unknown {0} Fault", Events[fltID].Machine),
			};
			fault.Tag = fault.Message;  // 2018-09-10 -- Use the tag of the fault for the Shree report

			// Search earlier in time to BufferSeconds before this event started
			var startIndex = fltID;
			for (int y = fltID - 1; y >= 0; --y) {  // search earlier
				if ((fault.Start - Events[y].Start).TotalSeconds > FindFaultBufferSeconds)
					break;  // Loop is done
				startIndex = y;
			}

			Event fltText = null;
			for (int y = startIndex; y < Events.Count; ++y) {  // search through event's faults
				var msg = Events[y];
				if (msg.Start > fault.End) // Past fault duration
					break;  // Loop is done

				// Check if this event is a better fault description
				if (msg.IsAFaultMessage && (msg.Start - fault.Start).TotalSeconds < FindFaultBufferSeconds && (fltText == null || (fltText.Duration < msg.Duration))) {
					fltText = msg;
				} else if (msg.Tag.StartsWith("RPIMTTR_OPRESP")) {
					fault.in_OPRespTime = msg.Duration;
				} else if (msg.Tag.StartsWith("RPIMTTR_RECOVE")) {
					fault.in_RecoveryTime = msg.Duration;
				} else if (msg.Tag.StartsWith("RPIMTTR_EMRESP")) {
					fault.in_EMRespTime = msg.Duration;
				} else if (msg.Tag.StartsWith("RPIMTTR_REPAIR")) {
					fault.in_RepairTime = msg.Duration;
				}
			}
			if (fltText != null) {
				fault.Message = fltText.Message;
				fault.Tag = fltText.Tag;        // 2018-09-10 -- Use the tag of the fault for the Shree report
			}

			return fault;
		}

		#endregion Private Methods

		#region Public Methods

		public void Clear() {
			Events = new List<Event>();
			Faults = new List<Event>();
		}

		public void AddEvents(List<Event> events, bool andBuild = false) {
			if (events.Count < 1) return;
			if (events.Count > 1 && events[0].Area != events.Last().Area) {
				throw new ArgumentException("IncrementalMTTRFaultBuilder.AddEvents called with an event list with differing Areas!");
			}
			if (Events.Count > 0 && Events[0].Area != events[0].Area) {
				throw new ArgumentException("IncrementalMTTRFaultBuilder.AddEvents called to add events with different areas than we already have!");
			}

			Events.AddRange(events);
			Events.Sort((x, y) => x.Start.CompareTo(y.Start));			

			if (andBuild) {
				BuildFaultsIterate();
			}
		}

		public List<Event> FinishBuildingFaults() {
			// Non iterative fault method
			BuildMTTRFaults();
			return Faults;
		}

		public void BuildFaultsIterate() {
			BuildMTTRFaults(false);
		}

		#endregion Public Methods
	}
}
