﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MTTROverview
{
	public partial class DetailReportForm : Form
	{
		private Timer FilterTypingTimeout;
		private readonly TimeSeries timeSeries = new TimeSeries();
		private readonly List<Event> AllFaults = new List<Event>();
		private readonly List<Event> AllMessages = new List<Event>();
		private bool Loaded = false;
		private bool ignoreFilters = false;

		private readonly string Title;

		public DetailReportForm(string title, List<Event> msgs, List<TimeSeriesRow> rows) {
			InitializeComponent();
			SetupGrids();

			// Setup Timeseries Control
			TimeSeriesBox.Controls.Add(timeSeries);
			timeSeries.BorderStyle = BorderStyle.FixedSingle;
			timeSeries.Dock = DockStyle.Fill;
			timeSeries.TimeClicked += TimeSeriesTimeClicked;

			// -- Remember this data
			Title = title;
			AllMessages = msgs;
			AllFaults = rows.SelectMany(x => x.Faults).ToList();
			timeSeries.Series.Clear();
			timeSeries.Series.AddRange(rows);

			// -- Load the UI
			LoadUI();
		}

		private void SetupGrids() {
			foreach (var grid in new List<DataGridView> { MessagesList, RFIDGrid, TopFaultAreas, TopFaultsGrid, FaultList, OverviewGrid }) {
				grid.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
				//grid.ColumnHeadersDefaultCellStyle.Font = new Font(DefaultFont, FontStyle.Italic);
				grid.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
				grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;
			}
		}

		// -- Load Data
		private void LoadUI() {
			TabControl1.TabPages.Clear();
			Loaded = true;
			AllMessages.Sort((x, y) => x.Start.CompareTo(y.Start));

			// Force the Timeseries to update itself
			timeSeries.Redraw();

			// -- Load the Overview Tab
			//StatusBarText.Text = "Loading Overview...";
			UpdateOverviewList();
			TabControl1.TabPages.Add(OverviewTab);

			// -- Load the Top Fault Areas
			//StatusBarText.Text = "Loading Top Fault Areas...";
			UpdateTopFaultAreasList();
			TopFaultAreas.Enabled = true;

			// -- Load the Fault Event List
			//StatusBarText.Text = "Loading Fault Events Tab...";
			UpdateFaultList();
			TabControl1.TabPages.Add(FaultsTab);

			// -- Load the Fault Count / Duration List
			//StatusBarText.Text = "Loading Fault Count / Duration Tab...";
			UpdateTopFaultsList();
			TabControl1.TabPages.Add(TopFaultsTab);

			// -- Load The Message List in the background (Longest Load)
			//StatusBarText.Text = "Loading Chronological Message Tab...";
			BuildModuleExcludeMenu();
			UpdateMessagesList();
			TabControl1.TabPages.Add(MessagesTab);

			// -- Load the RFID faults
			UpdateRFIDList();

			// -- Done, Clean up
			//ProgressBar.Style = ProgressBarStyle.Continuous;
			//StatusBarText.Text = string.Format("Loading Complete, took {0}ms", sw.ElapsedMilliseconds);
			TabControl1.SelectedTab = OverviewTab;
		}

		//private void LoadFilesBtn_Click(object sender, EventArgs e) {
		//    if (!Directory.Exists(DataPath)) {
		//        DataPathSelectBtn_Click(sender, e);
		//    }

		//    if (!Directory.Exists(DataPath)) {
		//        return;
		//    }

		//    if (!DateTime.TryParse(StartTimeBox.Text, out DateTime start)) {
		//        StartTimeBox.Focus();
		//        return;
		//    }
		//    if (!DateTime.TryParse(EndTimeBox.Text, out DateTime end)) {
		//        EndTimeBox.Focus();
		//        return;
		//    }

		//    if (start < end)
		//        NewTimeframeQuery(start, end, DataPath);
		//    else
		//        NewTimeframeQuery(end, start, DataPath);

		//    //using (OpenFileDialog dlg = new OpenFileDialog()) {
		//    //    dlg.Filter = "Files (Excel or CSV)|*.csv;*.xls;*.xlsx|All Files (*.*)|*.*";
		//    //    dlg.Multiselect = true;

		//    //    if (dlg.ShowDialog() == DialogResult.OK) {
		//    //        //LoadFromFiles(dlg.FileNames.ToList());

		//    //        var path = Path.GetDirectoryName(dlg.FileName);
		//    //        NewTimeframeQuery(DateTime.MinValue, DateTime.MaxValue, path);
		//    //    }

		//    //    //dlg.Title = "Select File to Compare to";
		//    //    //if (dlg.ShowDialog() == DialogResult.OK) {
		//    //    //    var events = ParseExcel(dlg.FileName).Where(x => x.Message.Contains("Productive")).Select(x => x).ToList();
		//    //    //    events.Sort((a, b) => a.Start.CompareTo(b.Start));
		//    //    //    // Write to file
		//    //    //    using (FileStream file = new FileStream("BusinessObjects.csv", FileMode.Create))
		//    //    //    using (StreamWriter writer = new StreamWriter(file)) {
		//    //    //        foreach (var ev in events) {
		//    //    //            writer.WriteLine("{0},{1},{2},{3}",
		//    //    //                ev.Start,
		//    //    //                ev.End,
		//    //    //                ev.Duration,
		//    //    //                ev.Message);
		//    //    //        }
		//    //    //    }

		//    //    //    var old_events = AllMessages.Where(x => x.Area == "PM2MM" && x.Message.Contains("Productive")).Select(x => x).ToList();
		//    //    //    old_events.Sort((a, b) => a.Start.CompareTo(b.Start));
		//    //    //    // Write to file
		//    //    //    using (FileStream file = new FileStream("ScadaAlarms.csv", FileMode.Create))
		//    //    //    using (StreamWriter writer = new StreamWriter(file)) {
		//    //    //        foreach (var ev in old_events) {
		//    //    //            writer.WriteLine("{0},{1},{2},{3}",
		//    //    //                ev.Start,
		//    //    //                ev.End,
		//    //    //                ev.Duration,
		//    //    //                ev.Message);
		//    //    //        }
		//    //    //    }
		//    //    //}

		//    //}
		//}
		//private async void LoadFromFiles(List<string> files) {
		//    AllFaults.Clear();
		//    AllMessages.Clear();
		//    timeSeries.Series.Clear();

		//    // Remove the Tab Pages before they're loaded
		//    TabControl1.TabPages.Clear();
		//    TopFaultAreas.Enabled = false;

		//    // Inform the user of a long operation
		//    StatusBarText.Text = "Loading, please wait...";
		//    ProgressBar.Style = ProgressBarStyle.Marquee;
		//    Stopwatch sw = new Stopwatch();
		//    sw.Start();

		//    await Task.Run(() => {
		//        // Create a new TimeSeriesRow for each file
		//        foreach (var f in files) {
		//            List<Event> events = null;
		//            if (Path.GetExtension(f) == ".xls" || Path.GetExtension(f) == ".xlsx") {
		//                events = ParseExcel(f);
		//            }
		//            else {
		//                events = ParseCSV(f);
		//            }
		//            if (events == null || events.Count < 1) continue;

		//            // Remember these Messages
		//            AllMessages.AddRange(events);
		//        }

		//        //var lists = AllMessages.GroupBy(x => x.Area).Select(x => x.Select(t=>t).ToList()).ToList();
		//        // Sort the lists in order
		//        List<List<Event>> lists = new List<List<Event>>();
		//        // Manually add in each row in order
		//        for (int i = 1; i < 15; i++) {
		//            foreach (var module in new List<string> { "MM", "EM", "IM", "TM" }) {
		//                var list = AllMessages.Where(x => x.Area == string.Format("PM{0}{1}", i, module)).OrderBy(x => x.Start).Select(x => x).ToList();

		//                if (list != null && list.Count > 0)
		//                    lists.Add(list);
		//            }
		//        }

		//        foreach (var area in lists) {
		//            // Setup Row
		//            TimeSeriesRow row = new TimeSeriesRow();
		//            row.Title = area.First().Area;
		//            row.Cycling = area.Where(x => x.Message.Contains("RPIMTTR_INAUTO MTTR:")).Select(x => x).ToList();
		//            row.Productive = area.Where(x => x.Message.Contains("RPIMTTR_INPROD MTTR:")).Select(x => x).ToList();
		//            row.LotChange = GetLotChangeEvents(area);
		//            row.Faults = GetFaultEvents(area);
		//            timeSeries.Series.Add(row);

		//            // Remember these faults
		//            AllFaults.AddRange(row.Faults);
		//        }
		//    });
		//    Loaded = true;
		//    AllMessages.Sort((x, y) => x.Start.CompareTo(y.Start));
		//    sw.Stop();

		//    // Force the Timeseries to update itself
		//    timeSeries.Redraw();

		//    // -- Load the Overview Tab
		//    StatusBarText.Text = "Loading Overview...";
		//    UpdateOverviewList();
		//    TabControl1.TabPages.Add(OverviewTab);

		//    // -- Load the Top Fault Areas
		//    StatusBarText.Text = "Loading Top Fault Areas...";
		//    UpdateTopFaultAreasList();
		//    TopFaultAreas.Enabled = true;

		//    // -- Load the Fault Event List
		//    StatusBarText.Text = "Loading Fault Events Tab...";
		//    UpdateFaultList();
		//    TabControl1.TabPages.Add(FaultsTab);

		//    // -- Load the Fault Count / Duration List
		//    StatusBarText.Text = "Loading Fault Count / Duration Tab...";
		//    UpdateTopFaultsList();
		//    TabControl1.TabPages.Add(TopFaultsTab);

		//    // -- Load The Message List in the background (Longest Load)
		//    StatusBarText.Text = "Loading Chronological Message Tab...";
		//    BuildModuleExcludeMenu();
		//    UpdateMessagesList();
		//    TabControl1.TabPages.Add(MessagesTab);

		//    // -- Load the RFID faults
		//    UpdateRFIDList();

		//    // -- Done, Clean up
		//    ProgressBar.Style = ProgressBarStyle.Continuous;
		//    StatusBarText.Text = string.Format("Loading Complete, took {0}ms", sw.ElapsedMilliseconds);
		//    TabControl1.SelectedTab = OverviewTab;
		//}

		// -- Load Data Grids
		private void UpdateMessagesList() {
			MessagesList.AutoGenerateColumns = false;
			//MessagesList.Rows.Clear();

			// -- Load the Filters
			FilterMessages.FilterAllWarnings = FilterAllWarningsBtn.Checked;
			FilterMessages.FilterCycleMsgs = FilterCycleMsgsBtn.Checked;
			FilterMessages.FilterDoorMsgs = FilterDoorMsgsBtn.Checked;
			FilterMessages.FilterRFIDMsgs = FilterRFIDMsgsBtn.Checked;
			FilterMessages.FilterSCEMsgs = FilterSCEMsgsBtn.Checked;
			FilterMessages.FilterMTTRMsgs = FilterMTTRMessagesBtn.Checked;
			FilterMessages.FilterKey = FilterTextBox.Text;

			List<DataGridViewRow> ret = new List<DataGridViewRow>();
			//foreach (var f in FilterMessages.Filter(AllMessages)) {
			//    var row = new DataGridViewRow();
			//    row.CreateCells(MessagesList,

			//        f.Area,
			//        f.Start,
			//        f.End,
			//        Utility.PrettyTimeSpan(f.End - f.Start),
			//        f.Message);

			//    row.Cells[4].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;

			//    if (IsAnActionMessage(f.Message)) {
			//        Color ActionColor = Color.LightGreen;
			//        row.Cells[0].Style.BackColor = ActionColor;
			//        row.Cells[1].Style.BackColor = ActionColor;
			//        row.Cells[2].Style.BackColor = ActionColor;
			//        row.Cells[3].Style.BackColor = ActionColor;
			//        row.Cells[4].Style.BackColor = ActionColor;
			//    }

			//    ret.Add(row);
			//}
			MessagesList.DataSource = FilterMessages.Filter(AllMessages);
			//MessagesList.Rows.AddRange(ret.ToArray());
		}

		private async void UpdateTopFaultsList() {
			Dictionary<string, Tuple<uint, double>> Faults = new Dictionary<string, Tuple<uint, double>>();

			// Count the Fault Occurances and Accumulated Duration
			await Task.Run(() => {
				foreach (var f in AllFaults) {
					if (Faults.ContainsKey(f.Message)) {
						var t = Faults[f.Message];
						Faults[f.Message] = new Tuple<uint, double>(t.Item1 + 1, t.Item2 + (f.End - f.Start).TotalHours);
					} else {
						Faults.Add(f.Message, new Tuple<uint, double>(1, (f.End - f.Start).TotalHours));
					}
				}
			});

			// Sort the list to add, then add to the ui
			TopFaultsGrid.Rows.Clear();
			var sorted = await Task.Run(() => Faults.OrderByDescending(x => x.Value.Item2).GroupBy(x => x.Key).SelectMany(x => x));
			foreach (var a in sorted) {
				var msg = AllFaults.Where(x => x.Message == a.Key).Select(x => x).First();
				var row = new DataGridViewRow();
				row.CreateCells(TopFaultsGrid,
					msg.Area,
					string.Format("{0:0.0}h", a.Value.Item2),
					a.Value.Item1,
					msg.Message
				);

				row.Cells[1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
				TopFaultsGrid.Rows.Add(row);
			}
		}

		private async void UpdateTopFaultAreasList() {
			Dictionary<string, Tuple<uint, double>> Areas = new Dictionary<string, Tuple<uint, double>>();

			await Task.Run(() => {
				foreach (var f in AllFaults) {
					//var area = ExtractStationFromMessageTag(f.Message, f.Area);
					var area = f.Area;

					if (Areas.ContainsKey(area)) {
						var t = Areas[area];
						Areas[area] = new Tuple<uint, double>(t.Item1 + 1, t.Item2 + (f.End - f.Start).TotalHours);
					} else {
						Areas.Add(area, new Tuple<uint, double>(1, (f.End - f.Start).TotalHours));
					}
				}
			});

			TopFaultAreas.Rows.Clear();
			var sorted = await Task.Run((() => Areas.OrderByDescending(x => x.Value.Item2).GroupBy(x => x.Key).SelectMany(x => x))).ConfigureAwait(true);
			foreach (var a in sorted) {
				TopFaultAreas.Rows.Add(
					a.Key,
					string.Format("{0:0.0}h", a.Value.Item2),
					a.Value.Item1);
			}
		}

		private void UpdateFaultList() {
			FaultList.Rows.Clear();

			foreach (var f in AllFaults) {
				var row = new DataGridViewRow();

				row.CreateCells(FaultList,
					f.Area,
					f.Start,
					Utility.PrettyTimeSpan(f.Duration),
					f.Message,
					Utility.PrettyTimeSpan(f.OPRespTime),
					Utility.PrettyTimeSpan(f.RecoveryTime),
					Utility.PrettyTimeSpan(f.EMRespTime),
					Utility.PrettyTimeSpan(f.RepairTime)
					);

				row.Cells[3].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
				FaultList.Rows.Add(row);
			}
			// Default to sort by Duration
			FaultList.Sort(FaultList.Columns[3], ListSortDirection.Ascending);
		}

		private void UpdateRFIDList() {
			var rfid_events = AllMessages.Where(x =>
				x.Area.Contains("MM") && x.Message.StartsWith("Electric Warning ST1")).Select(x => x);

			int[] FC = new int[14];
			int[] BC = new int[14];
			for (int i = 0; i < 14; i++) { FC[i] = BC[i] = 0; }
			Event lastFC = null, lastBC = null;
			int fcLost = 0, bcLost = 0;

			// Count the messages
			foreach (var msg in rfid_events) {
				bool msgIsFC = msg.Message[21] == 'F';

				// Count the RFID faults
				if (int.TryParse(msg.Message.Substring(22, 2), out int reader)) {
					if (msgIsFC) {
						FC[reader - 1]++;
					} else {
						BC[reader - 1]++;
					}
				}

				// Check for a last cycle
				if (msgIsFC && lastFC != null && lastFC.Start == msg.Start) {
					fcLost++;
				} else if (!msgIsFC && lastBC != null && lastBC.Start == msg.Start) {
					bcLost++;
				}
				// Assign last cycle
				if (msgIsFC) lastFC = msg;
				else lastBC = msg;
			}

			// Display the results
			RFIDGrid.Rows.Clear();
			for (int i = 0; i < 14; i++) {
				RFIDGrid.Rows.Add(i + 1, FC[i], BC[i]);
			}
			var row = new DataGridViewRow();
			row.CreateCells(RFIDGrid, "Lost", fcLost, bcLost);
			row.DefaultCellStyle.BackColor = Color.LightGray;
			RFIDGrid.Rows.Add(row);
		}

		private async void UpdateOverviewList() {
			const string PercFormat = "{0:0.0}%";
			const string AvgFormat = "{0:0.0}m";

			double TotalTime = (timeSeries.End - timeSeries.Start).TotalMinutes;

			var rows = await Task.Run(() => {
				List<DataGridViewRow> ret = new List<DataGridViewRow>();
				foreach (var s in timeSeries.Series) {
					// Calculate Avail%
					double avgMTTR = s.Faults.Count > 0 ? s.Faults.Average(x => x.Duration.TotalMinutes) : 0;
					double avgOPResp = s.Faults.Count > 0 ? s.Faults.Average(x => x.OPRespTime.TotalMinutes) : 0;
					double avgRecovery = s.Faults.Count > 0 ? s.Faults.Average(x => x.RecoveryTime.TotalMinutes) : 0;
					double avgEMResp = s.Faults.Count > 0 ? s.Faults.Average(x => x.EMRespTime.TotalMinutes) : 0;
					double avgRepair = s.Faults.Count > 0 ? s.Faults.Average(x => x.RepairTime.TotalMinutes) : 0;
					double avail = (s.Cycling.Sum(x => x.Duration.TotalMinutes) / TotalTime) * 100.0d;
					double prod = (s.Productive.Sum(x => x.Duration.TotalMinutes) / TotalTime) * 100.0d;
					double fault = (s.Faults.Sum(x => x.Duration.TotalMinutes) / TotalTime) * 100.0d;
					double wasted = ((s.Cycling.Sum(x => x.Duration.TotalMinutes) - s.Productive.Sum(x => x.Duration.TotalMinutes)) / TotalTime) * 100.0d;
					double lotchg = (s.LotChange.Sum(x => x.Duration.TotalMinutes) / TotalTime) * 100.0d;
					int stops = Math.Max(0, s.Cycling.Count - 1);
					int faults = s.Faults.Count;
					double avgCyc = s.Cycling.Count > 0 ? s.Cycling.Average(x => x.Duration.TotalMinutes) : 0;
					double avgProd = s.Productive.Count > 0 ? s.Productive.Average(x => x.Duration.TotalMinutes) : 0;
					double avgFault = s.Faults.Count > 0 ? s.Faults.Average(x => x.Duration.TotalMinutes) : 0;

					var row = new DataGridViewRow();
					row.CreateCells(OverviewGrid,
						s.Title,
						string.Format(AvgFormat, avgMTTR),
						string.Format(AvgFormat, avgOPResp),
						string.Format(AvgFormat, avgRecovery),
						string.Format(AvgFormat, avgEMResp),
						string.Format(AvgFormat, avgRepair),
						string.Format(PercFormat, avail),
						string.Format(PercFormat, prod),
						string.Format(PercFormat, fault),
						string.Format(PercFormat, wasted),
						string.Format(PercFormat, lotchg),
						stops,
						faults,
						string.Format(AvgFormat, avgCyc),
						string.Format(AvgFormat, avgProd),
						string.Format(AvgFormat, avgFault)
					);
					ret.Add(row);
				}
				return ret;
			});

			OverviewGrid.Rows.Clear();
			OverviewGrid.Rows.AddRange(rows.ToArray());
		}

		// -- Parse Data Souces
		//public List<Event> ParseExcel(string file) {
		//    List<Event> data = new List<Event>();

		//    if (!File.Exists(file)) return data;

		//    // Connect to the xls
		//    using (var stream = File.Open(file, FileMode.Open, FileAccess.Read)) {
		//        // Auto-detect format, supports:
		//        //  - Binary Excel files (2.0-2003 format; *.xls)
		//        //  - OpenXml Excel files (2007 format; *.xlsx)
		//        using (var reader = ExcelReaderFactory.CreateReader(stream)) {
		//            //do {
		//            // Check file type
		//            reader.Read();
		//            if (reader.FieldCount > 7) {
		//                // Business object xls
		//                reader.Read();

		//                while (reader.Read()) {
		//                    if (reader.FieldCount == 8) {
		//                        var ev = new Event {
		//                            Area = reader.GetString(1),
		//                            Start = reader.GetDateTime(2),
		//                            End = reader.GetDateTime(3),
		//                            Message = reader.GetString(6)
		//                        };
		//                        data.Add(ev);
		//                    }
		//                }
		//            } else {
		//                // Current Custom xls (manual query from Swapna)
		//                while(reader.Read()) {
		//                    if (reader.FieldCount == 7) {
		//                        var area = reader.GetString(0);
		//                        var apl = int.Parse(area[4].ToString());
		//                        var line = ((apl - 1) * 3) + int.Parse(area[8].ToString());
		//                        var machine = area.Substring(9, 2);

		//                        var start = DateTime.Parse(reader.GetString(4));
		//                        var end = DateTime.Parse(reader.GetString(5));
		//                        var message = reader.GetString(1);

		//                        var ev = new Event {
		//                            Area = string.Format("PM{1}{0}", machine, line),
		//                            Start = start,
		//                            End = end,
		//                            Message = message
		//                        };
		//                        data.Add(ev);
		//                    }
		//                }
		//            }

		//            //} while (reader.NextResult());
		//        }
		//    }
		//    return data;
		//}
		//public List<Event> ParseCSV(string file) {
		//    List<Event> events = new List<Event>();
		//    if (File.Exists(file)) {
		//        var lines = File.ReadAllLines(file);

		//        for (int i = 0; i < lines.Count(); i++) {
		//            var split = SplitCSVLine(lines[i]);

		//            if (i == 0 || split.Count < 1) continue;

		//            var apl = int.Parse(split[0][4].ToString());
		//            var line = ((apl - 1) * 3) + int.Parse(split[0][8].ToString());
		//            var machine = split[0].Substring(9, 2);

		//            var start = DateTime.Parse(split[4]);
		//            var end = DateTime.Parse(split[5]);

		//            Event ev = new Event() {
		//                Area = string.Format("PM{1}{0}", machine, line),
		//                Start = start,
		//                End = end,
		//                Message = split[1]
		//            };
		//            if (!ev.Message.Contains("OPC ") && machine.ToLower() != "im")  // Skip the IMS for now
		//                events.Add(ev);
		//        }
		//    }
		//    return events;
		//}
		//private List<string> SplitCSVLine(string line) {
		//    List<string> ret = new List<string>();

		//    int start = 0;
		//    bool inToken = false;

		//    for (int i = 0; i < line.Length; i++) {
		//        if (line[i] == '"') {
		//            inToken = !inToken;
		//        }
		//        else if (line[i] == ',' && !inToken) { // New lines (\r\n) already removed
		//            // New Token
		//            string token = string.Empty;
		//            if (line[start] == '"') {
		//                token = line.Substring(start + 1, i - start - 2);
		//            }
		//            else {
		//                token = line.Substring(start, i - start);
		//            }
		//            ret.Add(token.Trim());
		//            start = i + 1;
		//        }

		//        //if (line[i] == '"') {
		//        //    if (start < 0) {    // New Token
		//        //        start = i + 1;
		//        //    }
		//        //    else {            // End of token
		//        //        ret.Add(line.Substring(start, i - start).Trim());
		//        //        start = -1;
		//        //    }
		//        //}
		//    }
		//    return ret;
		//}
		public List<Event> GetFaultEvents(List<Event> events) {
			const int MessageStartArea = 0; // Seconds around message start time to group
			List<Event> list = new List<Event>();

			// Find all MTTR Faulted msgs
			var faults = from e in events
						 where e.Message.Contains("RPIMTTR_FAULTD") // || e.Message.StartsWith("Misc Warning RPIMTTR_OPRESP")
						 select e;

			foreach (var fault in faults) {
				// Find the messages with the same time
				// Should be 3
				var msgs = (from e in events
							where e.Start >= fault.Start.AddSeconds(-MessageStartArea) && e.Start <= fault.Start.AddSeconds(MessageStartArea) &&
									!e.Message.StartsWith("Misc Warning RPIMTTR_")  // Ignore the accompanying MTTR messages
							orderby e.Duration descending
							select e).ToList();
				if (msgs.Count > 0) {
					string flt = string.Empty;
					for (int i = 0; i < msgs.Count(); i++) {
						if (IsAFaultMessage(msgs[i].Message) || i == msgs.Count() - 1) {
							flt = msgs[i].Message;
							break;
						}
					}

					var newEvent = new Event() {
						Area = fault.Area,
						Message = flt,
						Start = fault.Start,
						End = fault.End.AddMinutes(-10) // Subtract the run time
					};

					// -- Update (Add Repsonse times)
					var otherMsgs = from e in events
									where e.Start >= newEvent.Start.AddSeconds(-MessageStartArea) && e.Start <= newEvent.End.AddSeconds(MessageStartArea) &&
									e.Message.Contains("RPIMTTR")
									select e;

					foreach (var msg in otherMsgs) {
						if (msg.Message.ToLower().Contains("RPIMTTR_OPRESP".ToLower())) {
							//Debug.Assert(newEvent.in_OPRespTime == TimeSpan.Zero);
							newEvent.in_OPRespTime += msg.Duration;
						} else if (msg.Message.ToLower().Contains("RPIMTTR_RECOVE".ToLower())) {
							//Debug.Assert(!newEvent.OPAck.HasValue);
							//newEvent.OPAck = msg.Start;
							//Debug.Assert(newEvent.in_RecoveryTime == TimeSpan.Zero);
							newEvent.in_RecoveryTime += msg.Duration;
						} else if (msg.Message.ToLower().Contains("RPIMTTR_EMRESP".ToLower())) {
							//Debug.Assert(!newEvent.ReqEM.HasValue);
							//newEvent.ReqEM = msg.Start;
							//Debug.Assert(newEvent.in_EMRespTime == TimeSpan.Zero);
							newEvent.in_EMRespTime += msg.Duration;
						} else if (msg.Message.ToLower().Contains("RPIMTTR_REPAIR".ToLower())) {
							//Debug.Assert(!newEvent.EMArr.HasValue);
							//newEvent.EMArr = msg.Start;
							//Debug.Assert(newEvent.in_RepairTime == TimeSpan.Zero);
							newEvent.in_RepairTime += msg.Duration;
						}
					}

					list.Add(newEvent);
				}
			}
			return list;
		}

		private bool IsAFaultMessage(string msg) {
			//List<string> SecondaryFaults = new List<string> {
			//    "MISC CB1DI13_CLEANM Cleaning Module CM - Error",
			//    "MISC CB1DI14_CLEANM Cleaning module CM - not ready",
			//    "Electric UV1HT03_RTERMM RMM fault, see local HMI on RMM for more informatio"
			//};
			List<string> SecondaryFaults = new List<string> {
				"CB1DI13_CLEANM",   // MISC CB1DI13_CLEANM Cleaning Module CM - Error
                "CB1DI14_CLEANM",   //"MISC CB1DI14_CLEANM Cleaning module CM - not ready"
                "UV1HT03_RTERMM",    //"Electric UV1HT03_RTERMM RMM fault, see local HMI on RMM for more informatio"
            };
			return !msg.StartsWith("H ") &&
					(!msg.Contains("Warning") || msg.Contains("leakage")) &&
					!SecondaryFaults.Contains(msg) &&
					SecondaryFaults.TrueForAll(x => !msg.StartsWith(x))
					;
		}

		private bool IsAnActionMessage(string msg) {
			// xxxxxxxxxxx fivetwo1
			//            123456789
			if (msg.Length > 9 && msg.Last() >= '0' && msg.Last() <= '9' && msg[msg.Length - 9] == ' ') {
				// Check that the user string is all caps
				string user = msg.Substring(msg.Length - 8, 7);
				return user.ToUpper() == user;
			}
			return false;
		}

		public List<Event> GetLotChangeEvents(List<Event> events) {
			List<Event> list = new List<Event>();

			// Find all Lot Change msgs
			var msgs = from e in events
					   where e.Message.StartsWith("H")
					   orderby e.Start
					   select e;

			if (msgs.Count() < 1)
				return list;

			// EM is backwards
			if (msgs.First().Area.EndsWith("EM")) {
				Event lotEnd = null;
				foreach (var msg in msgs) {
					if (msg.Message.ToLower().Contains("start")) {
						lotEnd = msg;
					} else if (msg.Message.ToLower().Contains("end") && lotEnd != null) {
						// Create a lot change event for this time
						list.Add(new Event() {
							Area = msg.Area,
							Start = lotEnd.Start,
							End = msg.End,
							Message = "Lot Change Active"
						});
						lotEnd = null;
					}
				}
			} else {
				Event lotEnd = null;
				foreach (var msg in msgs) {
					if (msg.Message.ToLower().Contains("end")) {
						lotEnd = msg;
					} else if (msg.Message.ToLower().Contains("start") && lotEnd != null) {
						// Create a lot change event for this time
						list.Add(new Event() {
							Area = msg.Area,
							Start = lotEnd.Start,
							End = msg.End,
							Message = "Lot Change Active"
						});
						lotEnd = null;
					}
				}
			}

			return list;
		}

		// -- Time Series Events
		private void MessageList_GoToTime(DateTime time) {
			for (int i = 0; i < MessagesList.Rows.Count; i++) {
				if (DateTime.Parse(MessagesList.Rows[i].Cells[1].Value.ToString()) >= time) {
					if (i == 0)
						MessagesList.Rows[0].Selected = true;
					else {
						MessagesList.Rows[i - 1].Selected = true;
						MessagesList.FirstDisplayedScrollingRowIndex = i - 1;
					}

					break;
				}
			}
		}

		private void TimeSeriesTimeClicked(DateTime time) {
			if (!TabControl1.TabPages.Contains(MessagesTab) || MessagesList.Rows.Count < 1) return;

			// Select the Messages at this timeframe
			TabControl1.SelectedTab = MessagesTab;

			MessageList_GoToTime(time);
		}

		private void TestTimeSeriesSetup() {
			//timeSeries.Start = DateTime.Now.AddHours(-4);
			//timeSeries.End = DateTime.Now;
			var start = DateTime.Now.AddHours(-4);
			var end = DateTime.Now;

			timeSeries.Series.Add(new TimeSeriesRow() {
				Title = "MM4",
				Cycling = new List<Event>(),
				Productive = new List<Event>() {
					new Event() { Start = start, End = end }
				},
				Faults = new List<Event>()
			});
			timeSeries.Series.Add(new TimeSeriesRow() {
				Title = "EM4",
				Cycling = new List<Event>() {
					new Event() { Start = start, End = end }
				},
				Productive = new List<Event>() {
					new Event() { Start = DateTime.Now.AddHours(-3), End = DateTime.Now.AddMinutes(-30) }
				},
				Faults = new List<Event>()
			});
			timeSeries.Series.Add(new TimeSeriesRow() {
				Title = "IM4",
				Cycling = new List<Event>(),
				Productive = new List<Event>(),
				Faults = new List<Event>() {
					new Event() { Start = start, End = end, Message = "Fault 1" }
				}
			});
			timeSeries.Series.Add(new TimeSeriesRow() {
				Title = "TM4",
				Cycling = new List<Event>() {
					new Event() { Start = DateTime.Now.AddHours(-2), End = DateTime.Now }
				},
				Productive = new List<Event>() {
					new Event() { Start = DateTime.Now.AddHours(-1), End = DateTime.Now }
				},
				Faults = new List<Event>() {
					new Event() { Start = DateTime.Now.AddHours(-3), End = DateTime.Now.AddHours(-2), Message = "APL01_PM1MM_GENVR_TRYT Super Stupid Fault Message" }
				}
			});
			timeSeries.Redraw();
			//var events = ParseExcel("test.xls");
			//var faults = GetFaultEvents(events);
			//var i = faults.Count;
		}

		private void MessagesList_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
			if (e.RowIndex > -1 && e.RowIndex < MessagesList.Rows.Count) {
				DateTime time = DateTime.Parse(MessagesList.Rows[e.RowIndex].Cells[1].Value.ToString());
				timeSeries.CreateMarker(time);
			}
		}

		private void FaultList_CellDoubleClick(Object sender, DataGridViewCellEventArgs e) {
			if (e.RowIndex < FaultList.Rows.Count) {
				DateTime time = DateTime.Parse(FaultList.Rows[e.RowIndex].Cells[1].Value.ToString());
				timeSeries.CreateMarker(time);
			}
		}

		// -- MessagesList Filter
		private void BuildModuleExcludeMenu() {
			ModuleVisibilityBtn.DropDownItems.Clear();

			// -- Two 'built in' buttons that do all the things
			Object SkipMe = new object();
			var ExlcudeAllBtn = new ToolStripMenuItem("<Include All>");
			ExlcudeAllBtn.Tag = SkipMe; // Object to know which menu items are built in actions
			ExlcudeAllBtn.Click += (x, y) => {
				ignoreFilters = true;
				foreach (var i in ModuleVisibilityBtn.DropDownItems) {
					var item = i as ToolStripMenuItem;
					if (item.Tag != SkipMe) {
						item.Checked = true;
					}
				}

				// Do the filter
				ignoreFilters = false;
				FilterMsgsBtn_Click(x, y);
			};
			ModuleVisibilityBtn.DropDownItems.Add(ExlcudeAllBtn);
			var ExlcudeNoneBtn = new ToolStripMenuItem("<Include None>");
			ExlcudeNoneBtn.Tag = SkipMe; // Object to know which menu items are built in actions
			ExlcudeNoneBtn.Click += (x, y) => {
				ignoreFilters = true;
				foreach (var i in ModuleVisibilityBtn.DropDownItems) {
					var item = i as ToolStripMenuItem;
					if (item.Tag != SkipMe) {
						item.Checked = false;
					}
				}

				// Do the filter
				ignoreFilters = false;
				FilterMsgsBtn_Click(x, y);
			};
			ModuleVisibilityBtn.DropDownItems.Add(ExlcudeNoneBtn);

			// -- Generate the Module Buttons
			var areas = AllMessages.Select(x => x.Area).Distinct().OrderBy(x => x);
			foreach (var area in areas) {
				var m = new ToolStripMenuItem(area);
				m.Tag = area;
				m.Checked = true;
				m.CheckOnClick = true;
				m.CheckedChanged += (x, y) => {
					if (!m.Checked && !FilterMessages.ExcludeModules.Contains(area)) {
						FilterMessages.ExcludeModules.Add(area);
					} else if (m.Checked && FilterMessages.ExcludeModules.Contains(area)) {
						FilterMessages.ExcludeModules.Remove(area);
					}

					// Do the filter
					FilterMsgsBtn_Click(x, y);
				};
				ModuleVisibilityBtn.DropDownItems.Add(m);
			}
		}

		private void FilterMsgsBtn_Click(object sender, EventArgs e) {
			if (!Loaded || AllMessages.Count < 1 || ignoreFilters) return;

			// Remember the time we were at
			DateTime time = MessagesList.FirstDisplayedScrollingRowIndex > -1 ?
				DateTime.Parse(MessagesList.Rows[MessagesList.FirstDisplayedScrollingRowIndex].Cells[1].Value.ToString()) : DateTime.MinValue;

			UpdateMessagesList();

			MessageList_GoToTime(time);
		}

		// -- Utility
		private string ExtractStationFromMessageTag(string tag, string area) {
			var split = tag.Split(' ');
			string t = string.Empty;
			if (split.Length < 2) return "error";
			if (split[1] == "Warning")
				t = split[2];
			else
				t = split[1];

			return string.Format("{0}_{1}", area.Substring(3, 2), t.Substring(0, 3));
		}

		private void FilterTextBox_KeyDown(Object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Enter) {
				FilterTypingTimeout?.Stop();
				FilterTypingTimeout?.Dispose();
				FilterTypingTimeout = null;
				FilterMsgsBtn_Click(null, null);
			} else if (FilterTypingTimeout == null) {
				FilterTypingTimeout = new Timer() {
					Enabled = true,
					Interval = 750
				};
				FilterTypingTimeout.Tick += (x, y) => {
					FilterTypingTimeout?.Stop();
					FilterTypingTimeout?.Dispose();
					FilterTypingTimeout = null;
					FilterMsgsBtn_Click(null, null);
				};
			} else {
				FilterTypingTimeout.Stop();
				FilterTypingTimeout.Start();
			}
		}

		//private async void NewTimeframeQuery(DateTime start, DateTime end, string location) {
		//    // 0 - Setup HMI
		//    AllFaults.Clear();
		//    AllMessages.Clear();
		//    timeSeries.Series.Clear();

		//    // Remove the Tab Pages before they're loaded
		//    TabControl1.TabPages.Clear();
		//    TopFaultAreas.Enabled = false;

		//    // Inform the user of a long operation
		//    StatusBarText.Text = "Loading, please wait...";
		//    ProgressBar.Style = ProgressBarStyle.Marquee;
		//    Stopwatch sw = new Stopwatch();
		//    sw.Start();

		//    // 1 - Find all the files for this query
		//    var files = FindFilesInQuery(start, end, location); // TODO async

		//    // 2 - Report # of files
		//    var totalFiles = files.Count();
		//    var currentFile = 0;

		//    // 3 - Parse Each File
		//    var messages = new List<Event>(files.Count * 1500); // rough estimate based on lines in file I have open now
		//    //foreach (var file in files) {
		//    //    var e = ParseFile(file);    // TODO async
		//    //    currentFile++;  // TODO Update HMI progres

		//    //    messages.AddRange(e);
		//    //}
		//    // Multithreaded
		//    var tasks = new List<Task<List<Event>>>();
		//    foreach (var file in files) {
		//        tasks.Add(Task.Run(() => {
		//            return ParseFile(file);
		//        }));
		//    }
		//    Task.WaitAll(tasks.ToArray());
		//    foreach (var e in tasks) {
		//        messages.AddRange(e.Result);
		//    }

		//    // 4 - Sort the lists in order
		//    List<List<Event>> lists = new List<List<Event>>();
		//    //for (int i = 1; i < 15; i++) {
		//    //    foreach (var module in new List<string> { "MM", "EM", "IM", "TM" }) {   // This allows us to order the lists how I like TODO: Add PP to list
		//    //        var list = messages.AsParallel().Where(x => x.Area == string.Format("PM{0}{1}", i, module)).OrderBy(x => x.Start).Select(x => x).ToList();

		//    //        if (list != null && list.Count > 0)
		//    //            lists.Add(list);
		//    //    }
		//    //}
		//    var Modules = new List<string> {
		//        "PM1MM", "PM1EM", "PM1IM", "PM1TM",
		//        "PM2MM", "PM2EM", "PM2IM", "PM2TM",
		//        "PM3MM", "PM3EM", "PM3IM", "PM3TM",
		//        "PP1", "PP2",
		//        "PM4MM", "PM4EM", "PM4IM", "PM4TM",
		//        "PM5MM", "PM5EM", "PM5IM", "PM5TM",
		//        "PM6MM", "PM6EM", "PM6IM", "PM6TM",
		//        "PP3", "PP4",
		//    };
		//    var splitTasks = new List<Task<List<Event>>>();
		//    foreach (var module in Modules) {
		//        var t = Task.Run(() => {
		//            var msgs = new List<Event>();
		//            foreach (var msg in messages) {
		//                if (msg.Area == module)
		//                    msgs.Add(msg);
		//            }

		//            msgs.Sort((a, b) => a.Start.CompareTo(b.Start));
		//            return msgs;
		//        });

		//        splitTasks.Add(t);
		//    }
		//    Task.WaitAll(splitTasks.ToArray());
		//    var c = 0;
		//    var all = new List<Event>();
		//    foreach (var l in splitTasks) {
		//        if (l.Result != null && l.Result.Count > 0) {
		//            lists.Add(l.Result);
		//            all.AddRange(l.Result);
		//            c += l.Result.Count;
		//        }
		//    }
		//    var except = messages.Except(all);
		//    Debug.Assert(c == messages.Count, "Not All messages were captured during the Module Split!");

		//    // 5 - Create the Series
		//    //foreach (var area in lists) {
		//    //    // Setup Row
		//    //    TimeSeriesRow row = BuildTimeSeriesRow(area);
		//    //    timeSeries.Series.Add(row);

		//    //    // Remember these faults
		//    //    AllFaults.AddRange(row.Faults);
		//    //}
		//    var rowTasks = new List<Task<TimeSeriesRow>>();
		//    foreach (var area in lists) {
		//        rowTasks.Add(Task.Run(() => {
		//            return BuildTimeSeriesRow(area);
		//        }));
		//    }
		//    Task.WaitAll(rowTasks.ToArray());
		//    foreach (var row in rowTasks) {
		//        timeSeries.Series.Add(row.Result);
		//        AllFaults.AddRange(row.Result.Faults);
		//    }

		//    // 6 - TODO
		//    Loaded = true;
		//    AllMessages = messages;
		//    AllMessages.Sort((x, y) => x.Start.CompareTo(y.Start));
		//    sw.Stop();

		//    // Force the Timeseries to update itself
		//    if (start > DateTime.MinValue && end < DateTime.MaxValue) {
		//        timeSeries.Start = start;
		//        timeSeries.End = end;
		//    }
		//    timeSeries.Redraw();

		//    // -- Temp -- Launch overview window
		//    var overWin = new OverviewForm("Title", start, end, timeSeries.Series);
		//    overWin.Show();

		//    // -- Load the Overview Tab
		//    StatusBarText.Text = "Loading Overview...";
		//    UpdateOverviewList();
		//    TabControl1.TabPages.Add(OverviewTab);

		//    // -- Load the Top Fault Areas
		//    StatusBarText.Text = "Loading Top Fault Areas...";
		//    UpdateTopFaultAreasList();
		//    TopFaultAreas.Enabled = true;

		//    // -- Load the Fault Event List
		//    StatusBarText.Text = "Loading Fault Events Tab...";
		//    UpdateFaultList();
		//    TabControl1.TabPages.Add(FaultsTab);

		//    // -- Load the Fault Count / Duration List
		//    StatusBarText.Text = "Loading Fault Count / Duration Tab...";
		//    UpdateTopFaultsList();
		//    TabControl1.TabPages.Add(TopFaultsTab);

		//    // -- Load The Message List in the background (Longest Load)
		//    StatusBarText.Text = "Loading Chronological Message Tab...";
		//    BuildModuleExcludeMenu();
		//    UpdateMessagesList();
		//    TabControl1.TabPages.Add(MessagesTab);

		//    // -- Load the RFID faults
		//    UpdateRFIDList();

		//    // -- Done, Clean up
		//    ProgressBar.Style = ProgressBarStyle.Continuous;
		//    StatusBarText.Text = string.Format("Loading Complete, took {0}ms", sw.ElapsedMilliseconds);
		//    TabControl1.SelectedTab = OverviewTab;
		//}

		//private List<string> FindFilesInQuery(DateTime start, DateTime end, string location) {
		//    const int HourPadding = 4;

		//    return Directory.EnumerateFiles(location, "*.csv").Where(x => {
		//        if (!x.EndsWith("1.csv") && !x.EndsWith("2.csv"))
		//            return false;
		//        var s = Path.GetFileName(x).Substring(0, 15);
		//        var t = DateTime.ParseExact(s, "yyyy-MM-dd_HHmm", System.Globalization.CultureInfo.InvariantCulture);
		//        return start.AddHours(-HourPadding) <= t && t <= end.AddHours(HourPadding);
		//    }).Select(x => x).ToList();
		//}

		//private List<Event> ParseFile(string path) {
		//    var Modules = new List<string> {
		//        "APL01_PM1MM", "APL01_PM1EM", "APL01_PM1IM", "APL01_PM1TM",
		//        "APL01_PM2MM", "APL01_PM2EM", "APL01_PM2IM", "APL01_PM2TM",
		//        "APL01_PM3MM", "APL01_PM3EM", "APL01_PM3IM", "APL01_PM3TM",
		//        //"APL01_PP1T1", "APL01_PP1T2",
		//        "APL02_PM1MM", "APL02_PM1EM", "APL02_PM1IM", "APL02_PM1TM",
		//        "APL02_PM2MM", "APL02_PM2EM", "APL02_PM2IM", "APL02_PM2TM",
		//        "APL02_PM3MM", "APL02_PM3EM", "APL02_PM3IM", "APL02_PM3TM",
		//        //"APL02_PP1T1", "APL02_PP1T2",
		//    };
		//    var ExcludeTags = new List<string> {
		//        "RM2FA01_ToABS2_ala", "RM2FA01_ToABS1_ala", // Waiting for Robot to load drawer X
		//        "COMOPC1_WD0001_bad", "COMOPC1_WD0002_bad", // OPC server messages
		//    };

		//    var events = new List<Event>(1500);
		//    using (var stream = File.Open(path, FileMode.Open))
		//    using (var reader = new StreamReader(stream)) {
		//        // 2018-08-10 --- Current Format Reading
		//        reader.ReadLine();  // First line is blank
		//        var header = reader.ReadLine();
		//        if (header == "Session altered.") {
		//            reader.ReadLine();  // blank line
		//            reader.ReadLine();  // blank line
		//            reader.ReadLine();  // Header line
		//            reader.ReadLine();  // ------ line

		//            while (!reader.EndOfStream) {
		//                //var split = reader.ReadLine().Split(',');
		//                var split = CSVSplitLine(reader.ReadLine());

		//                if (split.Count() >= 26 &&                                   // wrong line format
		//                    Modules.Contains(split[0]) &&                            // unsupported module
		//                    ExcludeTags.FindIndex(x => split[2].TrimEnd(' ').EndsWith(x)) < 0) {  // Excluded Tag
		//                    // Add this tag to the list
		//                    var apl = int.Parse(split[0][4].ToString());
		//                    var line = ((apl - 1) * 3) + int.Parse(split[0][8].ToString());
		//                    var machine = split[0][7] == 'P' ? split[0].Substring(6, 2) : split[0].Substring(9, 2);

		//                    var start = DateTime.ParseExact(split[6].Trim(), "MM/dd/yyyy HH:mm:ss", null);
		//                    var end = DateTime.ParseExact(split[8].Trim(), "MM/dd/yyyy HH:mm:ss", null);

		//                    Event ev = new Event() {
		//                        Area = string.Format("PM{1}{0}", machine, line),
		//                        Start = start,
		//                        End = end,
		//                        Tag = split[2].Trim().Substring(12),   // Chomp off "APL0X_PMXmm_"
		//                        Message = split[11].Trim()
		//                    };
		//                    events.Add(ev);
		//                }
		//            }
		//        }
		//        else {
		//            reader.ReadLine();  // Header line
		//            reader.ReadLine();  // ------ line

		//            while (!reader.EndOfStream) {
		//                var split = CSVSplitLine(reader.ReadLine());

		//                if (split.Count() == 15 &&                                   // wrong line format
		//                    Modules.Contains(split[0]) &&                            // unsupported module
		//                    ExcludeTags.FindIndex(x => split[1].TrimEnd(' ').EndsWith(x)) < 0) {  // Excluded Tag
		//                    // Add this tag to the list
		//                    var apl = int.Parse(split[0][4].ToString());
		//                    var line = ((apl - 1) * 3) + int.Parse(split[0][8].ToString());
		//                    var machine = split[0][7] == 'P' ? split[0].Substring(6, 2) : split[0].Substring(9, 2);

		//                    var start = DateTime.ParseExact(split[4].Trim(), "MM/dd/yyyy HH:mm:ss", null);
		//                    var end = DateTime.ParseExact(split[6].Trim(), "MM/dd/yyyy HH:mm:ss", null);

		//                    Event ev = new Event() {
		//                        Area = string.Format("PM{1}{0}", machine, line),
		//                        Start = start,
		//                        End = end,
		//                        Tag = split[1].Trim().Substring(12),   // Chomp off "APL0X_PMXmm_"
		//                        Message = split[9].Trim()
		//                    };
		//                    events.Add(ev);
		//                }
		//            }
		//        }
		//    }
		//    return events;
		//}
		//private List<string> CSVSplitLine(string line) {
		//    // Split a string via commas, allowing " to escape tokens
		//    List<string> results = new List<string>();
		//    int start = 0;
		//    bool Escaped = false;
		//    bool WasEscaped = false;
		//    for (int i = 0; i < line.Length; ++i) {
		//        if (line[i] == '"') {
		//            Escaped = !Escaped;
		//            WasEscaped = true;
		//        }
		//        else if (!Escaped && line[i] == ',') {
		//            var str = line.Substring(start, i - start);
		//            if (WasEscaped) {
		//                str = str.TrimStart('"', ' ').TrimEnd(' ', '"');
		//            }
		//            results.Add(str.Trim());
		//            start = i + 1;
		//        }
		//    }

		//    // Grab last (unfinished) token
		//    if (start < line.Length - 1) {
		//        var str = line.Substring(start);
		//        if (WasEscaped) {
		//            str = str.TrimStart('"', ' ').TrimEnd(' ', '"');
		//        }
		//        results.Add(str.Trim());
		//    }

		//    return results;
		//}

		//private TimeSeriesRow BuildTimeSeriesRow(List<Event> events) {
		//    var row = new TimeSeriesRow();
		//    if (events?.Count < 1) return row;

		//    List<long> times = new List<long>();

		//    //events.Sort((a, b) => a.Start.CompareTo(b.Start));
		//    Debug.Assert(events[0].Start <= events[1].Start && events.First().Start <= events.Last().Start, "Build Time Series Row called with a list of faults not sorted!");

		//    row.Title = events[0].Area;
		//    row.Group = events[0].Area.Substring(0, 3);

		//    bool IsEM = (row.Title[3] == 'E');  // PMXEM
		//    Event lotStart      = null;         //    ^
		//    Event currentFault  = null;

		//    var timer = Stopwatch.StartNew();
		//    for (int i = 0; i < events.Count; ++i) {
		//        Debug.Assert(i == (events.Count - 1) || events[i].Start <= events[i + 1].Start, "Message List input to BuildTimeSeriesRow not in order!");

		//        if (i % 10000 == 0) {
		//            times.Add(timer.ElapsedMilliseconds);
		//            timer.Restart();
		//        }

		//        var msg = events[i];
		//        if (msg.Tag == "RPIMTTR_INAUTO_ala") {      // Cycling
		//            row.Cycling.Add(msg);
		//        }
		//        else if (msg.Tag == "RPIMTTR_INPROD_ala") { // Productive
		//            row.Productive.Add(msg);
		//        }
		//        else if (msg.Message.StartsWith("H ")) {    // Lot Change
		//            bool isLotStartMsg  = IsEM ? msg.Message.ToLower().Contains("end") : msg.Message.ToLower().Contains("start");
		//            bool isLotEndMsg    = IsEM ? msg.Message.ToLower().Contains("start") : msg.Message.ToLower().Contains("end");

		//            if (isLotStartMsg) {
		//                lotStart = msg;
		//            } else if (lotStart != null && isLotEndMsg) {
		//                // Create a lot change event for this time
		//                row.LotChange.Add(new Event() {
		//                    Area = msg.Area,
		//                    Start = lotStart.Start,
		//                    End = msg.End,
		//                    Message = "Lot Change Active"
		//                });
		//                lotStart = null;
		//            }
		//        }
		//        else if (msg.Tag.StartsWith("RPIMTTR_")) { // Fault Related
		//            if (msg.Tag == "RPIMTTR_FAULTD_ala") {  // start a new fault
		//                if (currentFault != null) {
		//                    currentFault.End = currentFault.End.AddMinutes(-10);
		//                    row.Faults.Add(currentFault);
		//                }

		//                currentFault = new Event() {
		//                    Area = msg.Area,
		//                    Tag = msg.Tag,
		//                    Start = msg.Start,
		//                    End = msg.End,
		//                    Message = "Related Fault Not Found" // Default value
		//                };

		//                // Search around for the fault
		//                Event fltText = null;
		//                for (int y = i - 1; y >= 0; --y) {  // search earlier
		//                    if ((msg.Start - events[y].Start).TotalSeconds > 5)
		//                        break;  // Loop is done

		//                    if (IsAFaultMessage(events[y].Message) && (fltText == null || fltText.Duration < events[y].Duration)) {
		//                        fltText = events[y];
		//                    }
		//                }
		//                for (int y = i + 1; y < events.Count; ++y) {  // search Later
		//                    if ((events[y].Start - msg.Start).TotalSeconds > 5)
		//                        break;  // Loop is done

		//                    if (IsAFaultMessage(events[y].Message) && (fltText == null || fltText.Duration < events[y].Duration)) {
		//                        fltText = events[y];
		//                    }
		//                }
		//                if (fltText != null) {
		//                    currentFault.Message = fltText.Message;
		//                }

		//                // Check if the op resp happened just before us
		//                if (i > 0 && events[i - 1].Tag == "RPIMTTR_OPRESP_ala") {
		//                    currentFault.in_OPRespTime = events[i - 1].Duration;
		//                }
		//            }
		//            else if (currentFault != null && msg.Tag == "RPIMTTR_OPRESP_ala" && msg.Start >= currentFault.Start && msg.End <= currentFault.End) {
		//                currentFault.in_OPRespTime += msg.Duration;
		//            }
		//            else if (currentFault != null && msg.Tag == "RPIMTTR_RECOVE_ala" && msg.Start >= currentFault.Start && msg.End <= currentFault.End) {
		//                currentFault.in_RecoveryTime += msg.Duration;
		//            }
		//            else if (currentFault != null && msg.Tag == "RPIMTTR_EMRESP_ala" && msg.Start >= currentFault.Start && msg.End <= currentFault.End) {
		//                currentFault.in_EMRespTime += msg.Duration;
		//            }
		//            else if (currentFault != null && msg.Tag == "RPIMTTR_REPAIR_ala" && msg.Start >= currentFault.Start && msg.End <= currentFault.End) {
		//                currentFault.in_RepairTime += msg.Duration;
		//            }
		//        }
		//    }

		//    // Add any remaining fault
		//    if (currentFault != null) {
		//        currentFault.End = currentFault.End.AddMinutes(-10);
		//        row.Faults.Add(currentFault);
		//    }

		//    // What to do if we didn't find a end to a lot change? We'll ignore it here (Sound okay to me!)
		//    return row;
		//}

		//private void DataPathSelectBtn_Click(Object sender, EventArgs e) {
		//    using (OpenFileDialog dlg = new OpenFileDialog()) {
		//        dlg.Filter = "Files (Excel or CSV)|*.csv;*.xls;*.xlsx|All Files (*.*)|*.*";
		//        dlg.Multiselect = true;

		//        if (dlg.ShowDialog() == DialogResult.OK) {
		//            DataPath = Path.GetDirectoryName(dlg.FileName);
		//        }
		//    }
		//}
	}
}
