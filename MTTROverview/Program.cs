﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MTTROverview
{
	internal static class Program
	{
		public static string SettingsPath = "settings";

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		private static void Main(string[] args) {
			// If we're being launched for the morning report, don't load the UI.
			if (args.Length > 0) {
				// TODO support for more args in the future, maybe

				// Morning Report
				if (args[0] == "/mr") {
					DumpMorningReport();
					return;
				}
			}

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new ReportSelectionWindow());
		}

		private static void DumpMorningReport() {
			const string save_path = "MorningReport";
			Utility.TimeFrame_GetLastTwoShifts(out DateTime start, out DateTime end);
			var data = Utility.QueryCSVFilesForRows(start, end).SelectMany(x => x.Faults).ToList();

			var path = string.Format("{0}\\MorningReport_{1}.txt", save_path, end.ToShortDateString().Replace('/', '-'));
			var lines = new List<String>(data.Count + 1) {
				"Start\tEnd\tPM\tShift\tDur (m)\tKey\tMessage Text\tOpResp\tRecovery\tEMResp\tRepair"
			};

			foreach (var msg in data) {
				// -- Create the Fault
				var fault = new Fault() {
					Shift = msg.Shift,
					Start = msg.Start,
					End = msg.End,
					Message = msg.TrimmedMessage,
					Tag = msg.Tag,
					Area = msg.Area,
					OpRespTime = (ulong)(msg.OPRespTime.TotalMinutes + 0.5d),
					RecoveryTime = (ulong)(msg.RecoveryTime.TotalMinutes + 0.5d),
					EMRespTime = (ulong)(msg.EMRespTime.TotalMinutes + 0.5d),
					RepairTime = (ulong)(msg.RepairTime.TotalMinutes + 0.5d),
				};
				fault.Key = TopDowntimeReportForm.GetAutoKey(fault);

				// Lack of CTs, don't do response times here. It's held against them
				if (msg.Tag == "CLIFA01_LeerWT") {
					fault.OpRespTime = 0;
					fault.RecoveryTime = 0;
					fault.EMRespTime = 0;
					fault.RepairTime = 0;
				}

				// Store it in as text
				lines.Add(string.Format(
					"{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}",
					fault.Start,
					fault.End,
					fault.PM,
					fault.Shift,
					fault.Duration,
					fault.Key == string.Empty ? "< Unknown Fault >" : fault.Key,
					fault.Message,
					fault.OpRespTime,
					fault.RecoveryTime,
					fault.EMRespTime,
					fault.RepairTime));
			}
			File.WriteAllLines(path, lines);
		}
	}
}
