﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MTTROverview
{
	public partial class MessageQueryForm : Form
	{
		public List<Event> Query { get; private set; }

		public MessageQueryForm(DateTime start, DateTime end) {
			InitializeComponent();

			StartTimeBox.Text = start.ToString();
			EndTimeBox.Text = end.ToString();
			MessagesList.AutoGenerateColumns = false;

			// Show seconds in the message list
			MessagesList.Columns[2].DefaultCellStyle.Format = "MM/dd/yyyy HH:mm:ss";
			MessagesList.Columns[3].DefaultCellStyle.Format = "MM/dd/yyyy HH:mm:ss";

			// Testing
			//StartTimeBox.Text = "09Sep2019 06:30:00";
			//EndTimeBox.Text = "10Sep2019 06:30:00";
			//IncludeApl01Box.Checked = IncludeApl02Box.Checked = IncludeApl04Box.Checked = false;
			//IncludeMMBox.Checked = IncludeEMBox.Checked = IncludeIMBox.Checked = IncludeTMBox.Checked = false;
			//IncludePP2Box.Checked = false;
		}

		private void StartTimeBox_TextChanged(Object sender, EventArgs e) {
			var box = (sender as TextBox);
			if (DateTime.TryParse(box.Text, out DateTime r)) {
				box.BackColor = Color.LightGreen;
				box.Tag = r;
			} else {
				box.BackColor = Color.LightPink;
				box.Tag = null;
			}
		}

		private async void RunQueryBtn_Click(Object sender, EventArgs e) {
			if (!RunQueryBtn.Enabled)
				return; // Query In Progress

			// Check for Correct Dates
			if (StartTimeBox.Tag == null || EndTimeBox.Tag == null) {
				MessageBox.Show("Cannot parse query times.");
				return;
			}
			var start = (DateTime)StartTimeBox.Tag;
			var end = (DateTime)EndTimeBox.Tag;

			// Build APLs
			var areas = BuildModuleQuery();

			// Mark UI As Busy
			MarkUIBusy();

			// Get the Data
			var prog = new Progress<Utility.ProgressWithBarAndLabel>(x => { // Progress Reporter
				if (x.Max < 1)
					ProgressBar.Style = ProgressBarStyle.Marquee;
				else {
					ProgressBar.Style = ProgressBarStyle.Continuous;
					ProgressBar.Minimum = x.Min;
					ProgressBar.Maximum = x.Max;
					ProgressBar.Value = x.Current;
					ProgressLabel.Text = x.Label;
				}
			});
			Query = await Task.Run(async () => {
				return await Task.Run(() => {
					return Utility.QueryMessages(areas, start, end, MessageKeyBox.Text, prog);
				});
			});

			// Update UI
			ProgressLabel.Text = "Updating UI...";
			Application.DoEvents();
			MessagesList.DataSource = Query;

			MarkUIDone();
		}

		private async void GetMTTRBtn_Click(Object sender, EventArgs e) {
			if (!RunQueryBtn.Enabled)
				return; // Query In Progress

			// Check for Correct Dates
			if (StartTimeBox.Tag == null || EndTimeBox.Tag == null) {
				MessageBox.Show("Cannot parse query times.");
				return;
			}
			var start = (DateTime)StartTimeBox.Tag;
			var end = (DateTime)EndTimeBox.Tag;

			// Build APLs
			var areas = BuildModuleQuery();

			// Mark UI As Busy
			MarkUIBusy();

			// Get the Data
			var prog = new Progress<Utility.ProgressWithBarAndLabel>(x => { // Progress Reporter
				if (x.Max < 1)
					ProgressBar.Style = ProgressBarStyle.Marquee;
				else {
					ProgressBar.Style = ProgressBarStyle.Continuous;
					ProgressBar.Minimum = x.Min;
					ProgressBar.Maximum = x.Max;
					ProgressBar.Value = x.Current;
					ProgressLabel.Text = x.Label;
				}
			});
			var msgs = await Task.Run(async () => await Task.Run(() => Utility.QueryMessages(areas, start, end, MessageKeyBox.Text, prog)));
			ProgressLabel.Text = "Converting to MTTR...";
			ProgressBar.Style = ProgressBarStyle.Marquee;
			Query = await Task.Run(async () => await Task.Run(() => Utility.CreateRowsFromQuery(start, end, Utility.SortLists(msgs)).SelectMany(x => x.Faults).ToList()));

			// Update UI
			ProgressLabel.Text = "Updating UI...";
			Application.DoEvents();
			MessagesList.DataSource = Query;

			MarkUIDone();
		}

		private async void RunPPBtn_Click(Object sender, EventArgs e) {
			if (!RunQueryBtn.Enabled)
				return; // Query In Progress

			// Check for Correct Dates
			if (StartTimeBox.Tag == null || EndTimeBox.Tag == null) {
				MessageBox.Show("Cannot parse query times.");
				return;
			}
			var start = (DateTime)StartTimeBox.Tag;
			var end = (DateTime)EndTimeBox.Tag;

			// Build APLs
			var areas = BuildModuleQuery();

			// Mark UI As Busy
			MarkUIBusy();

			// Get the Data
			var prog = new Progress<Utility.ProgressWithBarAndLabel>(x => { // Progress Reporter
				ProgressLabel.Text = x.Label;
				if (x.Max < 1) {
					ProgressBar.Style = ProgressBarStyle.Marquee;
				} else {
					ProgressBar.Style = ProgressBarStyle.Continuous;
					ProgressBar.Minimum = x.Min;
					ProgressBar.Maximum = x.Max;
					ProgressBar.Value = x.Current;
				}
			});
			var msgs = await Task.Run(() => Utility.QueryMessages(areas, start, end, MessageKeyBox.Text, prog)).ConfigureAwait(true);
			ProgressLabel.Text = "Converting to MTTR...";
			ProgressBar.Style = ProgressBarStyle.Marquee;
			var mttr = await Task.Run(() => Utility.CreateRowsFromQuery(start, end, Utility.SortLists(msgs)).SelectMany(x => x.Faults).ToList()).ConfigureAwait(true);

			var uniqueHashes = new HashSet<int>();
			Query = msgs.Where(x => uniqueHashes.Add(x.GetHashCode())).ToList();

			// Update UI
			ProgressLabel.Text = "Updating UI...";
			Application.DoEvents();
			MessagesList.DataSource = Query;

			for (int i = 0; i < Query.Count; i++) {
				// Show the tag for conveince
				Query[i].Message = $"{Query[i].Tag} -- {Query[i].Message}";

				var mttr_i = mttr.FindIndex(x => x.GetHashCode() == Query[i].GetHashCode());
				if (mttr_i < 0) {
					// This row was excluded in the MTTR culling
					foreach (DataGridViewCell c in MessagesList.Rows[i].Cells) {
						c.Style.BackColor = Color.DarkGray;
					}
				} else {
					//mttr.RemoveAt(mttr_i);
				}

				ProgressBar.Style = ProgressBarStyle.Continuous;
				ProgressBar.Minimum = 0;
				ProgressBar.Maximum = Query.Count;
				ProgressBar.Value = i;
				// UI Time?
				if (i % 1000 == 0) await Task.Delay(10).ConfigureAwait(true);
			}

			MarkUIDone();
		}

		private async void RunPPBtn_Click_old(Object sender, EventArgs e) {
			if (!RunQueryBtn.Enabled)
				return; // Query In Progress

			var DeleteRows = (ModifierKeys & Keys.Shift) != 0;

			// Check for Correct Dates
			if (StartTimeBox.Tag == null || EndTimeBox.Tag == null) {
				MessageBox.Show("Cannot parse query times.");
				return;
			}
			var start = (DateTime)StartTimeBox.Tag;
			var end = (DateTime)EndTimeBox.Tag;

			// Build APLs
			var areas = BuildModuleQuery();
			areas.RemoveAll(x => !x.Contains("PP"));
			if (areas.Count < 1) return;

			MarkUIBusy();

			// Get the Data
			var prog = new Progress<Utility.ProgressWithBarAndLabel>(x => { // Progress Reporter
				ProgressLabel.Text = x.Label;
				if (x.Max < 1) {
					ProgressBar.Style = ProgressBarStyle.Marquee;
				} else {
					ProgressBar.Style = ProgressBarStyle.Continuous;
					ProgressBar.Minimum = x.Min;
					ProgressBar.Maximum = x.Max;
					ProgressBar.Value = x.Current;
				}
			});

			Query = await Task.Run(() => {
				var msgs = Utility.QueryMessages(areas, start, end, MessageKeyBox.Text, prog);

				((IProgress<Utility.ProgressWithBarAndLabel>)prog).Report(new Utility.ProgressWithBarAndLabel() {
					Label = "Splitting lists & Removing Duplicates...",
					Max = 0,
				});

				// -- Remove Duplicates
				var newList = new List<Event>();
				var knownKeys = new HashSet<string>();
				foreach (var x in msgs) {
					if (x.Tag == "CB1FA01_3041B7" || x.Tag == "CB1FA01_3343B3" || x.Tag == "CB1FA01_4041B9") {
						x.Message = "PP is Locked Out";
						x.Tag = "PPLockOut";
					}

					var hash = string.Format("{0}{1}{2}", x.Tag, x.Start.ToBinary(), x.Message);
					if (knownKeys.Add(hash))
						newList.Add(x);
				}
				msgs = newList;

				var split = Utility.SortLists(msgs);

				// Merge the lists again
				var list = new List<Event>();
				foreach (var l in split)
					list.AddRange(l);

				list = Utility.GroupFaults(list);

				if (DeleteRows) { list = list.Where(x => x.IsAFaultMessage).Select(x => x).ToList(); }

				return list;
			}).ConfigureAwait(true);

			// Update UI
			ProgressLabel.Text = "Updating UI...";
			Application.DoEvents();
			MessagesList.DataSource = Query;

			// Foreach each fault, highlight the rows that aren't PP faults
			if (!DeleteRows) {
				for (int i = 0; i < Query.Count; i++) {
					if (Query[i].Tag == "PPLockOut") {
						var lockoutEnd = Query[i].End;
						var lockoutModule = Query[i].Area;

						for (int x = i + 1; x < Query.Count; x++) {
							if (Query[x].Area == lockoutModule && Query[x].Start <= lockoutEnd) {
								foreach (DataGridViewCell c in MessagesList.Rows[x].Cells) {
									c.Style.BackColor = Color.CornflowerBlue;
								}
							} else if (Query[x].Area == lockoutModule && Query[x].Start > lockoutEnd) {
								i = x - 1;  // skip the faults we just processed
								break;
							}
						}
					} else if (!Query[i].IsAFaultMessage) {
						foreach (DataGridViewCell c in MessagesList.Rows[i].Cells) {
							c.Style.BackColor = Color.DarkGray;
						}
					}
				}
			}

			MarkUIDone();
		}

		private List<string> BuildModuleQuery() {
			List<String> areas = new List<string>(60);

			if (IncludeApl01Box.Checked) {
				if (IncludePm1Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL01_PM1MM"); areas.Add("APL01_PM1UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL01_PM1EM");
					if (IncludeIMBox.Checked) areas.Add("APL01_PM1IM");
					if (IncludeTMBox.Checked) areas.Add("APL01_PM1TM");
				}
				if (IncludePm2Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL01_PM2MM"); areas.Add("APL01_PM2UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL01_PM2EM");
					if (IncludeIMBox.Checked) areas.Add("APL01_PM2IM");
					if (IncludeTMBox.Checked) areas.Add("APL01_PM2TM");
				}
				if (IncludePm3Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL01_PM3MM"); areas.Add("APL01_PM3UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL01_PM3EM");
					if (IncludeIMBox.Checked) areas.Add("APL01_PM3IM");
					if (IncludeTMBox.Checked) areas.Add("APL01_PM3TM");
				}
				if (IncludePP1Box.Checked) areas.Add("APL01_PP1T1");
				if (IncludePP2Box.Checked) areas.Add("APL01_PP1T2");
			}
			if (IncludeApl02Box.Checked) {
				if (IncludePm1Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL02_PM1MM"); areas.Add("APL02_PM1UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL02_PM1EM");
					if (IncludeIMBox.Checked) areas.Add("APL02_PM1IM");
					if (IncludeTMBox.Checked) areas.Add("APL02_PM1TM");
				}
				if (IncludePm2Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL02_PM2MM"); areas.Add("APL02_PM2UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL02_PM2EM");
					if (IncludeIMBox.Checked) areas.Add("APL02_PM2IM");
					if (IncludeTMBox.Checked) areas.Add("APL02_PM2TM");
				}
				if (IncludePm3Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL02_PM3MM"); areas.Add("APL02_PM3UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL02_PM3EM");
					if (IncludeIMBox.Checked) areas.Add("APL02_PM3IM");
					if (IncludeTMBox.Checked) areas.Add("APL02_PM3TM");
				}
				if (IncludePP1Box.Checked) areas.Add("APL02_PP1T1");
				if (IncludePP2Box.Checked) areas.Add("APL02_PP1T2");
			}
			if (IncludeApl03Box.Checked) {
				if (IncludePm1Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL03_PM1MM"); areas.Add("APL03_PM1UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL03_PM1EM");
					if (IncludeIMBox.Checked) areas.Add("APL03_PM1IM");
					if (IncludeTMBox.Checked) areas.Add("APL03_PM1TM");
				}
				if (IncludePm2Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL03_PM2MM"); areas.Add("APL03_PM2UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL03_PM2EM");
					if (IncludeIMBox.Checked) areas.Add("APL03_PM2IM");
					if (IncludeTMBox.Checked) areas.Add("APL03_PM2TM");
				}
				if (IncludePm3Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL03_PM3MM"); areas.Add("APL03_PM3UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL03_PM3EM");
					if (IncludeIMBox.Checked) areas.Add("APL03_PM3IM");
					if (IncludeTMBox.Checked) areas.Add("APL03_PM3TM");
				}
				if (IncludePP1Box.Checked) areas.Add("APL03_PP1T1");
				if (IncludePP2Box.Checked) areas.Add("APL03_PP1T2");
			}
			if (IncludeApl04Box.Checked) {
				if (IncludePm1Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL04_PM1MM"); areas.Add("APL04_PM1UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL04_PM1EM");
					if (IncludeIMBox.Checked) areas.Add("APL04_PM1IM");
					if (IncludeTMBox.Checked) areas.Add("APL04_PM1TM");
				}
				if (IncludePm2Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL04_PM2MM"); areas.Add("APL04_PM2UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL04_PM2EM");
					if (IncludeIMBox.Checked) areas.Add("APL04_PM2IM");
					if (IncludeTMBox.Checked) areas.Add("APL04_PM2TM");
				}
				if (IncludePm3Box.Checked) {
					if (IncludeMMBox.Checked) { areas.Add("APL04_PM3MM"); areas.Add("APL04_PM3UV"); }
					if (IncludeEMBox.Checked) areas.Add("APL04_PM3EM");
					if (IncludeIMBox.Checked) areas.Add("APL04_PM3IM");
					if (IncludeTMBox.Checked) areas.Add("APL04_PM3TM");
				}
				if (IncludePP1Box.Checked) areas.Add("APL04_PP1T1");
				if (IncludePP2Box.Checked) areas.Add("APL04_PP1T2");
			}
			return areas;
		}

		private void MessageKeyBox_KeyDown(Object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Enter) {
				RunQueryBtn_Click(sender, null);
			}
		}

		private void MessagesList_KeyDown(Object sender, KeyEventArgs e) {
			if (e.Control && e.KeyCode == Keys.C) {
				MessagesList.Columns[1].Visible = true;
				MessagesList.Columns[3].Visible = true;
				MessagesList.Columns[6].Visible = true;
				MessagesList.Columns[7].Visible = true;
				MessagesList.Columns[8].Visible = true;
				MessagesList.Columns[9].Visible = true;
				MessagesList.SelectAll();
				Clipboard.SetDataObject(MessagesList.GetClipboardContent());
				MessagesList.Columns[1].Visible = false;
				MessagesList.Columns[3].Visible = false;
				MessagesList.Columns[6].Visible = false;
				MessagesList.Columns[7].Visible = false;
				MessagesList.Columns[8].Visible = false;
				MessagesList.Columns[9].Visible = false;
				MessagesList.ClearSelection();
				e.Handled = true;
			}
		}

		private void MarkUIDone() {
			RunQueryBtn.Enabled = true;
			GetMTTRBtn.Enabled = true;
			GetPPBtn.Enabled = true;
			ProgressLabel.Text = "Query Completed";
			ProgressBar.Style = ProgressBarStyle.Continuous;
			ProgressBar.Value = 0;
		}

		private void MarkUIBusy() {
			RunQueryBtn.Enabled = false;
			GetMTTRBtn.Enabled = false;
			GetPPBtn.Enabled = false;
			ProgressLabel.Text = "Query In Progress...";
			ProgressBar.Style = ProgressBarStyle.Marquee;
		}
	}
}
