﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MTTROverview
{
	public partial class TopDowntimeReportForm : Form
	{
		private string MappingTablePath = "FaultTagMapping";
		private Dictionary<string, string> FaultTagMapping;
		private int SelectedRow = -1;
		private List<Fault> Faults;
		private List<Event> Messages;

		public TopDowntimeReportForm(DateTime start, DateTime end) {
			InitializeComponent();
			LoadFaultTagMappingFromFile();			

			// Setup the Grid
			FaultList.Columns[4].DefaultCellStyle.Format = "N2";    // 2 decimal places
			FaultList.Columns[7].DefaultCellStyle.Format = "N2";
			FaultList.Columns[8].DefaultCellStyle.Format = "N2";
			FaultList.Columns[9].DefaultCellStyle.Format = "N2";
			FaultList.Columns[10].DefaultCellStyle.Format = "N2";
			FaultList.SelectionChanged += (x, y) => {
				SelectedRow = GetSelectedRow();
				if (SelectedRow < 0) return;

				// Selection Changed
				var fault = Faults[SelectedRow];
				MsgText.Text = fault.Message;
				MsgTag.Text = fault.Tag;
				MsgArea.Text = fault.Area;
				CategoriesBox.Text = fault.Key;
				CategoriesBox.Items.Clear();
				CategoriesBox.Items.AddRange(GetCurrentKeys(fault.PM));
				ApplyBtn.Enabled = !fault.Tag.StartsWith("Unknown ");   // Do not allow to assign tags to unknown tags (We assigned the "Unknown MM Fault", not SCADA)
			};
			FaultList.KeyDown += (o, e) => {
				if (e.KeyCode == Keys.Tab) {
					SelectRow(GetNextEmptyKeyRow());
				}
			};

			// Setup the Categories box with existing Categories
			CategoriesBox.Items.Clear();
			CategoriesBox.Items.AddRange(GetCurrentKeys());


			// Copy in the Data
			LoadData(start, end);
		}

		private async void LoadData(DateTime start, DateTime end) {
			BusyPanel.Visible = true;
			BusyShreeLabel.Visible = false;
			Progress<Utility.ProgressWithBarAndLabel> prog = new Progress<Utility.ProgressWithBarAndLabel>(x => {
				BusyStatusLabel.Text = x.Label;
				if (x.Max <= 0) {
					BusyProgressBar.Style = ProgressBarStyle.Marquee;
					BusyPercentLabel.Visible = false;
					BusyProgressLabel.Visible = false;
				} else {
					BusyProgressBar.Style = ProgressBarStyle.Continuous;
					BusyProgressBar.Minimum = x.Min;
					BusyProgressBar.Maximum = x.Max;
					BusyProgressBar.Value = x.Current;
					BusyPercentLabel.Visible = true;
					BusyPercentLabel.Text = string.Format("{0:0.0}%", (((double)x.Current / (double)(x.Max - x.Min)) * 100.0d));
					BusyProgressLabel.Visible = true;
					BusyProgressLabel.Text = $"{x.Current} / {x.Max}";

					if (!BusyShreeLabel.Visible && ((double)x.Current / (double)x.Max) > 0.30d) {
						var rand = new Random();
						var msg = rand.Next(ShreeMessages.Count);
						BusyShreeLabel.Visible = true;
						BusyShreeLabel.Text = ShreeMessages[msg];
					}

				}				
			});

			var rows = await Task.Run(() => Utility.QueryForRows(start, end, prog)).ConfigureAwait(true);
			//var rows = await Task.Run(() => Utility.QueryCSVFilesForRows(start, end, prog)).ConfigureAwait(true);
			Messages = rows.SelectMany(x => x.Faults).ToList();

			BusyProgressBar.Style = ProgressBarStyle.Marquee;
			BusyPercentLabel.Visible = false;
			BusyProgressLabel.Visible = false;
			BusyStatusLabel.Text = "Loading UI / Grid...";

			await Task.Run(() => {
				BuildFaults();
				//Faults = Utility.GroupFaults(Faults, 5); -- djb 2019-10-04 -- Done in the call to QueryForRows so we can parrallellize this
			}).ConfigureAwait(true);

			// Load the Grid
			FaultList.DataSource = Faults;

			// Done
			BusyProgressBar.Style = ProgressBarStyle.Continuous;
			BusyPanel.Dispose();
		}

		private void BuildFaults() {
			Faults = new List<Fault>(Messages.Count);
			foreach (var msg in Messages) {
				var fault = new Fault() {
					Shift = msg.Shift,
					Start = msg.Start,
					End = msg.End,
					Message = msg.TrimmedMessage,
					Tag = msg.Tag,
					Area = msg.Area,
					OpRespTime = msg.OPRespTime.TotalMinutes,
					RecoveryTime = msg.RecoveryTime.TotalMinutes,
					EMRespTime = msg.EMRespTime.TotalMinutes,
					RepairTime = msg.RepairTime.TotalMinutes,
				};
				fault.Key = GetKeyFromFault(fault);

				// Lack of CTs, don't do response times here. It's held against them
				if (msg.Tag == "CLIFA01_LeerWT") {
					fault.OpRespTime = 0;
					fault.RecoveryTime = 0;
					fault.EMRespTime = 0;
					fault.RepairTime = 0;
				}

				Faults.Add(fault);
			}
		}

		private void LoadFaultTagMappingFromFile() {
			FaultTagMapping = new Dictionary<string, string>(100);

			if (!File.Exists(MappingTablePath)) {
				return;
			}

			var maps = File.ReadAllLines(MappingTablePath);
			foreach (var line in maps) {
				if (line.Trim().StartsWith("//")) continue;
				var split = line.Split(';');
				if (split.Length == 2) {
					var key = split[0].Substring(0, 14);        //split[0].Length > 13 ? split[0].Substring(0, 14) : split[0],
					var val = split[1];

					if (FaultTagMapping.ContainsKey(key)) {
						if (FaultTagMapping[key] != val)
							MessageBox.Show(string.Format("Key {0} in list already with val: {1}. This iter wanted this val: {2}", key, val, FaultTagMapping[key]));
					} else {
						FaultTagMapping.Add(key, val);
					}
				}
			}
			SaveFaultTagMappingToFile();
		}

		private void SaveFaultTagMappingToFile() {
			var lines = new List<string>(FaultTagMapping.Count);
			foreach (var item in FaultTagMapping.OrderBy(x => x.Value).GroupBy(x => x.Key).SelectMany(x => x)) {    // Sort the KeyMapping by Key
				if (!item.Key.StartsWith("Unknown")) {
					lines.Add(string.Format("{0};{1}", item.Key, item.Value));
				}
			}
			File.WriteAllLines(MappingTablePath, lines);
		}

		private string GetKeyFromFault(Fault f) {
			if (f.Tag.StartsWith("Unknown"))
				return "< Unknown Fault >";
			var t = f.Tag.Substring(0, 14);
			return FaultTagMapping.ContainsKey(t) ? FaultTagMapping[t] : GetAutoKey(f);
		}

		private int GetSelectedRow() {
			for (int i = 0; i < FaultList.Rows.Count; ++i) {
				if (FaultList.Rows[i].Selected)
					return i;
			}
			return -1;
		}

		private int GetNextEmptyKeyRow() {
			int start = 0;
			if (SelectedRow >= 0 && SelectedRow < Faults.Count) {
				start = SelectedRow + 1;
			}
			for (int i = start; i < Faults.Count; ++i) {
				if (Faults[i].Key == string.Empty)
					return i;
			}

			// If nothing is left, what should we return? -- I guess don't move
			return 0;
		}

		private void ApplyBtn_Click(Object sender, EventArgs e) {
			if (SelectedRow < 0) return;

			// Save this category
			var newTag = Faults[SelectedRow].Tag.Substring(0, 14);
			var newKey = CategoriesBox.Text;
			FaultTagMapping[newTag] = newKey;
			UpdateKey(newTag, newKey);
			FaultList.Focus();
		}

		private void UpdateKeyAndReload() {
			SaveFaultTagMappingToFile();
			// Add new category to combo box
			CategoriesBox.Items.Clear();
			CategoriesBox.Items.AddRange(GetCurrentKeys());
			// Save where we were
			int selectedRow = GetNextEmptyKeyRow();
			// Reload Faults
			BuildFaults();
			FaultList.DataSource = Faults;
			// reselect row here
			FaultList.ClearSelection();
			SelectRow(selectedRow);
		}

		private void UpdateKey(string newTag, string newKey) {
			// Same as UpdateKeyAndReload, but tries to update the grid live (Faster)
			SaveFaultTagMappingToFile();
			// Save where we are
			int selectedRow = GetNextEmptyKeyRow();

			// Load through the faults, updating the keys. Keep the grid up to date
			for (int i = 0; i < Faults.Count; ++i) {
				if (Faults[i].Tag.StartsWith(newTag)) {
					Faults[i].Key = newKey;
					//FaultList.Rows[i].Cells[KeyColumn].Value = newKey;
				}
			}

			// reselect row here
			FaultList.ClearSelection();
			SelectRow(selectedRow);
		}

		private void SelectRow(int row) {
			if (row < 0 || row >= Faults.Count) return;
			FaultList.Rows[row].Selected = true;
			FaultList.CurrentCell = FaultList.Rows[row].Cells[0];
			FaultList.FirstDisplayedScrollingRowIndex = Math.Max(row - 4, 0);
		}

		private void CategoriesBox_KeyDown(Object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Enter && SelectedRow > -1)
				ApplyBtn_Click(sender, null);
		}

		private void RenameKeyBtn_Click(Object sender, EventArgs e) {
			var dlg = new Dialogs.RenameDowntimeKeyDialog(GetCurrentKeys().ToList());
			if (dlg.ShowDialog() == DialogResult.OK) {
				// Perform the Rename
				var keys = FaultTagMapping.Where(x => x.Value == dlg.OldKey).Select(x => x.Key).ToList();
				foreach (var key in keys) {
					FaultTagMapping[key] = dlg.NewKey;
				}
				UpdateKeyAndReload();
			}
		}

		private string[] GetCurrentKeys(string area = "") {
			if (area.Length > 2)
				return AvailableKeys.Where(x => x.StartsWith(area.Substring(0, 2)) || x.StartsWith("<")).Select(x => x).Distinct().OrderBy(x => x).ToArray();
			else
				return AvailableKeys.Distinct().OrderBy(x => x).ToArray();
		}

		private void TopTenKeysBtn_Click(Object sender, EventArgs e) {
			// Count the faults
			var map = new Dictionary<string, ulong>();
			foreach (var f in Faults) {
				if (map.ContainsKey(f.Key)) {
					map[f.Key]++;
				} else {
					map.Add(f.Key, 1);
				}
			}

			// Sort the faults
			var top = map.OrderBy(x => x.Value);

			// Create the List
			var list = new List<Dialogs.DowntimeDetail>(top.Count());
			foreach (var i in top) {
				var downtime = Faults.Where(x => x.Key == i.Key).Select(x => x.Duration).Aggregate((a, b) => a + b);
				list.Add(new Dialogs.DowntimeDetail() { Key = i.Key, Count = i.Value, TotalDowntime = new TimeSpan(0, (int)downtime, 0) });
			}

			// Display the top faults
			var win = new Dialogs.Top10KeysDialog(list);
			win.Show();
		}

		public static string GetAutoKey(Fault f) {
			// Map this fault to a key automatically
			var msg = f.Message.ToLower();

			if (f.PM.StartsWith("MM")) {
				// Specific faults
				if (f.Tag == "UV1HT01_ROBOTE" || f.Tag == "UV1CY01_Q971A1" || msg.Contains("robot"))
					return "MM - St6 Robot";
				if (f.Tag == "RM2FA01_UVLeb1")
					return "MM - UV";
				if (f.Tag == "VM2DI20_VACUUM" || f.Tag == "VM2DI19_VACUUM")
					return "MM - External Media Missing";

				// Pattern
				if (msg.Contains("rte") || msg.Contains("ads"))
					return "MM - Communication";
				if (msg.Contains("ethercat") || msg.Contains("iolink") || msg.Contains("profibus"))
					return "MM - EtherCat / Profibus";
				if (msg.Contains("belt tension") || msg.Contains("belt motor section"))
					return "MM - CM Belts";
				if (msg.Contains("pressure") && msg.Contains("filter") && (msg.Contains("c1") || msg.Contains("c2")))
					return "MM - CM Filters";
				if (msg.Contains("rmm") || msg.Contains("drawer 1") || msg.Contains("drawer 2") || f.Tag.StartsWith("PU") || f.Tag.StartsWith("GA"))    //msg.Contains("bay 1") || msg.Contains("bay 2") || msg.Contains("bay 3") || msg.Contains("bay 4") || msg.Contains("bay 5") || msg.Contains("bay 6")
					return "MM - RMM";
				if (msg.Contains("la") || msg.Contains("protective atmosphere"))
					return "MM - LA";
				if (msg.Contains("lost mold"))
					return "MM - Lost Mold";
				if (msg.Contains("hwr"))
					return "MM - Hot Water Rinse";
				if (msg.Contains("pushrod") || msg.Contains("pusher") || msg.Contains("outfeed axis female error") || msg.Contains("outfeed axis male error"))
					return "MM - TW1 Pushers";
				if (msg.Contains("axiserror transport") || ((msg.Contains("carrier") || msg.Contains("packet")) && msg.Contains("not in position")))
					return "MM - Transport";
				if (msg.Contains("pin") && (msg.Contains("transport") || msg.Contains("index")))
					return "MM - Index / Transport Pins";
				if (msg.Contains("lifting bar"))
					return "MM - Lift Bars";
				if (msg.Contains("sce") || f.Tag.StartsWith("ME2"))
					return "MM - SCE";
				if (msg.Contains("deforming") || msg.Contains("deforming"))
					return "MM - Deforming";
				if (msg.Contains("rfid"))
					return "MM - RFID";
				if (msg.Contains("carrier present in position"))
					return "MM - Nuisance Fault";
				if (msg.Contains("c1") || msg.Contains("c2") || msg.Contains("c3"))
					return "MM - Cleaning Module";

				// If we reach here, just group these faults by station
				if (f.Tag.StartsWith("ME1"))
					return "MM - MEC (Carrier Unloading)";
				if (f.Tag.StartsWith("TW1"))
					return "MM - Turning Wheel 1";
				if (f.Tag.StartsWith("LK1") || f.Tag.StartsWith("PR1"))
					return "MM - St1/2: RFID";
				if (f.Tag.StartsWith("DO1"))
					return "MM - Dosing";
				if (f.Tag.StartsWith("MA1"))
					return "MM - Mating";
				if (f.Tag.StartsWith("FO1"))
					return "MM - Forming";
				if (f.Tag.StartsWith("UV1") || f.Tag.StartsWith("RM"))
					return "MM - RMM";
				if (f.Tag.StartsWith("DF1"))
					return "MM - Deforming";
				if (f.Tag.StartsWith("RI1"))
					return "MM - Rinsing";
				if (f.Tag.StartsWith("WF1"))
					return "MM - Waterflow";
				if (f.Tag.StartsWith("TW2"))
					return "MM - Turning Wheel 2";
				if (f.Tag.StartsWith("TR"))
					return "MM - Transport";

				if (msg.Contains("door") || msg.Contains("safety"))
					return "MM - Safety / Doors";

			} else if (f.PM.StartsWith("EM")) {
				// Specific faults
				if (f.Tag == "GENCERR_000056" || f.Tag == "GENSAFD_ROBIDO")
					return "EM - Robot Safety Door";
				if (f.Tag == "GENERRX_000285")
					return "EM - CRC";
				if (f.Tag == "GENEROR_E00336" || f.Tag == "GENERRX_000320")
					return "EM - External Media Missing";
				if (f.Tag == "GENERRX_000377")
					return "EM - Fire Alarm";

				// Pattern
				if (msg.Contains("rte") || msg.Contains("rt ethernet") || msg.Contains("ads"))
					return "EM - Communication";
				if (f.Tag.StartsWith("LI1"))
					return "EM - Lift 1";
				if (f.Tag.StartsWith("LI2"))
					return "EM - Lift 2";
				if (f.Tag.StartsWith("BCY") || f.Tag.StartsWith("RCY") || f.Tag.StartsWith("CRC"))
					return "EM - CRC";
				if (msg.Contains("tankfarm") || msg.Contains("rto"))
					return "EM - Tankfarm / RTO";
				if (msg.Contains("starwheel") || msg.Contains("smallwheel"))
					return "EM - Starwheel";
				if (msg.Contains("conveyor"))
					return "EM - Conveyor";
				if (msg.Contains("lifting unit"))
					return "EM - Lifting Unit";
				if (msg.Contains("double carrier"))
					return "EM - Double Carrier";
				if (msg.Contains("rfid"))
					return "EM - RFID";
				if (msg.Contains("sluice"))
					return "EM - Sluice Gates";
				if (msg.Contains("leakage"))
					return "EM - Leakage Sensors";
				if (msg.Contains("ethercat") || msg.Contains("iolink") || msg.Contains("profibus"))
					return "EM - EtherCat / Profibus";
				if (msg.Contains("safety") || msg.Contains("cover") || msg.Contains("door") || msg.Contains("pss"))
					return "EM - Safety / Doors";
				if (msg.Contains("diaphragm pump"))
					return "EM - MEK Tranfer Pump";
				if (msg.Contains("x1") || msg.Contains("x2"))
					return "EM - X1 & X2 Faults";

				// If we reach here, just group by station
				if (int.TryParse(f.Tag.Substring(0, 2), out int bath))
					return string.Format("EM - Bath {0}", bath);

			} else if (f.PM.StartsWith("IM")) {
				// Pattern
				if (msg.Contains("rte") || msg.Contains("rt ethernet") || msg.Contains("ads"))
					return "IM - Communication";
				if (msg.Contains("robot"))
					return "IM - Lens Transfer Robot";
				if (msg.Contains("inverting"))
					return "IM - Inverting Station";
				if (msg.Contains("vacuum/fill"))
					return "IM - Vacuum Filling Station";
				if (msg.Contains("disturbed sequence"))
					return "IM - TM Faulted in IM";
				if (msg.Contains("seq. control ct pick and place") || msg.Contains("ct missing"))
					return "IM - Cuvette Sequence";
				if (msg.Contains("transversal pusher"))
					return "IM - Infeed / Outfeed";
				if (msg.Contains("vision system"))
					return "IM - Vision";
				if (msg.Contains("infeed/outfeed station"))
					return "IM - Lifting for TM";
				if (msg.Contains("ct pick and place"))
					return "IM - Pick and Place";
				if (msg.Contains("ethercat") || msg.Contains("iolink") || msg.Contains("profibus"))
					return "IM - EtherCat / Profibus";
				if (msg.Contains("door") || msg.Contains("safety"))
					return "IM - Safety / Doors";

				// If we reach here, just group these faults by station
				if (f.Tag.StartsWith("VFS"))
					return "IM - Vacuum / Fill";
				if (f.Tag.StartsWith("IVS"))
					return "IM - Vision";
				if (f.Tag.StartsWith("IST"))
					return "IM - Inverting";
				if (f.Tag.StartsWith("CPP"))
					return "IM - Pick and Place";
				if (f.Tag.StartsWith("CD1"))
					return "IM - Top Water Removal";

			} else if (f.PM.StartsWith("TM")) {
				// Specific
				if (msg.Contains("lack of cts"))
					return "TM - Lack of CTs";
				if (msg.Contains("lack of lids"))
					return "TM - Lack of Lids";
				if (msg.Contains("ct could not be written") || f.Tag.Contains("AEGERR") || f.Tag.StartsWith("CTT"))
					return "TM - RFID";
				if (f.Tag.StartsWith("PL") && (msg.Contains("vacuum") || msg.Contains("sticking")))
					return "TM - Grippers (Vacuum / Sticking)";
				if (f.Tag == "CB1FA01_VakSto")
					return "EM - External Media Missing";

				// Pattern & Station
				if (msg.Contains("rte") || msg.Contains("rt ethernet") || msg.Contains("ads"))
					return "TM - Communication";
				if (msg.Contains("shell vacuum"))
					return "TM - Shell Vacuum";
				if (msg.Contains("missing pin"))
					return "TM - Missing Pin";
				if (msg.Contains("lid lifter"))
					return "TM - Lid Lifter";
				if (msg.Contains("cam stopper") || f.Tag.StartsWith("CS"))
					return "TM - Cam Stopper";
				if (msg.Contains("communication") || f.Tag.StartsWith("ADS"))
					return "TM - Communication";
				if (msg.Contains("ct infeed") || f.Tag.StartsWith("IN"))
					return "TM - CT Infeed";
				if (msg.Contains("ct lifter") || f.Tag.StartsWith("CLI"))
					return "TM - CT Lifter";
				if (msg.Contains("ct placer") || f.Tag.StartsWith("CTP"))
					return "TM - CT Placer";
				if (msg.Contains("ct stacker") || f.Tag.StartsWith("ST"))
					return "TM - CT Stacker";
				if (msg.Contains("ethercat") || msg.Contains("iolink") || msg.Contains("profibus"))
					return "TM - EtherCat / Profibus";
				if (msg.Contains("lid loader") || f.Tag.StartsWith("LLO"))
					return "TM - Lid Loader";
				if (msg.Contains("predosing") || f.Tag.StartsWith("PDO"))
					return "TM - Predosing";
				if (msg.Contains("p loader") || msg.Contains("product loader") || f.Tag.StartsWith("PL"))
					return "TM - Product Loader";
				if (msg.Contains("stack buffer") || f.Tag.StartsWith("SB"))
					return "TM - Stack Buffer";
				if (msg.Contains("offloader") || f.Tag.StartsWith("SOL"))
					return "TM - Stack Offloader";
				if (msg.Contains("door") || msg.Contains("safety"))
					return "TM - Safety / Doors";

			} else if (f.PM.StartsWith("PP")) {
				// Specific
				if (f.Tag == "CB1FA01_0360S4")
					return "PP - External Media Missing";

				// Pattern
				if (msg.Contains("ethercat", true) || msg.Contains("iolink", true) || msg.Contains("profibus", true))
					return "PP - EtherCat / Profibus";
				if (msg.Contains("rte", true) || msg.Contains("rt ethernet", true) || msg.Contains("ads", true))
					return "PP - Communication";
				if (msg.Contains("transponder", true) || msg.Contains("aeg", true))
					return "PP - RFID";
				if (msg.Contains("ct stacker", true))
					return "PP - CT Stacker Bottom";
				if (msg.Contains("CT Lid Pusher", true))
					return "PP - Lid Pusher";
				if (msg.Contains("buffer zone", true))
					return "PP - Buffer Zone";
				if (msg.Contains("SRIS", true))
					return "PP - SRIS";

				// Group By Station
				if (f.Tag.StartsWith("RMH") || f.Tag.StartsWith("MF") || f.Tag.StartsWith("MG"))
					return "PP - Magazine Handling";
				if (f.Tag.StartsWith("RBH"))
					return "PP - Blister Handling";
				if (f.Tag.StartsWith("SHC") || msg.Contains("height control", true))
					return "PP - Height Control";
				if (f.Tag.StartsWith("SDO") || msg.Contains("saline dosing", true))
					return "PP - Saline Dosing";
				if (f.Tag.StartsWith("CLR") || msg.Contains("lid remover", true))
					return "PP - Lid Remover";
				if (f.Tag.StartsWith("CEC") || msg.Contains("empty control", true))
					return "PP - CT Empty Control";
				if (f.Tag.StartsWith("SPL") || f.Tag.StartsWith("LSP") || msg.Contains("shell placer", true))
					return "PP - Shell Placer";
				if (f.Tag.StartsWith("SST") || msg.Contains("sealing station", true) || msg.Contains("sealing head", true))
					return "PP - Sealing Station";
				if (f.Tag.StartsWith("SL1") || f.Tag.StartsWith("PRC") || msg.Contains("presence check"))
					return "PP - Presence Check";
				if (f.Tag.StartsWith("COP") || msg.Contains("outpusher", true))
					return "PP - CT Outpusher";
				if (f.Tag.StartsWith("QAH") || msg.Contains("qa handling", true))
					return "PP - QA Handling";
				if (f.Tag.StartsWith("LPR") || msg.Contains("laser", true))
					return "PP - Laser Printing";
				if (f.Tag.StartsWith("SL") || f.Tag.StartsWith("SF") || f.Tag.StartsWith("SS1"))
					return "PP - Shell Feeding";
				if (f.Tag.StartsWith("ESF"))
					return "PP - Elevator 1";
				if (f.Tag.StartsWith("CDT"))
					return "PP - Destacking Top";
				if (f.Tag.StartsWith("CDB") || f.Tag.StartsWith("DB1"))
					return "PP - Destacking Bottom";
				if (f.Tag.StartsWith("MAI"))
					return "PP - Magazine Infeed";
				if (f.Tag.StartsWith("FOP") || msg.Contains("foil placing", true))
					return "PP - Foil Placing";
				if (f.Tag.StartsWith("FOF") || msg.Contains("foil feeding", true))
					return "PP - Foil Feeding";
				if (f.Tag.StartsWith("EMA"))
					return "PP - Elevator 2";
				if (f.Tag.StartsWith("CTU"))
					return "PP - CT Unload";
				if (f.Tag.StartsWith("FPB"))
					return "PP - Lens On Shell";
				if (f.Tag.StartsWith("TC") || msg.Contains("stack conveyor", true))
					return "TC - Stack Conveyor";
				if (msg.Contains("door") || msg.Contains("safety", true))
					return "PP - Safety / Doors";
			}
			return string.Empty;
		}

		public List<string> AvailableKeys = new List<string> {
			"<Ignore/NotAFault>",
            // Shared
            "MM - Safety / Doors",
			"EM - Safety / Doors",
			"IM - Safety / Doors",
			"TM - Safety / Doors",
			"PP - Safety / Doors",
			"MM - Communication",
			"EM - Communication",
			"IM - Communication",
			"TM - Communication",
			"PP - Communication",
			"MM - RFID",
			"EM - RFID",
			"IM - RFID",
			"TM - RFID",
			"PP - RFID",
			"MM - EtherCat / Profibus",
			"EM - EtherCat / Profibus",
			"IM - EtherCat / Profibus",
			"TM - EtherCat / Profibus",
			"PP - EtherCat / Profibus",

			"MM - Adept",
			"MM - CM Belts",
			"MM - CM Errors",
			"MM - CM Filters",
			"MM - CM Infeed",
			"MM - CM T4",
			"MM - St1/2: RFID",
			"MM - Deforming",
			"MM - Dosing",
			"MM - Mating",
			"MM - Forming",
			"MM - EtherCat",
			"MM - External Media Missing",
			"MM - Hot Water Rinse",
			"MM - Index Pins",
			"MM - LA",
			"MM - Lift Bars",
			"MM - Lost Mold",
			"MM - Mating",
			"MM - MEC",
			"MM - Nuisance Faults",
			"MM - Out of Sequence",
			"MM - Rinsing",
			"MM - Waterflow",
			"MM - RMM",
			"MM - SCE",
			"MM - Transport",
			"MM - Transport Pins",
			"MM - Turning Wheel 1",
			"MM - TW1 Pushers",
			"MM - UV",
			"EM - Conveyor Faults",
			"EM - Cycle Timeout",
			"EM - Robot Safety Door",
			"EM - Double Carrier",
			"EM - Drive Fault",
			"EM - External Media Missing",
			"EM - Leakage Sensors",
			"EM - Lift 1",
			"EM - Lift 2",
			"EM - Lifting Unit Faults",
			"EM - Out Bath",
			"EM - CRC",
			"EM - RFID",
			"EM - Tankfarm / RTO",
			"EM - Sluice Gates",
			"EM - Starwheel Faults",
			"EM - X1 & X2 Faults",
			"EM - Bath 1",      // Generic Fault Grouping
            "EM - Bath 2",      // Generic Fault Grouping
            "EM - Bath 3",      // Generic Fault Grouping
            "EM - Bath 4",      // Generic Fault Grouping
            "EM - Bath 5",      // Generic Fault Grouping
            "EM - Bath 6",      // Generic Fault Grouping
            "EM - Bath 7",      // Generic Fault Grouping
            "EM - Bath 8",      // Generic Fault Grouping
            "EM - Bath 9",      // Generic Fault Grouping
            "EM - Bath 10",     // Generic Fault Grouping
            "EM - Bath 11",     // Generic Fault Grouping
            "EM - Bath 12",     // Generic Fault Grouping
            "EM - Bath 13",     // Generic Fault Grouping
            "EM - Bath 14",     // Generic Fault Grouping
            "EM - Bath 15",     // Generic Fault Grouping
            "EM - Bath 16",     // Generic Fault Grouping
            "IM - Vision",
			"IM - Cuvette Sequence",
			"IM - Inverting",
			"IM - Lifting for TM",
			"IM - Infeed / Outfeed",
			"IM - Robot Door Error",
			"IM - Safety Door",
			"IM - TM Faulted while in IM",
			"IM - Pick and Place",
			"IM - Vacuum / Fill",
			"IM - Top Water Removal",
			"TM - Cam Stopper",
			"TM - CT Infeed",
			"TM - CT Lifter",
			"TM - CT Stacker",
			"TM - External Media Missing",
			"TM - Grippers (Vacuum / Sticking)",
			"TM - Lack of CTs",
			"TM - Lack of Lids",
			"TM - Lid Lifter",
			"TM - Missing Pin",
			"TM - Predosing",
			"TM - Product Loader",
			"TM - Shell Vacuum",
			"TM - Stack Buffer",
			"TM - Stack Offloader",
			"PP - Magazine Filling / Handling",
			"PP - Blister Handling",
			"PP - Height Control",
			"PP - Saline Dosing",
			"PP - Lid Remover",
			"PP - CT Empty Control",
			"PP - Shell Placer",
			"PP - Sealing Station",
			"PP - Presence Check",
			"PP - CT Outpusher",
			"PP - QA Handling",
			"PP - Laser Printing",
			"PP - Shell Feeding",
			"PP - Elevator 1",
			"PP - Destacking Top",
			"PP - Destacking Bottom",
			"PP - CT Stacker Bottom",
			"PP - Magazine Infeed",
			"PP - Foil Placing",
			"PP - Foil Feeding",
			"PP - Elevator 2",
			"PP - CT Unload",
			"PP - Lens On Shell",
		};
		private static List<string> ShreeMessages = new List<string> {
			// v1
			"Hello, Shree!\nGood Morning!",
			"Shree!\nVAMOS ATL!",
			"You got coffee yet, Shree?",
			"Today's gonna be great.",
			"Bet's on when Smoly will get here",
			"Hello, Shree!\nGood Morning!",
			"Shree!\nVAMOS ATL!",
			"Hello, Shree!\nGood Morning!",
			"Take it easy, Shree\nIt's just numbers",
			//// v2 - 2019-10-04
			"Looks like a good night!\n...Oh, maybe not...  ( ⚆ _ ⚆ )",
			"Mmmm, Coffee  (ʘ‿ʘ) ",
			"Could this be any slower??\n(¬_¬) (¬_¬) (¬_¬)",
			"Numbers, numbers, number...",
			"bruh   ( ͡° ͜ʖ ͡°)",
			"Shree!! You're not supposed to hit that button!\nWHAT HAVE YOU DONE ლ(ಠ益ಠლ)",
			"Shree!! You're not supposed to hit that button!\nಠ_ಠ oh hell, you broke it ಠ_ಠ",
			"Shree!! You're not supposed to hit that button!\nNoooooo   (；一_一)",
			@"¯\_(ツ)_/¯",
			"(╯°□°）╯︵ ┻━┻  \n ┬──┬ ノ( ゜-゜ノ)  [chill, bro]",
			@"༼ ºل͟º ༼ ºل͟º ༼ ºل͟º ༽ ºل͟º ༽ ºل͟º ༽   [what?]",
			"╚(ಠ_ಠ)=┐ WHY ARE YOU WAKING ME UP\nSO EARLY! ╚(ಠ_ಠ)=┐",
		};
	}
	
	public class Fault
	{
		public string Shift { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public double Duration => (End - Start).TotalMinutes;
		public string Key { get; set; }
		public string Message { get; set; }
		public string Tag;
		public string Area;
		public double OpRespTime { get; set; }
		public double RecoveryTime { get; set; }
		public double EMRespTime { get; set; }
		public double RepairTime { get; set; }

		public string PM {
			get {
				if (Area.StartsWith("PM"))  // PM10MM -> MM10
					return string.Format("{1}{0}", Area.Substring(2, Area.Length - 4), Area.Substring(Area.Length - 2));
				if (Area.StartsWith("PP"))  // PP1T2
					return Area;
				if (Area.Length > 1 && Area[1] == 'M') // MM10
					return Area;

				return "??";
			}
		}

		public Fault() {
		}

		public Fault(DateTime s, DateTime e, string area, string message) {
			Start = s;
			End = e;
			Area = area;
			Message = message;
		}

		public override string ToString() {
			return string.Format("{0} -> {1} on {2}: {3}", Start, End, PM, Message);
		}

	}
}
